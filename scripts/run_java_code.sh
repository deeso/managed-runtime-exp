# perform core Java with modified TLS modified JVM experiments, destroy passwords
#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py


#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
#echo "Copying mod_ms JVM environment to VM" > /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py
#echo "Performing java_workx64_perform_runs_experiment on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 0
#mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/mod_jvm_mod_ms_no_destroy_data_mod_results_batch_x64_exp_gen_2560/
#echo "Completed java_workx64_perform_runs_experiment on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt
# echo "Performing post process on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt
# python /srv/nfs/cortana/logs/cmd/exp_code/driver/perform_post_process_dirs_strs.py 5 /home/dso/streaming_res/ -f /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
# echo "Completed post process on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt
# echo "Performing data agg on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt
# python /srv/nfs/cortana/logs/cmd/exp_code/driver/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
# echo "Done data agg on mod_ms" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "Copying mod_ms_no_ra_delete JVM environment to VM" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms_no_ra_delete.py
echo "Performing java_workx64_perform_runs_experiment on mod_ms_no_ra_delete" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 0
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/mod_jvm_mod_ms_no_ra_delete_no_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "Completed post process on mod_ms_no_ra_delete" >> /srv/nfs/cortana/logs/completed_log.txt


#echo "Completed java_workx64_perform_runs_experiment on mod_ms_no_ra_delete" >> /srv/nfs/cortana/logs/completed_log.txt
#echo "Performing post process on mod_ms_no_ra_delete" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/perform_post_process_dirs_strs.py 5 /home/dso/streaming_res/ -f /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
#echo "Performing data agg on mod_ms_no_ra_delete" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "Copying mod_ms_tenured_always_collect JVM environment to VM" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms_tenured_always_collect.py
echo "Performing java_workx64_perform_runs_experiment on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 0
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "Completed java_workx64_perform_runs_experiment on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt

#echo "Performing post process on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/perform_post_process_dirs_strs.py 5 /home/dso/streaming_res/ -f /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
#echo "Completed post process on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt
#echo "Performing data agg on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
#echo "Done data agg on mod_ms_tenured_always_collect" >> /srv/nfs/cortana/logs/completed_log.txt
