#!/bin/bash
# perform core Java with modified TLS modified JVM experiments, destroy passwords
python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py

echo "test Performing java_workx64_perform_runs_experiment on clean runtime files (no destroy)" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 10 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 0
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/test_unmod_jvm_no_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "test Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "test Performing java_workx64_perform_runs_experiment for bouncy castle on clean runtime files" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_bouncy_castle_runs_experiment.py 64 2560 10 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 1
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/test_unmod_jvm_bc_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "test Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "test Performing java_workx64_perform_runs_experiment on clean runtime files" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 10 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 1
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/test_unmod_jvm_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "test Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt





python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "Performing java_workx64_perform_runs_experiment on clean runtime files(no destroy)" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 0
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/unmod_jvm_no_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "Performing java_workx64_perform_runs_experiment for bouncy castle on clean runtime files" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_bouncy_castle_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 1 
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/unmod_jvm_bc_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_20.py
echo "Performing java_workx64_perform_runs_experiment on clean runtime files" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 50 /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 1
mv /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/ /srv/nfs/cortana/logs/unmod_jvm_destroy_data_mod_results_batch_x64_exp_gen_2560/
echo "Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

