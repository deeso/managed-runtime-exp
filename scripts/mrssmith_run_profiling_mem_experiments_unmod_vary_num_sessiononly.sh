BASE_RESULTS_DIR='/srv/nfs/cortana/logs/3_unmod_only_identify_potential_cliffs'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-26"
JAVA_HOST="java-workx64-23"
JAVA_USER="java"
JAVA_PASS="java"



UNMOD_RESULTS_12288=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_12288"
UNMOD_RESULTS_8192=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_8192"
UNMOD_RESULTS_4096=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_4096"
UNMOD_RESULTS_2048=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_2048"
UNMOD_RESULTS_1024=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_1024"
UNMOD_RESULTS_0512=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_mem_comp_run_x64_0512"

mkdir -p $UNMOD_RESULTS_8192
mkdir -p $UNMOD_RESULTS_4096

VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=25
LOOP_ITERATIONS=20
ITERATIONS=1
COUNTER=0
python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
while [  $COUNTER -lt $LOOP_ITERATIONS ]; do
    #python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    #sleep $SLEEP_TIME
    #MEM="10G"
    #eval $VIRSH_SETMAXMEM $MEM
    #eval $VIRSH_SETMEM $MEM
    #eval $VIRSH_START
    #sleep $SLEEP_TIME
    #echo "Performing non aggressive runs 8192"

    #python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192
    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="6G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME
    echo "Performing non aggressive runs 8192"

    python $PY_MANUAL_MEM_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_4096

    COUNTER=$((COUNTER+1));
done
