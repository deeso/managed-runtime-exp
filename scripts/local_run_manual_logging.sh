#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py java-workx64-20 java java
#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms_tenured_always_collect.py java-workx64-20 java java
#sleep 30s
#echo "$(/bin/date) Performing manual java_workx64_perform_runs_experiment unmodified " >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple.py java-workx64-20 exp-ssl-20 5 /srv/nfs/cortana/logs/manual_runs/profiling_1_runs_unmod_ms_x64_2560/


BASE_RESULTS_DIR='/srv/nfs/cortana/logs/manual_mrs_smith'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MOD_MS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'

PY_COPY_LOGGING_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modms_logging.py'
PY_COPY_LOGGING_UNMOD='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_unmod_logging.py'

EXP_SSL_HOST="exp-ssl-20"
JAVA_HOST="java-workx64-20"
JAVA_USER="java"
JAVA_PASS="java"

PY_MANUAL_MEM__5G=$PY_SCRIPT_DRIVER"/manual_run_example_simple__5g.py"
PY_MANUAL_MEM_1G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_1g.py"
PY_MANUAL_MEM_2G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2g.py"
PY_MANUAL_MEM_3G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_3g.py"
PY_MANUAL_MEM_4G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4g.py"
PY_MANUAL_MEM_8G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8g.py"
PY_MANUAL_MEM_16G=$PY_SCRIPT_DRIVER"/manual_run_example_simple_16g.py"

RESULTS__5G=$BASE_RESULTS_DIR"/manual_run_x64_512"
RESULTS_1G=$BASE_RESULTS_DIR"/manual_run_x64_1024"
RESULTS_2G=$BASE_RESULTS_DIR"/manual_run_x64_2048"
RESULTS_3G=$BASE_RESULTS_DIR"/manual_run_x64_3072"
RESULTS_4G=$BASE_RESULTS_DIR"/manual_run_x64_4096"
RESULTS_8G=$BASE_RESULTS_DIR"/manual_run_x64_8192"
RESULTS_16G=$BASE_RESULTS_DIR"/manual_run_x64_16384"

VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmaxmem ${JAVA_HOST} --config"

SLEEP_TIME=10
ITERATIONS=2

#python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example.py java-workx64-20 exp-ssl-20 5 /srv/nfs/cortana/logs/manual_runs/profiling_2_runs_unmod_ms_x64_2560/

#512-1G scripts
python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="2G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START

sleep $SLEEP_TIME
python $PY_COPY_LOGGING_UNMOD $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
python $PY_MANUAL_MEM__5G $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $RESULTS__5G

eval $PY_COPY_LOGGING_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME



#2G-3G scripts
MEM="6G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM



#4G 8G and 16G scripts
MEM="32G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM






