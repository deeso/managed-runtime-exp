BASE_RESULTS_DIR='/srv/nfs/cortana/logs/single_host_exp_memory_var'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-9"
JAVA_HOST="java-workx64-23"
JAVA_USER="java"
JAVA_PASS="java"


PY_MANUAL_MEM_0512=$PY_SCRIPT_DRIVER"/manual_run_example_simple_0512.py"
PY_MANUAL_MEM_1024=$PY_SCRIPT_DRIVER"/manual_run_example_simple_1024.py"
PY_MANUAL_MEM_2048=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2048.py"
PY_MANUAL_MEM_3072=$PY_SCRIPT_DRIVER"/manual_run_example_simple_3072.py"
PY_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096.py"
PY_MANUAL_MEM_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8192.py"
PY_MANUAL_MEM_16384=$PY_SCRIPT_DRIVER"/manual_run_example_simple_16384.py"


UNMOD_RESULTS_0512=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_0512"
UNMOD_RESULTS_1024=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_1024"
UNMOD_RESULTS_2048=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_2048"
UNMOD_RESULTS_3072=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_3072"
UNMOD_RESULTS_4096=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_4096"
UNMOD_RESULTS_8192=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_8192"
UNMOD_RESULTS_16384=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_16384"

MODMS_RESULTS_0512=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_0512"
MODMS_RESULTS_1024=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_1024"
MODMS_RESULTS_2048=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_2048"
MODMS_RESULTS_3072=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_3072"
MODMS_RESULTS_4096=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_4096"
MODMS_RESULTS_8192=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_8192"
MODMS_RESULTS_16384=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_16384"


VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=10
LOOP_ITERATIONS=20
ITERATIONS=1
COUNTER=0
while [  $COUNTER -lt $LOOP_ITERATIONS ]; do
    # 1024, 512
    python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="6G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME

    python $PY_MANUAL_MEM_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_4096
    python $PY_MANUAL_MEM_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_3072
    python $PY_MANUAL_MEM_2048 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_2048


    python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    python $PY_MANUAL_MEM_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_4096
    python $PY_MANUAL_MEM_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_3072
    python $PY_MANUAL_MEM_2048 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_2048


    ##8192
    python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="10G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME
    echo "Performing non aggressive runs 8192"

    python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192
    python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_8192


    # 16384
    python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    echo "Performing non aggressive runs 16384"
    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="18G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME

    echo "Performing non aggressive runs 16384"
    python $PY_MANUAL_MEM_16384 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_16384

    python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    python $PY_MANUAL_MEM_16384 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_16384

    let COUNTER=COUNTER+1
done