virsh start java-workx64-20
sleep 5s
virsh start exp-ssl-20
sleep 5s

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py java-workx64-20 java java
sleep 30s

echo "$(/bin/date) Performing manual java_workx64_perform_runs_experiment unmodified ms collect" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example.py java-workx64-20 exp-ssl-20 10 /srv/nfs/cortana/logs/manual_runs/profiling_2_runs_x64_2560/
