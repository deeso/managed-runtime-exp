BASE_RESULTS_DIR='/srv/nfs/cortana/logs/7_var_mem_exp_mod_jce/'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-0"
JAVA_HOST="java-workx64-23"
JAVA_USER="java"
JAVA_PASS="java"

UNMOD_APACHE_RESULTS_16384=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_16384"
UNMOD_APACHE_RESULTS_8192=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_8192"
UNMOD_APACHE_RESULTS_4096=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_4096"
UNMOD_APACHE_RESULTS_2048=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_2048"
UNMOD_APACHE_RESULTS_1024=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_1024"
UNMOD_APACHE_RESULTS_0512=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_apache_null_mem_comp_run_x64_0512"

PY_APACHE_MANUAL_MEM_0512=$PY_SCRIPT_DRIVER"/manual_run_example_simple_0512_microsim_modms_apache_null_5000.py"
PY_APACHE_MANUAL_MEM_1024=$PY_SCRIPT_DRIVER"/manual_run_example_simple_1024_microsim_modms_apache_null_5000.py"
PY_APACHE_MANUAL_MEM_2048=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2048_microsim_modms_apache_null_5000.py"
PY_APACHE_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096_microsim_modms_apache_null_10000.py"
PY_APACHE_MANUAL_MEM_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8192_microsim_modms_apache_null_10000.py"
PY_APACHE_MANUAL_MEM_16384=$PY_SCRIPT_DRIVER"/manual_run_example_simple_16384_microsim_modms_apache_null_15000.py"

UNMOD_SOCKET_RESULTS_16384=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_16384"
UNMOD_SOCKET_RESULTS_8192=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_8192"
UNMOD_SOCKET_RESULTS_4096=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_4096"
UNMOD_SOCKET_RESULTS_2048=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_2048"
UNMOD_SOCKET_RESULTS_1024=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_1024"
UNMOD_SOCKET_RESULTS_0512=$BASE_RESULTS_DIR"/113_nolog_mrssmith_unmod_socket_null_mem_comp_run_x64_0512"

PY_SOCKET_MANUAL_MEM_0512=$PY_SCRIPT_DRIVER"/manual_run_example_simple_0512_microsim_modms_socket_null_5000.py"
PY_SOCKET_MANUAL_MEM_1024=$PY_SCRIPT_DRIVER"/manual_run_example_simple_1024_microsim_modms_socket_null_5000.py"
PY_SOCKET_MANUAL_MEM_2048=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2048_microsim_modms_socket_null_5000.py"
PY_SOCKET_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096_microsim_modms_socket_null_10000.py"
PY_SOCKET_MANUAL_MEM_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8192_microsim_modms_socket_null_10000.py"
PY_SOCKET_MANUAL_MEM_16384=$PY_SCRIPT_DRIVER"/manual_run_example_simple_16384_microsim_modms_socket_null_15000.py"

PY_MANUAL_MEM_0512=$PY_SOCKET_MANUAL_MEM_0512
PY_MANUAL_MEM_1024=$PY_SOCKET_MANUAL_MEM_1024
PY_MANUAL_MEM_2048=$PY_SOCKET_MANUAL_MEM_2048
PY_MANUAL_MEM_4096=$PY_SOCKET_MANUAL_MEM_4096
PY_MANUAL_MEM_8192=$PY_SOCKET_MANUAL_MEM_8192
PY_MANUAL_MEM_16384=$PY_SOCKET_MANUAL_MEM_16384

UNMOD_RESULTS_16384=$UNMOD_SOCKET_RESULTS_16384
UNMOD_RESULTS_8192=$UNMOD_SOCKET_RESULTS_8192
UNMOD_RESULTS_4096=$UNMOD_SOCKET_RESULTS_4096
UNMOD_RESULTS_2048=$UNMOD_SOCKET_RESULTS_2048
UNMOD_RESULTS_1024=$UNMOD_SOCKET_RESULTS_1024
UNMOD_RESULTS_0512=$UNMOD_SOCKET_RESULTS_0512

#PY_MANUAL_MEM_0512=$PY_APACHE_MANUAL_MEM_0512
#PY_MANUAL_MEM_1024=$PY_APACHE_MANUAL_MEM_1024
#PY_MANUAL_MEM_2048=$PY_APACHE_MANUAL_MEM_2048
#PY_MANUAL_MEM_4096=$PY_APACHE_MANUAL_MEM_4096
#PY_MANUAL_MEM_8192=$PY_APACHE_MANUAL_MEM_8192
#PY_MANUAL_MEM_16384=$PY_APACHE_MANUAL_MEM_16384
#
#UNMOD_RESULTS_16384=$UNMOD_APACHE_RESULTS_16384
#UNMOD_RESULTS_8192=$UNMOD_APACHE_RESULTS_8192
#UNMOD_RESULTS_4096=$UNMOD_APACHE_RESULTS_4096
#UNMOD_RESULTS_2048=$UNMOD_APACHE_RESULTS_2048
#UNMOD_RESULTS_1024=$UNMOD_APACHE_RESULTS_1024
#UNMOD_RESULTS_0512=$UNMOD_APACHE_RESULTS_0512


VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=30
LOOP_ITERATIONS=20
ITERATIONS=1
COUNTER=0
#python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
while [  $COUNTER -lt $LOOP_ITERATIONS ]; do
    sleep $SLEEP_TIME
    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="18G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME
    
    echo "Performing runs 16384"
    python $PY_MANUAL_MEM_16384 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_16384


    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="10G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME

    echo "Performing runs 8192"
    python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192

    python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    MEM="6G"
    eval $VIRSH_SETMAXMEM $MEM
    eval $VIRSH_SETMEM $MEM
    eval $VIRSH_START
    sleep $SLEEP_TIME

    echo "Performing runs 4096"
    python $PY_MANUAL_MEM_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_4096

    echo "Performing runs 2048"
    python $PY_MANUAL_MEM_2048 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_2048

    echo "Performing runs 1024"
    python $PY_MANUAL_MEM_1024 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_1024

    echo "Performing runs 512"
    python $PY_MANUAL_MEM_0512 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_0512

    COUNTER=$((COUNTER+1));
done
