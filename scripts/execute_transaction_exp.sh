echo "$(/bin/date) Starting experiments for transaction 2560MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_random_transaction.py 64 2560 75 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560 H
echo "$(/bin/date) Performing post process experiments for transaction 2560MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/perform_post_process_dirs_strs.py 5 /home/dso/streaming_res/ -f /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
echo "$(/bin/date) Starting experiments for transaction 512MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_random_transaction.py 64 512 75 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_exp_gen_512 H
echo "$(/bin/date) Performing post process experiments for transaction 512MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/perform_post_process_dirs_strs.py 5 /home/dso/streaming_res/ -f /srv/nfs/cortana/logs/results_batch_x64_exp_gen_512/transaction/


echo "$(/bin/date) Performing data aggregations" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/
python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_x64_exp_gen_512/transaction/
