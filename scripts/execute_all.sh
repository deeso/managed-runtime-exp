echo "$(/bin/date) Starting experiments for transaction and streaming session lifetime 512MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_sess.py 64 512 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_sess_lifetime_512 H

echo "$(/bin/date) Starting experiments for transaction and streaming memory cont. 512MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_mem.py 64 512 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_mem_cont_512 H
#
echo "$(/bin/date) Starting experiments for transaction and streaming conc. session 512MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_conc_sess.py 64 512 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_conc_sess_512 H
#
echo "$(/bin/date) Starting experiments for transaction and streaming session lifetime 2560MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_sess.py 64 2560 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_sess_lifetime_2560 H
#
echo "$(/bin/date) Starting experiments for transaction and streaming memory cont. 2560MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_mem.py 64 2560 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_mem_cont_2560 H
#
echo "$(/bin/date) Starting experiments for transaction and streaming conc. session 2560MB" >> /home/dso/Debug.log
python /srv/nfs/cortana/logs/cmd/single_factors_iteration_perform_runs_conc_sess.py 64 2560 100 transaction no_mod /srv/nfs/cortana/logs/results_batch_x64_conc_sess_2560 H

#echo "$(/bin/date) performing data aggregation on all outputs" >> /home/dso/Debug.log
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_sess_lifetime_512/streaming/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_sess_lifetime_512/transaction/
#
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_mem_cont_512/transaction/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_mem_cont_512/streaming/
#
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_conc_sess_512/transaction/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_conc_sess_512/streaming/
#
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_sess_lifetime_2560/transaction/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_sess_lifetime_2560/streaming/
#
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_mem_cont_2560/transaction/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_mem_cont_2560/streaming/
#
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_conc_sess_2560/transaction/
#python /srv/nfs/cortana/logs/cmd/agg_exp_data.py /srv/nfs/cortana/logs/results_batch_conc_sess_2560/streaming/
#echo "$(/bin/date) data aggregation on all outputs complete" >> /home/dso/Debug.log
