BASE_RESULTS_DIR='/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-0"
JAVA_HOST="java-workx64-00"
JAVA_USER="java"
JAVA_PASS="java"


PY_MANUAL_MEM_0512=$PY_SCRIPT_DRIVER"/manual_run_example_simple_0512.py"
PY_MANUAL_MEM_1024=$PY_SCRIPT_DRIVER"/manual_run_example_simple_1024.py"
PY_MANUAL_MEM_2048=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2048.py"
PY_MANUAL_MEM_3072=$PY_SCRIPT_DRIVER"/manual_run_example_simple_3072.py"
PY_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096.py"
PY_MANUAL_MEM_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8192.py"
PY_MANUAL_MEM_16384=$PY_SCRIPT_DRIVER"/manual_run_example_simple_16384.py"


UNMOD_RESULTS_0512=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_0512"
UNMOD_RESULTS_1024=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_1024"
UNMOD_RESULTS_2048=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_2048"
UNMOD_RESULTS_3072=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_3072"
UNMOD_RESULTS_4096=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_4096"
UNMOD_RESULTS_8192=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_8192"
UNMOD_RESULTS_16384=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_16384"

MODMS_RESULTS_0512=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_0512"
MODMS_RESULTS_1024=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_1024"
MODMS_RESULTS_2048=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_2048"
MODMS_RESULTS_3072=$BASE_RESULTS_DIR"/unmod_mem_comp_run_x64_3072"
MODMS_RESULTS_4096=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_4096"
MODMS_RESULTS_8192=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_8192"
MODMS_RESULTS_16384=$BASE_RESULTS_DIR"/modms_mem_comp_run_x64_16384"

mkdir -p $UNMOD_RESULTS_0512
mkdir -p $UNMOD_RESULTS_1024
mkdir -p $UNMOD_RESULTS_2048
mkdir -p $UNMOD_RESULTS_3072
mkdir -p $UNMOD_RESULTS_4096
mkdir -p $UNMOD_RESULTS_8192
mkdir -p $UNMOD_RESULTS_16384

mkdir -p $MODMS_RESULTS_0512
mkdir -p $MODMS_RESULTS_1024
mkdir -p $MODMS_RESULTS_2048
mkdir -p $MODMS_RESULTS_3072
mkdir -p $MODMS_RESULTS_4096
mkdir -p $MODMS_RESULTS_8192
mkdir -p $MODMS_RESULTS_16384

VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=60
SLEEP_TIME=45
SLEEP_TIME=30
SLEEP_TIME=60
ITERATIONS=5

SLEEP_TIME=45

# python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS

# python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
# sleep $SLEEP_TIME
# MEM="10G"
# eval $VIRSH_SETMAXMEM $MEM
# eval $VIRSH_SETMEM $MEM
# eval $VIRSH_START

python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_8192.py java-workx64-00 exp-ssl-0 5 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_8192

python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="6G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START

python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_4096.py java-workx64-00 exp-ssl-0 5 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_4096

python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="4G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START


python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_0512.py java-workx64-00 exp-ssl-0 3 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_0512
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_1024.py java-workx64-00 exp-ssl-0 1 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_1024
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_2048.py java-workx64-00 exp-ssl-0 5 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_2048
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_3072.py java-workx64-00 exp-ssl-0 5 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_3072



# unmodified

python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="4G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START
sleep $SLEEP_TIME
echo "Performing non aggressive runs 512-3072"

python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_1024.py java-workx64-00 exp-ssl-0 4 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_1024
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_2048.py java-workx64-00 exp-ssl-0 3 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_2048
python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_3072.py java-workx64-00 exp-ssl-0 4 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_3072

python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="6G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START

python /srv/nfs/cortana/logs/cmd/exp_code/driver/manual_run_example_simple_4096.py java-workx64-00 exp-ssl-0 5 /srv/nfs/cortana/logs/cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_4096

