#!/bin/bash
# perform core Java with modified TLS modified JVM experiments, destroy passwords
#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_10.py
#python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_openjdk8.py
#echo "openjdk8 Performing java_workx64_perform_runs_experiment on clean runtime files (no destroy)" >> /srv/nfs/cortana/logs/completed_log.txt
#python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 20 /srv/nfs/cortana/logs/test_openjdk82_x64_exp_gen_2560 0
#echo "openjdk Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt

python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_start_vms_10.py
python /srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py
echo "oracle8 Performing java_workx64_perform_runs_experiment on clean runtime files (no destroy)" >> /srv/nfs/cortana/logs/completed_log.txt
python /srv/nfs/cortana/logs/cmd/exp_code/driver/java_workx64_perform_runs_experiment.py 64 2560 20 /srv/nfs/cortana/logs/test_oracle8_x64_exp_gen_2560 0
echo "oracle8 Completed post process on unmodified runtime runs" >> /srv/nfs/cortana/logs/completed_log.txt
