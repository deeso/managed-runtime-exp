BASE_RESULTS_DIR='/srv/nfs/cortana/logs/5_modms_only_var_exp'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'
PY_COPY_TESTING='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_testing_jre.py'
PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-0"
JAVA_HOST="java-workx64-23"
JAVA_USER="java"
JAVA_PASS="java"



#PY_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096_microsim_modms.py"
PY_MANUAL_MEM_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096_microsim_modms.py"
MODMS_RESULTS_4096=$BASE_RESULTS_DIR"/50_mrssmith_unmod_mem_comp_run_x64_4096"

mkdir -p $MODMS_RESULTS_4096

VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=30
LOOP_ITERATIONS=20
ITERATIONS=20
COUNTER=0

python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="6G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START
sleep $SLEEP_TIME

python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
while [  $COUNTER -lt $LOOP_ITERATIONS ]; do
    #python $PY_COPY_TESTING $JAVA_HOST $JAVA_USER $JAVA_PASS
    sleep $SLEEP_TIME
    echo "Performing non aggressive runs 8192"

    python $PY_MANUAL_MEM_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_4096

    COUNTER=$((COUNTER+1));
done
