BASE_RESULTS_DIR='/srv/nfs/cortana/logs/3_modms_testing_mrsmith_testing_memory_var_exp'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_COPY_TESTING='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_testing_jre.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-29"
JAVA_HOST="java-workx64-20"
JAVA_USER="java"
JAVA_PASS="java"


PY_MANUAL_MEM_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8192.py"


MODMS_RESULTS_8192=$BASE_RESULTS_DIR"/testing_modms_mem_comp_run_x64_8192"
UNMOD_RESULTS_8192=$BASE_RESULTS_DIR"/testing_unmod_mem_comp_run_x64_8192"

mkdir -p $MODMS_RESULTS_8192
mkdir -p $UNMOD_RESULTS_8192

VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=10
LOOP_ITERATIONS=50
ITERATIONS=2
COUNTER=0

python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="10G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START
sleep $SLEEP_TIME


while [  $COUNTER -lt $LOOP_ITERATIONS ]; do
    #python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
    #python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192
    python $PY_COPY_TESTING $JAVA_HOST $JAVA_USER $JAVA_PASS
    python $PY_MANUAL_MEM_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_8192

    COUNTER=$((COUNTER+1));
done
