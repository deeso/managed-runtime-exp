#!/bin/sh
#
# Purpose : launch dacapo benchmarks
#
# java  -Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:NativeMemoryTracking=summary -XX:+PrintAdaptiveSizePolicy -XX:+PrintGC -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps -Xloggc:/srv/result_data/working_dir/urtoatbojlphruqfclvc/gc_log.txt -XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=4096M -XX:MaxHeapSize=4096M -XX:NewSize=2048M -XX:MaxNewSize=2048M -XX:InitialSurvivorRatio=8 -jar /srv/nfs/cortana/logs/cmd/exp_code/libs/java-gc-experiments-1.0.jar JavaClientExperiments-BC 4096 800 720 60 10.18.120.100 /srv/result_data/working_dir/urtoatbojlphruqfclvc/authlog.txt HIGH MED HIGH HIGH 0
#JAVA_OPTS="-Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=4096M -XX:MaxHeapSize=4096M -XX:NewSize=2048M -XX:MaxNewSize=2048M -XX:InitialSurvivorRatio=8"
#JAVA_OPTS="-Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:NativeMemoryTracking=summary -XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=4096M -XX:MaxHeapSize=4096M -XX:NewSize=2048M -XX:MaxNewSize=2048M -XX:InitialSurvivorRatio=8 -Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:NativeMemoryTracking=summary -XX:+PrintAdaptiveSizePolicy -XX:+PrintGC -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps"
#JAVA_OPTS="-XX:NativeMemoryTracking=summary -Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=4096M -XX:MaxHeapSize=4096M -XX:NewSize=2048M -XX:MaxNewSize=2048M -XX:InitialSurvivorRatio=8 -Xnoclassgc -XX:-CMSClassUnloadingEnabled" # -XX:+PrintAdaptiveSizePolicy -XX:+PrintGC -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps"

JAVA_OPTS="-XX:NativeMemoryTracking=summary -Xnoclassgc -XX:-CMSClassUnloadingEnabled -XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=1024M -XX:MaxHeapSize=1024M -XX:NewSize=512M -XX:MaxNewSize=512M -XX:InitialSurvivorRatio=8 -Xnoclassgc -XX:-CMSClassUnloadingEnabled" # -XX:+PrintAdaptiveSizePolicy -XX:+PrintGC -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps"
if [ ! -z "$WORKSPACE" ]; then
  BASE_DIR=$WORKSPACE
else
  BASE_DIR=`dirname $0`
fi

if [ ! -f $BASE_DIR/dacapo-9.12-bach.jar ]; then
  echo "Downloading dacapo-9.12 bench..."
  curl -L http://downloads.sourceforge.net/project/dacapobench/9.12-bach/dacapo-9.12-bach.jar -o $BASE_DIR/dacapo-9.12-bach.jar
fi


benchme() {
  local COUNT=$1
  local BENCH=$2
  local FNAME=$BENCH"_gc.log"
  local GC_LOG=" -Xloggc:/home/java/"$FNAME
  rm -f dacapo-bench-$BENCH.log
  #echo "benching dacapo-9.12 - $BENCH - $COUNT iterations at `date`"
  java $JAVA_OPTS $GC_LOG -jar $BASE_DIR/dacapo-9.12-bach.jar -n $COUNT $BENCH >>dacapo-bench-$BENCH.log 2>&1
  #java $JAVA_OPTS -jar $BASE_DIR/dacapo-9.12-bach.jar -n $COUNT $BENCH >>dacapo-bench-$BENCH.log 2>&1
  cat dacapo-bench-$BENCH.log | grep PASSED
}

#benchme 10 avrora
#benchme 10 eclipse
benchme 10 fop
benchme 10 h2
benchme 10 jython
benchme 10 luindex
benchme 10 lusearch
#benchme 10 pmd
#benchme 11 sunflow
#benchme 10 tomcat
benchme 10 tradebeans
benchme 10 tradesoap
