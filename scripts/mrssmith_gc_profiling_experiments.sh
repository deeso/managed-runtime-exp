BASE_RESULTS_DIR='/srv/nfs/cortana/logs/mrssmith_fixed_single_host_gc_exp'
PY_SCRIPT_DRIVER='/srv/nfs/cortana/logs/cmd/exp_code/driver'
PY_COPY_MODMS='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_copy_over_modified_jre_mod_ms.py'
PY_REINSTALL='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reinstall_oracle8.py'

PY_REBOOT='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_reboot.py'
PY_SHUTDOWN='/srv/nfs/cortana/logs/cmd/exp_code/maintanence/java_workx64_shutdown.py'


EXP_SSL_HOST="exp-ssl-3"
JAVA_HOST="java-workx64-23"
JAVA_USER="java"
JAVA_PASS="java"

PY_MANUAL_MEM_NONAGG_2304=$PY_SCRIPT_DRIVER"/manual_run_example_simple_2304.py"
PY_MANUAL_MEM_NONAGG_4096=$PY_SCRIPT_DRIVER"/manual_run_example_simple_4096.py"
PY_MANUAL_MEM_NONAGG_8192=$PY_SCRIPT_DRIVER"/manual_run_example_simple_8096.py"
PY_MANUAL_MEM_NONAGG_3072=$PY_SCRIPT_DRIVER"/manual_run_example_simple_3072.py"

PY_MANUAL_MEM_AGG_2304=$PY_SCRIPT_DRIVER"/manual_run_example_agg_2304.py"
PY_MANUAL_MEM_AGG_4096=$PY_SCRIPT_DRIVER"/manual_run_example_agg_4096.py"
PY_MANUAL_MEM_AGG_8192=$PY_SCRIPT_DRIVER"/manual_run_example_agg_8192.py"
PY_MANUAL_MEM_AGG_3072=$PY_SCRIPT_DRIVER"/manual_run_example_agg_3072.py"

UNMOD_RESULTS_2304_NA=$BASE_RESULTS_DIR"/unmod_nonagg_run_x64_2304"
UNMOD_RESULTS_2304_A=$BASE_RESULTS_DIR"/unmod_agg_run_x64_2304"
UNMOD_RESULTS_4096_NA=$BASE_RESULTS_DIR"/unmod_nonagg_run_x64_4096"
UNMOD_RESULTS_4096_A=$BASE_RESULTS_DIR"/unmod_agg_run_x64_4096"
UNMOD_RESULTS_8192_NA=$BASE_RESULTS_DIR"/unmod_nonagg_run_x64_8192"
UNMOD_RESULTS_8192_A=$BASE_RESULTS_DIR"/unmod_agg_run_x64_8192"
UNMOD_RESULTS_3072_NA=$BASE_RESULTS_DIR"/unmod_nonagg_run_x64_3072"
UNMOD_RESULTS_3072_A=$BASE_RESULTS_DIR"/unmod_agg_run_x64_3072"

mkdir -p $MODMS_RESULTS_2304_NA
mkdir -p $MODMS_RESULTS_2304_A
mkdir -p $MODMS_RESULTS_4096_NA
mkdir -p $MODMS_RESULTS_4096_A
mkdir -p $MODMS_RESULTS_8192_NA
mkdir -p $MODMS_RESULTS_8192_A
mkdir -p $MODMS_RESULTS_3072_NA
mkdir -p $MODMS_RESULTS_3072_A

mkdir -p $UNMOD_RESULTS_2304_NA
mkdir -p $UNMOD_RESULTS_2304_A
mkdir -p $UNMOD_RESULTS_4096_NA
mkdir -p $UNMOD_RESULTS_4096_A
mkdir -p $UNMOD_RESULTS_8192_NA
mkdir -p $UNMOD_RESULTS_8192_A
mkdir -p $UNMOD_RESULTS_3072_NA
mkdir -p $UNMOD_RESULTS_3072_A

MODMS_RESULTS_2304_NA=$BASE_RESULTS_DIR"/modms_nonagg_run_x64_2304"
MODMS_RESULTS_2304_A=$BASE_RESULTS_DIR"/modms_agg_run_x64_2304"
MODMS_RESULTS_4096_NA=$BASE_RESULTS_DIR"/modms_nonagg_run_x64_4096"
MODMS_RESULTS_4096_A=$BASE_RESULTS_DIR"/modms_agg_run_x64_4096"
MODMS_RESULTS_8192_NA=$BASE_RESULTS_DIR"/modms_nonagg_run_x64_8192"
MODMS_RESULTS_8192_A=$BASE_RESULTS_DIR"/modms_agg_run_x64_8192"
MODMS_RESULTS_3072_NA=$BASE_RESULTS_DIR"/modms_nonagg_run_x64_3072"
MODMS_RESULTS_3072_A=$BASE_RESULTS_DIR"/modms_agg_run_x64_3072"


VIRSH_START="virsh start ${JAVA_HOST}"
VIRSH_SETMAXMEM="virsh setmaxmem ${JAVA_HOST} --config"
VIRSH_SETMEM="virsh setmem ${JAVA_HOST} --config"

SLEEP_TIME=30
ITERATIONS=5


# completed 2304
# run non-aggressive Javal
python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
MEM="4G"
eval $VIRSH_SETMAXMEM $MEM
eval $VIRSH_SETMEM $MEM
eval $VIRSH_START
sleep $SLEEP_TIME

echo "Performing non aggressive runs 2304"

python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
python $PY_MANUAL_MEM_NONAGG_2304 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_2304_NA
python $PY_MANUAL_MEM_AGG_2304 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_2304_A
python $PY_MANUAL_MEM_NONAGG_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_3072_NA
python $PY_MANUAL_MEM_AGG_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_3072_A


python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
sleep $SLEEP_TIME
python $PY_MANUAL_MEM_NONAGG_2304 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_2304_NA
python $PY_MANUAL_MEM_AGG_2304 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_2304_A
python $PY_MANUAL_MEM_NONAGG_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_3072_NA
python $PY_MANUAL_MEM_AGG_3072 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_3072_A


#python $PY_SHUTDOWN $JAVA_HOST $JAVA_USER $JAVA_PASS
#sleep $SLEEP_TIME
#MEM="6G"
#eval $VIRSH_SETMAXMEM $MEM
#eval $VIRSH_SETMEM $MEM
#eval $VIRSH_START
#sleep $SLEEP_TIME
#
#echo "Performing runs 4096"
#
#
#python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
#sleep $SLEEP_TIME
#python $PY_MANUAL_MEM_NONAGG_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_4096_NA
#python $PY_MANUAL_MEM_AGG_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_4096_A
#
#python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
#sleep $SLEEP_TIME
#python $PY_MANUAL_MEM_NONAGG_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_4096_NA
#python $PY_MANUAL_MEM_AGG_4096 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_4096_A
#
#
#MEM="10G"
#eval $VIRSH_SETMAXMEM $MEM
#eval $VIRSH_SETMEM $MEM
#eval $VIRSH_START
#sleep $SLEEP_TIME
#
#echo "Performing runs 8192"
#
#python $PY_REINSTALL $JAVA_HOST $JAVA_USER $JAVA_PASS
#sleep $SLEEP_TIME
##python $PY_MANUAL_MEM_NONAGG_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192_NA
#python $PY_MANUAL_MEM_AGG_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $UNMOD_RESULTS_8192_A
#
#python $PY_COPY_MODMS $JAVA_HOST $JAVA_USER $JAVA_PASS
#sleep $SLEEP_TIME
##python $PY_MANUAL_MEM_NONAGG_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_8192_NA
#python $PY_MANUAL_MEM_AGG_8192 $JAVA_HOST $EXP_SSL_HOST $ITERATIONS $MODMS_RESULTS_8192_A


