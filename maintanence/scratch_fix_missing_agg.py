import sys
import os
dest = "/srv/nfs/cortana/logs/cmd/"
sys.path.append(dest)
#from agg_exp_data import *
import single_factors_iteration_perform_runs as sifipr
import perform_post_process_dirs_strs as pppds 
#from perform_post_process_dirs_strs import *
import agg_exp_data as aed

from single_factors_iteration_perform_runs import *


USE_64 = False
JSON_DATA = 'agg_auth_data.json'
TIME_LOG = 'timelog_start_end_actual.txt'


def inventory_failed_agg(base_location_dir):
    missing_time_logs = []
    missing_agg_logs = []
    base_locations = [os.path.join(base_location_dir, i) for i in os.listdir(base_location_dir)]
    for base_location in base_locations:
        if not os.path.isdir(base_location):
            continue

        files = set(os.listdir(base_location))
        if not JSON_DATA in files:
            missing_agg_logs.append(base_location)
        if not TIME_LOG in files:
            missing_time_logs.append(base_location)
    return missing_time_logs, missing_agg_logs

def get_run_info(base_location):
    root, dest = os.path.split(base_location)
    root, _ = os.path.split(base_location)
    results, _ = os.path.split(root)
    mem = int(results.split('_')[-1])
    _factors = dest.split('_')[:4]
    factors = get_factors_dict_str(*_factors)
    is_streaming = base_location.find("streaming") > -1
    use_64 = base_location.find('x64') > -1
    return is_streaming, mem, factors


results_dirs = ['/srv/nfs/cortana/logs/results_batch_mem_cont_2560/',
'/srv/nfs/cortana/logs/results_batch_mem_cont_512/',
'/srv/nfs/cortana/logs/results_batch_sess_lifetime_2560/',
'/srv/nfs/cortana/logs/results_batch_sess_lifetime_512/',
'/srv/nfs/cortana/logs/results_batch_x64_mem_cont_2560/',
'/srv/nfs/cortana/logs/results_batch_x64_mem_cont_512/',
'/srv/nfs/cortana/logs/results_batch_x64_sess_lifetime_2560/',
'/srv/nfs/cortana/logs/results_batch_x64_sess_lifetime_512/',
]

missing_streaming_inventorys = {}
missing_transaction_inventorys = {}

missing_time_streaming_set = set()
missing_agg_streaming_set = set()
missing_time_transaction_set = set()
missing_agg_transaction_set = set()

for i in results_dirs:
    missing_time, missing_agg = inventory_failed_agg(os.path.join(i, 'streaming'))
    missing_time_streaming_set |= set(missing_time)
    missing_agg_streaming_set |= set(missing_agg)

    missing_time, missing_agg = inventory_failed_agg(os.path.join(i, 'transaction'))
    missing_time_transaction_set |= set(missing_time)
    missing_agg_transaction_set |= set(missing_agg)


missing_agg_list = [i for i in missing_agg_streaming_set] + [i for i in missing_agg_transaction_set]

working_list = []

use_64 = USE_64
if use_64:
    working_list = [i for i in missing_agg_list if i.find('x64') > -1]
else:
    working_list = [i for i in missing_agg_list if i.find('x64') == -1]
sifipr.WORKINGLIST = []
for i in working_list:
    _, _, factors = get_run_info(i)
    add_to_working_list(i, factors)

active_threads = []
destroy_passwords = 0
# sotp comment
#sifipr.WORKING_LIST = [i for i in generate_core_work_list()]
#sifipr.WORKING_LIST.sort()

print ("Experiment Log: Starting the work")
JAVA_WORK_HOST = JAVA_WORK_HOST_LIST if not use_64 else JAVA_WORK_HOST_X64_LIST 
SSL_WORK_HOST = SSL_HOST_LIST

start_host_list(JAVA_WORK_HOST)
init_available(JAVA_WORK_HOST, SSL_WORK_HOST)
first_run = True
sifipr.EXECING_VMS = True


while not is_working_list_empty() or len (active_threads) > 0:
    if is_java_host_avail() and is_ssl_host_avail() and not is_working_list_empty():
        while sifipr.HOSTS_DUMPING > sifipr.MAX_HOSTS_DUMPING:
            time.sleep( sifipr.HOSTS_DUMPING / float(sifipr.MAX_HOSTS_DUMPING))
        base_location, factors = next_working_list_item()
        if base_location is None or factors is None:
            continue
        java_host = get_java_host()
        if java_host is None:
            continue
        cmd_success = test_host_is_up(java_host, JAVA_USER, JAVA_PASS)
        if not cmd_success:
            add_java_host_back(java_host)
            time.sleep(5)
            continue
        else:
            ssl_host = get_ssl_host()
            print ("Experiment Log: Performing Java command using %s and %s, %d remaining"%(java_host, ssl_host, working_list_len()))
            #java_ip = get_host_ip(java_host)
            #ssl_ip = get_host_ip(ssl_host)
            is_streaming, jvm_mem, factors = get_run_info(base_location)
            t = threading.Thread(target=execute_command_steps, args=(java_host,
                                 ssl_host, factors, base_location, destroy_passwords, jvm_mem, is_streaming, use_64))
            t.start()
            active_threads.append((t, java_host, ssl_host,
                                    factors, destroy_passwords))
            #if first_run:
            time.sleep(10)
    else:
        first_run = False
        #cull_threads(ACTIVE_PROC_EXTRACTION_THREADS)
        cull_threads(active_threads)
        time.sleep(.1)
print "Finished submitting all requests for initial list, waiting for the threads to complete before starting the next one"

use_64 = True
stop_host_list(JAVA_WORK_HOST)
if use_64:
    working_list = [i for i in missing_agg_list if i.find('x64') > -1]
else:
    working_list = [i for i in missing_agg_list if i.find('x64') == -1]

for i in working_list:
    _, _, factors = get_run_info(i)
    add_to_working_list(i, factors)

JAVA_WORK_HOST = JAVA_WORK_HOST_LIST if not use_64 else JAVA_WORK_HOST_X64_LIST 
start_host_list(JAVA_WORK_HOST)
SSL_WORK_HOST = SSL_HOST_LIST

active_threads = []

init_available(JAVA_WORK_HOST, SSL_WORK_HOST)
first_run = True
sifipr.EXECING_VMS = True

while not is_working_list_empty() or len (active_threads) > 0:
    if is_java_host_avail() and is_ssl_host_avail() and not is_working_list_empty():
        while sifipr.HOSTS_DUMPING > sifipr.MAX_HOSTS_DUMPING:
            time.sleep( sifipr.HOSTS_DUMPING / float(sifipr.MAX_HOSTS_DUMPING))
        base_location, factors = next_working_list_item()
        if base_location is None or factors is None:
            continue
        java_host = get_java_host()
        if java_host is None:
            continue
        cmd_success = test_host_is_up(java_host, JAVA_USER, JAVA_PASS)
        if not cmd_success:
            add_java_host_back(java_host)
            time.sleep(5)
            continue
        else:
            ssl_host = get_ssl_host()
            print ("Experiment Log: Performing Java command using %s and %s, %d remaining"%(java_host, ssl_host, working_list_len()))
            #java_ip = get_host_ip(java_host)
            #ssl_ip = get_host_ip(ssl_host)
            is_streaming, jvm_mem, factors = get_run_info(base_location)
            t = threading.Thread(target=execute_command_steps, args=(java_host,
                                 ssl_host, factors, base_location, destroy_passwords, jvm_mem, is_streaming, use_64))
            t.start()
            active_threads.append((t, java_host, ssl_host,
                                    factors, destroy_passwords))
            #if first_run:
            time.sleep(10)
    else:
        first_run = False
        #cull_threads(ACTIVE_PROC_EXTRACTION_THREADS)
        cull_threads(active_threads)
        time.sleep(.1)
print "Finished submitting all requests for initial list, waiting for the threads to complete before starting the next one"

# TODO reinit WORKINGLIST
# shutdown Java and start java64, reinit machines
# run the above loop


pppds.ALLOWED_PROC_EXTRACTION_THREADS = 6

pppds.BASE_DIR = '/home/dso'
pppds.FAILED_POST_PROC_DUMP = "failed_post_proc.txt"
pppds.FAILED_POST_PROC_DUMP_F = os.path.join(pppds.BASE_DIR, pppds.FAILED_POST_PROC_DUMP)
pppds.FAILED_DUMP_OUT = open(pppds.FAILED_POST_PROC_DUMP_F, 'a')

base_locations = [i for i in missing_agg_streaming_set] + [i for i in missing_agg_transaction_set]
base_locations.sort()

COMPLETED = False
active_post_process_memdump_thread = threading.Thread(target=pppds.perform_string_extraction_thread_no_rm)
for base_location in base_locations:
    factors = {}
    pppds.add_job_to_queue(base_location, factors)

COMPLETED = True

active_post_process_memdump_thread.start()
active_post_process_memdump_thread.join()

import agg_exp_data as aed
aed.PROCESSES = 40
start_str = aed.time_str()

active_threads = []
procs = []
remaining = len(base_locations)
for base_location in base_locations[1:]:
    while len (active_threads) > aed.PROCESSES:
        if not aed.cull_threads(active_threads):
            time.sleep(2)
    proc = aed.init_task (base_location)
    procs.append(proc)
    active_threads.append((proc, (base_location,)))
    remaining += -1
    print ("%s: aggregating %s, %d remaining"%(aed.time_str(), base_location, remaining))

while len (active_threads) > 0:
    if not aed.cull_threads(active_threads):
        time.sleep(.1)

for r in procs:
    r.join()

end_str = aed.time_str()
print ("%s: Started"%start_str)
print ("%s: Ended"%end_str)