import sys
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(PATH_TO_RUN_MOD)
from basic_multi_host_commands import *

HOST_LIST = []
JAVA_USER = "java"
JAVA_PASSWORD = "java"
JAVA_HOST_FMT = "java-workx64-%02d"

if __name__ == "__main__":
    user = JAVA_USER
    password = JAVA_PASSWORD
    start = False if len(sys.argv) > 0 and sys.argv[-1] == '1' else True  
    if len(sys.argv) < 3:
        HOST_LIST = [JAVA_HOST_FMT%i for i in xrange(0, 10)]
    elif len(sys.argv) < 4:
      cmd = "%s <host_update> <user> <password> [1]\n 1 means stop"%sys.argv[0]
      print cmd
      sys.exit(-1)
    else:
        HOST_LIST = [sys.argv[1]]
        user = sys.argv[2]
        password = sys.argv[3]
    
    if start:
        start_hosts(user, password, HOST_LIST)
    else:
        stop_hosts(user, password, HOST_LIST)
