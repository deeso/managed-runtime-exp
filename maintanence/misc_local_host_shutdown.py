from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, calendar
from socket import error as socket_error
from datetime import datetime, timedelta
import traceback
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest) 
PATH_TO_JVM_MODULE = "/research_data/code/git/jvm_analysis"
sys.path.append(PATH_TO_JVM_MODULE)
 
import jvm
from jvm.mem_chunks import MemChunk
from jvm.extract_process import ExtractProc
 
from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

cmd = "sudo shutdown -h now"
start = datetime.now()
period = timedelta(minutes=120)
next_time = datetime.now() + period

SLEEP = 5 * 60.0
PERFORM_SHUTDOWN = True
def wait_n_minutes_shutdown(n_minutes):
    global SLEEP, PERFORM_SHUTDOWN
    cmd = "sudo shutdown -h now"
    start = datetime.now()
    period = timedelta(minutes=n_minutes)
    next_time = datetime.now() + period
    while True:
        now = datetime.now()
        if now > next_time:
            break
        time.sleep(SLEEP)
    
    if PERFORM_SHUTDOWN:
        os.system(cmd)