import sys
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(PATH_TO_RUN_MOD)
from basic_multi_host_commands import *

HOST_LIST = []
JAVA_USER = "java"
JAVA_PASSWORD = "java"
JAVA_HOST_FMT = "java-workx64-%02d"

COMMANDS = [
'''sudo sh -c "cp /srv/nfs/cortana/logs/cmd/modified_jars/jce.jar /usr/lib/jvm/java-8-oracle/jre/lib/"''',
'''sudo sh -c "cp /srv/nfs/cortana/logs/cmd/modified_jars/sunjce_provider.jar /usr/lib/jvm/java-8-oracle/jre/lib/ext/"''',
'''sudo sh -c "cp /srv/nfs/cortana/logs/cmd/modified_jars/rt.jar /usr/lib/jvm/java-8-oracle/jre/lib/"''',
'''sudo sh -c "cp /srv/nfs/cortana/logs/cmd/modified_jars/jsse.jar /usr/lib/jvm/java-8-oracle/jre/lib/"''',
]

if __name__ == "__main__":
    user = JAVA_USER
    password = JAVA_PASSWORD
    if len(sys.argv) < 2:
        HOST_LIST = [JAVA_HOST_FMT%i for i in xrange(0, 20)]
    elif len(sys.argv) < 3:
      cmd = "%s <host_update> <user> <password>"%sys.argv[0]
      print cmd
      sys.exit(-1)
    else:
        HOST_LIST = [sys.argv[1]]
        user = sys.argv[2]
        password = sys.argv[3]
    
    perform_command_set_on_hosts(user, password, HOST_LIST, COMMANDS)
