# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-00 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-01 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-02 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-03 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-04 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-05 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-06 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-07 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-08 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-09 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-10 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-11 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-12 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-13 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-14 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-15 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-16 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-17 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-18 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-19 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-20 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-21 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-22 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-23 java java
# python /srv/nfs/cortana/logs/cmd/copy_over_jre.py java-workx64-24 java java


HOST_LIST = []
EXP_SSL_USER = "expssl"
EXP_SSL_PASSWORD = "expssl"
EXP_SSL_FMT = "exp-ssl-%d"

import sys
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(PATH_TO_RUN_MOD)
from basic_multi_host_commands import *

if __name__ == "__main__":
    user = EXP_SSL_USER
    password = EXP_SSL_PASSWORD
    if len(sys.argv) < 2:
        HOST_LIST = [EXP_SSL_FMT%i for i in xrange(0, 20)]
    elif len(sys.argv) < 3:
      cmd = "%s <host_update> <user> <password>"%sys.argv[0]
      print cmd
      sys.exit(-1)
    else:
        HOST_LIST = [sys.argv[1]]
        user = sys.argv[2]
        password = sys.argv[3]
    
    perform_shutdown_on_hosts (user, password, HOST_LIST)
