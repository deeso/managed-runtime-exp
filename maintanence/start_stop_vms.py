from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
from datetime import datetime, timedelta

from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

WORKING_LIST = []
SLEEP_SECS = 5.0
EXPSSL_USER = EXPSSL_PASS = "expssl"
MAX_JAVA_HOST = 10
JAVA_HOST_FMT = "java-work-%d"
MAX_SSL_HOST = 10
SSL_HOST_FMT = "exp-ssl-%d"
MAX_RETRYS = 3
MAX_RETRY_SLEEP = 4.0
MAX_EXP_TIME = 5 # minutes

SSL_HOST_LIST = [SSL_HOST_FMT%i for i in xrange(0, MAX_SSL_HOST)]
SSL_HOST_LIST_2 = [SSL_HOST_FMT%i for i in xrange(0+10, MAX_SSL_HOST+10)]
 
HOST_TO_IP_MAPPING = {}
 
# sudo virsh destroy java-work-0
# virsh setmaxmem java-work-0 1048576 --config
# virsh start java-work-0
 
VIR_START = "virsh start {client}"
VIR_STOP = "virsh shutdown {client}"
VIRT_CONN = libvirt.open("qemu:///system")
 


def bounce_host_list(host_list):
    for host in host_list:
        bounce_function(host)

def send_enter_key(client):
    try:
        dom = VIRT_CONN.lookupByName(client)
        dom.sendKey(0, 0, [28,], 1, 0)
        return True
    except:
        return False

def bounce_function(client):
    stop_function(client)
    time.sleep(SLEEP_SECS)
    start_function(client)
    time.sleep(SLEEP_SECS)
 
def start_function (client):
    keyed = {"client":client}
    #cmd  = VIR_START.format(**keyed)
    dom = VIRT_CONN.lookupByName(client)
    res = -1
    try:
        res = dom.create()
        if res == 0:
            time.sleep(.75)
            # stupid bootloader menu
            dom.sendKey(0, 0, [28,], 1, 0)
    except:
        pass
    return res#exec_command(cmd)
 
def stop_function (client, timeout=.5):
    keyed = {"client":client}
    #cmd  = VIR_STOP.format(**keyed)
    dom = VIRT_CONN.lookupByName(client)
    res = -1
    try:
        res = dom.shutdown()
        time.sleep(.5)
        if dom.isActive() and res == 0:
            dom.destroy()
    except:
        pass
    return res#exec_command(cmd)
 
def start_host_list (host_list):
    for host in host_list:
        start_function(host)
 
def stop_host_list (host_list):
    for host in host_list:
        stop_function(host)
 
def snapshot_function(host):
    keyed = {"client":host}
    cmd  = VIR_SNAPSHOT.format(**keyed)
    return exec_command(cmd)
 
def snapshot_host_list (host_list):
    for host in host_list:
        snapshot_function(host)
 
def revert_function(host):
    keyed = {"client":host}
    cmd  = VIR_REVERT.format(**keyed)
    return exec_command(cmd)
 
def revert_host_list (host_list):
    for host in host_list:
        revert_function(host)


if __name__ == "__main__":
    # ip -s -s neigh flush all
    start = True if len(sys.argv) < 2 else False
    if not start and (sys.argv[1] == "-h" or sys.argv[1] == "--help"):
        print ("%s: start or stop SSL VMS, if no parameter is provided start,  otherwise stop.") 
        sys.exit(0)
    start_str = time_str()
    if not start:
        print ("Stopping all SSL VM Hosts (%d)"%(len(SSL_HOST_LIST+SSL_HOST_LIST_2)))
        stop_host_list(SSL_HOST_LIST+SSL_HOST_LIST_2)
    else:
        print ("Starting all SSL VM Hosts (%d)"%len(SSL_HOST_LIST+SSL_HOST_LIST_2))
        start_host_list(SSL_HOST_LIST+SSL_HOST_LIST_2)
    print "%s Started processing"%start_str
    print "%s Ended processing"%time_str()

