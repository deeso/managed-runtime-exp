from __future__ import print_function
import sys, os
BASE_EXP_DIR = "/srv/nfs/cortana/logs/cmd/python-gc-experiments/"
sys.path.append(BASE_EXP_DIR)
from python_experiments.java_exp import run_java_exp
__author__ = 'dso'


if __name__ == "__main__":
    if len(sys.argv) < 7:
        print (sys.argv[0] + \
            "JavaClientExperiment <host> <log_dir_location> <log_filename> <memory_pressure_factor>"+\
            " <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n"+\
            "Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n")
        print ("====ERROR====", file=sys.stderr)
        sys.exit(-1)
    run_java_exp_ssl(*(sys.argv[1:]))

