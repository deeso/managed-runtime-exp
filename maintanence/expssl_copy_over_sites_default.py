import sys
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(PATH_TO_RUN_MOD)
from basic_multi_host_commands import *

HOST_LIST = []
EXP_SSL_USER = "expssl"
EXP_SSL_PASSWORD = "expssl"
EXP_SSL_FMT = "exp-ssl-%d"

COMMANDS = [
'''sudo sh -c "cp /srv/nfs/cortana/logs/cmd/working-default-site /etc/nginx/sites-available/"'''
]
    
if __name__ == "__main__":
    user = EXP_SSL_USER
    password = EXP_SSL_PASSWORD
    if len(sys.argv) < 2:
        HOST_LIST = [EXP_SSL_FMT%i for i in xrange(0, 20)]
    elif len(sys.argv) < 3:
      cmd = "%s <host_update> <user> <password>"%sys.argv[0]
      print cmd
      sys.exit(-1)
    else:
        HOST_LIST = [sys.argv[1]]
        user = sys.argv[2]
        password = sys.argv[3]

    perform_command_set_on_hosts (user, password, HOST_LIST, COMMANDS)
