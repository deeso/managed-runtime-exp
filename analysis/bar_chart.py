import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import pandas as pd
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt

import agg_exp_data as aed
import single_factors_iteration_perform_runs as sifipr
java_heap_locs = lambda loc: os.path.join(loc, 'java_heap_key_locations.txt')

base_locations = ['/srv/nfs/cortana/logs/6_var_mem_exp/',]
locations = [[os.path.join(i, j) for j in os.listdir(i)] for i in base_locations]

heap_locs = []
for locs in locations:
    loc_dirs = []
    for i in locs:
        loc_dirs = loc_dirs + [os.path.join(i, j) for j in os.listdir(i)]
    for loc in loc_dirs:
        if os.path.exists(java_heap_locs(loc)):
            heap_locs.append(java_heap_locs(loc))

MODELS = {'LOW_MED_LOW_HIGH':'TCM5', 'HIGH_MED_HIGH_HIGH':'SM5'}
COLUMNS = ['eden_hits', 'hemi_hits', 'tenure_hits', 'eden_unique', 'hemi_unique', 'tenured_unique']
def parse_heap_loc_data(bloc, columns=COLUMNS):
    type_run = 'Apache' if bloc.find('apache_null') > -1 else 'Socket'

    _, _, heap_size, factors = aed.get_run_info(os.path.split(bloc)[0].strip())
    fstr = sifipr.get_factors_str(factors)
    key = "64_%s_%04d"%(fstr, heap_size)
    p = os.path.split(os.path.split(bloc)[0])[1]
    rvalue = int(p.split('_')[-1])
    line = open(bloc).readline().strip()
    items = line.split('-')
    d = {}
    for item in items:
        k, v = item.split(':')
        d[k] = int(v)
    model = MODELS[fstr]
    res = {'type':type_run, 'mem_size':heap_size, 'run':rvalue, 'key':key, 'model':model}
    for c in columns:
        if c in d:
            res[c] = d[c]
        else:
            res[c] = 0
    return res


def build_heap_locs_dataframe(locs):
    data = []
    for loc in locs:
        data.append(parse_heap_loc_data(loc))
    df = pd.DataFrame(data)
    return df

def plot_clustered_stacked(dfall, labels=None, title="multiple stacked bar plot",  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    axe = plt.subplot(111)

    for df in dfall : # for each data frame
        axe = df.plot(kind="bar",
                      linewidth=0,
                      stacked=True,
                      ax=axe,
                      legend=False,
                      grid=False,
                      **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * (i / n_col))
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)
    axe.set_title(title)


data_points = {}
for heap_loc in heap_locs:
    _, _, heap_size, _ = aed.get_run_info(os.path.split(heap_loc)[0].strip())
    if not heap_size in data_points:
        data_points[heap_size] = {'eden_hits':[], 'hemi_hits':[], 'tenure_hits':[],
                                  'eden_unique':[], 'hemi_unique':[], 'tenured_unique':[]}
    data = parse_heap_loc_data(heap_loc)
    for i in data_points[heap_size]:
        data_points[heap_size][i].append(data[i])





def get_means_std(df, rkeys, types):
    data = []
    for rkey in rkeys:
        for t in types:
            res = {}
            target_df = df[df.type == t]
            target_df = target_df[target_df.key == rkey]
            res['type'] = t
            res['key'] = rkey
            res['model'] = target_df.iloc[0].model
            res['mem_size'] = target_df.iloc[0].mem_size
            res['ehs_mean'] = np.round(np.mean(target_df.eden_hits))
            res['ehs_unique_means'] = np.round(np.mean(target_df.eden_unique))
            res['ehs_std'] = np.round(np.std(target_df.eden_hits))
            res['ehs_unique_std'] = np.round(np.std(target_df.eden_unique))

            res['hhs_mean'] = np.round(np.mean(target_df.hemi_hits))
            res['hhs_unique_means'] = np.round(np.mean(target_df.hemi_unique))
            res['hhs_std'] = np.round(np.std(target_df.hemi_hits))
            res['hhs_unique_std'] = np.round(np.std(target_df.hemi_unique))

            res['ths_mean'] = np.round(np.mean(target_df.tenure_hits))
            res['ths_unique_means'] = np.round(np.mean(target_df.tenured_unique))
            res['ths_std'] = np.round(np.std(target_df.tenure_hits))
            res['ths_unique_std'] = np.round(np.std(target_df.tenured_unique))

            data.append(res)
    return pd.DataFrame(data)


df = build_heap_locs_dataframe(heap_locs)
index = [ i for i in set(df[:].mem_size)]
rkeys = set([i for i in df[:].key])
types = set([i for i in df[:].type])

df_means_std = get_means_std(df, rkeys, types)

tcm5_df_eden = df_means_std[df_means_std.model == 'TCM5'][['ehs_mean',  'ehs_std',  'ehs_unique_means',  'ehs_unique_std', 'mem_size', 'type']]
tcm5_df_eden.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
tcm5_df_eden = tcm5_df_eden.set_index(['type', 'mem_size'])
tcm5_df_hemi = df_means_std[df_means_std.model == 'TCM5'][['hhs_mean',  'hhs_std',  'hhs_unique_means',  'hhs_unique_std', 'mem_size', 'type']]
tcm5_df_hemi.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
tcm5_df_hemi.set_index(['type', 'mem_size'])
tcm5_df_tenure = df_means_std[df_means_std.model == 'TCM5'][['ths_mean',  'ths_std',  'ths_unique_means',  'ths_unique_std', 'mem_size', 'type']]
tcm5_df_tenure.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
tcm5_df_tenure.set_index(['type', 'mem_size'])

sm5_df_eden = df_means_std[df_means_std.model == 'SM5'][['ehs_mean',  'ehs_std',  'ehs_unique_means',  'ehs_unique_std', 'mem_size', 'type']]
sm5_df_eden.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
sm5_df_eden.set_index(['type', 'mem_size'])
sm5_df_hemi = df_means_std[df_means_std.model == 'SM5'][['hhs_mean',  'hhs_std',  'hhs_unique_means',  'hhs_unique_std', 'mem_size', 'type']]
sm5_df_hemi.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
sm5_df_hemi.set_index(['type', 'mem_size'])
sm5_df_tenure = df_means_std[df_means_std.model == 'SM5'][['ths_mean',  'ths_std',  'ths_unique_means',  'ths_unique_std', 'mem_size', 'type']]
sm5_df_tenure.columns = ['mean',  'std',  'unique_mean',  'unique_std', 'mem_size', 'type']
sm5_df_tenure.set_index(['type', 'mem_size'])

fig = plt.figure()
ax = fig.add_subplot(111)
keys = data_points.keys()
keys.sort()
keys_s = [str(i) for i in keys]
ind = np.arange(len(keys))
space=.3
n = len(keys)
width = (1 - space) / n

for i,cond in enumerate(conditions):
    print "cond:", cond
    vals = dpoints[dpoints[:,0] == cond][:,2].astype(np.float)
    pos = [j - (1 - space) / 2. + i * width for j in range(1,len(categories)+1)]
    ax.bar(pos, vals, width=width)

df_0512 = pd.DataFrame([{''},])


df_1024 = df[df.mem_size == 1024][df.type == 'Apache']
df_2048 = df[df.mem_size == 2048][df.type == 'Apache']
df_4096 = df[df.mem_size == 4096][df.type == 'Apache']
df_8192 = df[df.mem_size == 8192][df.type == 'Apache']
df_16382 = df[df.mem_size == 16382][df.type == 'Apache']

width = 0.35
ehs_means = [np.mean(data_points[k]['eden_hits']) for k in keys]
hhs_means = [np.mean(data_points[k]['hemi_hits']) for k in keys]
ths_means = [np.mean(data_points[k]['tenure_hits']) for k in keys]

ehs_unique_means = [np.mean(data_points[k]['eden_unique']) for k in keys]
hhs_unique_means = [np.mean(data_points[k]['hemi_unique']) for k in keys]
ths_unique_means = [np.mean(data_points[k]['tenured_unique']) for k in keys]

ehs_std = [np.std(data_points[k]['eden_hits']) for k in keys]
hhs_std = [np.std(data_points[k]['hemi_hits']) for k in keys]
ths_std = [np.std(data_points[k]['tenure_hits']) for k in keys]
ehs_unique_std = [np.std(data_points[k]['eden_unique']) for k in keys]
hhs_unique_std = [np.std(data_points[k]['hemi_unique']) for k in keys]
ths_unique_std = [np.std(data_points[k]['tenured_unique']) for k in keys]

ehs_df = []
hhs_df = []
ths_df = []


ehs_means_plt = ax.bar(ind, ehs_means, width, color='r', yerr=ehs_std)
ehs_unique_means_plt = ax.bar(ind, ehs_unique_means, width, color='y',
             bottom=ehs_means, yerr=ehs_unique_std)

hhs_means_plt = ax.bar(ind+width, hhs_means, width, color='r', yerr=hhs_std)
hhs_unique_means_plt = ax.bar(ind+width, hhs_unique_means, width, color='y',
             bottom=hhs_means, yerr=hhs_unique_std)

ths_means_plt = ax.bar(ind+2*width, ths_means, width, color='r', yerr=ths_std)
ths_unique_means_plt = ax.bar(ind+2*width, ths_unique_means, width, color='y',
             bottom=ths_means, yerr=ths_unique_std)

ax.set_xlim(-width,len(ind)+4*width)
ax.set_ylabel('Number of Keys')

ax.set_xticks(ind + (4*width))
xtickNames = ax.set_xticklabels(keys_s)
plt.setp(xtickNames, rotation=45, fontsize=10)
#plt.yticks(np.arange(0, 81, 10))
#plt.legend((p1[0], p2[0]), ('Men', 'Women'))