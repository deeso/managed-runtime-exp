
TABLE_HEADER4 = u'''
\\begin{{table}}[h]
\\begin{{center}}
    \\resizebox{{\columnwidth}}{{!}}{{
    \\begin{{tabular}}{{l|rrrrr}}
    \multicolumn{{1}}{{c|}}{{TLS Client}}
    & \multicolumn{{1}}{{c}}{{\# of TLS}}
    & \multicolumn{{1}}{{c}}{{PMS}}
    & \multicolumn{{1}}{{c}}{{MS}}
    & \multicolumn{{1}}{{c}}{{Keys}}
    & \multicolumn{{1}}{{c}}{{STD Keys }} \\
    \multicolumn{{1}}{{c|}}{{Implementation}}
    & \multicolumn{{1}}{{c}}{{Sessions}
    & \multicolumn{{1}}{{c}}{{Found}}
    & \multicolumn{{1}}{{c}}{{Found}}
    & \multicolumn{{1}}{{c}}{{Recovered}}
    & \multicolumn{{1}}{{c}}{{Recovered}} \\
    \hline
    {table_data}
    \end{{tabular}}
}}
\\end{{center}}
\\end{{table}}
'''


base_dir = '/srv/nfs/cortana/logs/5_modms_only_var_exp/'
modms_data_dirs = [
['Apache', '112_nolog_mrsmith_apache_modms_mem_comp_run_x64_4096'],
['Apache - {\\tt null}', '112_nolog_mrsmith_apache_null_modms_mem_comp_run_x64_4096'],
['Socket - {\\tt null}', '113_nolog_mrsmith_socket_null_1gc_modms_mem_comp_run_x64_4096'],
['Apache with BouncyCastle', '113_nolog_mrssmith_bouncycastle_null_unmod_mem_comp_run_x64_4096'],
]

unmod_data_dirs = [
['Apache', '113_nolog_mrssmith_apache_unmod_mem_comp_run_x64_4096'],
['Apache - {\\tt null}', '113_nolog_mrssmith_apache_null_unmod_mem_comp_run_x64_4096'],
['Socket - {\\tt null}', '113_nolog_mrssmith_socket_null_unmod_mem_comp_run_x64_4096'],
['Apache with BouncyCastle', '113_nolog_mrssmith_bouncycastle_null_unmod_mem_comp_run_x64_4096'],
]


pausing_data_dirs = [
['Apache - {\\tt null}', '112_nolog_mrsmith_apache_null_pause_unmod_mem_comp_run_x64_4096'],
['Apache - {\\tt null}', '112_nolog_mrsmith_apache_null_pause_modms_mem_comp_run_x64_4096'],
['Socket - {\\tt null}', '112_nolog_mrsmith_socket_null_pause_unmod_mem_comp_run_x64_4096'],
['Socket - {\\tt null}', '112_nolog_mrsmith_socket_null_pause_modms_mem_comp_run_x64_4096'],
]



tcm_line = "{program} & {tcm5_mean_ssl} & {tcm5_mean_pms_hits} & {tcm5_mean_mkeyblock_hits} & {tcm5_mean_unique_keys} & {tcm5_std_unique_keys} \\\\"
sm_line = "{program} & {sm5_mean_ssl} & {sm5_mean_pms_hits} & {sm5_mean_mkeyblock_hits} & {sm5_mean_unique_keys} & {sm5_std_unique_keys} \\\\"

def extract_df_info(df):
    results = {}
    t = df[df.model == 'TCM5']
    #t = t[t.num_ssl > 8000]
    results['tcm5_mean_ssl'] = int(np.round(np.mean(t.num_ssl)))
    results['tcm5_mean_unique_keys'] = int(np.round(np.mean(t.unique_keys)))
    results['tcm5_std_unique_keys'] = int(np.round(np.std(t.unique_keys)))
    results['tcm5_mean_mkeyblock_hits'] = int(np.round(np.mean(t.unique_mkeyblock_hits)))
    results['tcm5_mean_pms_hits'] = int(np.round(np.mean(t.unique_pms_hits)))
    t = df[df.model == 'SM5']
    #t = t[t.num_ssl > 8000]
    results['sm5_mean_ssl'] = int(np.round(np.mean(t.num_ssl)))
    results['sm5_mean_unique_keys'] = int(np.round(np.mean(t.unique_keys)))
    results['sm5_std_unique_keys'] = int(np.round(np.std(t.unique_keys)))
    results['sm5_mean_mkeyblock_hits'] = int(np.round(np.mean(t.unique_mkeyblock_hits)))
    results['sm5_mean_pms_hits'] = int(np.round(np.mean(t.unique_pms_hits)))
    return results

tcm5_modms_lines = []
sm5_modms_lines = []
modms_results = []
for program, data_dir in modms_data_dirs:
    ds = os.path.join(base_dir, data_dir)
    df = ana.build_data_frame([ds, ])
    r = extract_df_info(df)
    r['program'] = program
    tcm5_modms_lines.append(tcm_line.format(**r))
    sm5_modms_lines.append(sm_line.format(**r))
    modms_results.append(r)

table_data = {'table_data':"\n".join(tcm5_modms_lines)}

print TABLE_HEADER4.format(**table_data)
table_data = {'table_data':"\n".join(sm5_modms_lines)}
print TABLE_HEADER4.format(**table_data)


tcm5_unmod_lines = []
sm5_unmod_lines = []
unmod_results = []
for program, data_dir in unmod_data_dirs:
    ds = os.path.join(base_dir, data_dir)
    df = ana.build_data_frame([ds, ])
    r = extract_df_info(df)
    r['program'] = program
    tcm5_unmod_lines.append(tcm_line.format(**r))
    sm5_unmod_lines.append(sm_line.format(**r))
    unmod_results.append(r)


table_data = {'table_data':"\n".join(tcm5_unmod_lines)}
print TABLE_HEADER4.format(**table_data)
table_data = {'table_data':"\n".join(sm5_unmod_lines)}
print TABLE_HEADER4.format(**table_data)


print "\n".join(tcm5_modms_lines)

print "\n".join(tcm5_unmod_lines)

print "\n".join(sm5_modms_lines)

print "\n".join(sm5_unmod_lines)


pausing_tcm5_unmod_lines = []
pausing_sm5_unmod_lines = []
pausing_results = []
for program, data_dir in pausing_data_dirs:
    ds = os.path.join(base_dir, data_dir)
    df = ana.build_data_frame([ds, ])
    r = extract_df_info(df)
    r['program'] = program
    pausing_tcm5_unmod_lines.append(tcm_line.format(**r))
    pausing_sm5_unmod_lines.append(sm_line.format(**r))
    pausing_results.append(r)


print "\n".join(pausing_tcm5_unmod_lines)
print "\n".join(pausing_sm5_unmod_lines)
