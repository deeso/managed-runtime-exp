dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
sys.path.append(dest)
import unzip_perform_process_dirs as uppd
import agg_exp_data as aed
import single_factors_iteration_perform_runs as sifipr
import matplotlib.pyplot as plt
from multiprocessing import Process
import os, urllib, json, pipes, traceback
import binascii, subprocess, json, shutil
import multiprocessing
from datetime import datetime
import numpy as np
from numpy import mean
from pandas import DataFrame
#from single_factors_iteration_perform_runs import *
import single_factors_iteration_perform_runs as sifipr
import matplotlib.pyplot as plt
import unzip_perform_process_dirs as uppd
import pandas as pd


data_set = [

'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_0512',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_0512',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_1024',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_1024',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_2048',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_2048',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_3072',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_3072',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_4096',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_4096',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_2304',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_2304',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_8192',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_8192',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_16384',
'/srv/nfs/cortana/logs/2_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_16384',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_0512',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_0512',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_1024',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_1024',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_2048',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_2048',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_3072',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_3072',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_4096',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_4096',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_2304',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_2304',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_8192',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_8192',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/unmod_mem_comp_run_x64_16384',
'/srv/nfs/cortana/logs/2_modms_only_cortana_single_host_memory_var_exp/modms_mem_comp_run_x64_16384',

'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_2048',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_2048',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_2048',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_3072',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_3072',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_3072',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_4096',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_4096',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_4096',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_2304',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_2304',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_2304',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_8192',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_8192',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_8192',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_semi_agg_run_x64_16384',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_agg_run_x64_16384',
'/srv/nfs/cortana/logs/3_mrsmith_fixed_single_host_gc_exp/unmod_nogc_run_x64_16384',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_0512',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_1024',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_2048',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_3072',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_4096',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_8192',
'/srv/nfs/cortana/logs/3_mrsmith_single_host_memory_var_exp/unmod_mem_bc_comp_run_x64_16384',
'/srv/nfs/cortana/logs/3_modms_testing_mrsmith_testing_memory_var_exp/testing_modms_mem_comp_run_x64_8192/',

]

data_set = [
'/srv/nfs/cortana/logs/3_unmod_only_identify_potential_cliffs/mrsmith_unmod_mem_comp_run_x64_4096/',
'/srv/nfs/cortana/logs/3_unmod_only_identify_potential_cliffs/mrssmith_unmod_mem_comp_run_x64_4096/',
'/srv/nfs/cortana/logs/3_modms_only_5000/microsim_modms_mem_comp_run_x64_4096/',
'/srv/nfs/cortana/logs/3_modms_only_5000/2_mrsmith_modms_mem_comp_run_x64_4096/',
]

data_set = [
'/srv/nfs/cortana/logs/5_modms_only_var_exp/2_mrsmith_modms_mem_comp_run_x64_4096',
]


def build_data_frame(locations):
    dframe_list = []
    blocs = locations
    bloc_files = []
    for bloc in blocs:
        if os.path.isdir(bloc):
            bloc_files += [os.path.join(bloc, i) for i in os.listdir(bloc) if os.path.isdir(os.path.join(bloc, i))]
    print ("Building data frame list from %d data samples"%len(bloc_files))
    for bloc in bloc_files:
        #print ("Processing file: %s"%bloc)
        if not uppd.check_completed_memory_extraction_completed(bloc):
            print ("Memory extraction not complete for: %s"%bloc)
            continue
        use_64, _, jvm_mem, factors = aed.get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        key = "64_%s_%d"%(fstr, jvm_mem)
        if bloc.find('unmod_mem_bc') > -1:
           exp_type='unmod_bcastle'
        elif bloc.find('unmod_nogc') > -1:
           exp_type = "unmod_nogc"
        elif bloc.find('unmod_semi_agg') > -1:
           exp_type = "unmod_semi_agg"
        elif bloc.find('unmod_agg') > -1:
           exp_type = "unmod_agg"
        elif bloc.find('modms_only') > -1:
           exp_type = "modms_only"
        elif bloc.find('testing_modms') > -1:
           exp_type = "testing_modms"
        elif bloc.find('test_modms') > -1:
           exp_type = "test_modms"
        elif bloc.find('modms_') > -1:
           exp_type = "modms"
        elif bloc.find('unmod_') > -1:
           exp_type = "unmod"
        else:
            exp_type = "unknown"

        if bloc.find('_shared_') > -1:
           is_share = 1

        model = aed.ABBR_MAPPING[fstr]
        try:
            data = aed.get_augmented_data(bloc)
        except:
            try:
                aed.write_augmented_data(bloc)
            except:
                print ("Failed to read and write  augmented data from: %s"%bloc)
                continue
            try:
                data = aed.get_augmented_data(bloc)
            except:
                print ("Failed to read augmented data from: %s"%bloc)
                continue

        data['model'] = model
        data['exp_type'] = exp_type
        data['key'] = key
        data['heap_mem'] = jvm_mem
        r_value = -1
        try:
            r_value = int(bloc.split("_")[-1])
        except:
            pass
        data['run'] = r_value
        gclog_data = {}
        try:
            gclog_data = aed.get_gclog_summary_data(bloc)
        except:
            try:
                perform_gc_log_parse_save(bloc)
                gclog_data = aed.get_gclog_summary_data(bloc)
            except:
                print ("Failed to read the GC log for %s, may want to ignore"%bloc)
                #continue
        data.update(gclog_data)
        comp_key_data = aed.get_compromised_key_summary_data(bloc)
        if len(comp_key_data) == 0:
            print ("Suspicious data point, no keys found: %s, excluding"%bloc)
            continue
        data['unique_keys'] = len(comp_key_data)
        data['unique_pms_hits'] = sum([1 for i in comp_key_data if i['pms_hits'] > 0])
        data['pms_hits'] = sum([i['pms_hits'] for i in comp_key_data ])
        data['unique_mkeyblock_hits'] = sum([1 for i in comp_key_data if i['mkeyblock_hits'] > 0])
        data['mkeyblock_hits'] = sum([i['mkeyblock_hits'] for i in comp_key_data ])
        data['key_avg_age'] = np.mean([i['age'] for i in comp_key_data])
        data['key_max_age'] = max([i['age'] for i in comp_key_data])
        data['key_min_age'] = min([i['age'] for i in comp_key_data])
        dframe_list.append(data)
    df = pd.DataFrame(dframe_list)
    return df

