import shlex
from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, calendar
from socket import error as socket_error
from datetime import datetime, timedelta
import traceback
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
PATH_TO_JVM_MODULE = "/research_data/code/git/jvm_analysis"
sys.path.append(PATH_TO_JVM_MODULE)

import jvm
from jvm.mem_chunks import MemChunk
from jvm.extract_process import ExtractProc

from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

USING_SINGLE_HOST = False
MAX_MEM_USED = 2504
SLEEP_MULTIPLIER = 0.0

def set_single_host_mem(mem):
    global USING_SINGLE_HOST, MAX_MEM_USED, SLEEP_MULTIPLIER
    USING_SINGLE_HOST = True
    MAX_MEM_USED = mem
    SLEEP_MULTIPLIER = 30.0 * (MAX_MEM_USED / 2048.0)
    SLEEP_MULTIPLIER = SLEEP_MULTIPLIER if SLEEP_MULTIPLIER < 120 else 80


MAX_ALLOWED_REQ = 240
MAX_CONCURRENT_SESSIONS = 240

ZIP_ARGS = "zip -r {archive} {target_file}"
REMOVE_MEMORY = False
RM_FILE_CMD = "rm -rf {the_file}"
JAVA_HPROF = 'java.hprof.txt'
JAVA_HPROF_RESULTS = 'extracted_java_hprof_info.txt'
MIN_LOG_COUNT = 50
MIN_AUTH_COUNT = 10
TIME_LOG = "timelog_start_end_actual.txt"
READY_FILE = "ready_process.txt"
TESTING = False
WORKING_LIST = []
SLEEP_SECS = 5.0
PYTHON_USER = PYTHON_PASS = "java"
JAVA_USER = JAVA_PASS = "java"
EXPSSL_USER = EXPSSL_PASS = "expssl"
MAX_JAVA_HOST = 25 if not TESTING else 5
JAVA_HOST_FMT = "java-work-%d"
JAVA_HOST_X64_FMT = "java-workx64-%02d"
PYTHON_HOST_FMT = "python-work-%02d"
PYTHON_HOST_X64_FMT = "python-workx64-%02d"
HOST_USED_LOCK = threading.Lock()
HOST_PAUSED_LOCK = threading.Lock()
JAVA_VM_RUNNING = False

JAVA_DUMPING_MEMORY = False
JAVA_PERFORM_DUMPS = False
HOSTS_READY_TO_DUMP = []
HOST_USED = 0
MAX_HOST_USED = 0
MAX_SSL_HOST = 25 if not TESTING else 5
SSL_HOST_FMT = "exp-ssl-%d"
MAX_RETRYS = 3
MAX_RETRY_SLEEP = 4.0
MAX_EXP_TIME = 60*10 # minutes
GC_START_TIME = MAX_EXP_TIME - 30
MAX_NUM_REQUESTS = 3700
GC_SPACING_TIME = 0
BATCH_HOST_SZ = 25 if not TESTING else 5
REBOOT = '''sudo sh -c "reboot"'''

JAVALOG_STDERR = "stderr_log.txt"
JAVALOGBASE_DIR = "/srv/nfs/cortana/logs/output/"
ERROR_FILE = os.path.join(JAVALOGBASE_DIR, JAVALOG_STDERR)
ERROR_OUT_LOCK = threading.Lock()
ERROR_OUT = open(ERROR_FILE, 'w')

PYTHON_WORK_HOST_X64_LIST = [PYTHON_HOST_X64_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
PYTHON_WORK_HOST_X64_LIST_2 = [PYTHON_HOST_X64_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]
JAVA_WORK_HOST_X64_LIST = [JAVA_HOST_X64_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
JAVA_WORK_HOST_X64_LIST_2 = [JAVA_HOST_X64_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]

JAVA_WORK_HOST_LIST = [JAVA_HOST_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
JAVA_WORK_HOST_LIST_2 = [JAVA_HOST_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]
PYTHON_WORK_HOST_LIST = [PYTHON_HOST_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
PYTHON_WORK_HOST_LIST_2 = [PYTHON_HOST_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]
SSL_HOST_LIST = [SSL_HOST_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
SSL_HOST_LIST_2 = [SSL_HOST_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]

HOST_TO_IP_MAPPING = {}
FIND_POST_RE = re.compile(".*user=.*&password=.*Content-Type.*")
FIND_POST_ALL_RE = re.compile(".*user=.*&password=.*")

JBGREP_FILE = "jbgrep_strings.txt"
JBGREP_WKEY_FILE = "jbgrep_wkey_strings.txt"
MERGED_KEYINFO = "merged_keyinfo.txt"

# sudo virsh destroy java-work-0
# virsh setmaxmem java-work-0 1048576 --config
# virsh start java-work-0

VIR_START = "virsh start {client}"
VIR_STOP = "virsh shutdown {client}"
#VIRT_CONN = libvirt.open("qemu:///system")

FACTORS_FILE = "factors.txt"
STDOUT_LOG = "stdout_log.txt"
STDERR_LOG = "stderr_log.txt"
AUTH_LOG = "authlog.txt"

BASE_DIR = "/srv/nfs/cortana/logs/"

SSLLOG_DIR = "ssllogs"
SSLLOG_BASE = os.path.join(BASE_DIR, SSLLOG_DIR)
SSLLOG_FMT_PREMASTER = "server-{ssl_host}-info-premaster.txt"
SSLLOG_FMT_KEYBLOCK = "server-{ssl_host}-info.txt"
SSLLOG_KEYBLOCK = "ssl_server_keyblock.txt"
SSLLOG_PREMASTER = "ssl_server_premaster.txt"

# factor directory
JAVALOG_DIR = "output"
JAVALOG_BASE = os.path.join(BASE_DIR, JAVALOG_DIR)
AUTH_LOG = JAVALOG_AUTH = "authlog.txt"
JSON_FILE = "agg_auth_data.json"
COMPLETE_FILE = "completed.txt"
STDOUT_LOG = JAVALOG_STDOUT = "stdout_log.txt"
MISSING_USERS = "err_missing_users.txt"
FAIL_STRING = "FAILLAIF" * 3

FACTOR_DIR_FMT = "{mem_pressure_factor}_{sess_lifetime_factor}"+\
             "_{conc_session}_{request_factor}"

FACTORS_MIN = 0
FACTORS_MAX = 4
FACTORS = {
"VHIGH":4,
"HIGH":3,
"MED":2,
"LOW":1,
"VLOW":0,
"RANDOM":0,
4:"VHIGH",
3:"HIGH",
2:"MED",
1:"LOW",
0:"VLOW",
-1:"RANDOM",
}

CORE_FACTORS_MIN = 1
CORE_FACTORS_MAX = 3
CORE_FACTORS = {
"HIGH":3,
"MED":2,
"LOW":1,
"RANDOM":-1,
3:"HIGH",
2:"MED",
1:"LOW",
-1:"RANDOM"
}

FACTOR_VALUE = {
'VHIGH': .9,
'HIGH': .8,
'MED': .4,
'LOW': .2,
'VLOW': .1,
}


FACTOR_NAMES = ["mem_pressure_factor", "sess_lifetime_factor"+\
             "conc_session", "request_factor"]
JVM_USE_SERIAL = True
#JVM_MEMORY_USED = 512
JVM_NEW_MEMORY_USED = None
JVM_SURVIVOR_MEM = None

VOL_MEMMAP_FILE = "java_memmaps.txt"
VOL_PATH = "/research_data/code/git/volatility/vol.py"
VOL_CMD = "python {vol} --profile {profile} -f {dump_file} -p {pid} linux_memmap > {target}"

JAR_BASE = DEST_LIBS

AUTH_DATA = JSON_FILE
COMPROMISED_SESSIONS = "compromised_sessions.txt"
COMPROMISED_PASSWORDS = "compromised_passwords.txt"
COMPROMISED_KEYS = "compromised_keys.txt"
COMPROMISED_POST_DATA = "compromised_post_data.txt"
JAVA_HEAP_KEY_LOCATIONS = "java_heap_key_locations.txt"
JAVA_HEAP_KEY_LOCATIONS_SUMMARY = "java_heap_key_locations_summary.txt"
PCT_DATA = "summary_compromised_data_pct.txt"

JBGREP_COUNT_RES_FILE = "found_ssl_keys_counts.txt"
JBGREP_FNAME_RES_FILE = "found_ssl_keys_f.txt"
JBGREP_GREP_RES_FILE = "found_ssl_keys_g.txt"
JBGREP_WKEY_COUNT_RES_FILE = "found_ssl_wkeys_counts.txt"
JBGREP_WKEY_FNAME_RES_FILE = "found_ssl_wkeys_f.txt"
JBGREP_WKEY_GREP_RES_FILE = "found_ssl_wkeys_g.txt"

JBGREP_NUM_THREADS = 3
JBGREP_NUM_THREADS_NO_EXECING = 21

JAVA_JBGREP_JAR_FILE = "jbgrep-all-1.0.jar"
JAVA_JBGREP_JAR_CMD = os.path.join(JAR_BASE, JAVA_JBGREP_JAR_FILE)
JAVA_JBGREP_EXP_ARGS = " ".join(['java', '-jar', JAVA_JBGREP_JAR_CMD,
                       "-binaryFile {java_dump} ",
                       "-binaryStrings {jbgrep_strings} ",
                       "-byFilenameOutput {fname_output}",
                       "-grepOutput {grep_output}",
                       "-keyCountOutput {keycount_output}",
                       "-numThreads {num_threads}",
                       #"-liveUpdate"
                       ])

#JBGREP_WORKER_POOL = Pool(MAX_JAVA_HOST)
#JBGREP_WORKER_RESULTS = []


VIR_DUMP = """virsh dump {client} {dumploc} --bypass-cache""" +\
       """ --live --verbose --memory-only"""
VIR_SUSPEND = """virsh suspend {client}"""
VIR_DOMINFO = """virsh dominfo {client}"""
VIR_RESUME = """virsh resume {client}"""

PYTHON_DUMPS = "python_dumps"
JAVA_DUMPS = "java_dumps"
VIR_SNAPSHOT = """virsh snapshot-create {client}"""
VIR_REVERT = """virsh snapshot-revert {client} --current"""

JAVA_DUMP_FINAL_ZIP = "memory-work.dump.zip"
JAVA_DUMP_FINAL = "memory-work.dump"
PYTHON_DUMP_FINAL_ZIP = "memory-work.dump.zip"
PYTHON_DUMP_FINAL = "memory-work.dump"
STRING_OUTPUT = "strings_java_proc.txt"
STRING_OUTPUT_FULL = "strings_full.txt"
CHMOD_DUMP = '''sudo chmod a+rw %s'''
VOL_HOST_PROFILE_x64 = 'LinuxUbuntu1404x64x64'
VOL_PY_HOST_PROFILE = 'LinuxUbuntu1504x64'
VOL_HOST_PROFILE_x86 = 'LinuxUbuntu1404x86'
VOL_HOST_PROFILE_x86 = 'LinuxUbuntu1504-whatx86'
# TODO Change this back to x64 before experiments
VOL_HOST_PROFILE = VOL_HOST_PROFILE_x86 #= VOL_HOST_PROFILE_x64

AVAILABLE_EXP_HOSTS = []
AVAILABLE_SSL = []


ALLOWED_PROC_EXTRACTION_THREADS_EXEC_VMS = 2
ALLOWED_PROC_EXTRACTION_THREADS = 10
ACTIVE_PROC_EXTRACTION_THREADS = []
PROC_EXTRACTION_QUEUE = []
EXECING_VMS = False


DUMP_LOCK = threading.Lock()
HOSTS_DUMPING = 0
MAX_HOSTS_DUMPING = 0
SIGNAL_HOST = "master-chief"
SIGNAL_PORT = 4545

MAX_ALLOWED_RUNNING = 0

FREE_SERVER_MEM = ["sudo", "sh",  '-c', 'echo 3 > /proc/sys/vm/drop_caches']

CP_LOCK = threading.Lock()
COMPRESSION_PROCS = []
PRUNE_COMPRESSION_PROCS_THREAD = None
POLL_COMPRESSION_PROCS = False
SLEEP_TIME_PRUNE_COMP_PROCS=45

def poll_compression_procs():
    global POLL_COMPRESSION_PROCS, SLEEP_TIME_PRUNE_COMP_PROCS, PRUNE_COMPRESSION_PROCS_THREAD, COMPRESSION_PROCS
    POLL_COMPRESSION_PROCS = True
    while POLL_COMPRESSION_PROCS:
        remove_the_dead_compression_procs()
        if POLL_COMPRESSION_PROCS:
            time.sleep(SLEEP_TIME_PRUNE_COMP_PROCS)

    while len(COMPRESSION_PROCS) > 0:
        remove_the_dead_compression_procs()
        time.sleep(SLEEP_TIME_PRUNE_COMP_PROCS)


def start_compression_procs_thread():
    global POLL_COMPRESSION_PROCS, SLEEP_TIME_PRUNE_COMP_PROCS, PRUNE_COMPRESSION_PROCS_THREAD, COMPRESSION_PROCS
    if PRUNE_COMPRESSION_PROCS_THREAD and PRUNE_COMPRESSION_PROCS_THREAD.isAlive():
        return
    PRUNE_COMPRESSION_PROCS_THREAD = threading.Thread(target=poll_compression_procs)

def stop_compression_procs_thread():
    global POLL_COMPRESSION_PROCS, SLEEP_TIME_PRUNE_COMP_PROCS, PRUNE_COMPRESSION_PROCS_THREAD, COMPRESSION_PROCS
    POLL_COMPRESSION_PROCS = False

def wait_compression_procs_thread():
    global POLL_COMPRESSION_PROCS, SLEEP_TIME_PRUNE_COMP_PROCS, PRUNE_COMPRESSION_PROCS_THREAD, COMPRESSION_PROCS
    if PRUNE_COMPRESSION_PROCS_THREAD and PRUNE_COMPRESSION_PROCS_THREAD.isAlive():
        PRUNE_COMPRESSION_PROCS_THREAD.join()

def capture_hprof_file(java_host, base_location):
    client = ssh_to_target(java_host, JAVA_USER, JAVA_PASS)
    hprof_fname = get_java_hprof()
    wait_for_hprof_dump_complete(client, hprof_fname)
    dst = get_java_hprof(base_location)
    data = exec_remote_cmd(client, "cp %s %s"%(hprof_fname, dst))
    data = exec_remote_cmd(client, "rm java.hprof.*")
    #open(dst, 'w').write(data)
    perform_java_hprof_compression(base_location)
    try:
        client.close()
    except:
        pass


def create_zip_cmd(archive, target_files ):
    keyed = {'target_file':" ".join(target_files),
             'archive':archive}
    return ZIP_ARGS.format(**keyed)

def perform_mem_dump_compression(base_location, use_python):
    global COMPRESSION_PROCS, CP_LOCK
    res = 1
    dump_file = get_java_dump_location(base_location, use_python=use_python)
    dump_zip_file = get_java_dump_zip_location(base_location, use_python=use_python)
    zip_command = create_zip_cmd(dump_file+'.zip', [dump_file,])
    #print ("Experiment Log: zipiing the memory file %s"%dump_file)
    #print ("Experiment Log: zip command: %s and list:[%s]"%(zip_command, zip_command.split()))
    try:
        p = subprocess.Popen(zip_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        CP_LOCK.acquire()
        COMPRESSION_PROCS.append((p, dump_file, True))
        # try:
        #     print p.communicate()[0]
        # except:
        #     traceback.print_exc()
        res = 0
    except:
        traceback.print_exc()
    finally:
        CP_LOCK.release()
    try:
        remove_the_dead_compression_procs()
    except:
        print "Exception in remove_the_dead_compression_procs"
        traceback.print_exc()
    return res

def perform_java_hprof_compression(base_location):
    global COMPRESSION_PROCS, CP_LOCK
    fname = get_java_hprof(base_location)
    zip_cmd = create_zip_cmd(fname+'.zip', [fname,])
    print ("Experiment Log: zipiing the hprof file %s"%fname)
    p = subprocess.Popen(zip_cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    CP_LOCK.acquire()
    COMPRESSION_PROCS.append((p, fname, True))
    CP_LOCK.release()
    try:
        remove_the_dead_compression_procs()
    except:
        print "Exception in remove_the_dead_compression_procs"
        traceback.print_exc()

def remove_the_dead_compression_procs():
    global REMOVE_MEMORY, CP_LOCK, COMPRESSION_PROCS
    the_live_ones = []
    CP_LOCK.acquire()
    try:
        for item in COMPRESSION_PROCS:
            p, compressed_file, rm_file = item[0], item[1], item[2]
            if p.poll() is None:
                the_live_ones.append((p, compressed_file, rm_file))
            elif rm_file:
                print ("Experiment Log: Removing Memory as requested for %s "%compressed_file)
                try:
                    print ("%s Experiment Log: Truncating the file %s "%(time_str(), compressed_file))
                    open(compressed_file, 'w').write("0")
                    print ("%s Experiment Log: removing the file %s "%(time_str(), compressed_file))
                    rm_cmd = RM_FILE_CMD.format(**{"the_file":compressed_file})
                    p = subprocess.Popen(rm_cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    the_live_ones.append((p, compressed_file, False))
                except:
                    traceback.print_exc()
        COMPRESSION_PROCS = the_live_ones
    except:
        traceback.print_exc()

    CP_LOCK.release()

def free_server_memory():
    exec_command(FREE_SERVER_MEM)

def reapply_work_unit(base_location, factors, reason="unknown"):
    global WORKING_LIST
    print ("%s: adding %s back to work queue: %s"%(time_str(), base_location, reason))
    WORKING_LIST.append((base_location, factors))

def working_list_len():
    return len(WORKING_LIST)

def next_working_list_item():
    if len(WORKING_LIST) > 0:
        return WORKING_LIST.pop(0)
    return None, None

def add_to_working_list(base_location, factors):
    WORKING_LIST.append((base_location, factors))

def is_working_list_empty():
    return len(WORKING_LIST) == 0

def add_ssl_host_back(ssl_host):
    AVAILABLE_SSL.append(ssl_host)

def get_ssl_host():
    if len(AVAILABLE_SSL):
        return AVAILABLE_SSL.pop(0)
    return None

def is_ssl_host_avail():
    return len(AVAILABLE_SSL) > 0

def is_java_host_avail():
    global AVAILABLE_EXP_HOSTS, HOST_USED, MAX_HOST_USED
    return len(AVAILABLE_EXP_HOSTS) > 0 and\
            HOST_USED < MAX_HOST_USED and\
            not is_dumping_memory()

def add_java_host_back(java_host):
    global HOST_USED_LOCK, AVAILABLE_EXP_HOSTS, HOST_USED
    HOST_USED_LOCK.acquire()
    HOST_USED -= 1
    HOST_USED_LOCK.release()
    AVAILABLE_EXP_HOSTS.append(java_host)

def get_java_host_len():
   return len(AVAILABLE_EXP_HOSTS)

def get_java_host():
    global HOST_USED_LOCK, AVAILABLE_EXP_HOSTS, HOST_USED
    if len(AVAILABLE_EXP_HOSTS):
        HOST_USED_LOCK.acquire()
        HOST_USED += 1
        HOST_USED_LOCK.release()
        return AVAILABLE_EXP_HOSTS.pop(0)
    return None

def complete_dump():
    global DUMP_LOCK, HOSTS_DUMPING
    DUMP_LOCK.acquire()
    HOSTS_DUMPING -= 1
    DUMP_LOCK.release()

def start_dump():
    global DUMP_LOCK, HOSTS_DUMPING
    DUMP_LOCK.acquire()
    HOSTS_DUMPING += 1
    DUMP_LOCK.release()

def read_hosts_dumping():
    global DUMP_LOCK, HOSTS_DUMPING
    DUMP_LOCK.acquire()
    i = HOSTS_DUMPING
    DUMP_LOCK.release()
    return i

def log_error(msg):
    ERROR_OUT_LOCK.acquire()
    ERROR_OUT.write(msg+"\n")
    ERROR_OUT_LOCK.release()

def get_java_dumps_location(base_location, use_python=False):
    java_base = base_location
    if use_python:
        return os.path.join(java_base, PYTHON_DUMPS, '')
    return os.path.join(java_base, JAVA_DUMPS, '')

def get_java_process_with_base_location(base_location, use_64=False, use_python=False):
    dump_file = get_java_dump_location(base_location, use_python)
    return get_java_process_with_dump_file(dump_file, use_64=use_64, use_python=use_python)

def get_java_process_with_dump_file(dump_file, use_64=False, use_python=False):
    profile = get_vol_profile(use_64=use_64, use_python=use_python)
    name = 'java' if not use_python else 'python'
    ex_java = None
    print ("Extracting profile: {} looking for process named: {}".format(profile, name))
    try:
        ex_java = ExtractProc(the_file=dump_file, profile=profile)
        ex_java.update_process_info(name=name, lookup_lib=(not use_python))
        return ex_java
    except:
        traceback.print_exc()
        return None

def add_host_ip_mapping(host, ip):
    HOST_TO_IP_MAPPING[host] = ip

def get_host_ip(host):
    if not host in HOST_TO_IP_MAPPING:
        ipadd = determine_host_ip_addr(host)
        if ipadd is None:
            raise Exception("Could not resolve vm name: %s"%host)
    return HOST_TO_IP_MAPPING[host]

def determine_ips_for_host_list (host_list):
    for host in host_list:
        determine_host_ip_addr(host)

def determine_host_ip_addr(client):
    ip_address = None
    try:
        ip_address = socket.gethostbyname(client)
        add_host_ip_mapping(client, ip_address)
        return ip_address
    except:
        pass
    virt_conn = open_virt_connection()
    if virt_conn:
        try:
            dom = virt_conn.lookupByName(client)
            ip_address = None
            mac_address = dom.XMLDesc(0).split("<mac address='")[1].split("'/>")[0]
            process = subprocess.Popen(['/usr/sbin/arp', '-n'], stdout=subprocess.PIPE,
                                               stderr=subprocess.STDOUT)
            process.wait()  # Wait for it to finish with the command
            for line in process.stdout.readlines():
                if line.find(mac_address) > -1:
                    #print line
                    ip_address = line.split()[0]
                    add_host_ip_mapping(client, ip_address)
                    break
        except:
            pass

    close_virt_connection(virt_conn)
    return ip_address

def ssh_to_target (hostname, username, password, retrys=5):
    client = None
    if retrys < 0:
        raise
    try:
        client = paramiko.SSHClient()
    except:
        return ssh_to_target(hostname, username, password, retrys-1)
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, username=username, password=password)
    return client

def signal_java_prof_capture(hostname, username, password, processname, hprof_fname, dst_dir ):
    client = ssh_to_target(hostname, username, password)
    res = exec_remote_quit_signal(client, processname)

    wait_for_hprof_dump_complete(client, hprof_fname)
    #if res:
    #    exec_remote_cmd(client, "cp %s %s"%(hprof_fname, dst_dir))
    try:
        client.close()
    except:
        pass
    return res

def exec_remote_quit_signal(client, processname):
    pid_lines = exec_remote_cmd(client, "pgrep %s"%processname).splitlines()
    if len(pid_lines) > 0 and len(pid_lines[0]) > 1:
        pid = pid_lines[0]
        exec_remote_cmd(client, "kill -QUIT %s"%pid)
        return True
    return False

def exec_remote_cmd (client, cmd, password='', read_out=True, block_cmd=True, wait_time=120):
    transport = client.get_transport()
    session = transport.open_session()
    session.set_combine_stderr(True)
    session.get_pty()
    lines = []
    print ("%s: Executing remote cmd: %s"%(time_str(), cmd ))
    session.exec_command(cmd)
    stdin = session.makefile('wb', -1)
    stdout = session.makefile('rb', -1)
    if cmd.find('sudo') > -1:
        #you have to check if you really need to send password here
        stdin.write(password +'\n')
        stdin.flush()
    if block_cmd and wait_time <= 0:
        session.recv_exit_status()
    elif block_cmd:
        start = datetime.now()
        period = timedelta(seconds=wait_time)
        next_time = datetime.now() + period
        while True:
            now = datetime.now()
            if session.exit_status_ready():
                break
            if now >= next_time:
                break
    if read_out:
        lines = []
        for line in stdout.read().splitlines():
            #print 'host: %s' % (line)
            lines.append(line)
    return "\n".join(lines)

def exec_grep (expression, filename):
    p = subprocess.Popen(("grep", "-e", "%s"%expression, filename), stdout = subprocess.PIPE)
    data = p.communicate()[0]
    return data

def get_gclog_entries(gclog):
    expression = "^Time:"
    p = subprocess.Popen(("grep", "-v", "%s"%expression, gclog), stdout = subprocess.PIPE)
    data = p.communicate()[0]
    return data.replace('\x00', '')

def get_introspection_entries(gclog):
    expression = "Time:"
    p = subprocess.Popen(("grep", "%s"%expression, gclog), stdout = subprocess.PIPE)
    data = p.communicate()[0]
    v = data.replace('\x00', '')
    v = v.splitlines()
    p = []
    for i in v:
        if i.find('Time:') > 0:
            k = 'Time:'+i.split('Time:')[1]
            p.append(k.strip())
        else:
            p.append(i.strip())
    data = "\n".join(v)
    return data

def clean_up_gclog(base_location):
    gclog = get_gc_log_name(base_location)
    if not os.path.exists(gclog):
        return
    open(gclog+'.orig.txt', 'w').write(open(gclog).read().replace('\x00', ''))
    open(gclog+'.txt', 'w').write(open(gclog+'.orig.txt').read())
    try:
        print ("Experiment Log: zipiing the hprof file %s"%(gclog+'.orig.txt'))
        zip_command = create_zip_cmd((gclog+'.orig.txt')+'.zip', [gclog+'.orig.txt'])
        p = subprocess.Popen(zip_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        CP_LOCK.acquire()
        COMPRESSION_PROCS.append((p, (gclog+'.orig.txt'), True))
        res = 0
    except:
        traceback.print_exc()
    finally:
        CP_LOCK.release()
    introspection_data = get_introspection_entries(gclog)
    gclog_data = get_gclog_entries(gclog)
    introspection = get_introspection_name(base_location)
    # WARNING Overwriting the original gc log!
    open(gclog, 'w').write(gclog_data)
    if len(introspection_data) > 0:
        open(introspection, 'w').write(introspection_data)
        try:
            print ("Experiment Log: zipiing the hprof file %s"%introspection)
            zip_command = create_zip_cmd(introspection+'.zip', [introspection])
            p = subprocess.Popen(zip_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            CP_LOCK.acquire()
            COMPRESSION_PROCS.append((p, introspection, True))
            # try:
            #     print p.communicate()[0]
            # except:
            #     traceback.print_exc()
            res = 0
        except:
            traceback.print_exc()
        finally:
            CP_LOCK.release()

def exec_command (cmd, shell=False):
    cmdlist = []
    if isinstance(cmd, list):
        cmdlist = cmd
        cmd = " ".join(cmd)
    elif isinstance(cmd, str):
        cmdlist = cmd.split()
    else:
        raise Exception("exec_command requires a string or list as a parameter")

    print ("%s: Executing cmd: %s"%(time_str(), cmd ))
    p = subprocess.Popen(shlex.split(cmd), shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    content = None
    try:
        content = p.communicate()[0].decode('ascii', 'replace')
        p.wait()
        content = content + p.communicate()[0].decode('ascii', 'replace')
    except:
        pass

    if content:
         content = content.replace('\x1b[0K\n', '').replace('\x1b[0K\r', '')
    return content

def exec_command_for_proc (cmd):
    cmdlist = []
    if isinstance(cmd, list):
        cmdlist = cmd
        cmd = " ".join(cmd)
    elif isinstance(cmd, str):
        cmdlist = cmd.split()
    else:
        raise Exception("exec_command requires a string or list as a parameter")
    print ("%s: Executing cmd: %s"%(time_str(), cmd ))
    p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p


def bounce_host_list(host_list):
    for host in host_list:
        bounce_function(host)

def send_enter_key(dom=None):
    virt_conn = None
    dom_set = False
    try:
        if dom is None:
            dom_set = True
            virt_conn = open_virt_connection()
            dom = virt_conn.lookupByName(client)
        dom.sendKey(0, 0, [28,], 1, 0)
        if dom_set:
            close_virt_connection(virt_conn)
        return True
    except:
        if dom_set:
            close_virt_connection(virt_conn)
        return False

def bounce_function(client):
    stop_function(client)
    time.sleep(SLEEP_SECS)
    start_function(client)
    time.sleep(SLEEP_SECS)

def close_virt_connection(virt_conn):
    if virt_conn:
        try:
            virt_conn.close()
        except:
            pass

def open_virt_connection(uri="qemu:///system"):
    try:
        return libvirt.open(uri)
    except:
        print "XXXX - Failed to open a sockt to libvirt"
        raise
        return None

def start_function (client):
    keyed = {"client":client}
    #cmd  = VIR_START.format(**keyed)
    virt_conn = open_virt_connection()
    res = -1
    if virt_conn:
        print ("%s Experiment Log: starting up %s"%(time_str(), client))
        try:
            dom = virt_conn.lookupByName(client)
            res = dom.create()
            if res == 0:
                time.sleep(1)
                # stupid bootloader menu
                dom.sendKey(0, 0, [28,], 1, 0)
        except:
            pass
    close_virt_connection(virt_conn)
    return res#exec_command(cmd)

def stop_function (client, timeout=.5):
    keyed = {"client":client}
    virt_conn = open_virt_connection()
    res = -1
    if virt_conn:
        print ("%s Experiment Log: Shutting down %s"%(time_str(), client))
        try:
            dom = virt_conn.lookupByName(client)
            res = dom.shutdown()
            #print ("%s Experiment Log: shutdown sent %s"%(time_str(), client))
            time.sleep(1)
            if dom.isActive() and res == 0:
                dom.destroy()
        except:
            print ("%s Experiment Log: unable to shutdown sent %s"%(time_str(), client))
            pass
    close_virt_connection(virt_conn)
    #cmd  = VIR_STOP.format(**keyed)
    return res#exec_command(cmd)

def start_host_list (host_list):
    for host in host_list:
        start_function(host)

def stop_host_list (host_list):
    for host in host_list:
        stop_function(host)

def snapshot_function(host):
    keyed = {"client":host}
    cmd  = VIR_SNAPSHOT.format(**keyed)
    return exec_command(cmd)

def snapshot_host_list (host_list):
    for host in host_list:
        snapshot_function(host)

def revert_function(host):
    keyed = {"client":host}
    cmd  = VIR_REVERT.format(**keyed)
    return exec_command(cmd)

def revert_host_list (host_list):
    for host in host_list:
        revert_function(host)

def get_keyblock_log(ssl_host):
    keyed = {"ssl_host":ssl_host}
    keyblock = SSLLOG_FMT_KEYBLOCK.format(**keyed)
    return os.path.join(SSLLOG_BASE, keyblock)

def get_premaster_log(ssl_host):
    keyed = {"ssl_host":ssl_host}
    premaster = SSLLOG_FMT_PREMASTER.format(**keyed)
    return os.path.join(SSLLOG_BASE, premaster)

def clear_ssl_server_logs(ssl_host):
    # just make the files empty
    kb_file = get_keyblock_log(ssl_host)
    pm_file = get_premaster_log(ssl_host)
    f = open(kb_file, 'w')
    g = open(pm_file, 'w')
    f.close()
    g.close()
    exec_command("chmod a+rw %s"%pm_file)
    exec_command("chmod a+rw %s"%kb_file)

def get_java_auth_log(base_location):
    factor_dir = base_location
    return os.path.join(factor_dir, JAVALOG_AUTH)

def clear_java_auth_log(base_location):
    # just make the files empty
    f = open(get_java_auth_log(base_location), 'w')
    f.close()

def get_factors_str(factors):
    return FACTOR_DIR_FMT.format(**factors)

def get_java_dumps_dir(base_location, use_python=False):
    if use_python:
        return os.path.join(base_location, PYTHON_DUMPS)
    return os.path.join(base_location, JAVA_DUMPS)

def get_java_stdoutlog(base_location):
    java_log_base = base_location
    return os.path.join(java_log_base, STDOUT_LOG)

def get_java_stderrlog(base_location):
    java_log_base = base_location
    return os.path.join(java_log_base, STDERR_LOG)

def get_java_hprof(base_location=None):
    if base_location is None:
        return JAVA_HPROF
    return os.path.join(base_location, JAVA_HPROF)

def get_java_hprof_results(base_location=None):
    if base_location is None:
        return JAVA_HPROF_RESULTS
    return os.path.join(base_location, JAVA_HPROF_RESULTS)

def get_java_hprof_zip(base_location=None):
    if base_location is None:
        return JAVA_HPROF+'.zip'
    return os.path.join(base_location, JAVA_HPROF+'.zip')

def get_java_stderrlog(base_location):
    java_log_base = base_location
    return os.path.join(java_log_base, JAVALOG_STDERR)

def create_factor_dir (base_location, use_python=False):
    try:
        os.makedirs(base_location)
        os.makedirs(get_java_dumps_dir(base_location, use_python))
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def get_python_dump_location(base_location):
    java_base = base_location
    return os.path.join(java_base, PYTHON_DUMP_FINAL)

def get_python_dump_zip_location(base_location):
    java_base = base_location
    return os.path.join(java_base, PYTHON_DUMP_FINAL_ZIP)

def get_java_dump_location(base_location, use_python=False):
    java_base = base_location
    if use_python:
        return get_python_dump_location(base_location)
    return os.path.join(java_base, JAVA_DUMP_FINAL)

def get_java_dump_zip_location(base_location, use_python=False):
    java_base = base_location
    if use_python:
        return get_python_dump_zip_location(base_location)
    return os.path.join(java_base, JAVA_DUMP_FINAL_ZIP)

def get_vol_java_dump_location(base_location):
    java_base = base_location
    return "file:///" + os.path.join(java_base, JAVA_DUMP_FINAL)

def create_tls_client_java_cmd (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False, perform_profiling=False):
    keys = dict([(k,v) for k,v in factors.items()])
    keys['ssl_host'] = ssl_host
    keys['log_fname'] = get_java_auth_log(base_location)
    keys['destroy_passwords'] = 1 if destroy_passwords else 0
    keys['jvm_mem'] = jvm_mem
    keys['run_time'] = MAX_EXP_TIME
    keys['gc_start_time'] = GC_START_TIME
    keys['gc_spacing_time'] = GC_SPACING_TIME
    java_client_args = get_jar_cmd(base_location, jvm_mem=jvm_mem, is_streaming=is_streaming, perform_profiling=perform_profiling)
    return java_client_args.format(**keys)

def create_tls_client_python_cmd (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False):
    keys = dict([(k,v) for k,v in factors.items()])
    keys['ssl_host'] = ssl_host
    keys['log_fname'] = get_java_auth_log(base_location)
    keys['destroy_passwords'] = 1 if destroy_passwords else 0
    python_client_args = get_python_cmd(jvm_mem=jvm_mem, is_streaming=is_streaming)
    return python_client_args.format(**keys)

def get_tls_client_java_cmd (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False, perform_profiling=False):
    cmd = create_tls_client_java_cmd(java_host, ssl_host, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming, perform_profiling=perform_profiling)
    return cmd

def get_tls_client_python_cmd (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False):
    cmd = create_tls_client_python_cmd(java_host, ssl_host, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming)
    return cmd

def suspend_host(java_host):
    keyed = {'client':java_host}
    cmd = VIR_SUSPEND.format(**keyed)
    print ("%s: Executing VIRSH cmd: %s"%(time_str(), cmd ))
    return exec_command(cmd)

def is_host_suspended(java_host):
    keyed = {'client':java_host}
    cmd = VIR_DOMINFO.format(**keyed)
    print ("%s: Executing VIRSH cmd: %s"%(time_str(), cmd ))
    data = exec_command(cmd)
    p = data.splitlines()
    for i in p:
        if i.find("State:") > -1:
            return i.find('suspended') > 0
    return False

def is_host_running(java_host):
    keyed = {'client':java_host}
    cmd = VIR_DOMINFO.format(**keyed)
    print ("%s: Executing VIRSH cmd: %s"%(time_str(), cmd ))
    data = exec_command(cmd)
    p = data.splitlines()
    for i in p:
        if i.find("State:") > -1:
            return i.find('running') > 0
    return False

def resume_host(java_host):
    keyed = {'client':java_host}
    cmd = VIR_RESUME.format(**keyed)
    print ("%s: Executing VIRSH cmd: %s"%(time_str(), cmd ))
    return exec_command(cmd)

def dump_memory_function (client, dumploc):
    keyed = {"client":client, "dumploc":dumploc}
    cmd  = VIR_DUMP.format(**keyed)
    print ("%s: Executing VIRSH cmd: %s"%(time_str(), cmd ))
    return exec_command(cmd)

def test_connection (client, base_location):
    chan = client.get_transport().open_session()
    chan.exec_command("ls -all %s"%base_location)
    res = False
    while True:
        rl, wl, xl = select.select([chan],[],[],0.0)
        if len(rl) > 0 and chan.exit_status_ready():
            tmp = chan.recv(1024)
            if len(tmp) == 0:
                break
            elif tmp.find("cannot access") > 0:
                break
            else:
                res = True
                break
        time.sleep(0.500)
    chan.close()
    return res

def perform_rm_memory_dump(base_location):
    java_dump = get_java_dump_location (base_location)
    rm_cmd = RM_FILE_CMD.format(**{"the_file":java_dump})
    exec_command(rm_cmd)

def perform_rm_introspection_data(base_location):
    introspection = get_introspection_name (base_location)
    try:
        os.stat(introspection)
        rm_cmd = "rm %s"%introspection
        exec_command(rm_cmd)
    except:
        pass

def perform_memory_dump(java_host, ssl_host, base_location, username=EXPSSL_USER, password=EXPSSL_PASS, use_python=False):
    res = 1
    print ("Experiment Log: Dumping memory")
    dump_file = get_java_dump_location(base_location, use_python)
    try:
        start_dump()
        dump_memory_function (java_host, dump_file)
        print ("Experiment Log: updating file permisions using sudo")
        chmod_cmd = CHMOD_DUMP%(dump_file)
        exec_command(chmod_cmd)
        perform_mem_dump_compression(base_location, use_python)
        res = 0
    except:
        print ("Experiment Log: Unable to dump memory for %s:%s"%(java_host, dump_file))
        traceback.print_exc()
        raise
    finally:
        complete_dump()
    return res
    #print ("Experiment Log: updating file permisions %s with sudo user %s"%(ssl_host, username))
    #java_ip = get_host_ip(java_host)
    #ssl_ip = get_host_ip(ssl_host)
    #client = ssh_to_target(ssl_ip, username=username, password=password)
    #chmod_cmd = CHMOD_DUMP%(dump_file)
    #exec_remote_cmd(client, chmod_cmd, password)
    #try:
    #    client.close()
    #except:
    #    pass


def test_host_is_up(vmhost, username, password):
    num_retrys = 0
    res = False
    host_ip = get_host_ip(vmhost)
    client = None
    while num_retrys < MAX_RETRYS:
        try:
            print("Testing %s(%s) is up, retrys=%d"%(vmhost, host_ip, num_retrys))
            client = ssh_to_target(host_ip, username, password)
            data = exec_remote_cmd(client, "ls -all %s"%JAVA_JBGREP_JAR_FILE, read_out=True)
            if len(data) > 0:
                print("Host %s(%s) is up"%(vmhost, host_ip))
                res = True
                break
        except socket_error as serr:
            bounce_function(vmhost)
            time.sleep(60)
            client = None
        except:
            try:
                client.close()
            except:
                pass
            client = None
        num_retrys+=1
        time.sleep(MAX_RETRY_SLEEP)
    try:
        if client:
            client.close()
    except:
        pass
    if not res:
        bounce_function(vmhost)
        time.sleep(60)
    return res

def wait_till_accessible(client, retrys=4):
    bad = 'ls: cannot access'

    while retrys > 0:
        data = exec_remote_cmd(client, "ls /srv/nfs/cortana/logs/cmd/exp_code")
        if data.find(bad) == -1:
            return True
        time.sleep(SLEEP_SECS*2)
        retrys = retrys -1
    return False

def get_factors_data(base_location):
    i = eval(open(get_factors_filename(base_location)).read())
    return i

def get_factors_filename(base_location):
    return os.path.join(base_location, FACTORS_FILE)

def execute_java_cmd (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False, use_64=False, use_python=True, perform_profiling=False):
    java_ip = get_host_ip(java_host)
    ssl_ip = get_host_ip(ssl_host)
    open(get_factors_filename(base_location), 'w').write("%s"%factors)
    cmd = None
    if not use_python:
        cmd = get_tls_client_java_cmd (java_ip, ssl_ip, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming, perform_profiling=perform_profiling)
    else:
        cmd = get_tls_client_python_cmd (java_ip, ssl_ip, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming)
    stdout = open(get_java_stdoutlog(base_location), 'w')
    stderr = open(get_java_stderrlog(base_location), 'w')
    mesg = None
    if not use_python:
        mesg = ("# SSL| %s(%s) Java| %s(%s) Command| %s\n"%(ssl_host, ssl_ip, java_host, java_ip, cmd))
    else:
        mesg = ("# SSL| %s(%s) Python| %s(%s) Command| %s\n"%(ssl_host, ssl_ip, java_host, java_ip, cmd))
    stdout.write(mesg)
    client = ssh_to_target(java_ip, username=JAVA_USER, password=JAVA_PASS)
    if perform_profiling:
        hprof_fname = get_java_hprof()
        data = exec_remote_cmd(client, "rm %s*"%(hprof_fname))
    print ("Experiment Log: connected to the %s(%s) and excuted test command"%(java_host, java_ip))
    if not wait_till_accessible(client):
        print ("XXXX Error Unable to mount NFS directory")
        return 1
    chan = client.get_transport().open_session()
    print (mesg)
    chan.set_combine_stderr(True)
    chan.exec_command(cmd)
    time.sleep(SLEEP_SECS)
    data = ''
    res = 0
    take_snapshot = False
    start = datetime.now()
    period = timedelta(seconds=MAX_EXP_TIME)
    next_time = datetime.now() + period
    now = datetime.now()
    target_string = "-NumRequest:%d\n"%MAX_NUM_REQUESTS
    print ("Performing the experiment: Waiting for %d second or %s target string"%(MAX_EXP_TIME, target_string))
    print ("Waiting a few minute for the experiment to start")
    time.sleep(60)
    while True:
        res_ready = chan.exit_status_ready()
        if res_ready:
            res = res_ready
            break
        rl, wl, xl = select.select([chan],[],[],0.0)
        now = datetime.now()
        if now > next_time:
            take_snapshot = True
        if len(rl) > 0:
            tmp = chan.recv(1024)
            if len(tmp) == 0:
                continue
            if len(tmp) > 0:
                stdout.write(tmp)
                data = data + tmp
        elif res_ready:
            res = res_ready
            break

        if take_snapshot or \
           data.find(target_string) > -1 or \
           data.find("====DONE====") > -1 or \
           False: #data.find("-gc_paused_all_experiments:01-next_gc_time:00000000") > -1:
            time.sleep(1.5)
            res = 0
            break

        if len(data) > 168192:
           data = data[-(168192/2):]
    #print data, "completed run"

    # this may not be sufficient because the host may not complete dumping profile information in time
    if perform_profiling:
        hprof_fname = JAVA_HPROF
        dst_dir = os.path.join(base_location, hprof_fname)
        # assumes nfs mount
        signal_java_prof_capture(java_host, JAVA_USER, JAVA_PASS, "java", hprof_fname, dst_dir )

    rl, wl, xl = select.select([chan],[],[],0.0)
    while len(rl) > 0:
        try:
            tmp = chan.recv(10000)
            stdout.write(tmp)
            rl, wl, xl = select.select([chan],[],[],0.0)
            break
        except:
            break

    stdout.close()
    suspend_host(java_host)
    start_ts = calendar.timegm(start.timetuple()) * 1000
    act_ts = calendar.timegm(now.timetuple()) * 1000
    factors['age'] = (act_ts - start_ts)/1000.0
    factors['max_age'] = (act_ts - start_ts)/1000.0
    open(get_factors_filename(base_location), 'w').write("%s"%factors)
    write_start_end_times(base_location, start, next_time, now)
    try:
        client.close()
    except:
        #pass
        traceback.print_exc()
    return res

def get_time_log(base_location):
    return os.path.join(base_location, TIME_LOG)

def get_ready_file(base_location):
    return os.path.join(base_location, READY_FILE)

def write_start_end_times(base_location, start, end, actual):
    output = get_time_log(base_location)
    start_ts = calendar.timegm(start.timetuple()) * 1000
    end_ts = calendar.timegm(end.timetuple()) * 1000
    act_ts = calendar.timegm(actual.timetuple()) * 1000
    open(output, 'w').write("%08x,%08x,%08x"%(start_ts, end_ts, act_ts))

def write_ready_signal(base_location):
    try:
        csock = socket.socket()
        csock.connect((SIGNAL_HOST, SIGNAL_PORT))
        csock.send(base_location)
    except socket_error:
        pass

    open(get_ready_file(base_location), 'w').write("ready process me")

def get_auth_log_line_count(base_location):
    log = os.path.join(base_location, AUTH_LOG)
    try:
        os.stat(log)
        return len(open(log).readlines())
    except:
        return 0

def get_stdout_log_line_count(base_location):
    log = os.path.join(base_location, STDOUT_LOG)
    try:
        os.stat(log)
        return len(open(log).readlines())
    except:
        return 0

def generate_jbgrep_merged_info(base_location):
    kbinfos = parse_merge_secret_info(base_location)
    generate_jbgrep_file(base_location, kbinfos)
    generate_keyinfo_file(base_location, kbinfos)

def parse_merge_secret_info(base_location):
    pminfos = parse_premaster_location(base_location)
    kbinfos = parse_keyblock_location(base_location)
    msinfos = {}
    for random_bytes,_v in pminfos.items():
        premaster_secret = pminfos[random_bytes]['pms']
        master_secret = kbinfos[random_bytes]['ms'] if random_bytes in kbinfos else ''
        if master_secret == '':
            continue
        msinfos[master_secret] = {}
        msinfos[master_secret].update(kbinfos[random_bytes])
        msinfos[master_secret]['pms'] = premaster_secret
    return msinfos

def parse_premaster_location(base_location):
    filename = os.path.join(base_location, SSLLOG_PREMASTER)
    pminfos = {}
    try:
        statinfo = os.stat(filename)
    except:
        return pminfos
    lines = [i.strip() for i in open(filename).read().splitlines() if len(i.strip()) > 0]

    for line in lines:
        pminfo = parse_premaster_line(line)
        if pminfo is None:
            continue
        key = pminfo['client_random'] + pminfo['server_random']
        if pminfo:
            pminfos[key] = pminfo
    return pminfos

def parse_keyblock_location(base_location):
    kbinfos = {}
    filename = os.path.join(base_location, SSLLOG_KEYBLOCK)
    try:
        statinfo = os.stat(filename)
    except:
        return kbinfos
    lines = [i.strip() for i in open(filename).read().splitlines() if len(i.strip()) > 0]
    for line in lines:
        kbinfo = parse_keyblock_line(line)
        if kbinfo is None:
            continue
        key = kbinfo['client_random'] + kbinfo['server_random']
        if kbinfo:
            kbinfos[key] = kbinfo
    return kbinfos

def try_long_none(string, base):
    try:
        return long(string, base)
    except:
        return None

def parse_premaster_line(line):
    pminfo = line.split(',')
    if len(pminfo) != 6:
        return None
    t = try_long_none(pminfo[0], 16)
    res = {"time":t,'ms':pminfo[2], 'pms':pminfo[3], "server_random":pminfo[4], "client_random":pminfo[5]}
    if res['time'] is None:
        return None
    return res

def parse_keyblock_line(line):
    kbinfo = line.split(',')
    if len(kbinfo) != 8:
        return None
    return {"time":long(kbinfo[0], 16)& 0xffffffff,'cwkey':kbinfo[2], 'swkey':kbinfo[3],
        "mkeyblock":kbinfo[4], "server_random":kbinfo[5], "client_random":kbinfo[6], 'ms':kbinfo[7]}

def get_compromised_keys(base_location):
    return os.path.join(base_location, COMPROMISED_KEYS)

def get_compromised_post_data(base_location):
    return os.path.join(base_location, COMPROMISED_POST_DATA)

def get_java_heap_key_locations(base_location):
    return os.path.join(base_location, JAVA_HEAP_KEY_LOCATIONS)

def get_java_heap_key_locations_summary(base_location):
    return os.path.join(base_location, JAVA_HEAP_KEY_LOCATIONS_SUMMARY)

def get_compromised_sessions(base_location):
    return os.path.join(base_location, COMPROMISED_SESSIONS)

def get_compromised_passwords(base_location):
    return os.path.join(base_location, COMPROMISED_PASSWORDS)

def get_compressed_auth_data(base_location):
    return os.path.join(base_location, AUTH_DATA) + ".zip"

def get_auth_data_name(base_location):
    return os.path.join(base_location, AUTH_DATA)

def get_pct_file(base_location):
    return os.path.join(base_location, PCT_DATA)

def get_keyinfo_name(base_location):
    return os.path.join(base_location, MERGED_KEYINFO)

def get_jbgrep_name(base_location):
    return os.path.join(base_location, JBGREP_FILE)

def get_jbgrep_wkey_name(base_location):
    return os.path.join(base_location, JBGREP_WKEY_FILE)

def get_jbgrep_grep_res_name(base_location):
    return os.path.join(base_location, JBGREP_GREP_RES_FILE)

def get_jbgrep_fname_res_name(base_location):
    return os.path.join(base_location, JBGREP_FNAME_RES_FILE)

def get_jbgrep_count_res_name(base_location):
    return os.path.join(base_location, JBGREP_COUNT_RES_FILE)

def get_jbgrep_wkey_grep_res_name(base_location):
    return os.path.join(base_location, JBGREP_WKEY_COUNT_RES_FILE)

def get_jbgrep_wkey_fname_res_name(base_location):
    return os.path.join(base_location, JBGREP_WKEY_FNAME_RES_FILE)

def get_jbgrep_wkey_count_res_name(base_location):
    return os.path.join(base_location, JBGREP_WKEY_COUNT_RES_FILE)

def has_log_lines(base_location):
    auth_line = ope

def generate_jbgrep_file(base_location, merged_keyinfo):
    km_result_lines = []
    wkey_result_lines = []
    for ms, keyinfo in merged_keyinfo.items():
        km_result_lines.append(ms)
        if 'pms' in keyinfo:
            km_result_lines.append(keyinfo['pms'])
        if 'cwkey' in keyinfo:
            wkey_result_lines.append(keyinfo['cwkey'])
        if 'swkey' in keyinfo:
            wkey_result_lines.append(keyinfo['swkey'])
        if 'mkeyblock' in keyinfo:
            km_result_lines.append(keyinfo['mkeyblock'])

    open(get_jbgrep_name(base_location), 'w').write("\n".join(km_result_lines))
    open(get_jbgrep_wkey_name(base_location), 'w').write("\n".join(wkey_result_lines))

def generate_keyinfo_file(base_location, merged_keyinfo):
    fmt_keys = "time:{time}-mkeyblock:{mkeyblock}-ms:{ms}-pms:{pms}-cwkey:{cwkey}-swkey:{swkey}-crandom:{cr}-srandom:{sr}"

    result_lines = []
    for ms, keyinfo in merged_keyinfo.items():
        keyed = {'ms':ms, 'pms':keyinfo.get('pms',''), 'mkeyblock':keyinfo.get('mkeyblock',''),
                'cwkey':keyinfo.get('cwkey',''), 'swkey':keyinfo.get('swkey',''),
                'cr':keyinfo.get('client_random',''), 'sr':keyinfo.get('server_random',''),
                'time':"%08x"%keyinfo['time']}
        result_lines.append(fmt_keys.format(**keyed))
    open(get_keyinfo_name(base_location), 'w').write("\n".join(result_lines))

def copy_ssllogs_to_factors(ssl_host, base_location):
    factor_dir = base_location
    keyed = {"ssl_host":ssl_host}
    keyblock_log = os.path.join(factor_dir, SSLLOG_KEYBLOCK)
    premaster_log = os.path.join(factor_dir, SSLLOG_PREMASTER)
    data = open(get_keyblock_log(ssl_host)).read()
    open(keyblock_log, 'w').write(data)
    print ("Experiment Log: copied %d lines from %s to %s directory"%( len(data.splitlines()), get_keyblock_log(ssl_host), keyblock_log) )
    data = open(get_premaster_log(ssl_host)).read()
    open(premaster_log, 'w').write(data)
    print ("Experiment Log: copied %d lines from %s to %s factors directory"%( len(data.splitlines()),get_premaster_log(ssl_host), premaster_log ))
    clear_ssl_server_logs(ssl_host)
    generate_jbgrep_merged_info(base_location)


def execute_command_steps (java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False, use_64=False, use_python=False, perform_profiling=False):
    global AVAILABLE_EXP_HOSTS, AVAILABLE_SSL, SLEEP_SECS, ACTIVE_PROC_EXTRACTION_THREADS, HOST_USED, HOSTS_READY_TO_DUMP
    sleepy = SLEEP_SECS*3
    # create a factor directory
    create_factor_dir(base_location, use_python)
    # check and remove ssl_target logs
    clear_ssl_server_logs(ssl_host)
    # check and remove java_host logs
    clear_java_auth_log(base_location)
    # execute the java program
    print ("Experiment Log: Created results dir and cleared logs")
    res = 1
    cmd_success = test_host_is_up(java_host, JAVA_USER, JAVA_PASS)
    if not cmd_success:
        #AVAILABLE_SSL.add(ssl_host)
        #bounce_function(java_host)
        #time.sleep(sleepy)
        # program execution complete add java_host and ssl_host back to pools
        #VAILABLE_JAVA.add(java_host)
        #add_ssl_host_back(ssl_host)
        #add_java_host_back(java_host)
        add_host_for_dumping(base_location, java_host, ssl_host, factors, use_64, use_python, False)
        raise Exception ("Experiment Log: failed to connect to the %s"%(java_host))
    cmd_success = test_host_is_up(ssl_host, EXPSSL_USER, EXPSSL_PASS)
    if not cmd_success:
        #add_ssl_host_back(ssl_host)
        #add_java_host_back(java_host)
        #AVAILABLE_SSL.add(java_host)
        add_host_for_dumping(base_location, java_host, ssl_host, factors, use_64, use_python, False)
        raise Exception ("Experiment Log: failed to connect to the %s(%s)"%(ssl_host))
    try:
        res = execute_java_cmd(java_host, ssl_host, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming, use_64=use_64, use_python=use_python, perform_profiling=perform_profiling)
    except:
        traceback.print_exc()
        #reapply_work_unit(base_location, factors, reason="Exception occurred during execute java command")
        #pass
        raise
    add_host_for_dumping(base_location, java_host, ssl_host, factors, use_64, use_python, perform_profiling)
    #if res == 0:
    #    if get_auth_log_line_count(base_location) < MIN_AUTH_COUNT or\
    #       get_stdout_log_line_count(base_location) < MIN_LOG_COUNT  or\
    #       get_java_process_with_base_location(base_location, use_64=use_64) is None:
    #        print ("Experiment Log: Unable to find the java process")
    #        reason = "no java process found."
    #        if get_stdout_log_line_count(base_location) < MIN_LOG_COUNT:
    #            reason = "not enough logs in stdout, maybe it failed?"
    #        elif get_auth_log_line_count(base_location) < MIN_AUTH_COUNT:
    #            reason = "not enough logs in authlog, maybe it failed?"
    #        reapply_work_unit(base_location, factors, reason=reason)
    #        res = 1
    #    else:
    #        print ("Experiment Log: Successfully dumped memory, processing the rest")
    #        # copy ssllogs
    #        copy_ssllogs_to_factors(ssl_host, base_location)

    #bounce_function(java_host)
    #print ("Experiment Log: rebooting the host and sleeping for %ds for %s and %s"%(sleepy, java_host, ssl_host))
    #time.sleep(sleepy)
    # program execution complete add java_host and ssl_host back to pools
    print ("Experiment Log: completed command executions for %s and %s, waiting for %d host before performing dumps"%(java_host, ssl_host, HOST_USED - len(HOSTS_READY_TO_DUMP)))
    #if res == 0:
    #    print ("Experiment Log: signaling %s(%d) to start processing %s"%(SIGNAL_HOST, SIGNAL_PORT, base_location))
    #    write_ready_signal(base_location)
    # If running machines in a "hot mode" sleeping is not necessary
    #time.sleep(sleepy)
    #add_ssl_host_back(ssl_host)
    #add_java_host_back(java_host)
    #AVAILABLE_EXP_HOSTS.add(java_host)
    #AVAILABLE_SSL.add(ssl_host)



def init_available(java_list, ssl_list=None, max_used=None):
    global HOST_USED_LOCK, AVAILABLE_EXP_HOSTS, HOST_USED, MAX_HOST_USED, AVAILABLE_SSL
    if java_list:
        AVAILABLE_EXP_HOSTS = list()
        available = set()
        for h in java_list:
            if (test_host_is_up(h, JAVA_USER, JAVA_PASS)):
                available.add(h)#AVAILABLE_EXP_HOSTS.add(h)
        AVAILABLE_EXP_HOSTS = [i for i in available]
        if max_used is None:
             max_used = 2 * (len(AVAILABLE_EXP_HOSTS) / 3)
        elif max_used <= 0 or max_used > len(AVAILABLE_EXP_HOSTS):
             max_used = 2 * (len(AVAILABLE_EXP_HOSTS) / 3)
        MAX_HOST_USED = max_used

    if ssl_list:
        AVAILABLE_SSL = list()
        available = set()
        for h in ssl_list:
            if (test_host_is_up(h, EXPSSL_USER, EXPSSL_PASS)):
                available.add(h)
        AVAILABLE_SSL = [i for i in available]
    return AVAILABLE_EXP_HOSTS, AVAILABLE_SSL

def cull_threads (active_threads):
    cnt = 0
    # lock acquire
    orig_len = len(active_threads)
    while cnt < len(active_threads):
        if active_threads[cnt][0].is_alive():
            cnt += 1
            continue
        active_threads.pop(cnt)
    return len(active_threads) != orig_len

def convert_enum (val):
    return FACTORS[val]

def calculate_concurrent_sessions(factor):
    return int(MAX_CONCURRENT_SESSIONS*FACTOR_VALUE[factor])

def get_factors_dict_str(mem_press, sess_lifetime, conc_session, req_factor):
    return {"mem_pressure_factor":mem_press,
            "sess_lifetime_factor":sess_lifetime,
            "conc_session":conc_session,
            "request_factor":req_factor}

def get_factors_dict(mem_press, sess_lifetime, conc_session, req_factor):
    return {"mem_pressure_factor":convert_enum(mem_press),
            "sess_lifetime_factor":convert_enum(sess_lifetime),
            "conc_session":convert_enum(conc_session),
            "request_factor":convert_enum(req_factor)}

def generate_core_work_list():
    return generate_work_list (CORE_FACTORS_MIN, CORE_FACTORS_MAX+1)

def generate_work_list(min_=FACTORS_MIN, max_=FACTORS_MAX):
    work_list = []
    for mem_press in xrange(min_, max_):
        for sess_lifetime in xrange(min_, max_):
            for conc_session in xrange(min_, max_):
                for req_factor in xrange(min_, max_):
                    values = get_factors_dict(mem_press, sess_lifetime, conc_session, req_factor)
                    work_list.append(values)
    return work_list

def get_results_base(results_base, is_streaming=False, is_mod_vm=False):
    join_ = "streaming" if is_streaming else "transaction"
    join_ = "%s_mod_vm"%join_ if is_mod_vm else join_
    return os.path.join(results_base, join_)

def set_multiple_runs_working_list(factors_list, num_iterations, results_base):
    global WORKING_LIST
    #WORKING_LIST = generate_multiple_runs_factor_list(factors_list, num_iterations, results_base)
    WORKING_LIST = generate_multiple_runs_factor_list2(factors_list, num_iterations, results_base)

def generate_multiple_runs_factor_list (factor_list, num_runs, results_base):
    wlist = []
    for factors in factor_list:
        wlist += [ (i, factors) for i in generate_multiple_runs_same_factor(factors, num_runs, results_base) ]
    wlist.sort()
    return wlist

def generate_multiple_runs_factor_list2(factor_list, num_runs, results_base):
    wlist = []
    base_fmt = "%s_%03d"
    for factors in factor_list:
        factors_str = get_factors_str(factors)
        for i in xrange(0, num_runs):
            base_location = os.path.join(results_base, base_fmt%(factors_str, i))
            wlist.append((base_location, factors))
    return wlist

def generate_multiple_runs_same_factor(factors, num_runs, base_results):
    wlist = []
    base_fmt = "%s_%03d"
    factors_str = get_factors_str(factors)
    for i in xrange(0, num_runs):
        wlist.append(os.path.join(base_results, base_fmt%(factors_str, i)))
    return wlist


GC_INTROSPECTION = "gc_introspection.txt"
GC_LOG = "gc_log.txt"
NUM_ITERATIONS = 100
JAVA_SCLIENT_JAR_FILE = "java-gc-experiments-1.0.jar"
JAVA_TCLIENT_JAR_FILE = "java-gc-experiments-1.0.jar"
USE_GC_VARIANT = False
def set_use_gc_variant(s=True):
    global USE_GC_VARIANT
    USE_GC_VARIANT = s
    return USE_GC_VARIANT

JAVA_GC_VARIANT_JAR_FILE = "java-gc-experiments-10min-gc-variant-1.0.jar"
JAVA_CLIENT_JAR_CMD = None
JAVA_CLIENT_EXP_ARGS = None

PYTHON_TCLIENT_FILE = "main_exp.py"
PYTHON_BASE = "/srv/nfs/cortana/logs/cmd/"


JAVA_WORK_HOST = None
SSL_WORK_HOST = None

JAVA_CORE = "JavaClientExperiments {jvm_mem} {run_time} " +\
                     "{gc_start_time} " +\
                     "{gc_spacing_time} {ssl_host} {log_fname} "+\
                     "{mem_pressure_factor} {sess_lifetime_factor} "+\
                     "{conc_session} {request_factor} {destroy_passwords}"

JAVA_BOUNCY_CASTLE = "JavaClientExperiments-BC {jvm_mem} {run_time} " +\
                     "{gc_start_time} " +\
                     "{gc_spacing_time} {ssl_host} {log_fname} "+\
                     "{mem_pressure_factor} {sess_lifetime_factor} "+\
                     "{conc_session} {request_factor} {destroy_passwords}"

JAVA_SSL_SOCKETS = "JavaClientExperiments-Socket {jvm_mem} {run_time} " +\
                     "{gc_start_time} " +\
                     "{gc_spacing_time} {ssl_host} {log_fname} "+\
                     "{mem_pressure_factor} {sess_lifetime_factor} "+\
                     "{conc_session} {request_factor} {destroy_passwords}"

JAVA_SSL_SOCKETS_NULL = "JavaClientExperiments-Socket-Null {jvm_mem} {run_time} " +\
                     "{gc_start_time} " +\
                     "{gc_spacing_time} {ssl_host} {log_fname} "+\
                     "{mem_pressure_factor} {sess_lifetime_factor} "+\
                     "{conc_session} {request_factor} {destroy_passwords}"
JAVA_CORE = JAVA_CORE

USING_BOUNCY_CASTLE=False
USING_SSL_SOCKET=False
USING_SSL_SOCKET_NULL=False

USING_SERIAL_GC = False
USING_CMS = False
USING_G1GC = False
USING_NMT = True
USING_GC_LOGGING = True
def get_python_cmd(jvm_mem=512, is_streaming=False):
    py_file = PYTHON_TCLIENT_FILE
    cmd_args = ["python", ]
    py_cmd = os.path.join(PYTHON_BASE, py_file)
    return " ".join(cmd_args + [py_cmd,
                           "{ssl_host} {log_fname} "+\
                           "{mem_pressure_factor} {sess_lifetime_factor} "+\
                           "{conc_session} {request_factor} {destroy_passwords}"])

def get_jar_cmd(base_location, jvm_mem=512, is_streaming=False, perform_profiling=False):
    global USE_GC_VARIANT, JAVA_TCLIENT_JAR_FILE, JAVA_GC_VARIANT_JAR_FILE, USING_BOUNCY_CASTLE
    jar_file = JAVA_TCLIENT_JAR_FILE if not USE_GC_VARIANT else JAVA_GC_VARIANT_JAR_FILE
    #jar_file = JAVA_TCLIENT_JAR_FILE if not is_streaming else JAVA_SCLIENT_JAR_FILE
    cmd_args = get_jvm_cmd(base_location, jvm_mem, perform_profiling)
    jar_cmd = os.path.join(JAR_BASE, jar_file)
    jar_args = JAVA_CORE
    if USING_BOUNCY_CASTLE:
        jar_args = JAVA_BOUNCY_CASTLE
    elif USING_SSL_SOCKET:
        jar_args = JAVA_SSL_SOCKETS
    elif USING_SSL_SOCKET_NULL:
        jar_args = JAVA_SSL_SOCKETS_NULL

    return " ".join(cmd_args + [jar_cmd, jar_args] )

def get_gc_log_name(base_location):
    return os.path.join(base_location, GC_LOG)

def get_introspection_name(base_location):
    return os.path.join(base_location, GC_INTROSPECTION)

def get_jvm_cmd(base_location, memory_used=512, perform_profiling=False):
    global JVM_SURVIVOR_RATIO, JVM_USE_SERIAL, JVM_NEW_MEMORY_USED
    JVM_MEMORY_USED = memory_used
    #JVM_MEMORY_USED = 512
    new_memory_used = JVM_NEW_MEMORY_USED
    if JVM_NEW_MEMORY_USED is None:
        new_memory_used = memory_used/2

    survivor_ratio = JVM_SURVIVOR_RATIO
    if JVM_SURVIVOR_RATIO is None:
        survivor_ratio = (memory_used-2*new_memory_used)/2

    MEM_KEYS = {"jvm_mem":memory_used}
    NEW_MEM_KEYS = {"jvm_mem":new_memory_used}
    SURV_FACTOR = {"survivor_ratio":survivor_ratio}
    profiler_agent = '-agentlib:hprof=heap=all'
    heap_profile_opt= {'heap_profile_opt': "" if not perform_profiling else profiler_agent}

    gc_type = "-XX:+UseSerialGC"
    if USING_G1GC:
        gc_type = "-XX:+UseG1GC"

    gc_logging = ''
    if USING_GC_LOGGING:
        gc_logging = "-XX:+PrintAdaptiveSizePolicy -XX:+PrintGC " +\
              "-XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime "+\
              "-XX:+PrintGCDateStamps -XX:+PrintGCTaskTimeStamps -XX:+PrintGCTimeStamps "+\
              "-Xloggc:%s"%get_gc_log_name(base_location)

    nmt_track = ''
    if USING_NMT:
        nmt_track = "-XX:NativeMemoryTracking=summary"

    cmd_args = ["java", "{heap_profile_opt}".format(**heap_profile_opt),
        "-Xnoclassgc",
        "-XX:-CMSClassUnloadingEnabled",
        #"-XX:+UseLargePages",
        nmt_track,
        gc_logging,
        gc_type,
        "-XX:+AlwaysPreTouch",
        "-XX:InitialHeapSize={jvm_mem}M".format(**MEM_KEYS),
        "-XX:MaxHeapSize={jvm_mem}M".format(**MEM_KEYS),
        "-XX:NewSize={jvm_mem}M".format(**NEW_MEM_KEYS),
        "-XX:MaxNewSize={jvm_mem}M".format(**NEW_MEM_KEYS),
        "-XX:InitialSurvivorRatio={survivor_ratio}".format(**SURV_FACTOR),
        "-jar",]
    return cmd_args

def get_vol_profile(use_64=False, use_python=False):
    print ("Using x64 version of profile: %d"%int(use_64))
    if use_python:
        return VOL_PY_HOST_PROFILE
    elif use_64:
        return VOL_HOST_PROFILE_x64
    return VOL_HOST_PROFILE_x86

def set_execing_vms(running=True):
    global EXECING_VMS
    EXECING_VMS = running

def is_safe_to_dump():
    global HOSTS_READY_TO_DUMP
    return len(HOSTS_READY_TO_DUMP) == HOST_USED

def set_perform_dumps(doit=False):
    global JAVA_PERFORM_DUMPS
    JAVA_PERFORM_DUMPS = doit

def set_dumping_memory(doit=False):
    global JAVA_DUMPING_MEMORY
    JAVA_DUMPING_MEMORY = doit

def is_dumping_memory():
    global JAVA_DUMPING_MEMORY
    return JAVA_DUMPING_MEMORY

def handle_dumping_java_host():
    global JAVA_PERFORM_DUMPS
    JAVA_PERFORM_DUMPS = True
    procs = []
    while JAVA_PERFORM_DUMPS:
        for p in procs:
            p.join()
        procs = []
        while not is_safe_to_dump():
            time.sleep(1.0)
        set_dumping_memory(True)
        while len(HOSTS_READY_TO_DUMP) > 0:
            base_location, java_host, ssl_host, factors, use_64, use_python, perform_profiling  = HOSTS_READY_TO_DUMP.pop(0)
            #remote_reboot(ssl_host, EXPSSL_USER, EXPSSL_PASS)
            res = perform_java_dump(java_host, ssl_host, base_location, factors, use_64, use_python, perform_profiling)
            print ("Experiment Log: completed command executions for %s and %s"%(java_host, ssl_host))
            if res == 0:
                print ("Experiment Log: signaling %s(%d) to start processing %s"%(SIGNAL_HOST, SIGNAL_PORT, base_location))
                write_ready_signal(base_location)
            p = threading.Thread(target=release_hosts, args=(java_host, ssl_host))
            p.start()
            procs.append(p)
        set_dumping_memory(False)

    while len(HOSTS_READY_TO_DUMP) > 0:
        for p in procs:
            p.join()
        procs = []
        while not is_safe_to_dump():
            time.sleep(1.0)
        set_dumping_memory(True)
        while len(HOSTS_READY_TO_DUMP) > 0:
            base_location, java_host, ssl_host, factors, use_64, use_python, perform_profiling = HOSTS_READY_TO_DUMP.pop(0)
            try:
                #remote_reboot(ssl_host, EXPSSL_USER, EXPSSL_PASS)
                res = perform_java_dump(java_host, ssl_host, base_location, factors, use_64, use_python, perform_profiling)
            except:
                print "unable to dump %s"%base_location
                p = threading.Thread(target=release_hosts, args=(java_host, ssl_host))
                p.start()
                procs.append(p)

            print ("Experiment Log: completed command executions for %s and %s"%(java_host, ssl_host))
            if res == 0:
                print ("Experiment Log: signaling %s(%d) to start processing %s"%(SIGNAL_HOST, SIGNAL_PORT, base_location))
                write_ready_signal(base_location)
            p = threading.Thread(target=release_hosts, args=(java_host, ssl_host))
            p.start()
            procs.append(p)
        set_dumping_memory(False)
    free_server_memory()
    for p in procs:
        p.join()

    while len(COMPRESSION_PROCS) > 0:
        remove_the_dead_compression_procs()
        time.sleep(1)

def add_host_for_dumping(base_location, java_host, ssl_host, factors, use_64=False, use_python=False, perform_profiling=False):
    HOSTS_READY_TO_DUMP.insert(0, (base_location, java_host, ssl_host, factors, use_64, use_python, perform_profiling))


def remote_reboot(hostname, username, password):
    global REBOOT
    try:
        client = ssh_to_target(hostname, username, password)
        exec_remote_cmd(client, REBOOT, password=password)
        return True
    except:
        return False

def reboot_host(hostname, username, password):
    global REBOOT
    if is_host_suspended(hostname):
        resume_host(hostname)
        while is_host_suspended(hostname):
            time.sleep(SLEEP_SECS)
        while not is_host_running(hostname):
            time.sleep(SLEEP_SECS)
    elif not is_host_running(hostname):
        start_function(hostname)
        return True
    return remote_reboot(hostname, username, password)

def release_hosts(java_host, ssl_host, perform_reboot=True):
    print ("%s: Releasing hosts %s and %s"%(time_str(), java_host, ssl_host))
    host_cold_start = False
    if is_host_suspended(java_host):
        resume_host(java_host)
        print ("%s: Resumed host %s"%(time_str(), java_host))
        while is_host_suspended(java_host):
            time.sleep(SLEEP_SECS)

    elif not is_host_running(java_host):
        print ("%s: Cold tarting host %s"%(time_str(), java_host))
        start_function(hostname)
        host_cold_start = True

    if perform_reboot and not host_cold_start and not reboot_host(java_host, JAVA_USER, JAVA_PASS):
        print ("%s: Failed to reboot host %s, bouncing"%(time_str(), java_host))
        bounce_function(java_host)
    add_ssl_host_back(ssl_host)
    add_java_host_back(java_host)
    #update_jave_host_cntr(java_host)

def perform_java_dump(java_host, ssl_host, base_location, factors, use_64=False, use_python=False, perform_profiling=False):
    global EXPSSL_PASS, EXPSSL_PASS
    res = 0
    try:
        perform_memory_dump(java_host, ssl_host, base_location, EXPSSL_USER, EXPSSL_PASS, use_python)
    except:
        traceback.print_exc()
        res = 1
        reason = "perform_memory_dump failed"
        reapply_work_unit(base_location, factors, reason=reason)
        resume_host(java_host)
        return res

    if res == 0:
        if get_auth_log_line_count(base_location) < MIN_AUTH_COUNT or\
           get_stdout_log_line_count(base_location) < MIN_LOG_COUNT  or\
           get_java_process_with_base_location(base_location, use_64=use_64, use_python=use_python) is None:
            print ("Experiment Log: Unable to find the %s process"%('python' if use_python else 'java'))
            reason = "no java process found."
            if get_stdout_log_line_count(base_location) < MIN_LOG_COUNT:
                reason = "not enough logs in stdout, maybe it failed?"
            elif get_auth_log_line_count(base_location) < MIN_AUTH_COUNT:
                reason = "not enough logs in authlog, maybe it failed?"
            reapply_work_unit(base_location, factors, reason=reason)
            res = 1
        else:
            print ("Experiment Log: Successfully dumped memory, processing the rest")
            # copy ssllogs
            copy_ssllogs_to_factors(ssl_host, base_location)

    resume_host(java_host)
    clean_up_gclog(base_location)
    if res == 0 and perform_profiling:
        #time.sleep(SLEEP_SECS*8)
        capture_hprof_file(java_host, base_location)
    reboot_host(java_host, JAVA_USER, JAVA_PASS)
    return res

def perform_single_host_run_512_x32(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=512, use_64=False, perform_profiling=False)

def perform_single_host_run_512_x32_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=512, use_64=False, perform_profiling=True)

def perform_single_host_run_1024_x32(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=1024, use_64=False, perform_profiling=False)

def perform_single_host_run_1024_x32_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=1024, use_64=False, perform_profiling=True)

def perform_single_host_run_2048_x32(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2048, use_64=False, perform_profiling=False)

def perform_single_host_run_2048_x32_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2048, use_64=False, perform_profiling=True)

def perform_single_host_run_3072_x32(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=3072, use_64=False, perform_profiling=False)

def perform_single_host_run_3072_x32_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=3072, use_64=False, perform_profiling=True)



def perform_single_host_run_512_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=512, use_64=True, perform_profiling=False)

def perform_single_host_run_512_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=512, use_64=True, perform_profiling=True)

def perform_single_host_run_1024_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=1024, use_64=True, perform_profiling=False)

def perform_single_host_run_1024_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=1024, use_64=True, perform_profiling=True)

def perform_single_host_run_2048_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2048, use_64=True, perform_profiling=False)

def perform_single_host_run_2048_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2048, use_64=True, perform_profiling=True)

def perform_single_host_run_3072_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=3072, use_64=True, perform_profiling=False)

def perform_single_host_run_3072_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=3072, use_64=True, perform_profiling=True)

def perform_single_host_run_4096_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=4096, use_64=True, perform_profiling=False)

def perform_single_host_run_4096_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=4096, use_64=True, perform_profiling=True)

def perform_single_host_run_8192_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=8192, use_64=True, perform_profiling=False)

def perform_single_host_run_8192_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=8192, use_64=True, perform_profiling=True)

def perform_single_host_run_16384_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=(8192*2), use_64=True, perform_profiling=False)

def perform_single_host_run_16384_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=(8192*2), use_64=True, perform_profiling=True)


def perform_single_host_run_2304_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2304, use_64=True, perform_profiling=False)

def perform_single_host_run_2304_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2304, use_64=True, perform_profiling=True)

def perform_single_host_run_2560_x64(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2560, use_64=True, perform_profiling=False)

def perform_single_host_run_2560_x64_profiled(java_host, ssl_host, factors, base_location, destroy_passwords=0):
    return perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=destroy_passwords, jvm_mem=2560, use_64=True, perform_profiling=True)

def perform_single_host_run(java_host, ssl_host, factors, base_location, destroy_passwords=0, jvm_mem=512, is_streaming=False, use_64=False, use_python=False, perform_profiling=False):
    global SLEEP_SECS, EXPSSL_PASS, EXPSSL_USER, JAVA_PASS, JAVA_USER
    sleepy = SLEEP_SECS*3
    # create a factor directory
    create_factor_dir(base_location, use_python)
    # check and remove ssl_target logs
    clear_ssl_server_logs(ssl_host)
    # check and remove java_host logs
    clear_java_auth_log(base_location)
    # execute the java program
    print ("Experiment Log: Created results dir and cleared logs")
    res = 1
    cmd_success = test_host_is_up(java_host, JAVA_USER, JAVA_PASS)
    if not cmd_success:
        raise Exception ("Experiment Log: failed to connect to the %s"%(java_host))
    cmd_success = test_host_is_up(ssl_host, EXPSSL_USER, EXPSSL_PASS)
    if not cmd_success:
        raise Exception ("Experiment Log: failed to connect to the %s(%s)"%(ssl_host))
    try:
        res = execute_java_cmd(java_host, ssl_host, factors, base_location, destroy_passwords, jvm_mem=jvm_mem, is_streaming=is_streaming, use_64=use_64, use_python=use_python, perform_profiling=perform_profiling)
    except:
        traceback.print_exc()
        raise
    try:
        #remote_reboot(ssl_host, EXPSSL_USER, EXPSSL_PASS)
        res = perform_java_dump(java_host, ssl_host, base_location, factors, use_64, use_python, perform_profiling)
        WORKING_LIST = []
    except:
        res = 1
        traceback.print_exc()

    if not reboot_host(java_host, JAVA_USER, JAVA_PASS):
        bounce_function(java_host)
    secs = 0
    while len(COMPRESSION_PROCS) > 0:
        remove_the_dead_compression_procs()
        secs += 1
        time.sleep(1)
    try:
        #perform_rm_memory_dump(base_location)
        perform_rm_introspection_data(base_location)
    except:
        traceback.print_exc()
        raise

    #if secs < SLEEP_SECS*5:
    #    time.sleep((SLEEP_SECS*5)-secs)
    print ("Experiment Log: completed command executions for %s and %s"%(java_host, ssl_host))
    return res

def wait_for_hprof_dump_complete(client, hprof_loc):
    size = ''
    last_size = ''
    data = exec_remote_cmd(client, "ls -all %s"%(hprof_loc))
    try:
        size = data.split()[4]
    except:
        pass

    time.sleep(SLEEP_SECS*2)
    while size != last_size:
        try:
            data = exec_remote_cmd(client, "ls -all %s"%(hprof_loc))
            last_size = size
            size = data.split()[4]
            if size != last:
                time.sleep(SLEEP_SECS)
        except:
            break
    return

