import os, errno, traceback
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno, shutil
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(dest)
import single_factors_iteration_perform_runs as sifipr
import unzip_perform_process_dirs as uppd

from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

FACTORS = [
# ['HIGH','LOW', 'LOW',  'HIGH'],
# ['LOW', 'LOW', 'LOW', 'HIGH'],
#['HIGH','MED', 'LOW',  'HIGH'],
['HIGH', 'MED', 'HIGH', 'HIGH'],
#['LOW', 'MED', 'LOW', 'HIGH'],
#['LOW', 'MED', 'LOW', 'LOW'],
#['HIGH', 'MED', 'LOW', 'LOW'],
# ['HIGH','LOW', 'LOW',  'HIGH'],
# ['LOW', 'LOW', 'LOW', 'HIGH'],
# ['HIGH','MED', 'LOW',  'HIGH'],
# ['LOW', 'MED', 'LOW', 'HIGH'],
]


# system memory not heap
sifipr.set_single_host_mem(10*1024)

# compute survivor space ratios

GC_START_TIME = 650
MAX_EXP_TIME = GC_START_TIME+130
sifipr.JVM_SURVIVOR_RATIO = None
sifipr.JVM_NEW_MEMORY_USED = None
sifipr.USING_SSL_SOCKET = False
sifipr.USING_BOUNCY_CASTLE = False
sifipr.USING_G1GC = True
sifipr.USING_GC_LOGGING = True
sifipr.USING_SSL_SOCKET_NULL = True
sifipr.USING_NMT = True
GC_START_TIMES = {512:8000, 1024:8000,2048:8000, 4096:8000, 8192:8000, 16384:8000} 
MAX_NUM_REQUESTS = {512:5000, 1024:5000,2048:8000, 4096:13000, 8192:20000, 16384:32000} 
#MAX_NUM_REQUESTS = {512:5000, 1024:5000,2048:8000, 4096:5000, 8192:20000, 16384:32000} 
#MAX_NUM_REQUESTS = [2000, 2500, 3000, 3500, 4000, 4500, 5000,]
SINGLE_HOST_FN = {
  512:sifipr.perform_single_host_run_512_x64,
  1024:sifipr.perform_single_host_run_1024_x64,
  2048:sifipr.perform_single_host_run_2048_x64,
  4096:sifipr.perform_single_host_run_4096_x64,
  8192:sifipr.perform_single_host_run_8192_x64,
  16384:sifipr.perform_single_host_run_16384_x64,
}
SLEEP_TIME = 60
LOG_FILE= "manual_execution_log.txt"
BASE_BASE_LOCATION = "/srv/nfs/cortana/logs/manual_runs"
try:
    os.stat(BASE_BASE_LOCATION)
except:
    os.mkdir(BASE_BASE_LOCATION)

LOG_OUT = open(os.path.join(BASE_BASE_LOCATION, LOG_FILE), 'w')

def copytree(src, dst, symlinks=False, ignore=None):
    if os.path.isdir(src):
        mkdir_p(dst)

    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            try:
                shutil.copy(s, d)
            except:
                print "Failed to copy %s -> %s"%(s, d)

def check_del_exists(location):
    try:
        os.stat(location)
        shutil.rmtree(location)
        return True
    except:
        return False

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def get_perform_single_host_function(jvm_mem):
    return SINGLE_HOST_FN[jvm_mem]

def run_manual_iteration(jvm_mem, factors, base_location, retrys=5):
    if retrys < 0:
        factors_str = sifipr.get_factors_str(factors)
        print "Failed to run manual test on %s"%(factors_str)
        return -1
    try:
        os.stat(base_location)
        shutils.rmtree(base_location)
    except:
        pass

    working_dir = uppd.get_working_dir()
    factors_str = sifipr.get_factors_str(factors)
    res = -1
    try:
        perform_single_host_run_x64 = get_perform_single_host_function(jvm_mem)
        res = perform_single_host_run_x64(java_host, ssl_host, factors, working_dir, destroy_passwords=0)
        #perform_extraction_work(working_dir, working_dir)
        if res != 0:
            LOG_OUT.write("%s: Experiment Failed on run (%d): %s\n"%(time_str(), 5-retrys, factors_str))
            raise
    except:
        #time.sleep(SLEEP_TIME)
        try:
            os.stat(working_dir)
            shutil.rmtree(working_dir)
        except:
            print "Unable to remove %s"%working_dir
            pass
        #time.sleep(SLEEP_TIME)
        return run_manual_iteration(jvm_mem, factors, base_location, retrys-1)

    LOG_OUT.write("%s: Completed factors: %s\n"%(time_str(), factors_str))
    print("%s: Completed factors: %s\n"%(time_str(), factors_str))
    check_del_exists(base_location)
    #shutil.copytree(working_dir, base_location)
    copytree(working_dir, base_location)
    LOG_OUT.write("%s: Copy %s -> %s\n"%(time_str(),working_dir, base_location))
    print("%s: Copy %s -> %s\n"%(time_str(),working_dir, base_location))
    shutil.rmtree(working_dir)
    LOG_OUT.write("%s: Removing %s\n"%(time_str(),working_dir))
    print("%s: Removing %s\n"%(time_str(),working_dir))
    #time.sleep(SLEEP_TIME)

def run_manual_iterations(java_host, ssl_host, num_iterations, base_results):
    #base_results = os.path.join(BASE_BASE_LOCATION, base_location_name)
    try:
        os.stat(base_results)
    except:
        os.mkdir(base_results)

    iteration = 0
    file_run = iteration
    out_dir = "112_nolog_mrsmith_apache_null_pause_unmod_comp_run_x64_%04d"
    jvm_mems = GC_START_TIMES.keys()
    jvm_mems.sort()
    #jvm_mems = [4096, 2048, 1024]
    #jvm_mems.reverse()
    while iteration < num_iterations:
        #for jvm_mem, gc_start_time in GC_START_TIMES.items():
        for jvm_mem in jvm_mems:
            gc_start_time = GC_START_TIMES[jvm_mem]
            new_memory_value = jvm_mem / 2
            num_requests = MAX_NUM_REQUESTS[jvm_mem]
            sifipr.MAX_NUM_REQUESTS = num_requests
            sifipr.GC_START_TIME = gc_start_time
            sifipr.MAX_EXP_TIME = gc_start_time+190
            sifipr.JVM_NEW_MEMORY_USED = new_memory_value
            sifipr.JVM_SURVIVOR_RATIO = 8 
            #sifipr.GC_START_TIME = 60+int((5.0/2500)*num_requests *60)-30
            #sifipr.MAX_EXP_TIME = 60+int((5.0/2500)*num_requests *60)
            sifipr.GC_SPACING_TIME = -1
            base_fmt = (out_dir%jvm_mem)+"/%s_%d_%03d"
            for factors_params in FACTORS:
                factors  = sifipr.get_factors_dict_str(*factors_params)
                factors_str = sifipr.get_factors_str(factors)
                base_location = os.path.join(base_results, base_fmt%(factors_str, num_requests, file_run))
                while True:
                    try:
                        os.stat(base_location)
                        file_run += 1
                        base_location = os.path.join(base_results, base_fmt%(factors_str, num_requests, file_run))
                    except:
                        break
                run_manual_iteration(jvm_mem, factors, base_location)
        file_run += 1
        iteration += 1

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print "%s <java_host> <ssl_host> <num_iterations> <base_location>"
    java_host = sys.argv[1]
    ssl_host = sys.argv[2]
    num_iterations = int(sys.argv[3])
    base_location = sys.argv[4]
    #test_manual_run("test_it_works")
    sifipr.start_host_list([java_host,])
    sifipr.start_compression_procs_thread()
    run_manual_iterations(java_host, ssl_host, num_iterations, base_location)
    sifipr.stop_compression_procs_thread()
    sifipr.wait_compression_procs_thread()
