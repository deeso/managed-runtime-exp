extract_time = lambda line: int(line.split(' msec =====')[0].split('PASSED in ')[-1])
handle_data = lambda data: [extract_time(i.strip()) for i in data.splitlines()]

open_jdk_bm_data ='''===== DaCapo 9.12 avrora PASSED in 7621 msec =====
===== DaCapo 9.12 fop PASSED in 676 msec =====
===== DaCapo 9.12 h2 PASSED in 4672 msec =====
===== DaCapo 9.12 jython PASSED in 2703 msec =====
===== DaCapo 9.12 luindex PASSED in 1024 msec =====
===== DaCapo 9.12 lusearch PASSED in 7060 msec =====
===== DaCapo 9.12 pmd PASSED in 2227 msec =====
===== DaCapo 9.12 sunflow PASSED in 13205 msec =====
===== DaCapo 9.12 tomcat PASSED in 6914 msec =====
===== DaCapo 9.12 tradebeans PASSED in 12122 msec =====
===== DaCapo 9.12 tradesoap PASSED in 23447 msec ====='''

oracle_bm_data = '''===== DaCapo 9.12 avrora PASSED in 7484 msec =====
===== DaCapo 9.12 fop PASSED in 345 msec =====
===== DaCapo 9.12 h2 PASSED in 3998 msec =====
===== DaCapo 9.12 jython PASSED in 1860 msec =====
===== DaCapo 9.12 luindex PASSED in 931 msec =====
===== DaCapo 9.12 lusearch PASSED in 4274 msec =====
===== DaCapo 9.12 pmd PASSED in 2489 msec =====
===== DaCapo 9.12 sunflow PASSED in 11317 msec =====
===== DaCapo 9.12 tomcat PASSED in 6596 msec =====
===== DaCapo 9.12 tradebeans PASSED in 8494 msec =====
===== DaCapo 9.12 tradesoap PASSED in 18321 msec ====='''

sani_only_collect_bm_data = '''===== DaCapo 9.12 avrora PASSED in 6535 msec =====
===== DaCapo 9.12 fop PASSED in 327 msec =====
===== DaCapo 9.12 h2 PASSED in 4131 msec =====
===== DaCapo 9.12 jython PASSED in 2094 msec =====
===== DaCapo 9.12 luindex PASSED in 1298 msec =====
===== DaCapo 9.12 lusearch PASSED in 5250 msec =====
===== DaCapo 9.12 pmd PASSED in 2283 msec =====
===== DaCapo 9.12 sunflow PASSED in 12296 msec =====
===== DaCapo 9.12 tomcat PASSED in 6765 msec =====
===== DaCapo 9.12 tradebeans PASSED in 8510 msec =====
===== DaCapo 9.12 tradesoap PASSED in 17333 msec ====='''

sani_tenure_collect_bm_data = '''===== DaCapo 9.12 avrora PASSED in 7475 msec =====
===== DaCapo 9.12 fop PASSED in 450 msec =====
===== DaCapo 9.12 h2 PASSED in 3616 msec =====
===== DaCapo 9.12 jython PASSED in 4428 msec =====
===== DaCapo 9.12 luindex PASSED in 1498 msec =====
===== DaCapo 9.12 lusearch PASSED in 5747 msec =====
===== DaCapo 9.12 pmd PASSED in 2155 msec =====
===== DaCapo 9.12 sunflow PASSED in 12102 msec =====
===== DaCapo 9.12 tomcat PASSED in 5742 msec =====
===== DaCapo 9.12 tradebeans PASSED in 9423 msec =====
===== DaCapo 9.12 tradesoap PASSED in 20388 msec ====='''

sani_no_ra_bm_data = '''===== DaCapo 9.12 avrora PASSED in 6127 msec =====
===== DaCapo 9.12 fop PASSED in 339 msec =====
===== DaCapo 9.12 h2 PASSED in 3976 msec =====
===== DaCapo 9.12 jython PASSED in 2308 msec =====
===== DaCapo 9.12 luindex PASSED in 1316 msec =====
===== DaCapo 9.12 lusearch PASSED in 5063 msec =====
===== DaCapo 9.12 pmd PASSED in 2300 msec =====
===== DaCapo 9.12 sunflow PASSED in 11535 msec =====
===== DaCapo 9.12 tomcat PASSED in 6287 msec =====
===== DaCapo 9.12 tradebeans PASSED in 9422 msec =====
===== DaCapo 9.12 tradesoap PASSED in 17805 msec ====='''

openjdk_data = handle_data(open_jdk_bm_data)
oracle_data = handle_data(oracle_bm_data)
sani_only_collect = handle_data(sani_only_collect_bm_data)
sani_tenure_collect = handle_data(sani_tenure_collect_bm_data)
sani_no_ra = handle_data(sani_no_ra_bm_data)
pos_fmt = "{:0.01f}$\% \uparrow$" # speed up
neg_fmt = "{:0.01f}$\% \downarrow$" # speed down
lambda_perf = lambda b, x: (float(b-x)/x)*100
#base = oracle_data
app_names = ["avrora", "fop", "h2", "jython", "luindex", "lusearch", "pmd", "sunflow", "tomcat", "tradebeans", "tradesoap", ]
table_line = "{app_name} & {openjdk} & {oracle} & {sani_no_ra} & {sani_only_collect} & {sani_tenure_collect} \\\\"
table_line = "{app_name} & {oracle} & {sani_no_ra} & {sani_only_collect} & {sani_tenure_collect} \\\\"
num_exp = len(openjdk_data)
pos = 0
table_line_keys = []
get_perf_string = lambda b, x: neg_fmt.format(abs(lambda_perf(b, x))) if lambda_perf(b, x) <= 0 else pos_fmt.format(abs(lambda_perf(b, x)))
while pos < num_exp:
    keys = {}
    base = openjdk_data[pos]
    base = oracle_data[pos]
    keys['app_name'] = app_names[pos]
    keys['openjdk'] = "%d ms"%openjdk_data[pos]
    keys['oracle'] = "%d ms"%openjdk_data[pos]#get_perf_string(base, oracle_data[pos])
    keys['sani_no_ra'] = get_perf_string(base, sani_no_ra[pos])
    keys['sani_only_collect'] = get_perf_string(base, sani_only_collect[pos])
    keys['sani_tenure_collect'] = get_perf_string(base, sani_tenure_collect[pos])
    table_line_keys.append(keys)
    pos += 1

table_lines = []

for line_keys in table_line_keys:
    table_lines.append(table_line.format(**line_keys))

print "\n".join(table_lines)