import multiprocessing, subprocess, time, signal
import os, sys, shutil

dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(dest)
import single_factors_iteration_perform_runs as sifipr

from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

FACTORS = [
    ['HIGH', 'LOW', 'LOW', 'HIGH'],
    ['HIGH', 'LOW', 'LOW', 'MED'],
    ['HIGH', 'MED', 'LOW', 'HIGH'],
    ['HIGH', 'MED', 'LOW', 'MED'],
    ['LOW', 'HIGH', 'LOW', 'LOW'],
    ['LOW', 'MED', 'LOW', 'LOW'],
    ['LOW', 'LOW', 'LOW', 'LOW'],
    ['LOW', 'HIGH', 'LOW', 'MED'],
    ['LOW', 'MED', 'LOW', 'MED'],
    ['LOW', 'LOW', 'LOW', 'MED'],
    ['LOW', 'LOW', 'MED', 'LOW'],
    ['MED', 'LOW', 'MED', 'LOW'],
    ['HIGH', 'LOW', 'MED', 'LOW'],
]

# python start process from command line

# move java.hprof.txt iteration to log directory java.hprof.txt+iteration



HPROF_FILE = "java.hprof.txt"
DEST_HPROF_DIR = "/srv/nfs/cortana/logs/profiling_data/"
DEST_HPROF_FMT = os.path.join(DEST_HPROF_DIR, "java.hprof-{settings}-{iteration}.txt")

command_line = [
'''java -agentlib:hprof=heap=sites -Xnoclassgc -XX:-CMSClassUnloadingEnabled ''',
'''-XX:+UseSerialGC -XX:+AlwaysPreTouch -XX:InitialHeapSize=2560M ''',
'''-XX:MaxHeapSize=2560M -XX:NewSize=1280M -XX:MaxNewSize=1280M ''',
'''-XX:InitialSurvivorRatio=0 -jar ''',
'''/srv/nfs/cortana/logs/cmd/exp_code/libs/java-gc-experiments-1.0.jar ''',
'''JavaClientExperiments 10.16.121.195 /tmp/authlog.txt {settings} 0''',
]

cmd_format = " ".join(command_line)
keywords = {}
iteration = 0
while iteration < 10:
    keywords['iteration'] = iteration
    for setting in FACTORS:
        keywords['settings'] = "_".join(setting)
        cmd_setting = {'settings':" ".join(setting)}
        print ("%s: Iteration: %02d Executing profile for settings: %s"%(time_str(), iteration, cmd_setting['settings']))
        cmd = cmd_format.format(**cmd_setting)
        p = sifipr.exec_command_for_proc(cmd)
        time.sleep(300)
        os.kill(p.pid,signal.SIGINT)
        p.wait()
        dst_dir = DEST_HPROF_FMT.format(**keywords)
        print ("%s: Iteration: %02d Copy %s -> %s"%(time_str(), iteration, HPROF_FILE, dst_dir))
        shutil.copy(HPROF_FILE, dst_dir)
    iteration += 1



