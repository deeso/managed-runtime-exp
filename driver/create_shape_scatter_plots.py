import itertools
import pandas as pd
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
from multiprocessing import Process
import os, urllib, json, pipes, traceback
import binascii, subprocess, json, shutil
import multiprocessing
from datetime import datetime
import numpy as np
from numpy import mean
from pandas import DataFrame
#from single_factors_iteration_perform_runs import *
import single_factors_iteration_perform_runs as sifipr
import matplotlib.pyplot as plt
import agg_exp_data as aed

title=''
cbar_label="Experiment Run #"
xlabel = "Number of TLS Keys Observed"
ylabel = "Number of Recovered TLS Keys"


#base_dir = "/research_data/code/git/measuring-managed-runtime-artifacts/figs/"
base_dir = "/srv/nfs/cortana/logs/output/figs/"
get_out_file = lambda x: os.path.join(base_dir, x)

vsi_aam_out_file = "aam-unmod-jvm-vsi-single.png"
vsi_sm_out_file = "sm-unmod-jvm-vsi-single.png"
vsi_tcm_out_file = "tcm-unmod-jvm-vsi-single.png"

ten_aam_out_file = "aam-ten-jvm-single-collect-always.png"
ten_sm_out_file = "sm-ten-jvm-single-collect-always.png"
ten_tcm_out_file = "tcm-ten-jvm-single-collect-always.png"

ten_marker = lambda n:('vsi_'+n,"VSI %s"%(n.upper()))
vsi_marker = lambda n:('vsi_'+n,"VSI %s"%(n.upper()))
manual_marker = lambda n:('manual_'+n, "Single Host %s"%(n.upper()))
get_mapped_marker_labels = lambda markers, fn: dict([fn(i) for i in markers])

tcm_markers = dict(zip(aed.CBAR_LABELS_TCM, aed.SHAPES[:len(aed.CBAR_LABELS_TCM)]))
sm_markers = dict(zip(aed.CBAR_LABELS_SM, aed.SHAPES[:len(aed.CBAR_LABELS_SM)]))
aam_markers = dict(zip(aed.CBAR_LABELS_AAM, aed.SHAPES[:len(aed.CBAR_LABELS_AAM)]))

all_tcm_markers = {}
all_tcm_markers.update(tcm_markers)
all_sm_markers = {}
all_sm_markers.update(sm_markers)
all_aam_markers = {}
all_aam_markers.update(aam_markers)

all_dicts = [all_tcm_markers, all_aam_markers, all_sm_markers]
for d in all_dicts:
    keys = d.keys()
    for k in keys:
        d['vsi_'+k] = d[k]
        d['tenured_'+k] = d[k]
        d['mod_ms_'+k] = d[k]
        d['manual_'+k] = d[k]


models = aed.CBAR_LABELS_AAM + aed.CBAR_LABELS_SM + aed.CBAR_LABELS_TCM
def get_frame_stats (dft, models):
    data = []
    columns = ["Model", 'Num Samples', 'Mean Keys Recovered', 'Mean TLS Sessions', 'Mean % Keys Recovered']
    for model in models:
        dft_s = dft[dft.model == model.lower()]
        rows = dft_s.shape[0]
        print "Model: %s Count: %d"%(model, rows)
        ratio = dft_s.unique_heap_hits / dft_s.tot_ssl
        mean_r = np.mean(ratio)
        mean_h = np.mean(dft_s.unique_heap_hits)
        mean_t = np.mean(dft_s.tot_ssl)
        try:
            mean_r = int(np.round(mean_r*100))
            mean_t = int(np.round(mean_t))
            mean_h = int(np.round(mean_h))
        except:
            mean_r = 0
            mean_h = 0
            mean_t = 0
        row = [model, rows, mean_h, mean_t, mean_r]
        data.append(row)
    return columns, data

# columns, data = get_frame_stats(dft_unmod, models)
# columns, data = get_frame_stats(dft_, models)
# print tabulate.tabulate(data, headers=columns)

def plot_data(groups, groups2, x_lim, y_lim, aam_out_file, sm_out_file, tcm_out_file):

    fig,ax = plt.subplots()
    build_scatter_plot(ax, [("vsi_"+n,g) for n,g in groups if n in aam_markers], 
        xlabel, ylabel, all_aam_markers, 
        mapped_marker_labels=get_mapped_marker_labels(aam_markers, vsi_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='full', ncol=len(aam_markers),
        mark_90=True, shape_size=10)


    build_scatter_plot(ax, [("manual_"+n,g) for n,g in groups2 if n in aam_markers], 
        xlabel, ylabel, all_aam_markers,
        mapped_marker_labels=get_mapped_marker_labels(aam_markers, manual_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='none', ncol=len(aam_markers),
        shape_size=8)

    plt.tight_layout()
    plt.savefig(get_out_file(aam_out_file), bbox_inches='tight')#, bbox_extra_artist=[lgd])

    # TCM output
    fig,ax = plt.subplots()
    build_scatter_plot(ax, [("vsi_"+n,g) for n,g in groups if n in tcm_markers], 
        xlabel, ylabel, all_tcm_markers, 
        mapped_marker_labels=get_mapped_marker_labels(tcm_markers, vsi_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='full', ncol=3,
        mark_90=True, shape_size=10)


    build_scatter_plot(ax, [("manual_"+n,g) for n,g in groups2 if n in tcm_markers], 
        xlabel, ylabel, all_tcm_markers,
        mapped_marker_labels=get_mapped_marker_labels(tcm_markers, manual_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='none', ncol=3,
        shape_size=8)

    plt.tight_layout()
    plt.savefig(get_out_file(tcm_out_file), bbox_inches='tight')#, bbox_extra_artist=[lgd])

    # SM output
    fig,ax = plt.subplots()
    build_scatter_plot(ax, [("vsi_"+n,g) for n,g in groups if n in sm_markers], 
        xlabel, ylabel, all_sm_markers,
        mapped_marker_labels=get_mapped_marker_labels(sm_markers, vsi_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='full', ncol=3,
        mark_90=True, shape_size=10)


    build_scatter_plot(ax, [("manual_"+n,g) for n,g in groups2 if n in sm_markers], 
        xlabel, ylabel, all_sm_markers,
        mapped_marker_labels=get_mapped_marker_labels(sm_markers, manual_marker),
        fontsize=12, x_lim=x_lim, y_lim=y_lim, fillstyle='none', ncol=3,
        shape_size=8)

    #plt.tight_layout()
    plt.savefig(get_out_file(sm_out_file), bbox_inches='tight')#, bbox_extra_artist=[lgd])


def build_scatter_plot(ax, groups, xlabel, ylabel, mapped_markers={}, 
    mapped_marker_labels={},fontsize=12, y_lim=None, x_lim=None, 
    fillstyle='full', ncol=4, shape_size=4, mark_90=False):
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=12)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=12)

    max_y = max_x = 0
    min_y = min_x = 0

    max_y = 0
    min_y = 0

    ax.margins(0.05) # Optional, just adds 5% padding to the autoscaling
    shape_keys = []
    for name, group in groups:
        if not name in mapped_markers:
            continue
        marker = mapped_markers[name] if name in mapped_markers else None
        label = mapped_marker_labels[name] if name in mapped_marker_labels else name
        ax.plot(group.tot_ssl, group.unique_heap_hits, ms=shape_size, color='k', marker=marker, label=label, linestyle='', fillstyle=fillstyle)
        max_x = max(max(group.tot_ssl), max_x)
        max_y = max(max(group.unique_heap_hits), max_y)
        min_x = min(min(group.tot_ssl), min_x)

    for y_factor, linestyle, color, linewidth in aed.MAX_ALIVE_SESSIONS:
        ax.axhline(y_factor, color=color, linewidth=linewidth, linestyle=linestyle)

    # mark 90%
    if mark_90:
        p = pd.concat([g for n,g in groups])
        num_rows = int(p.shape[0]*.9)
        p = p.sort(columns=['unique_heap_hits'], ascending=False)
        ninety = p.iloc[num_rows].unique_heap_hits
        ax.axhline(ninety, color='k', linewidth=2, linestyle='--')

    max_x += 500
    min_x -= 500
    max_y = (max_y + 500) - (max_y % 500)
    if xlabel:
        min_x = x_lim[0] if x_lim else min_x
        max_x = x_lim[1] if x_lim else max_x
        ax.set_xlim(min_x, max_x)
    if ylabel:
        min_y = y_lim[0] if y_lim else min_y
        max_y = y_lim[1] if y_lim else max_y
        ax.set_ylim(min_y, max_y)

    handles, labels = ax.get_legend_handles_labels()
    use_labels = []
    for i in labels:
        if i in mapped_marker_labels:
            use_labels.append(mapped_marker_labels[i])
        else:
            use_labels.append(i.upper())

    ax.legend(handles,
           use_labels,
           loc=3,
           bbox_to_anchor=(0., 1.02, 1., .102),
           scatterpoints=1,
           ncol=ncol,
           fontsize=fontsize,
           numpoints=1)


multi_mapped_fields = {}
multi_shared_unmod = '/srv/nfs/cortana/logs/small_oracle8_unmod_jvm_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_ten_10 = '/srv/nfs/cortana/logs/10_small_mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_ten_20 = '/srv/nfs/cortana/logs/20_small_mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_ten_30 = '/srv/nfs/cortana/logs/30_small_mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_mod_ms_10 = '/srv/nfs/cortana/logs/10_small_mod_jvm_mod_ms_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_mod_ms_20 = '/srv/nfs/cortana/logs/20_small_mod_jvm_mod_ms_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
multi_shared_mod_ms_30 = '/srv/nfs/cortana/logs/30_small_mod_jvm_mod_ms_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'



multi_mapped_fields[multi_shared_unmod] = {'jvm_conf':'unmodified'}
multi_mapped_fields[multi_shared_ten_10] = {'jvm_conf':'collect_always'}
multi_mapped_fields[multi_shared_ten_20] = {'jvm_conf':'collect_always'}
multi_mapped_fields[multi_shared_ten_30] = {'jvm_conf':'collect_always'}
multi_mapped_fields[multi_shared_mod_ms_10] = {'jvm_conf':'mod_ms'}
multi_mapped_fields[multi_shared_mod_ms_20] = {'jvm_conf':'mod_ms'}
multi_mapped_fields[multi_shared_mod_ms_30] = {'jvm_conf':'mod_ms'}


manual_unmod = '/srv/nfs/cortana/logs/manual_runs/profiling_runs_x64_2560'
manual_ten = '/srv/nfs/cortana/logs/manual_runs/profiling_runs_mod_ms_tenured_collect_always_x64_2560'

dirs2 = [
multi_shared_unmod,
multi_shared_ten_10,
multi_shared_ten_20,
#multi_shared_ten_30,
multi_shared_mod_ms_10,
multi_shared_mod_ms_20,
#multi_shared_mod_ms_30,
]

manual_dirs = [
manual_unmod,
manual_ten,
]

manual_mapped_fields = {}
manual_mapped_fields[manual_unmod] = {'jvm_conf':'unmodified'}
manual_mapped_fields[manual_ten] = {'jvm_conf':'collect_always'}

heap_infos = {}
for base_location in dirs2:
    heap_infos[base_location] = aed.read_heap_summary_data(base_location)

manual_heap_infos = {}
for base_location in manual_dirs:
    manual_heap_infos[base_location] = aed.read_heap_summary_data(base_location)


df = aed.data_frame_from_heap_data(heap_infos, multi_mapped_fields)
df2 = aed.data_frame_from_heap_data(manual_heap_infos, manual_mapped_fields)


unmod_groups = df[df.jvm_conf=='unmodified'].groupby('model')
unmod_groups2 = df2[df2.jvm_conf=='unmodified'].groupby('model')

collect_always_groups = df[df.jvm_conf=='collect_always'].groupby('model')
collect_always_groups2 = df2[df2.jvm_conf=='collect_always'].groupby('model')

mod_ms_groups = df[df.jvm_conf=='mod_ms'].groupby('model')
mod_ms_groups2 = df2[df2.jvm_conf=='mod_ms'].groupby('model')


y_lim = (0, max(df['unique_heap_hits'])+200)
x_lim = (min(df['tot_ssl'])-100, max(df['tot_ssl'])+200)

plot_data(unmod_groups, unmod_groups2, x_lim, y_lim,
         vsi_aam_out_file, vsi_sm_out_file, vsi_tcm_out_file)


y_lim = (0, max(df['unique_heap_hits'])+200)
x_lim = (min(df['tot_ssl'])-100, max(df['tot_ssl'])+200)

plot_data(collect_always_groups, collect_always_groups2, x_lim, y_lim,
    ten_aam_out_file, ten_sm_out_file, ten_tcm_out_file)
