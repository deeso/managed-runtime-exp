import sh
import itertools
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
from multiprocessing import Process
import os, urllib, json, pipes, traceback
import binascii, subprocess, json, shutil
import multiprocessing
from datetime import datetime
import numpy as np
from numpy import mean
from pandas import DataFrame
#from single_factors_iteration_perform_runs import *
import single_factors_iteration_perform_runs as sifipr
import matplotlib.pyplot as plt
AUGMENTING_DATA = "augmented_data.txt"

DESCR_MAPPING = {
# server/service
'HIGH_LOW_LOW_HIGH':"Service with a low session life span and high number of concurrent requests.",
'HIGH_LOW_LOW_MED':"Service with a low session life span and nominal number of concurrent requests.",
'HIGH_MED_LOW_HIGH':"Service with a nominal session life span and high number of concurrent requests.",
'HIGH_MED_LOW_MED':"Service with a nominal session life span and nominal number of concurrent requests.",
'HIGH_MED_HIGH_HIGH':"Service with a nominal session life span and nominal number of concurrent requests.",
# idle client
'LOW_HIGH_LOW_LOW':"Thin client with a high session lifespan and low number of concurrent requests.",
'LOW_MED_LOW_LOW':"Thin client with a nominal session lifespan and low number of concurrent requests.",
'LOW_LOW_LOW_LOW':"Thin client with a low session lifespan and low number of concurrent requests.",
'LOW_HIGH_LOW_MED':"Thin client with a high session lifespan and nominal number of concurrent requests.",
'LOW_MED_LOW_MED':"Thin client with a nominal session lifespan and nominal number of concurrent requests.",
'LOW_LOW_LOW_MED':"Thin client with a low session lifespan and nominal number of concurrent requests.",
'LOW_LOW_LOW_HIGH':"Thin client with a low session lifespan and nominal number of concurrent requests.",
'LOW_MED_LOW_HIGH':"Thin client with a nominal session lifespan and nominal number of concurrent requests.",
# active gaming application
'LOW_LOW_MED_LOW':"Active application with a low memory contention with a nominal number of requests and low number of concurrent sessions.",
'MED_LOW_MED_LOW':"Active application with a nominal memory contention with a nominal number of requests and low number of concurrent sessions.",
'HIGH_LOW_MED_LOW':"Active application with a high memory contention with a nominal number of requests and low number of concurrent sessions.",
}


NAME_MAPPING = {
# server/service
'HIGH_LOW_LOW_HIGH':"Service Model #1",
'HIGH_LOW_LOW_MED':"Service Model #2",
'HIGH_MED_LOW_HIGH':"Service Model #3",
'HIGH_MED_LOW_MED':"Service Model #4",
'HIGH_MED_HIGH_HIGH':"Service Model #5",
# idle client
'LOW_HIGH_LOW_LOW':"Thin Client Model #1",
'LOW_MED_LOW_LOW':"Thin Client Model #2",
'LOW_LOW_LOW_LOW':"Thin Client Model #3",
'LOW_HIGH_LOW_MED':"Thin Client Model #4",
'LOW_MED_LOW_MED':"Thin Client Model #5",
'LOW_LOW_LOW_MED':"Thin Client Model #6",

'LOW_MED_LOW_HIGH':"Thin Client Model #5",
'LOW_LOW_LOW_HIGH':"Thin Client Model #6",
'LOW_MED_HIGH_HIGH':"Service Model #7",
# active gaming application
'LOW_LOW_MED_LOW':"Active Application Model #1",
'MED_LOW_MED_LOW':"Active Application Model #2",
'HIGH_LOW_MED_LOW':"Active Application Model #3",
}

ABBR_MAPPING = {
"Service Model #1":'SM1',
"Service Model #2":'SM2',
"Service Model #3":'SM3',
"Service Model #4":'SM4',
"Service Model #5":'SM5',
"Thin Client Model #1":'TCM1',
"Thin Client Model #2":'TCM2',
"Thin Client Model #3":'TCM3',
"Thin Client Model #4":'TCM4',
"Thin Client Model #5":'TCM5',
"Thin Client Model #6":'TCM6',
"Thin Client Model #7":'TCM7',
"Active application Model #1":'AAM1',
"Active application Model #2":'AAM2',
"Active application Model #3":'AAM3',

'HIGH_LOW_LOW_HIGH':'SM1',
'HIGH_LOW_LOW_MED':'SM2',
'HIGH_MED_LOW_HIGH':'SM3',
'HIGH_MED_LOW_MED':'SM4',
'HIGH_MED_HIGH_HIGH':'SM5',

'LOW_HIGH_LOW_LOW':'TCM1',
'LOW_MED_LOW_LOW':'TCM2',
'LOW_LOW_LOW_LOW':'TCM3',
'LOW_HIGH_LOW_MED':'TCM4',

'LOW_MED_LOW_HIGH':'TCM5',
'LOW_MED_LOW_MED':'TCM5',
'LOW_MED_HIGH_HIGH':'TCM7',

'LOW_LOW_LOW_MED':'TCM6',
'LOW_LOW_LOW_HIGH':'TCM6',
'LOW_LOW_MED_LOW':'AAM1',
'MED_LOW_MED_LOW':'AAM2',
'HIGH_LOW_MED_LOW':'AAM3',
}

TABLE_HEADER = u'''
\\begin{{table}}[h]
\\begin{{center}}
    \\caption{{Mean values over *** runs for: {base_location}}}
    \\begin{{tabular}}{{ c c c c c c }}
    Program  & Mean of & \\% PMS & \\% Keyblock & \\% Keys \\\\
     Model   & Total Keys &  Found  & Found  & Found   \\\\
    \hline
    {table_data}
    \end{{tabular}}
\\end{{center}}
\\end{{table}}
'''

TABLE_HEADER3 = u'''
\\begin{{table}}[h]
\\begin{{center}}
    \\caption{{Mean values over *** runs for: {base_location}}}
    \\begin{{tabular}}{{ c c c c }}
Program  & \\% Key Parts & \\% Unique Key           & \\% Recovered Keys\\\\
 Model   &   Found      &  Parts Found with STD   & with STD \\\\
    %\hline inserted by script
    {table_data}
    \end{{tabular}}
\\end{{center}}
\\end{{table}}
'''

TABLE_HEADER2 = u'''
\\begin{{table}}[h]
\\begin{{center}}
    \\caption{{Sensitive Application Secrets recovered : {base_location}}}
    \\begin{{tabular}}{{ | c | c | c | c | c | }}
    Program  & Mean of & \\% Post Data& \\% Passwords  & \\% Session IDs\\\\
     Model   & Total Session & Recovered &Recovered  & Recovered  \\\\
    \hline\hline
    {table_data}
    \hline
    \end{{tabular}}
\\end{{center}}
\\end{{table}}
'''

TABLE_HEADER4 = u'''
\\begin{{table}}[h]
\\begin{{center}}
    \\caption{{Sensitive Application Secrets recovered : {base_location}}}
    \\begin{{tabular}}{{  c  c  c  c  }}
    Program  & \\% Post Data& \\% Passwords  & \\% Session IDs\\\\
     Model   &   Recovered \& STD   & Recovered \& STD & Recovered \& STD \\\\
    {table_data}
    \end{{tabular}}
\\end{{center}}
\\end{{table}}
'''

TABLE_LINE4 = ''''''+ \
'''{model_abbr} & {mean_pct_post_data_discovered:0.2f} $\pm$ {var_pct_post_data_discovered:0.2f} & '''+\
'''{mean_pct_passwords_discovered:0.2f} $\pm$ {var_pct_passwords_discovered:0.2f} & '''+\
'''{mean_pct_sesssion_ids_discovered:0.2f} $\pm$ {var_pct_sesssion_ids_discovered:0.2f}\\\\'''


TABLE_LINE3 = ''''''+ \
'''{model_abbr} & {scaled_tot_all_hits:.02f} & ''' +\
'''{scaled_tot_unique_mean:0.2f} $\pm$ {scaled_tot_unique_std:0.2f} & ''' +\
'''{scaled_key_tot_unique_mean:0.2f} $\pm$ {scaled_key_tot_unique_std:0.2f} \\\\'''


TABLE_LINE = ''''''+ \
'''{model_abbr} & {mean_total_keys:.0f} & {mean_pct_pms_keys_discovered:0.2f} & '''+\
'''{mean_pct_mkeyblock_keys_discovered:0.2f} & {mean_pct_dicovered_keys:0.2f} \\\\'''

TABLE_LINE2 = ''''''+ \
'''{model_abbr} & {mean_total_sessions:.0f} & {mean_pct_post_data_discovered:0.2f} & '''+\
'''{mean_pct_passwords_discovered:0.2f} & {mean_pct_sesssion_ids_discovered:0.2f} \\\\'''

CBAR_LABELS_TCM = ['tcm1', 'tcm2','tcm3','tcm4', 'tcm5','tcm6']
CBAR_LABELS_AAM = ['aam1', 'aam2','aam3']
CBAR_LABELS_SM = ['sm1', 'sm2','sm3','sm4']

def data_frame_from_heap_data(heap_infos, mapped_fields={}):
    data_frames = {}
    model_datas = []
    labels = CBAR_LABELS_TCM + CBAR_LABELS_AAM + CBAR_LABELS_SM
    for k in heap_infos:
        heap_data = heap_infos[k]
        model_data = get_scatter_data(labels, heap_data)
        add_fields = mapped_fields[k] if k in mapped_fields else {}
        for f in model_data:
            f.update(add_fields)
        model_datas.append(model_data)

    data = list(itertools.chain(*model_datas))
    df = DataFrame(data)
    return df

def get_scatter_data(model_abbrs, heap_data):
    data = []
    for model_abbr in model_abbrs:
        rvalues = [i['run_values'] for i in heap_data.values() if i['model_abbr'].lower() == model_abbr.lower()]
        for rvalue in rvalues:
            for run, _data in rvalue.items():
                items_list = {}
                if 'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey' in _data:
                    gh = _data['com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey']
                    items_list['live_TLS_MS'] = gh['live']
                    items_list['dead_TLS_MS'] = gh['dead']
                items_list['model'] = model_abbr
                items_list['run'] = run
                #items_list['tot_sdata_found'] = _data['key_mkeyblock_hits']
                #items_list['tot_sdata_found'] += _data['key_pms_hits']
                items_list['unique_pms_kb_found'] = _data['key_pms_unique_hits']
                items_list['unique_pms_kb_found'] += _data['key_mkeyblock_unique_hits']
                items_list['unique_pms_hits'] = _data['key_pms_unique_hits']
                items_list['unique_kb_hits'] = _data['key_mkeyblock_unique_hits']
                items_list['key_eden_hits'] = _data['key_eden_hits']
                items_list['key_hemi_hits'] = _data['key_hemi_hits']
                items_list['key_tenure_hits'] = _data['key_tenure_hits']
                items_list['key_stack_hits'] = _data['key_thread_hits']
                items_list['key_heap_hits'] = _data['key_tot_all_hits']
                items_list['unique_heap_hits'] = _data['key_tot_unique']
                items_list['tot_ssl'] = _data['tot_ssl']
                data.append(items_list)
    return data

def get_scatter_data_unique(model_abbrs, heap_data):
    data = []
    for model_abbr in model_abbrs:
        rvalues = [i['run_values'] for i in heap_data.values() if i['model_abbr'].lower() == model_abbr.lower()]
        for rvalue in rvalues:
            for run, _data in rvalue.items():
                items_list = {}
                items_list['model'] = model_abbr
                items_list['run'] = run
                items_list['unique_keys'] = _data['key_tot_unique']
                items_list['tot_ssl'] = _data['tot_ssl']
                data.append(items_list)
    return data

MAX_ALIVE_SESSIONS = [(.8*240, ':', 'k', 1), (1.6*240, '-', 'r', 2 )]
def make_scatter_plot(x, y, colors, title, xlablel, ylabel, cbar_label, cbar_labels=None, ylim=(0.0,1.0), grid={'b':False, 'which':'major', 'axis':'both'}, fontsize=12, output_filename=None):
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    #plt.ylim(ylim)
    max_x = max(x)+500
    plt.xlim(0, max_x)


    sc = plt.scatter(x,y,c=colors, s=35,)
    cbar = None
    if cbar_labels is None:
        cbar = plt.colorbar(sc)
    else:
        bounds = len([i for i in enumerate(cbar_labels)])
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cbar = plt.colorbar(sc, boundaries=bounds, ticks=bounds, norm=norm)
        cbar.ax.set_yticklabels(cbar_labels)

    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(cbar_label, rotation=270, fontsize=fontsize)

    #plot the lines for different factors
    for y_factor, linestyle, color, linewidth in MAX_ALIVE_SESSIONS:
        plt.hlines(y_factor, 0, max_x, color=color, linewidth=linewidth, linestyle=linestyle)

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)

def make_scatter_plot_df(groups, colors_markers, title, xlablel, ylabel, cbar_label, cbar_labels=None, ylim=(0.0,1.0), grid={'b':False, 'which':'major', 'axis':'both'}, fontsize=12, output_filename=None):

    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    #plt.ylim(ylim)
    max_x = max(x)+500
    plt.xlim(0, max_x)


    sc = plt.scatter(x,y,c=colors, s=35,)
    cbar = None
    if cbar_labels is None:
        cbar = plt.colorbar(sc)
    else:
        bounds = len([i for i in enumerate(cbar_labels)])
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cbar = plt.colorbar(sc, boundaries=bounds, ticks=bounds, norm=norm)
        cbar.ax.set_yticklabels(cbar_labels)

    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(cbar_label, rotation=270, fontsize=fontsize)

    #plot the lines for different factors
    for y_factor, linestyle, color, linewidth in MAX_ALIVE_SESSIONS:
        plt.hlines(y_factor, 0, max_x, color=color, linewidth=linewidth, linestyle=linestyle)

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)

def make_scatter_shape_plot(shape_data, title, xlablel, ylabel, cbar_labels=None, ylim=(0.0,1.0), grid={'b':False, 'which':'major', 'axis':'both'}, fontsize=12, output_filename=None):
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    #plt.ylim(ylim)
    max_y = max_x = 0
    min_y = min_x = 0
    plt.xlim(min_x, max_x)
    plt.ylim(min_y, max_y)

    max_y = 0
    min_y = 0

    shape_keys = shape_data.keys()
    shape_keys.sort()
    sc_plots = []
    for shape in shape_keys:
        print shape_data[shape]['shape']
        x = shape_data[shape]['x']
        y = shape_data[shape]['y']
        if min_x == 0:
            min_x = min(x)
        marker = shape_data[shape]['shape']
        sc = plt.scatter(x, y,marker=marker)
        sc_plots.append(sc)
        max_x = max(x+[max_x])
        max_y = max(y+[max_y])
        min_x = min(x+[min_x])

    max_x += 500
    min_x -= 500
    max_y = (max_y + 500) - (max_y % 500)
    plt.xlim(min_x, max_x)
    plt.ylim(min_y, max_y)
    plt.legend(sc_plots,
           [i.upper() for i in shape_keys],
           loc=3,
           bbox_to_anchor=(0., 1.02, 1., .102),
           scatterpoints=1,
           ncol=4,
           fontsize=fontsize)

    #plot the lines for different factors
    for y_factor, linestyle, color, linewidth in MAX_ALIVE_SESSIONS:
        plt.hlines(y_factor, 0, max_x, color=color, linewidth=linewidth, linestyle=linestyle)

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)



SHAPES = ["p", 'o', '^', 's', '*', 'd']
def get_x_y_shapes(data):
    shape_results = {}
    shape_labels = set([i['model'] for i in data])
    shape_labels = list(shape_labels)
    shape_labels.sort()
    shape_map = dict([(i[1], i[0]) for i in enumerate(shape_labels)])
    for l in shape_labels:
        shape = SHAPES[shape_map[l]]
        shape_results[l] = {'x':[], 'y':[], 'shape':shape}
        x = []
        y = []
        for i in data:
            x = [i['tot_ssl'] for i in data if i['model'] == l]
            y = [i['unique_keys'] for i in data if i['model'] == l]
        shape_results[l]['x'] = x
        shape_results[l]['y'] = y
    return shape_results

def get_x_y_colors(data):
    cbar_labels = set([i['model'] for i in data])
    cbar_labels = list(cbar_labels)
    cbar_labels.sort()
    color_map = dict([(i[1], i[0]) for i in enumerate(cbar_labels)])
    colors = [color_map[i['model']] for i in data]
    x = [i['tot_ssl'] for i in data]
    y = [i['unique_keys'] for i in data]
    return x, y, colors, cbar_labels


def get_app_data_summarys(compromised_app_data):
    results = {}
    # summarize the percent of keys that were discovered
    for rgroup, block in compromised_app_data.items():
        model_name = block['model_name'] if 'model_name' in block else ''
        model_abbr = block['model_abbr'] if 'model_abbr' in block else ''
        factors_str = block['factors_str'] if 'factors_str' in block else ''
        base_location = block['base_location'] if 'base_location' in block else ''
        pct_passwords = []
        tot_sessions = []
        pct_post_datas = []
        pct_session_ids = []
        infos = block['run_values'].values()
        for info in infos:
            tot_sessions.append(info['tot_sessions'])
            pct_post_datas.append(info['pct_post_uri'])
            pct_session_ids.append(info['pct_session_data'])
            pct_passwords.append(info['pct_password_data'])

        results[model_name] = {'mean_total_sessions':int(mean(tot_sessions)),
                               'mean_pct_post_data_discovered':np.mean(pct_post_datas),
                               'mean_pct_passwords_discovered':np.mean(pct_passwords),
                               'mean_pct_sesssion_ids_discovered':np.mean(pct_session_ids),
                               'var_pct_post_data_discovered':np.std(pct_post_datas),
                               'var_pct_passwords_discovered':np.std(pct_passwords),
                               'var_pct_sesssion_ids_discovered':np.std(pct_session_ids),
                               'model_abbr':model_abbr,
                               'model_name':model_name,
                               'base_location':pipes.quote(base_location)
                               }
    return results

INTERESTED_CLASSES = [
    'sun.security.ssl.SSLContextImpl$TLSContext',
    'sun.security.ssl.SSLSessionContextImpl',
    'sun.security.ssl.SSLSessionContextImpl$1',
    'sun.security.ssl.SSLSessionImpl',
    'sun.security.ssl.SSLSocketImpl',
    'java.util.TimerThread',
    'java.lang.Thread',
    'javax.net.ssl.SSLContext',
    'javax.net.ssl.SSLContextSpi',
    'javax.net.ssl.SSLSessionContext',
    'com.sun.crypto.provider.TlsKeyMaterialGenerator',
    'com.sun.crypto.provider.TlsMasterSecretGenerator',
    'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey',
    'com.sun.crypto.provider.TlsPrfGenerator$V12',
    'edu.rice.cs.seclab.dso.ExperimentSession',
    'org.apache.http.client.methods.HttpGet',
    'org.apache.http.client.methods.HttpRequestWrapper',
    'org.apache.http.client.protocol.HttpClientContext',
    'org.apache.http.impl.client.BasicCookieStore',
    'org.apache.http.impl.client.InternalHttpClient',
    'org.apache.http.impl.client.HttpClientBuilder',
    'org.apache.http.impl.client.HttpClientBuilder$2',
    'org.apache.http.impl.io.DefaultHttpRequestWriter',
    'org.apache.http.impl.io.SessionOutputBufferImpl',
]

def read_heap_key_location(base_location):
    java_heap_key_locs_out = sifipr.get_java_heap_key_locations(base_location)
    line = open(java_heap_key_locs_out).readline()
    data = {}
    for e_p in line.split('-'):
        k,v = e_p.strip().split(":")
        data[k] = eval(v)

    tot_ssl = float(data['tot_ssl'])
    # scale values by ssl observed
    for k,v in data.items():
        if k == 'tot_ssl':
            continue
        if k.find("key_") == 0 or k.find("scaled_key_") == 0:
            data['scaled_'+k] = (1.0*v)/tot_ssl
        else:
            data['scaled_'+k] = (1.0*v)/(2*tot_ssl)

    try:
        os.stat(sifipr.get_java_hprof_results(base_location))
        hprof_data = get_extracted_hprof_data(base_location, INTERESTED_CLASSES)
        data.update(hprof_data)
    except:
        pass
    return data

def calculate_heap_prob_info(heap_data):
    stat_info = {}
    data = {}
    data.update(heap_data)
    # scale the data by the total ssl sessions observed
    for k,v in heap_data.items():
        try:
            mn = min(v)
            mx = max(v)
            x_s = 1.0*(mx - mn)
            scaled = [x/x_s for x in v] if x_s > 0 else 0.0
            data[k+"_scaled"] = scaled
        except:
            print "failed to create a scaled value for: %s"%k


    for k,v in data.items():
        try:
            stat_info[k+"_var"] = np.var(v)
            stat_info[k+"_mean"] = np.mean(v)
            stat_info[k+"_std"] =  np.std(v)
        except:
            print "failed to create a scaled value for: %s"%k

    return stat_info

def write_factors_file(base_location):
    factor_values = aed.get_factors(base_location, trim=True)
    open(sifipr.get_factors_filename(base_location), 'w').write('%s'%factor_values)

def write_heap_stats_summary(base_location, sresults):
    summary_files = sifipr.get_java_heap_key_locations_summary(base_location)
    sf_out = open(summary_files, 'w')
    for key in sresults:
        stat_info = sresults[key]['stat_info']
        fmt_data = {}
        fmt_data.update(stat_info)
        fmt_data['model_abbr'] = sresults[key]['model_abbr']
        fmt_data['model_name'] = sresults[key]['model_name']
        fmt_unique_hits = "-".join(["%s:{%s}"%(name, name) for name in fmt_data])
        out = fmt_unique_hits.format(**fmt_data)
        sf_out.write(out+"\n")

def read_heap_summary_data(base_location, is_streaming=False):
    sresults = {}
    sresults_x64 = {}
    tir = "streaming" if is_streaming else "transaction"
    try:
        os.stat(os.path.join(base_location,tir))
        blocs = [os.path.join(base_location,tir),]
    except:
        blocs = [base_location, ]

    heap_data_keys = {}
    bloc_files = []
    for bloc in blocs:
        if os.path.isdir(bloc):
            bloc_files += [os.path.join(bloc, i) for i in os.listdir(bloc) if os.path.isdir(os.path.join(bloc, i))]

    for bloc in bloc_files:
        try:
            use_64, _, jvm_mem, factors = get_run_info(bloc)
        except:
            print ("Attempting to interpret file %s failed"%(bloc))

        use_64, _, jvm_mem, factors = get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        key = "32_%s_%d"%(fstr, jvm_mem) if not use_64 else "64_%s_%d"%(fstr, jvm_mem)

        if not key in  sresults:
            factor_values = get_factors(bloc, trim=True)
            bloc_parameters = {"factors_str":fstr, 'factors':factors,
            'jvm_mem':jvm_mem, 'use_64':use_64, 'run_values':{},
            'group_name':GROUP_MAPPING[fstr] if fstr in GROUP_MAPPING else fstr,
            'model_name':NAME_MAPPING[fstr] if fstr in NAME_MAPPING else fstr,
            'descr_name':DESCR_MAPPING[fstr] if fstr in DESCR_MAPPING else fstr,
            'model_abbr':ABBR_MAPPING[fstr] if fstr in ABBR_MAPPING else fstr,
            'base_location':bloc
            }
            sresults[key] = {}
            sresults[key].update(bloc_parameters)

        agg_data = None
        r_value = None
        try:
            r_value = int(bloc.split("_")[-1])
            agg_data = read_heap_key_location(bloc)
        except:
            print ("Unable to read aggregated data from: %s"%bloc)


        if agg_data is None:
            print ("Unable to read aggregated data from: %s"%bloc)
            continue

        info_dict = agg_data
        sresults[key]['run_values'][r_value] = info_dict


    for key in sresults:
        proto = sresults[key]['run_values'].values()[0] if len(sresults[key]['run_values'].values()) > 0 else None
        if proto is None:
            continue
        heap_data = dict([(k, []) for k in proto.keys()])

        for agg_data in sresults[key]['run_values'].values():
            for k,v in agg_data.items():
                if not k in heap_data:
                    heap_data[k] = []
                heap_data[k].append(v)
        stat_info = calculate_heap_prob_info(heap_data)
        sresults[key]['stat_info'] = stat_info

    return sresults

def get_key_info_summarys(ssl_key_data, count_live=False):
    results = {}
    # summarize the percent of keys that were discovered
    for rgroup, block in ssl_key_data.items():
        model_name = block['model_name'] if 'model_name' in block else ''
        model_abbr = block['model_abbr'] if 'model_abbr' in block else ''
        factors_str = block['factors_str'] if 'factors_str' in block else ''
        base_location = block['base_location'] if 'base_location' in block else ''
        pct_dicovered_keys = []
        pct_pms_keys_discovered = []
        pct_ms_keys_discovered = []
        pct_mkeyblock_keys_discovered = []
        tot_keys = []
        key_infos = block['run_values'].values()
        for key_info in key_infos:
            num_either_key_discovered = key_info['either_hits']
            num_pms_key_discovered = key_info['pms_hits']
            num_ms_key_discovered = key_info['ms_hits']
            num_mkeyblock_key_discovered = key_info['mkeyblock_hits']
            tot_keys.append(key_info['num_ssl'])
            tot_key = key_info['num_ssl']
            pct_dicovered_keys.append(float(num_either_key_discovered) / tot_key)
            pct_pms_keys_discovered.append(float(num_pms_key_discovered) / tot_key)
            pct_ms_keys_discovered.append(float(num_ms_key_discovered) / tot_key)
            pct_mkeyblock_keys_discovered.append(float(num_mkeyblock_key_discovered) / tot_key)
        results[model_name] = {'mean_pct_dicovered_keys':mean(pct_dicovered_keys)*100,
                               'mean_pct_pms_keys_discovered':mean(pct_pms_keys_discovered)*100,
                               'mean_pct_ms_keys_discovered':mean(pct_ms_keys_discovered)*100,
                               'mean_pct_mkeyblock_keys_discovered':mean(pct_mkeyblock_keys_discovered)*100,
                               'mean_total_keys':mean(tot_keys),
                               'model_abbr':model_abbr,
                               'model_name':model_name,
                               'base_location':pipes.quote(base_location)
                               }
    return results


def build_table(ssl_summary_info):
    table_data = []
    keys = ssl_summary_info.keys()
    keys.sort()
    base_location = ""
    nkey_group = []
    last = None
    for k in keys:
        if not last is None and not last[:2] == k[:2]:
            nkey_group.append("\hline")
        nkey_group.append(k)
        last = k

    for k in keys:
        if k == "\hline":
            table_data.append(k)
            continue
        i = ssl_summary_info[k]
        base_location = i['base_location']
        if 'model_name' in i and i['model_name'] in ABBR_MAPPING:
            i['model_abbr'] = ABBR_MAPPING[i['model_name']]
        f = TABLE_LINE.format(**i)
        table_data.append(f)
    data = {"table_data":"\n".join(table_data),
            "base_location":base_location.replace("_", "\\_")}
    table = TABLE_HEADER.format(**data)
    return table

def build_table3(heap_data):
    new_d = {}
    for v in heap_data.values():
        model_name = v['model_name']
        new_d[model_name] = v

    keys = new_d.keys()
    keys.sort()

    new_keys = []
    last = None
    for k in keys:
        if last is None or last[:2] != k[:2]:
            new_keys.append("\hline")
        new_keys.append(k)
        last = k
    table_data = []
    base_location = ''
    for k in new_keys:
        r = {}
        if k == "\hline":
            table_data.append(k)
            continue
        v = new_d[k]

        base_location = v['base_location']
        if 'model_name' in v and v['model_name'] in ABBR_MAPPING:
            v['model_abbr'] = ABBR_MAPPING[v['model_name']]

        model_name = v['model_name']
        scaled_tot_all_hits =               v['stat_info']['scaled_tot_all_hits_mean']
        scaled_tot_unique_mean =     v['stat_info']['scaled_tot_unique_mean']
        scaled_key_tot_unique_mean = v['stat_info']['scaled_key_tot_unique_mean']
        tot_all_hits_std =           v['stat_info']['scaled_tot_all_hits_std']
        scaled_tot_unique_std =      v['stat_info']['scaled_tot_unique_std']
        scaled_key_tot_unique_std =  v['stat_info']['scaled_key_tot_unique_std']
        r['model_name'] = model_name
        r['model_abbr'] = v['model_abbr']
        r['scaled_tot_all_hits'] = 100*scaled_tot_all_hits
        r['scaled_tot_unique_mean'] = 100*scaled_tot_unique_mean
        r['scaled_key_tot_unique_mean'] = 100*scaled_key_tot_unique_mean

        r['scaled_tot_all_hits_std'] = 100*tot_all_hits_std
        r['scaled_tot_unique_std'] = 100*scaled_tot_unique_std
        r['scaled_key_tot_unique_std'] = 100*scaled_key_tot_unique_std

        f = TABLE_LINE3.format(**r)
        table_data.append(f)
    data = {"table_data":"\n".join(table_data),
            "base_location":base_location.replace("_", "\\_")}
    table = TABLE_HEADER3.format(**data)
    return table

def build_table2(application_info):
    table_data = []
    keys = application_info.keys()
    keys.sort()
    base_location = ""
    for k in keys:
        i = application_info[k]
        base_location = i['base_location']
        if 'model_name' in i and i['model_name'] in ABBR_MAPPING:
            i['model_abbr'] = ABBR_MAPPING[i['model_name']]
        f = TABLE_LINE2.format(**i)
        table_data.append(f)
    data = {"table_data":"\n".join(table_data),
            "base_location":base_location.replace("_", "\\_")}
    table = TABLE_HEADER2.format(**data)
    return table

def build_table4(application_info):
    new_d = {}
    for v in application_info.values():
        model_name = v['model_name']
        new_d[model_name] = v

    keys = new_d.keys()
    keys.sort()

    new_keys = []
    last = None
    for k in keys:
        if last is None or last[:2] != k[:2]:
            new_keys.append("\hline")
        new_keys.append(k)
        last = k
    table_data = []

    for k in new_keys:
        r = {}
        if k == "\hline":
            table_data.append(k)
            continue
        i = new_d[k]
        #i = application_info[k]
        base_location = i['base_location']
        if 'model_name' in i and i['model_name'] in ABBR_MAPPING:
            i['model_abbr'] = ABBR_MAPPING[i['model_name']]
        f = TABLE_LINE4.format(**i)
        table_data.append(f)
    data = {"table_data":"\n".join(table_data),
            "base_location":base_location.replace("_", "\\_")}
    table = TABLE_HEADER4.format(**data)
    return table

def get_table(base_location):
    ssl_key_data = read_ssl_key_info([base_location, ], False)
    summary_info = get_key_info_summarys(ssl_key_data)
    table = build_table(summary_info)
    return table

def get_table3(base_location):
    java_heap_data = read_heap_summary_data(base_location)
    table = build_table3(java_heap_data)
    return table

def get_table2(base_location):
    app_data = read_compromised_application_data([base_location, ], False)
    summary_info = get_app_data_summarys(app_data)
    table = build_table2(summary_info)
    return table

def get_table4(base_location):
    app_data = read_compromised_application_data([base_location, ], False)
    summary_info = get_app_data_summarys(app_data)
    table = build_table4(summary_info)
    return table


DESCR_MNAME_MAPPING = {
# server/service
'Service Model #1':"Service with a low session life span and high number of concurrent requests.",
'Service Model #2':"Service with a low session life span and nominal number of concurrent requests.",
'Service Model #3':"Service with a nominal session life span and high number of concurrent requests.",
'Service Model #4':"Service with a nominal session life span and nominal number of concurrent requests.",
# idle client
'Thin Client Model #1':"Thin client with a high session lifespan and low number of concurrent requests.",
'Thin Client Model #2':"Thin client with a nominal session lifespan and low number of concurrent requests.",
'Thin Client Model #3':"Thin client with a low session lifespan and low number of concurrent requests.",
'Thin Client Model #4':"Thin client with a high session lifespan and nominal number of concurrent requests.",
'Thin Client Model #5':"Thin client with a nominal session lifespan and nominal number of concurrent requests.",
'Thin Client Model #6':"Thin client with a low session lifespan and nominal number of concurrent requests.",
# active gaming application
'Active application Model #1':"Active application with a low memory contention with a nominal number of requests and low number of concurrent sessions.",
'Active application Model #2':"Active application with a nominal memory contention with a nominal number of requests and low number of concurrent sessions.",
'Active application Model #3':"Active application with a high memory contention with a nominal number of requests and low number of concurrent sessions.",
}


GROUP_MAPPING = {
# server/service
'HIGH_LOW_LOW_HIGH':"Service Group",
'HIGH_LOW_LOW_MED':"Service Group",
'HIGH_MED_LOW_HIGH':"Service Group",
'HIGH_MED_LOW_MED':"Service Group",
# idle client
'LOW_HIGH_LOW_LOW':"Thin Client Group",
'LOW_MED_LOW_LOW':"Thin Client Group",
'LOW_LOW_LOW_LOW':"Thin Client Group",
'LOW_HIGH_LOW_MED':"Thin Client Group",
'LOW_MED_LOW_MED':"Thin Client Group",
'LOW_LOW_LOW_MED':"Thin Client Group",
'LOW_LOW_LOW_HIGH':"Thin Client Group",
# active gaming application
'LOW_LOW_MED_LOW':"Active Application Group",
'MED_LOW_MED_LOW':"Active Application Group",
'HIGH_LOW_MED_LOW':"Active Application Group",
}


SERVICE_MAPPING = set(['HIGH_LOW_LOW_HIGH', 'HIGH_LOW_LOW_MED',
    'HIGH_MED_LOW_HIGH', 'HIGH_MED_LOW_MED',])
THIN_CLIENT_MAPPING = set(['LOW_HIGH_LOW_LOW', 'LOW_MED_LOW_LOW',
    'LOW_LOW_LOW_LOW', 'LOW_HIGH_LOW_MED',
    'LOW_MED_LOW_MED', 'LOW_LOW_LOW_MED',])
ACTIVE_APPLICATION_MAPPING = set(['LOW_LOW_MED_LOW', 'MED_LOW_MED_LOW',
    'HIGH_LOW_MED_LOW',])

PROCESSES = 40
OUT_LOCK = threading.Lock()
JAVA_DUMP_FINAL = "java-work.dump"
BASE_DIR = None
STRING_OUTPUT_FULL = "ffastrings_len_full.txt"
STDOUT_LOG = "stdout_log.txt"
AUTH_LOG = "authlog.txt"
SSL_MERGED_INFO = "merged_keyinfo.txt"
SSL_FOUND_KEYS = "found_ssl_keys_hits.txt"
SSL_FOUND_KEYS_G = "found_ssl_keys_g.txt"
JSON_DATA = JSON_FILE = "agg_auth_data.json"
MISSING_USERS = "err_missing_users.txt"
JAVA_DUMPS = "java_dumps"
VOL_HOST_PROFILE = 'LinuxUbuntu1404x86'

LOG_OUT_FILE = "stderr_agg_auth_data.txt"
LOG_OUT = None

ERR_FILE = "stderr_agg_auth_data.txt"
ERR_OUT = None
FAIL_STRING = "FAILLAIF" * 3

FIND_POST_RE = re.compile(".*user=.*&password=.*Content-Type.*")
FIND_POST_ALL_RE = re.compile(".*user=.*&password=.*")

MISSING_USERS = "err_missing_users.txt"
BASE_NFS_DIR = None
FAIL_STRING = "FAILLAIF" * 3

AUTH_JSON_OUTFILE = "agg_auth_data.json"
COMPROMISED_SUMMARY = "summary_compromised_data_pct.txt"
GREP_STRING = "username\|password\|Time:\|Starting\|THESESSIONID"
EXPERIMENT_TIME = 240
CUTOFF_TIME = 300
MAX_SESSION_LIFETIME = EXPERIMENT_TIME
MAX_CONCURRENT_SESSIONS = 240
MAX_ALLOWED_REQ = 240

FACTOR_CONV = {"LOW":.1, "MED":.4, "HIGH":.6, 'RANDOM':-1}
SSL_JSON_FILE = 'ssl_info.json'
MAX_INT = 0xffffffff
TIME_LOG = "timelog_start_end_actual.txt"
LINE_LEN = 86
STDOOUT_LINE_LEN = 136#120
STDOOUT_LINE_LEN1 = 131#120
STDOOUT_LINE_LEN2 = 56#120


def get_run_info(base_location):
    base_location = base_location.rstrip('/')
    root, dest = os.path.split(base_location)
    root, _ = os.path.split(base_location)
    results = root
    if root.find('transaction') > 0 or root.find('streaming') > -1:
        results, _ = os.path.split(root)
    mem = int(results.split('_')[-1])
    _factors = dest.split('_')[:4]
    factors = sifipr.get_factors_dict_str(*_factors)
    is_streaming = base_location.find("streaming") > -1
    use_64 = base_location.find('x64') > -1
    return use_64, is_streaming, mem, factors


def load_all_dur_exp(base_locations, is_streaming=False):
    sresults = {}
    sresults_x64 = {}
    tir = "streaming" if is_streaming else "transaction"
    blocs = [os.path.join(base_location,"streaming") for base_location in base_locations]
    streaming = []
    for bloc in blocs:
        streaming += [os.path.join(bloc, i) for i in os.listdir(bloc)]

    for bloc in streaming:
        use_64, _, jvm_mem, factors = get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        agg_data = None
        r_value = None
        try:
            r_value = int(bloc.split("_")[-1])
            agg_data = json.load(open(os.path.join(bloc, JSON_DATA)))
        except:
            print ("Could not extract run value or aggregated data")


        if not fstr in sresults_x64:
            sresults_x64[fstr] = []
        if not fstr in sresults:
            sresults[fstr] = []
        try:
            if use_64:
                sresults_x64[fstr].append((r_value, jvm_mem, agg_data['ssl_quant']['prop_exp_ms_hits'], agg_data['ssl_quant']['prop_exp_pms_hits'], agg_data['ssl_quant']['mean_pms_dur_exp']))
            else:
                sresults[fstr].append((r_value, jvm_mem, agg_data['ssl_quant']['prop_exp_ms_hits'], agg_data['ssl_quant']['prop_exp_pms_hits'], agg_data['ssl_quant']['mean_pms_dur_exp']))
        except:
            print (bloc+"missing a key or value")
    return sresults, sresults_x64

def read_compromised_application_data(base_locations, is_streaming=False):
    sresults = {}
    sresults_x64 = {}
    tir = "streaming" if is_streaming else "transaction"
    blocs = [os.path.join(base_location,tir) for base_location in base_locations]
    bloc_files = []
    for bloc in blocs:
        if os.path.isdir(bloc):
            bloc_files += [os.path.join(bloc, i) for i in os.listdir(bloc) if os.path.isdir(os.path.join(bloc, i))]

    for bloc in bloc_files:
        use_64, _, jvm_mem, factors = get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        key = "32_%s_%d"%(fstr, jvm_mem) if not use_64 else "64_%s_%d"%(fstr, jvm_mem)

        if not key in  sresults:
            bloc_parameters = {"factors_str":fstr, 'factors':factors,
            'jvm_mem':jvm_mem, 'use_64':use_64, 'run_values':{},
            'group_name':GROUP_MAPPING[fstr] if fstr in GROUP_MAPPING else fstr,
            'model_name':NAME_MAPPING[fstr] if fstr in NAME_MAPPING else fstr,
            'descr_name':DESCR_MAPPING[fstr] if fstr in DESCR_MAPPING else fstr,
            'model_abbr':ABBR_MAPPING[fstr] if fstr in ABBR_MAPPING else fstr,
            'base_location':bloc
            }
            sresults[key] = {}
            sresults[key].update(bloc_parameters)

        agg_data = None
        r_value = None
        tot_sessions = 0
        try:
            r_value = int(bloc.split("_")[-1])
            agg_data = get_auth_data(bloc)
            agg_data_counts = [len(agg_data['post_uri_infos'].keys()),
                len(agg_data['cookie_sessionid_infos'].keys()),
                len(agg_data['password_infos'].keys()),
            ]
            tot_sessions = max(agg_data_counts)
            app_data = get_compromised_summary_data(bloc)
        except:
            print ("Unable to read aggregated data from: %s"%bloc)
            continue

        if app_data is None:
            print ("Unable to read aggregated data from: %s"%bloc)
            continue
        app_data['tot_sessions'] = tot_sessions
        info_dict = app_data
        sresults[key]['run_values'][r_value] = info_dict
    return sresults

def read_ssl_key_info(base_locations, is_streaming=False):
    sresults = {}
    sresults_x64 = {}
    tir = "streaming" if is_streaming else "transaction"
    blocs = [os.path.join(base_location,tir) for base_location in base_locations]
    bloc_files = []
    for bloc in blocs:
        if os.path.isdir(bloc):
            bloc_files += [os.path.join(bloc, i) for i in os.listdir(bloc) if os.path.isdir(os.path.join(bloc, i))]

    for bloc in bloc_files:
        use_64, _, jvm_mem, factors = get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        key = "32_%s_%d"%(fstr, jvm_mem) if not use_64 else "64_%s_%d"%(fstr, jvm_mem)

        if not key in  sresults:
            bloc_parameters = {"factors_str":fstr, 'factors':factors,
            'jvm_mem':jvm_mem, 'use_64':use_64, 'run_values':{},
            'group_name':GROUP_MAPPING[fstr] if fstr in GROUP_MAPPING else fstr,
            'model_name':NAME_MAPPING[fstr] if fstr in NAME_MAPPING else fstr,
            'descr_name':DESCR_MAPPING[fstr] if fstr in DESCR_MAPPING else fstr,
            'model_abbr':ABBR_MAPPING[fstr] if fstr in ABBR_MAPPING else fstr,
            'base_location':bloc
            }
            sresults[key] = {}
            sresults[key].update(bloc_parameters)

        agg_data = None
        r_value = None
        try:
            r_value = int(bloc.split("_")[-1])
            agg_data = get_auth_data(bloc)
        except:
            print ("Unable to read aggregated data from: %s"%bloc)
            continue

        if agg_data is None:
            print ("Unable to read aggregated data from: %s"%bloc)
            continue

        info_dict = get_key_info_profile(bloc, agg_data)
        sresults[key]['run_values'][r_value] = info_dict
    return sresults

def read_all_key_values(base_locations, main_key, value_keys=[], is_streaming=False, default_dead_values = {}):
    sresults = {}
    sresults_x64 = {}
    tir = "streaming" if is_streaming else "transaction"
    blocs = [os.path.join(base_location,tir) for base_location in base_locations]
    bloc_files = []
    for bloc in blocs:
        if os.path.isdir(bloc):
            bloc_files += [os.path.join(bloc, i) for i in os.listdir(bloc) if os.path.isdir(os.path.join(bloc, i))]

    for bloc in bloc_files:
        use_64, _, jvm_mem, factors = get_run_info(bloc)
        fstr = sifipr.get_factors_str(factors)
        key = "32_%s_%d"%(fstr, jvm_mem) if not use_64 else "64_%s_%d"%(fstr, jvm_mem)

        if not key in  sresults:
            bloc_parameters = {"factors_str":fstr, 'factors':factors,
            'jvm_mem':jvm_mem, 'use_64':use_64, 'run_values':{},
            'group_name':GROUP_MAPPING[fstr] if fstr in GROUP_MAPPING else fstr,
            'model_name':NAME_MAPPING[fstr] if fstr in NAME_MAPPING else fstr,
            'descr_name':DESCR_MAPPING[fstr] if fstr in DESCR_MAPPING else fstr,
            'model_abbr':ABBR_MAPPING[fstr] if fstr in ABBR_MAPPING else fstr,
            }
            sresults[key] = {}
            sresults[key].update(bloc_parameters)

        agg_data = None
        r_value = None
        try:
            r_value = int(bloc.split("_")[-1])
            agg_data = json.load(open(os.path.join(bloc, JSON_DATA)))
        except:
            traceback.print_exc()
            print ("Could not extract run value or aggregated data")
            continue

        values = agg_data[main_key].values()
        act_values = []
        for value in values:
            vals = {'path':bloc}
            for k in value_keys:
                vals[k] = value[k] if k in value else default_dead_values.get(k, -1)
            act_values.append(vals)

        sresults[key]['run_values'][r_value] = act_values
    return sresults

def inventory_failed_agg(base_location_dir):
    missing_time_logs = []
    missing_agg_logs = []
    base_locations = [os.path.join(base_location_dir, i) for i in os.listdir(base_location_dir)]
    for base_location in base_locations:
        if not os.path.isdir(base_location):
            continue

        files = set(os.listdir(base_location))
        if not JSON_DATA in files:
            missing_agg_logs.append(base_location)
        if not TIME_LOG in files:
            missing_time_logs.append(base_location)
    return missing_time_logs, missing_agg_logs

def get_key_info_profile(base_location, auth_data=None):
    auth_data = get_auth_data(base_location) if auth_data is None else auth_data
    num_ssl = len(auth_data['ssl_infos'])
    num_expired = len(auth_data['matched_comms'])
    key_infos = auth_data['matched_comms']
    mkeyblock_hits = 0
    for v in key_infos:
        if v['mkeyblock_hit'] > 0:
            mkeyblock_hits += 1

    pms_hits = 0
    for v in key_infos:
        if v['pms_hit'] > 0:
            pms_hits += 1

    both_hits = 0
    for v in key_infos:
        if v['pms_hit'] > 0 and v['mkeyblock_hit'] > 0:
            both_hits += 1

    all_hits = 0
    for v in key_infos:
        if v['ms_hit'] > 0 and v['pms_hit'] > 0 and v['mkeyblock_hit'] > 0:
            all_hits += 1

    ms_hits = 0
    for v in key_infos:
        if v['ms_hit'] > 0:
            ms_hits += 1

    either_hits = 0
    for v in key_infos:
        if v['mkeyblock_hit'] > 0 or v['ms_hit'] > 0 or v['pms_hit'] > 0:
            either_hits += 1

    info_dict = {'either_hits':either_hits,
                 'pms_hits':pms_hits, 'ms_hits':ms_hits,
                 'mkeyblock_hits':mkeyblock_hits,
                 'both_hits':both_hits,
                 'all_hits':all_hits,
                 'expired':num_expired, 'num_ssl':num_ssl}
    return info_dict

def generate_memory_image_ranges(runs):
    file_ranges = []
    mappings = []
    for i in runs:
        base, offset, sz = i
        mapping = {'file':[offset, offset+sz],
            'mem':[base, base+sz], 'sz':sz}
        mappings.append(mapping)
    return mappings

def file_addr_in_memory(foffset, mappings=[]):
    for v in mappings:
        frange = v['file']
        if foffset_in_range(foffset, frange):
            return frange[0], frange[1], v['sz']
    return None

def foffset_in_range(foffset, frange):
    return frange[0] <= foffset and foffset < frange[1]

def convert_offset(foffset, frange, prange):
    offset = foffset - frange[0]
    return offset+prange[0]

def get_phys_addr(foffset, mappings):
    for v in mappings:
        frange = v['file']
        prange = v['mem']
        if foffset_in_range(foffset, frange):
            return convert_offset(foffset, frange, prange)
    return None

def scale_timestamps_comms(comm_data, times):
    data_times = [i['timestamp'] for i in comm_data]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta
    for i in comm_data:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts

def scale_timestamps_http_values(http_values, times):
    data_times = [i['timestamp'] for i in http_values]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta

    for i in http_values:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts
        if i['end'] != -1:
            ts = host_start + long(scale * (i['end'] - min_ts))
            i['unscaled_end'] = i['end']
            i['end'] = ts
            i['age'] = i['end'] - i['timestamp']

        ts = 0
        _ts = 0
        for e in i['events']:
            if not 'timestamp' in e:
                e['timestamp'] = ts
                e['unscaled_timestamp'] = ts

            ts = host_start + long(scale * (e['timestamp'] - min_ts))
            _ts = e['unscaled_timestamp'] = e['timestamp']
            e['timestamp'] = ts

def scale_timestamps_ssl_values(ssl_values, times):
    data_times = [i['timestamp'] for i in ssl_values]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta

    for i in ssl_values:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts
        i['end'] = -1


def get_found_keys_greppable_data(base_location):
    return [parse_found_keys_g(i.strip()) for i in
            open(os.path.join(base_location, SSL_FOUND_KEYS_G)).readlines()]

def parse_found_keys_g(line):
    fname, loc, data = line.split()
    fname.strip(":")
    loc = long(loc, 16)
    return {"file":fname, "offset":loc, "key":data}

def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

def get_times(base_location):
    data = open(sifipr.get_time_log(base_location)).read().strip()
    start, end, act_end = [long(i, 16) for i in data.split(',')]
    return {'start':start, 'end':act_end, 'act_end':act_end}

def get_host_guest_time_delta(host_vm_start, guest_vm_start_time):
    return host_vm_start - guest_vm_start_time

def get_host_guest_time(guest_vm_time, delta):
    return guest_vm_time + delta

def log_err(msg):
    global BASE_DIR, ERR_OUT
    if ERR_OUT is None:
        try:
            ERR_OUT = open(os.path.join(BASE_DIR,ERR_FILE), 'a')
        except:
            print (msg)
            return
    OUT_LOCK.acquire()
    ERR_OUT.write(msg+'\n')
    OUT_LOCK.release()

def log_out(msg):
    global BASE_DIR, LOG_OUT, LOG_OUT_FILE, OUT_LOCK
    if LOG_OUT is None:
        try:
            LOG_OUT = open(os.path.join(BASE_DIR,LOG_OUT_FILE), 'a')
        except:
            print msg
            return
    OUT_LOCK.acquire()
    print(msg)
    OUT_LOCK.release()

def get_java_dump_location(factors):
    java_base = get_dir_name_from_factors(factors)
    return os.path.join(java_base, JAVA_DUMP_FINAL)

def get_vol_java_dump_location(factors):
    java_base = get_dir_name_from_factors(factors)
    return "file:///" + os.path.join(java_base, JAVA_DUMP_FINAL)

def dump_process(filename, dump_dir):
    profile = VOL_HOST_PROFILE
    ex_java = None
    try:
        ex_java = ExtractProc(the_file=filename, profile=profile)
        ex_java.update_process_info()
    except:
        msg = "%s: Failed to find Java in %s "%(time_str(), filename)
        log_err(msg)
        return

    chunks = MemChunk.chunks_from_task_or_file(task=ex_java.proc,
                                            MChunkCls=MemChunk)
    for chunk in chunks.values():
        chunk.check_load()

    for chunk in chunks.values():
        chunk.dump_data(outdir=dump_dir)


def perform_long_way (base_location):
    java_dumps = os.path.join(base_location, JAVA_DUMPS)
    java_dump_files = [os.path.join(java_dumps, k) for k in os.listdir(java_dumps)]

    strings_location = os.path.join(base_location, STRING_OUTPUT)
    cmd_fmt_0 = "strings -n 8 -t x -f %s > %s"
    cmd_fmt_1 = "strings -n 8 -t x -f %s >> %s"
    cmds = [cmd_fmt_0%(java_dump_files[0], strings_location)]
    if len (cmds) == 0:
        return -1
    for f in java_dump_files[1:]:
        cmds.append(cmd_fmt_1%(f, strings_location))

    #print ("Executing: %s"%(str(cmd)))
    for cmd in cmds:
        try:
            result = os.system(cmd)
            if result != 0:
                print ("[------] Failed: %s"%(str(cmd)))
        except KeyboardInterrupt:
            break
    return result

def try_long_hex (v):
    try:
        return long(v, 16)
    except:
        return None

def try_long (v):
    try:
        return long(v)
    except:
        return None

def parse_log_event(line):
    event = {}
    set_break = False
    pos = 0
    values = line.split('-')
    for i in values:
        if set_break:
            break
        vals = i.split(':')
        if len(vals) < 2:
            continue
        k = vals[0].lower()
        v = vals[1]
        #print k,v, len(vals)
        if k.find('session') == 0 and len(v) >= 12:
            v = v[:12]
            set_break = True
        elif k.find('steps') == 0 and len(v) >= 4:
            v = v[:4]
            set_break = True
        elif k.find('gc_next_time') == 0 and len(v) >= 11:
            v = v[:11]
            set_break = True
        elif k.find('next_gc_time') == 0 and len(v) >= 11:
            v = v[:11]
            set_break = True
        elif k.find('expired') == 0 and len(v) >= 2:
            v = v[:2]
            #set_break = True
        else:
            v = ":".join(vals[1:])

        if k == 'time':
            v = try_long_hex(v)
            if v is None:
                break
            k = 'timestamp'
        elif k == 'min':
            v = long(v, 16)
        elif k == 'steps':
            v = long(v, 16)
        elif k == 'max':
            v = long(v, 16)
        elif k == 'completed':
            v = bool(long(v,16))
        elif k == 'maxed':
            v = bool(long(v,16))
        elif k == 'session':
            k = 'user'
        elif k == 'mem' or k == 'smem':
            v = long(v, 16)
        elif k == 'gc_called':
            v = long(v)
        elif k == 'next_gc_time':
            v = long(v, 16)
        elif k == 'expired':
            v = bool(long(v, 16))
        event[k] = v
    return event


def create_event_json(line):
    event = {}
    if line.find("Starting-Time:") == 0 and\
       line.find("-Session:") > -1 and\
       line.find("-Session:") +12 < len(line):
        event['starting'] = True
        line = line[len("Starting:"):]

    if line.find('Time:') == 0:
        e = parse_log_event(line)
        event.update(e)
    return event




def extract_auth_info_from_log(line):
    v = line.strip().split(',')
    if len(v) == 4:
        try:
            long(v[0], 16)
            return v
        except:
            return None
    else:
        return None

def form_post_uri(user, password):
    post_uri_fmt = "user=%s&password=%s"
    return post_uri_fmt%( urllib.quote_plus(user), urllib.quote_plus(password))

TIME="Time:"
STARTTING_TIME="Starting-Time:"
SESSION = "-Session:"
ERROR = "Error: "
EXCEPTION = " org.apache.http.conn.ConnectTimeoutException: Connect to"
def clean_stdout(stdout_filename):
    lines = [i.rstrip() for i in open(stdout_filename).readlines()]
    clean_lines = []
    for line in lines:
        if line.find(STARTTING_TIME) > 0 and line.find("-Session:") > line.find(STARTTING_TIME):
            end_len = len(SESSION) + 12
            if line.find(SESSION) + end_len > len(line):
                clean_lines.append("<<<< "+line)
                continue
            newl = line.split(STARTTING_TIME)
            clean_lines.append(newl[0])
            for l in newl[1:]:
                clean_lines.append(STARTTING_TIME+l)
        elif line.find(STARTTING_TIME) == -1 and line.find(TIME) > 0:
            newl = line.split(TIME)
            clean_lines.append(newl[0])
            for l in newl[1:]:
                clean_lines.append(TIME+l)
        elif line.find(EXCEPTION) > 0 and line.find(ERROR) != 0:
            newl = line.split(ERROR)
            clean_lines.append(newl[0])
            for l in newl[1:]:
                clean_lines.append(ERROR+l)
        else:
            clean_lines.append(line)
    return len(lines) == len(clean_lines), clean_lines

def clean_up_gclog_data(gclog_filename):
    lines = [i.rstrip() for i in open(gclog_filename).readlines()]
    clean_lines = []
    pos = 0
    while pos < len(lines):
        line = lines[pos]
        line = line.strip('\x00')
        if line.find("ObjAddr:") > 0:
            line = line.split("Time:")[0]
            if pos+1 < len(lines) and lines[pos+1].find("-600") == -1:
                line = line+lines[pos+1]
            clean_lines.append(line)
            pos += 2
            continue
        elif line.find("Time:") > 0 and line.find("Space:") > 0:
            line = line.split("Time:")[0]
        clean_lines.append(line)
        pos +=1
    return len(lines) == len(clean_lines), clean_lines

def read_event_information(base_location, factors):
    auth_filename = os.path.join(base_location, AUTH_LOG)
    auth_data = [i.strip().split(',') for i in open(auth_filename).read().splitlines() if len(i.strip()) > 0]
    comm_steps = set()
    jsonified = {'http_values':{}, 'http_ordered_comms':[], }
    logfile_fmt = "%08x,%s,%s,%s"
    cookie_sessionid_fmt = "Cookie: THESESSIONID=%s"
    timestamps = set()
    for i in auth_data:
        d = {'end':-1, 'last':long(i[0], 16), 'timestamp':long(i[0], 16), "user":i[1], "password":i[2], 'logentry':'',
             'sessionid':i[3], 'events':[], 'shadow':False, 'requires_update':False,
             'completed':False, 'live':True, 'age':0, 'expired':False, 'error':False}
        timestamps.add(d['timestamp'])
        entry = logfile_fmt%(d['timestamp'], d['user'], d['password'], d['sessionid'])
        d['logentry'] = entry
        d['post_uri'] = form_post_uri(i[1], i[2])
        d['cookie_sessionid'] = cookie_sessionid_fmt%i[3]
        event = {'timestamp':d['timestamp'], 'min':0, 'max':0, 'steps':0, 'completed':False, 'user':d['user']}
        d['events'].append(event)
        jsonified['http_values'][d['user']] = d

    find_error = lambda line: line.find(" org.apache.http.conn.ConnectTimeoutException: Connect to") > 0 and\
                              line.find('Error: ') == 0
    stdout_filename = os.path.join(base_location, STDOUT_LOG)
    t, stdout_lines = clean_stdout(stdout_filename)
    if not t:
        print "%s: STDOUT Log has other information in it: %s."%(time_str(), stdout_filename)
    stdout_data = [i.strip() for i in stdout_lines if len(i.strip()) > 0 and (find_error(i) or i.find("Time:") == 0 or i.find("Starting-Time:") == 0)]
    for line in stdout_data:
        event = {}
        if find_error(line) and len(line.split(':')) > 2:
            user = line.split(':')[2]
            user.strip()
            event = {'error':True, 'statement':line}
            if user in jsonified['http_values']:
                jsonified['http_values'][user]['error'] = True
                jsonified['http_values'][user]['completed'] = True
                jsonified['http_values'][user]['expired'] = True
                jsonified['http_values'][user]['events'].append(event)
            else:
                d = {'end':-1, 'last':-1, 'timestamp':-1, "user":user, "password":"", 'logentry':'',
                'sessionid':'', 'events':[event], 'shadow':False, 'requires_update':False,
                'completed':False, 'live':False, 'age':0, 'expired':False, 'error':True}
                jsonified['http_values'][user] = d
            continue

        event = create_event_json(line)
        user = event.get('user', None)
        expired = event.get('expired', False)
        completed = event.get('completed', False) or expired
        starting = event.get('starting', False)
        if not starting and 'steps' in event:
            step = event['steps']
            timestamps.add(event['timestamp'])
            key = "%s:%d"%(user, step)
            if not key in comm_steps:
                jsonified['http_ordered_comms'].append({'timestamp':event['timestamp'], 'user':user, 'step':step})
                comm_steps.add(key)
        if user and user in jsonified['http_values'] and jsonified['http_values'][user]['completed']:
            print("%s: %s found event after %s completed."%(time_str(), base_location, user))
            continue
        if starting and user and user in jsonified['http_values']:
            timestamps.add(event['timestamp'])
            info = jsonified['http_values'][user]
            info['end'] = event['timestamp'] if completed or expired else -1
            info['timestamp'] = event['timestamp']
            info['last'] = event['timestamp']
            info['completed'] = False
            info['age'] = info['last'] - info['timestamp']
            info['expired'] = expired
            info['events'].append(event)
        elif user and user in jsonified['http_values']:
            timestamps.add(event['timestamp'])
            info = jsonified['http_values'][user]
            #if not completed:
            info['events'].append(event)
            info['end'] = event['timestamp'] if completed or expired else -1
            info['last'] = event['timestamp']
            info['completed'] = completed or expired or info['completed']
            info['live'] = not completed or not expired or not info['completed'] or not info['expired']
            info['expired'] = expired or info['expired']
            info['age'] = info['last'] - info['timestamp']
        elif user and len(i) > 3:
            timestamps.add(event['timestamp'])
            d = {'end':event['timestamp'], 'last':event['timestamp'], 'timestamp':event['timestamp'], "user":user, "password":None, 'logentry':'',
             'sessionid':i[3], 'events':[event], 'shadow':True, 'requires_update':True, 'completed':completed, 'expired':expired,
             'cookie_sessionid':'','post_uri':'', 'live':(not completed or not expired), 'age':0, 'error':False}
            jsonified['http_values'][user] = d
            d['cookie_sessionid'] = cookie_sessionid_fmt%i[3]
            d['end'] = event['timestamp'] if completed or expired else -1
            ut = "%s: [----] %s::%s not found in the authlog, potential race condition line: %s"%(time_str(), base_location, user, line)
            open(os.path.join(base_location, MISSING_USERS), 'a').write(ut+'\n')
            #print (ut)
    max_time = max(timestamps)
    min_time = min(timestamps)
    jsonified['max_time'] = max_time
    jsonified['min_time'] = min_time
    jsonified['max_age'] = (max_time-min_time)/1000
    return jsonified

def update_auth_user_password_from_post_uri(candidates, auth_value):
    user = auth_value['user']
    for str_dict in candidates:
        v = str_dict['value']
        t = extract_user_password_from_string(v)
        if t:
            auth_value['password'] = t[1]
            auth_value['post_uri'] = form_post_uri(auth_value['user'], auth_value['password'])
            return True
    return False

def extract_user_password_from_string(line):
    s = FIND_POST_ALL_RE.match(line)
    if s:
        data = line[s.start():s.end()].split("Content-Type:")[0]
        user = data.split("user=")[1].split("&password=")[0]
        password = urllib.unquote(data.split("&password=")[1])
        return urllib.unquote(user), password[:20]
    return None

def update_auth_value_from_strings(candidates, auth_value):
    logfile_fmt = "%08x,%s,%s,%s"
    user = auth_value['user']
    for str_dict in candidates:
        v = str_dict['value']
        value = extract_auth_info_from_log(v)
        if value and value[1] == user :
            timestamp = long(value[0], 16)
            if timestamp < auth_value['timestamp'] and auth_value['timestamp'] > 0:
                auth_value['timestamp'] = timestamp
            if timestamp > auth_value['last']:
                auth_value['last'] = auth_value['timestamp']
            if 'completed' in value:
                completed = bool(long(v,16))
                if completed:
                    auth_value['live'] = not completed
                    auth_value['end'] = timestamp
                    auth_value['completed'] = completed
            auth_value['age'] = auth_value['last'] - auth_value['timestamp']
            auth_value['password'] = value[2]
            auth_value['sessionid'] = value[3]
            auth_value['post_uri'] = form_post_uri(value[1], value[2])
            entry = logfile_fmt%(auth_value['timestamp'], auth_value['user'], auth_value['password'], auth_value['sessionid'])
            auth_value['logentry'] = entry
            auth_value['requires_update'] = False
            break
    return not auth_value['requires_update']



def update_shadow_auth_info(strings_data, auth_values):
    update_values = [i for i in auth_values.values() if i['requires_update']]
    if len(update_values) == 0:
        return auth_values
    print("%s: Updating shadow auth information for %d records"%(time_str(), len(update_values)))
    strings_value = strings_data['full_dump']
    candidates_logs = [i for i in strings_value.values() if i['str_len']==86]
    for auth_value in update_values:
        r = update_auth_value_from_strings (candidates_logs, auth_value)
        if not r:
            auth_value['password'] = FAIL_STRING
            auth_value['sessionid'] = FAIL_STRING
            auth_value['logentry'] = FAIL_STRING
            auth_value['post_uri'] = FAIL_STRING

    if len(update_values) == 0:
        return auth_values

    update_values = [i for i in auth_values.values() if i['requires_update']]
    candidates_post_uri = [i for i in strings_value.values() if i['value'].find('user=') > -1]
    for i in update_values:
        update_auth_user_password_from_post_uri(candidates_post_uri, auth_value)
    return auth_values

def parse_strings_line(line):
    items = line.split()
    filename, offset, str_len, data = ('', -1,-1,'')
    filename = items[0].strip(':')
    try:
        if len(items) > 1:
            offset = long(items[1].strip() , 16)
        if len(items) > 2:
            str_len = long(items[2].strip() , 16)
        if len(items) > 3:
            data = " ".join(line.split(" ")[3:])
    except:
        print ("Failed to process line: %s"%line)

    if str_len == -1 or offset == -1:
        print ("Failed to process line: %s"%line)

    return {'filename':filename, "offset":offset, "str_len":str_len, "value":data}

def scan_auth_data(strings_full_data, auth_data):
    auth_set = generate_auth_set(auth_data)
    this_data = []
    for v in strings_full_data:
        e = v['value']
        if e.find('Time:') > -1 or e.find('Starting:') > -1:
            this_data.append(v)
            continue
        if e.find("Cookie") > -1 or e.find('user') > -1 or e.find('password') > -1:
            this_data.append(v)
            continue
        if e.find("THESESSIONID") > -1:
            this_data.append(v)
            continue
        if e.find("=") > -1:
            this_data.append(v)
            continue
        if e.find(",") > -1 and len(set(e.split(',')) & auth_set) > 0:
            this_data.append(v)
            continue
    return this_data

def get_auth_data_passwords_sessionids(auth_data):
    this_data = []
    if not 'http_values' in auth_data:
        return this_data
    for i in auth_data['http_values'].values():
        this_data.append((i['password'], i['sessionid']))
    return this_data

def perform_grep_log_strings(base_location):
    strings_full_filename = os.path.join(base_location, STRING_OUTPUT_FULL)
    tmp_file = os.path.join(base_location, "GREP_RESULTS.txt")
    #grep_cmd = 'grep -e "%s" %s > %s'%(GREP_STRING, strings_full_filename, tmp_file)
    data = sifipr.exec_grep(GREP_STRING, strings_full_filename)
    #data = open(tmp_file).read()
    print ("%s: Done grepping strings"%(time_str()))
    return data.splitlines()

def get_strings_data_with_grep(base_location, auth_data):
    data1 = perform_grep_log_strings(base_location)
    #data2 = perform_grep_passwords(base_location, auth_data)
    data2 = read_heuristic_lines(base_location)
    d = set(data1+data2)
    strings_full_data = [parse_strings_line(i.strip()) for i in d]
    return strings_full_data


def read_heuristic_lines(base_location):
    strings_full_filename = os.path.join(base_location, STRING_OUTPUT_FULL)
    results = []
    data = [i.strip() for i in open(strings_full_filename).readlines()]
    for line in data:
        vals = line.split()
        if len(vals) > 4:
            continue
        elif len(vals) == 4 and len(vals[3]) < 12:
            continue
        elif len(vals) > 4 and len(vals[3]) + len(vals[4]) < 12:
            continue
        sz = int(vals[2], 16)
        if (sz >= 12 and sz <= 80) or sz == 86:
            results.append(line)
    print ("%s: Done gretting strings"%(time_str()))
    return results

def perform_grep_passwords(base_location, auth_data):
    strings_full_filename = os.path.join(base_location, STRING_OUTPUT_FULL)
    passwords_sessionids = get_auth_data_passwords_sessionids(auth_data)
    this_data = []
    for password, sessionid in passwords_sessionids:
        tmp_file = os.path.join(base_location, "GREP_RESULTS.txt")
        data = sifipr.exec_grep(GREP_STRING, strings_full_filename)
        this_data.append(data.splitlines())
    return list(itertools.chain(*this_data))


def read_strings_information(base_location, auth_data={}):
    strings_full_filename = os.path.join(base_location, STRING_OUTPUT_FULL)
    strings_full_data = None
    try:
        #strings_full_data = [parse_strings_line(i.strip()) for i in open(strings_full_filename).read().splitlines() if len(i.strip()) > 0]
        strings_full_data = get_strings_data_with_grep(base_location, auth_data)
    except:
        log_err("%s: %s had an error with reading %s"%(time_str(), base_location, strings_full_filename))
        print("%s: %s had an error with reading %s"%(time_str(), base_location, strings_full_filename))
        raise
    #this_data = scan_auth_data(strings_full_data, auth_data)
    return {'full_dump':dict([ (i['offset'], i) for i in strings_full_data]),}


def create_logentry_term_set (auth_data):
    return set(i['logentry'] for i in auth_data['http_values'].values() if 'logentry' in i)

def create_cookie_sessionid_term_set (auth_data):
    return set(i['cookie_sessionid'] for i in auth_data['http_values'].values() if 'cookie_sessionid' in i)

def create_sessionid_term_set (auth_data):
    return set(i['sessionid'] for i in auth_data['http_values'].values() if 'sessionid' in i)

def create_password_term_set (auth_data):
    return set(i['password'] for i in auth_data['http_values'].values() if 'password' in i)

def create_post_uri_term_set (auth_data):
    return set(i['post_uri'] for i in auth_data['http_values'].values() if 'post_uri' in i)

def build_keyed_db (auth_data):
    keyed_db = {}
    for rec in auth_data.values():
        keyed_db[rec['logentry']] = rec
        keyed_db[rec['password']] = rec
        keyed_db[rec['post_uri']] = rec
    return keyed_db

def get_result_keys(auth_data):
    data = None
    for data in auth_data.values():
        break
    return [i for i in data.keys() if i.find('results') > -1]


def search_data(data_set, term_set):
    results = dict([(i, 0) for i in term_set])
    for rec in data_set.values():
        if 'value' in rec and rec['value'] in term_set:
            results[rec['value']] += 1
    return results

def check_post_uri_and_other(data_set, term_set):
    results = dict([(i, 0) for i in term_set])
    for rec in data_set.values():
        if 'value' in rec and rec['value'] in term_set:
            results[rec['value']] += 1
        elif FIND_POST_ALL_RE.match(rec['value']):
            t = extract_user_password_from_string(rec['value'])
            p = form_post_uri(t[0], t[1])
            if not p in results:
                results[p] = 0
            results[p] += 1
    return results

def search_logfile_data(strings_data, auth_data):
    # log entries
    term_set = create_logentry_term_set(auth_data)
    # find occ in logfile set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_logentry_results'] = full_results

def search_post_uri_data(strings_data, auth_data):
    # post uris
    term_set = create_post_uri_term_set(auth_data)
    full_results = check_post_uri_and_other(strings_data['full_dump'], term_set)#search_data(strings_data['full_dump'], term_set)
    auth_data['full_post_uri_results'] = full_results

def search_password_data(strings_data, auth_data):
    # Password terms
    term_set = create_password_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_password_results'] = full_results


def search_cookie_sessionid_data(strings_data, auth_data):
    # Password terms
    term_set = create_cookie_sessionid_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_cookie_sessionid_results'] = full_results


def search_sessionid_data(strings_data, auth_data):
    # Password terms
    term_set = create_sessionid_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_sessionid_results'] = full_results

def perform_data_search(base_location, strings_data, auth_data):
    print("%s: performing password search in %s"%(time_str(), base_location))
    search_password_data(strings_data, auth_data)
    print("%s: performing post uri search in %s"%(time_str(), base_location))
    search_post_uri_data(strings_data, auth_data)
    print("%s: performing logfile search in %s"%(time_str(), base_location))
    search_logfile_data(strings_data, auth_data)
    print("%s: performing sessionid search in %s"%(time_str(), base_location))
    search_sessionid_data(strings_data, auth_data)
    print("%s: performing cookie sessionid search in %s"%(time_str(), base_location))
    search_cookie_sessionid_data(strings_data, auth_data)
    print("%s: [++] Searches complete for %s"%(time_str(), base_location))

def perform_results_write(base_location, auth_data):
    json_file = os.path.join(base_location, JSON_FILE)
    json.dump(auth_data, open(json_file, 'w'))
    missing_data = get_missing_data(auth_data)
    write_augmented_data(base_location, missing_data)

def match_http_with_ssl_keys(auth_data, times, is_streaming=False):
    host_start = times['start']
    host_end = times['act_end']
    ssl_comms = auth_data['ssl_ordered_comms']
    scale_timestamps_comms(ssl_comms, times)
    ssl_comms = sorted(ssl_comms, key=lambda x: x['timestamp'])
    http_comms = auth_data['http_ordered_comms']
    scale_timestamps_comms(http_comms, times)
    http_comms = sorted(http_comms, key=lambda x: x['timestamp'])
    if is_streaming:
        http_comms = [i for i in http_comms if 'steps' in i and i['steps'] == 1]
    # num ssl keys should always be greater than http
    end = max(len(http_comms), len(ssl_comms))
    s_pos = 0
    h_pos = 0
    h_end = len(http_comms)
    s_end = len(ssl_comms)
    matched_comms = []
    while (h_pos < h_end and s_pos < s_end and s_pos < end and h_pos < end):
    #for pos in xrange(0, end):
        m = {}
        s = ssl_comms[s_pos]
        h = http_comms[h_pos]
        age = -1
        live = is_streaming and h['timestamp'] != -1
        # these were normalized to 0 so they should be close to correct
        # http is always reported after the communication takes place
        # assuming the ssl key is created before the session ends
        m['plausible_match'] = False
        if h['timestamp'] >= s['timestamp']:
            age = abs(h['timestamp'] - s['timestamp'])
            if age == 0:
                age = 1
            m['plausible_match'] = True
            h_pos += 1
            s_pos += 1
        elif h['timestamp'] < s['timestamp']:
            h_pos += 1
            continue

        #  update with http last, b/c that is the timestamp we care about
        # both http and ssl should have the same timestamp
        m.update(s)
        m.update(h)
        m['ssl_timestamp'] = s['timestamp']
        m['http_timestamp'] = h['timestamp']
        m['unscaled_ssl_timestamp'] = s['unscaled_timestamp']
        m['unscaled_http_timestamp'] = h['unscaled_timestamp']
        m['timestamp'] = s['timestamp']
        m['end'] = h['timestamp'] - host_start
        m['start'] = s['timestamp'] - host_start
        m['dur_exp'] = host_end - h['timestamp']
        # update these after finalizing the https data
        m['live'] = live
        m['age'] = age

        matched_comms.append(m)
    return matched_comms

def generate_auth_set(auth_data):
    auth_set = set()
    for i in auth_data['http_values'].values():
        auth_set.add(i['user'])
        auth_set.add(i['password'])
        auth_set.add(i['cookie_sessionid'])
        auth_set.add(i['sessionid'])
        if i['user']:
            auth_set.add(urllib.quote_plus(i['user']))
        if i['password']:
            auth_set.add(urllib.quote_plus(i['password']))
    if None in auth_set:
        auth_set.remove(None)
    return auth_set

def perform_agg_tasks(base_location, is_streaming=None):
    is_streaming = base_location.find("streaming") > -1 if is_streaming is None else is_streaming
    log_out("%s: Reading strings and event information for %s"%(time_str(), base_location))
    factors = sifipr.get_factors_data(base_location)
    times = get_times(base_location)
    auth_data = read_event_information(base_location, factors)
    factors['age'] = (auth_data['max_time'] - auth_data['min_time'])/1000
    auth_data['ssl_values'] = merge_key_hits_key_info(base_location)
    auth_data['ssl_ordered_comms'] = build_ordered_ssl_comms(auth_data['ssl_values'])
    auth_data.update(times)

    strings_data = read_strings_information(base_location, auth_data)
    auth_data['http_values'] = update_shadow_auth_info(strings_data, auth_data['http_values'])
    scale_timestamps_ssl_values(auth_data['ssl_values'].values(), times)
    scale_timestamps_http_values(auth_data['http_values'].values(), times)
    auth_data['matched_comms'] = match_http_with_ssl_keys(auth_data, times, is_streaming=is_streaming )
    # scale all the timestamp http_values

    # make sure the external end clock time is the same, and if not update
    #update_end_time(auth_data, times)
    infos = extract_basic_infos(auth_data, factors)
    for user, info in infos.items():
        auth_data['http_values'][user]['age'] = info['age']
        auth_data['http_values'][user]['live'] = info['live']

    #update the ssl ages here
    # comm timestamp is considered the end of a communication
    host_start = times['start']
    host_end = times['act_end']

    for comm in auth_data['matched_comms']:
        user = comm['user']
        ms = comm['ms']
        age = comm['age']
        dur_exp = -1
        v = auth_data['ssl_values'][ms]
        if is_streaming:
            live = not auth_data['http_values'][user]['completed']
            age = auth_data['http_values'][user]['age']
        else:
            live = False

        if not live and (comm['ms_hit'] or comm['pms_hit']):
            dur_exp = auth_data['http_values'][user]['end'] - host_start
            dur_exp = 0 if dur_exp < 0 else dur_exp

        v['live'] = live
        v['matched'] = True
        v['dur_exp'] = comm['dur_exp']
        v['unscaled_timestamp'] = v['timestamp']
        v['timestamp'] = comm['ssl_timestamp']
        v['age'] = age
        v['start'] = comm['ssl_timestamp'] - host_start
        v['end'] = v['start'] + age if not live else -1
        v['end_timestamp'] = comm['http_timestamp'] if not live else -1


        comm['dur_exp'] = host_end - comm['end'] if not live else 0
        comm['live'] = live
        comm['end'] = comm['start'] + age if not live else -1
        comm['end_timestamp'] = comm['http_timestamp'] if not live else -1

    matched_comm_ssl = {}
    for comm in auth_data['matched_comms']:
        matched_comm_ssl[comm['ms']] = comm

    for comm in auth_data['ssl_ordered_comms']:
        ms = comm['ms']
        v = auth_data['ssl_values'][ms]
        age = comm['age']
        comm['end'] = matched_comm_ssl[ms]['end'] if ms in matched_comm_ssl else -1
        comm['end_timestamp'] = matched_comm_ssl[ms]['end_timestamp'] if ms in matched_comm_ssl else -1

        if 'matched' in v and v['matched']:
            continue
        v['age'] = 0
        v['live'] = True
        v['unscaled_timestamp'] = auth_data['ssl_values'][ms]['timestamp']
        v['timestamp'] = comm['timestamp']
        v['start'] = comm['timestamp'] - host_start
        v['end'] = -1
        v['end_timestamp'] = -1
        v['dur_exp'] = -1


    auth_data.update(times)
    log_out("%s: Performing data searches for %s"%(time_str(), base_location))
    perform_data_search(base_location, strings_data, auth_data)
    log_out("%s: Caclulating the probabilities %s"%(time_str(), base_location))
    # probability that a session token could be recovered
    # probability that a password could be recovered (alone)
    # probability that a post uri could be recovered
    auth_data['live_quant'] = {"est_comms":len(auth_data['matched_comms']),
                               "num_ssl_sessions":len(auth_data['ssl_values']),
                               "num":len(auth_data['http_values'].values()),
                               "num_live":sum([1 for i in auth_data['http_values'].values() if i['live']]) }
    infos, results = calc_password_info(auth_data, factors)
    auth_data['password_infos'] = infos
    auth_data['password_quant'] = results
    infos, results = calc_sessionid_info(auth_data, factors)
    auth_data['sessionid_infos'] = infos
    auth_data['sessionid_quant'] = results
    infos, results = calc_cookie_sessionid_info(auth_data, factors)
    auth_data['cookie_sessionid_infos'] = infos
    auth_data['cookie_sessionid_quant'] = results
    infos, results = calc_post_uri_info(auth_data, factors)
    auth_data['post_uri_infos'] = infos
    auth_data['post_uri_quant'] = results
    infos, results = calc_logentry_info(auth_data, factors)
    auth_data['logentry_infos'] = infos
    auth_data['logentry_quant'] = results
    memory_consumption = calculate_memory_consumption(auth_data, factors)
    auth_data['memory_consumption'] = memory_consumption
    infos, results = calc_ssl_info_quant(auth_data, factors)
    auth_data['ssl_infos'] = infos
    auth_data['ssl_quant'] = results
    infos, results = calc_ssl_info_quant(auth_data, factors, use_unmatched=True)
    auth_data['ssl_infos_unmatched'] = infos
    auth_data['ssl_quant_unmatched'] = results

    log_out("%s: writing results to json file %s"%(time_str(), base_location))
    perform_results_write(base_location, auth_data)
    log_out("%s: job complete %s"%(time_str(), base_location))
    return auth_data

KEY_HITS = 'dumps_found_ssl_keys_counts.txt'
KEY_HITS2 = 'found_ssl_keys_counts.txt'
FOUND_KEY_HITS_GREP = 'dumps_found_ssl_keys_g.txt'
MERGED_KEY_INFO = 'merged_keyinfo.txt'

KEY_VALUES = set(['mkeyblock', 'ms', 'pms', 'swkey', 'cwkey', 'crandom', 'srandom'])

def get_found_key_offsets(base_location):
    dump_locs = {}
    dump_name = os.path.join(base_location, FOUND_KEY_HITS_GREP)
    data = [i.strip().split() for i in open(dump_name).readlines() if len(i.strip()) > 0]

    for fname, offset, hit in data:
        fname = fname.strip().strip(':')
        dump_range = os.path.split(fname)[1].strip('.bin')
        start, end = dump_range.split('-')
        hit_offset = long(start, 16) + long(offset.strip(), 16)
        if not hit in dump_locs:
            dump_locs[hit] = []
        dump_locs[hit].append( (hit_offset, dump_range))
    return dump_locs


def get_found_key_counts_dict(base_location):
    data = None
    try:
        fname = os.path.join(base_location, KEY_HITS)
        data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]
    except:
        fname = os.path.join(base_location, KEY_HITS2)
        data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]

    results = {}
    for i in data:
        key, hit = i.split()
        results[key] = long(hit, 16)
    return results


def get_found_key_counts_dict(base_location):
    data = None
    try:
        fname = os.path.join(base_location, KEY_HITS)
        data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]
    except:
        fname = os.path.join(base_location, KEY_HITS2)
        data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]

    results = {}
    for i in data:
        key, hit = i.split()
        results[key] = long(hit, 16)
    return results

def parse_merged_info_line(line, key_counts={}):
    results = {}
    if not line or line.strip() == "":
        return results

    p = line.strip()
    results['tot_hits'] = 0
    elements = p.split("-")
    #print elements
    for e in elements:
        s = e.split(":")
        k, v, =  s[0], s[1] if len(s) == 2 else (None, None,)
        if k is None or k == '':
            continue
        if k == 'time':
            v = long(v, 16)
            k = 'timestamp'
        elif k in KEY_VALUES:
            hits = key_counts.get(v, 0)
            results['tot_hits'] += hits
            results['%s_hits'%k] = hits
        results[k] = v
    return results

def normalize_comm_timestamps(comm_info, times):
    scale_timestamps(comm_info, times)
    min_ts = min([i['timestamp'] for i in comm_info])
    delta = get_host_guest_time_delta(times['start'], min_ts)
    #for i in comm_info:
    #    i['timestamp'] = get_host_guest_time(i['timestamp'], delta)

# def normalize_http_data_timestamps(http_data, act_start):
#     min_ts = min([i['timestamp'] for i in http_data.values()])
#     delta = get_host_guest_time_delta(act_start, min_ts)
#     for i in http_data.values():
#         i['timestamp'] = get_host_guest_time(i['timestamp'], delta)
#         i['end'] = get_host_guest_time(i['end'], delta)
#         for e in i['events']:
#             if 'timestamp' in e:
#                 e['timestamp'] = get_host_guest_time(e['timestamp'], delta)

def merge_key_hits_key_info(base_location, act_start=0):
    key_counts = get_found_key_counts_dict(base_location)
    merged_info = get_merged_key_info(base_location, key_counts)
    return merged_info

def build_ordered_ssl_comms(ssl_data):
    ordered_use = []
    for ms, v in ssl_data.items():
        pms_hit = 1 if v['pms_hits'] > 0 else 0
        ms_hit = 1 if v['ms_hits'] > 0 else 0
        mkeyblock_hit = 1 if v['mkeyblock_hits'] > 0 else 0
        ordered_use.append({'timestamp':v['timestamp'], 'ms':ms, 'ms_hit':ms_hit, 'pms_hit':pms_hit,
            'end':-1, 'live':True, 'age':-1, 'dur_exp':-1, 'start':-1, 'mkeyblock_hit':mkeyblock_hit})
    return sorted(ordered_use, key=lambda k:k['timestamp'])

def get_merged_key_info(base_location, key_counts={}):
    fname = os.path.join(base_location, MERGED_KEY_INFO)
    data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]
    results = {}

    for line in data:
        pline = parse_merged_info_line(line, key_counts)
        if 'ms' in pline:
            pline['age'] = 0
            pline['live'] = True
            pline['matched'] = False
            pline['dur_exp'] = 0
            pline['end'] = -1
            pline['start'] = 0
            results[pline['ms']] = pline
    return results

def calc_password_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='password')

def calc_sessionid_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='sessionid')

def calc_cookie_sessionid_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='cookie_sessionid')

def calc_post_uri_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='post_uri')

def calc_logentry_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='logentry')

def calculate_memory_consumption(auth_data, _):
    memory_consumption= {}
    for session in auth_data['http_values'].values():
        # so its possible to have multiple events in a single quantum
        # so we use the last event in the list at the time slot.
        # filtering loop
        events = {}
        for event in session['events']:
            t = event.get('timestamp', 0)
            if t == 0 or not 'mem' in event:
                continue
            mem = event['mem']
            max = event['max'] if 'max' in event else mem
            maxed = mem == max and event['maxed'] if 'maxed' in event else False
            events[t] = (mem, max, maxed)

        for t, vals in events.items():
            #print vals
            #print mem
            mem, max, maxed = vals
            if not t in memory_consumption:
                memory_consumption[t] = {"tot_mem":0, "maxed_sessions":dict()}
            memory_consumption[t]['tot_mem'] += mem
            if maxed:
                memory_consumption[t]['maxed_sessions'][session['user']] = 1
    return memory_consumption

def find_key_by_10(val, ssl_data):
    results = []
    for k in ssl_data.keys():
        if k[:10] == val:
            results.append(k)
    return results

def get_ssl_data(base_location):
    json_file = os.path.join(base_location, SSL_JSON_FILE)
    ssl_data = json.load(open(json_file))
    # update times since there was a bug in the capture
    for k in ssl_data:
        if 'timestamp' in ssl_data[k]:
            ssl_data[k]['timestamp'] &= MAX_INT
    return ssl_data

def print_summary_pms(full_dump_results):
    cnt = 0
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        pms = results.get('pre_master_secret_results_cnts', 0)
        if pms > 0:
            cnt += 1

    print "Showing %d locations with pms keys:"%cnt
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        cw = results.get('client_wkey_results_cnts', 0)
        sw = results.get('server_wkey_results_cnts', 0)
        ms = results.get('master_secret_results_cnts', 0)
        pms = results.get('pre_master_secret_results_cnts', 0)
        if pms > 0:
            cnt += 1
            print "Master key[:10] = %s client_wkey_cnts = %d server_wkey_cnts = %d ms_cnts = %d pms_cnts = %d "%(master_key[:10], cw, sw, ms, pms)

def print_summary_ms(full_dump_results):
    cnt = 0
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        ms = results.get('master_secret_results_cnts', 0)
        if ms > 0:
            cnt += 1

    print "Showing %d locations with ms keys:"%cnt
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        cw = results.get('client_wkey_results_cnts', 0)
        sw = results.get('server_wkey_results_cnts', 0)
        ms = results.get('master_secret_results_cnts', 0)
        pms = results.get('pre_master_secret_results_cnts', 0)
        if ms > 0:
            cnt += 1
            print "Master key[:10] = %s client_wkey_cnts = %d server_wkey_cnts = %d ms_cnts = %d pms_cnts = %d "%(master_key[:10], cw, sw, ms, pms)


def get_factors(filepath, trim=False):
    res = {}
    convert_string = os.path.split(filepath.strip("/"))
    if len(convert_string) < 2:
        return None
    factors = convert_string[-1].split("_")
    if (trim and factors[-1].isdigit()):
        factors = factors[0:4]
    if len(factors) == 4:
        is_random = lambda x:factors[x] == "RANDOM"
        res['mem_pressure_factor'] = FACTOR_CONV[factors[0]]
        res['lifetime_factor'] = FACTOR_CONV[factors[1]]
        res['requests_factor'] = FACTOR_CONV[factors[2]]
        res['concurrent_sessions_factor'] = FACTOR_CONV[factors[3]]
        res['mem_pressure'] = long(FACTOR_CONV[factors[0]] * (2560*1024*1024*8)*.8)
        res['age'] = -1
        res['requests'] = long(FACTOR_CONV[factors[2]] *MAX_ALLOWED_REQ)
        res['concurrent_sessions'] = long(FACTOR_CONV[factors[3]] *MAX_CONCURRENT_SESSIONS)
        res['exp_cut_off'] = CUTOFF_TIME*1000
        return res
    return None

def get_auth_data_filename(base_location):
    return os.path.join(base_location, AUTH_JSON_OUTFILE)

def get_compromised_summary_filename(base_location):
    return os.path.join(base_location, COMPROMISED_SUMMARY)

def get_compromised_key_filename(base_location):
    return os.path.join(base_location, COMPROMISED_SUMMARY)

def get_gclog_summary_filename(base_location):
    return os.path.join(base_location, "gc_token.txt")

def get_compromised_key_filename(base_location):
    return os.path.join(base_location, "compromised_keys.txt")


def get_ssl_data_filename(base_location):
    return os.path.join(base_location, SSL_JSON_OUTFILE)

def get_auth_data(base_location):
    return json.load(open(get_auth_data_filename(base_location)))

def get_compromised_summary_data(base_location):
    split_element = lambda e: (e.split(":")[0], float(e.split(':')[1]))
    split_line = lambda dline: [i for i in dline.split("-")]
    line = open(get_compromised_summary_filename(base_location)).read().strip()
    data = dict([split_element(e) for e in split_line(line)])
    return data

def get_gclog_summary_data(base_location):
    split_element = lambda e: (e.split(":")[0], float(e.split(':')[1]))
    split_line = lambda dline: [i for i in dline.split("-")]
    line = open(get_gclog_summary_filename(base_location)).read().strip()
    data = dict([split_element(e) for e in split_line(line)])
    return data

def get_compromised_key_summary_data(base_location):
    split_element = lambda e: (e.split(":")[0], float(e.split(':')[1]))
    split_line = lambda dline: [i for i in dline.split("-")]
    split_count = lambda e: (e.split("|")[0], int(e.split('|')[1]))
    split_int = lambda dline: (e.split(":")[0], int(e.split(':')[1]))
    lines = open(get_compromised_key_filename(base_location)).read().strip()
    lines = [i.strip() for i in lines.splitlines()]
    summary_data = get_compromised_summary_data(base_location)
    if not 'max_age' in summary_data:
        print "Failed to find max_age in summary data: %s"%(base_location)
        summary_data['max_age'] = 0
        
    data = []
    for line in lines:
        lres = {}
        try:
            for e in split_line(line):
                name, v = e.split(':')
                if name.find('pms') > -1 or name.find('mkeyblock') > -1:
                    key, count = split_count(v)
                    lres[name] = key
                    lres[name] = key
                    lres[name+'_hits'] = count
                elif name.find('expstart') > -1:
                    v = int(v)
                    lres['age'] = summary_data['max_age'] - v
                    lres['exp_start'] = v
        except:
            print "Failed to extract data from: %s\n    in %s"%(line, base_location)

        data.append(lres)
    return data

def get_missing_data_from_base_location(base_location):
    auth_data = get_auth_data(base_location)
    return get_missing_data(auth_data)

def get_missing_data(auth_data):
    results = {}
    post_uri_infos = auth_data['post_uri_infos'].values()
    sid_infos = auth_data['cookie_sessionid_infos'].values()
    http_values = auth_data['http_values']
    ex_pu_infos = [i for i in post_uri_infos if i['found_in_full_mem'] and\
                  (not i['live'] or i['age'] > 10000)]
    ex_sid_infos = [i for i in sid_infos if i['found_in_full_mem'] and\
                  (not i['live'] or i['age'] > 10000)]

    ex_pu_infos_errors = [i for i in ex_pu_infos if i['user'] in http_values and\
                     http_values[i['user']]['error']]
    ex_sid_infos_errors = [i for i in ex_sid_infos if i['found_in_full_mem'] and\
                  (not i['live'] or i['age'] > 10000)]


    results['num_errors'] = len([i for i in http_values.values() if i['error']])
    results['num_ssl'] = len(auth_data['ssl_infos'])
    results['num_ssl_expired'] = len(auth_data['matched_comms'])
    results['num_post_uri'] = len(post_uri_infos)
    results['num_post_uri_expired'] = len(ex_pu_infos)
    results['num_post_uri_expired_error'] = len(ex_pu_infos_errors)
    results['num_sessionid'] = len(sid_infos)
    results['num_sessionid_expired'] = len(ex_sid_infos)
    results['num_sessionid_expired_error'] = len(ex_sid_infos_errors)
    return results

def get_augmented_data_filename(base_location):
    return os.path.join(base_location, AUGMENTING_DATA)

def get_augmented_data(base_location):
    fname = get_augmented_data_filename(base_location)
    data = open(fname).read()
    split_element = lambda e: (e.split(":")[0], float(e.split(':')[1]))
    split_line = lambda dline: [i for i in dline.split("-")]
    split_int = lambda e: (e.split(":")[0], int(e.split(':')[1]))
    line_items = split_line(data.strip())
    results = dict([split_int(item) for item in line_items])
    return results


def capture_missing_data_str(missing_data):
    fmt = "-".join(["%s:{%s}"%(k, k) for k in missing_data])
    s = fmt.format(**missing_data)
    return s

def read_auth_data_writed_augments(base_location):
    missing_data = get_missing_data_from_base_location(base_location)
    write_augmented_data(missing_data)

def write_augmented_data(base_location, missing_data=None):
    if missing_data is None:
        missing_data = get_missing_data_from_base_location(base_location)
    s = capture_missing_data_str(missing_data)
    open(get_augmented_data_filename(base_location), 'w').write(s)

def get_ssl_data_from_json(base_location):
    return json.load(open(get_ssl_data_filename(base_location)))


# 1) Keys Recovered
# 2) Keys recovered after lifetime ended
# 3) Post Uri Copies present per key
# 4) SessionIds available after lifetime ended
# 5) Sessions that could be recovered after lifetime ended

def find_age_range(agg_auth_data):
    ages = []
    for v in agg_auth_data['http_values'].values():
        start = v['timestamp']
        end = v['end']
        ages.append(end-start)
    return min(ages), max(ages)

def get_final_deadline(agg_auth_data):
    end = -1
    for v in agg_auth_data['http_values'].values():
        end = max(end, v['end'])
    return end

def calc_age(agg_auth_value):
    start = agg_auth_value['timestamp']
    end = agg_auth_value['end']
    if end == -1:
        return start-min_start
    return end-start


def get_ages(agg_auth_data):
    ages = []
    min_start = get_act_start_time(agg_auth_data)
    for v in agg_auth_data['http_values'].values():
        ages.append(calc_age(v))
    ages.sort()
    return ages

def find_max_ages(agg_auth_data,n=1):
    ages = get_ages(agg_auth_data)
    return ages[-n:]

def get_act_start_time(agg_auth_data):
    #XXX - this needs to be fixed
    try:
        return min([i['timestamp'] for i in agg_auth_data['http_values'].values()])
    except:
        return 0

def get_act_end_time(agg_auth_data):
    #XXX - this needs to be fixed
    try:
        return max([i['end'] for i in agg_auth_data['http_values'].values()])
    except:
        return 0

def update_end_time(auth_data, times):
    max_ts = get_act_end_time(auth_data)
    host_start = times['start']
    delta = get_host_guest_time_delta(host_start, max_ts)
    if times['start'] + delta > times['act_end']:
        times['act_end'] = delta+host_start


def find_min_ages(agg_auth_data,n=1):
    ages = get_ages(agg_auth_data)
    return ages[:n]

def calculate_max_age(agg_auth_data, n=10):
    return sum(find_max_ages(agg_auth_data,n))/n

def quantify(infos):
    results = {}
    results['total_found_live'] = 0
    results['num_found_live'] = 0
    results['total_found_dead_value'] = 0
    results['num_found_dead_value'] = 0
    results['num_live'] = 0
    results['num_dead'] = 0
    results['tot'] = len(infos)
    exp_ages = []
    liv_ages = []
    dur_expired = []
    results['min_age_found_expired'] = 0
    results['max_age_found_expired'] = 0
    results['mean_age_found_expired'] = 0
    results['std_age_found_expired'] = 0
    results['var_age_found_expired'] = 0

    results['min_age_found_live'] = 0
    results['max_age_found_live'] = 0
    results['mean_age_found_live'] = 0
    results['std_age_found_live'] = 0
    results['var_age_found_live'] = 0

    results['min_age_dur_exp'] = 0
    results['max_age_dur_exp'] = 0
    results['mean_age_dur_exp'] = 0
    results['std_age_dur_exp'] = 0
    results['var_age_dur_exp'] = 0

    for info in infos.values():
        if not info['live']:
            results['num_dead'] += 1
        else:
            results['num_live'] += 1
        if info['full_occ'] > 0 and not info['live']:
            exp_ages.append(info['age'])
            results['num_found_dead_value'] +=1
            results['total_found_dead_value'] += info['full_occ']
        elif info['full_occ'] > 0 and info['live']:
            liv_ages.append(info['age'])
            dur_expired.append(info['dur_exp'])
            results['num_found_live'] +=1
            results['total_found_live'] += info['full_occ']
    results['prop_found_live'] = float(results['total_found_live']) / results['tot']

    if len(exp_ages) > 0:
        results['min_age_found_expired'] = min(exp_ages)
        results['max_age_found_expired'] = max(exp_ages)
        results['mean_age_found_expired'] = np.mean(exp_ages)
        results['std_age_found_expired'] = np.std(exp_ages)
        results['var_age_found_expired'] = np.var(exp_ages)
    if len(liv_ages) > 0:
        results['min_age_found_live'] = min(liv_ages)
        results['max_age_found_live'] = max(liv_ages)
        results['mean_age_found_live'] = np.mean(liv_ages)
        results['std_age_found_live'] = np.std(liv_ages)
        results['var_age_found_live'] = np.var(liv_ages)
    if len(dur_expired) > 0:
        results['min_age_dur_exp'] = min(liv_ages)
        results['max_age_dur_exp'] = max(liv_ages)
        results['mean_age_dur_exp'] = np.mean(liv_ages)
        results['std_age_dur_exp'] = np.std(liv_ages)
        results['var_age_dur_exp'] = np.var(liv_ages)

    return results

def extract_basic_infos(agg_auth_data, factors):
    # 1 get all the password keys
    host_start = agg_auth_data['start']
    host_end = agg_auth_data['end']
    min_ts = get_act_start_time(agg_auth_data)
    delta = get_host_guest_time_delta(host_start, min_ts)

    max_age = factors['age']

    infos = {}
    for user,value in agg_auth_data['http_values'].items():
        info = {}
        start = get_host_guest_time(value['timestamp'], delta)
        end = value['end']
        if end != -1:
            end = get_host_guest_time(value['end'], delta)
        info['live'] = not value['completed']
        info['dur_exp'] = 0 if info['live'] else host_end - end
        info['user'] = user
        info['start'] = start
        info['end'] = end
        info['age'] = end - start if value['completed'] else host_end - start
        infos[user] = info
    return infos


def calculate_prob_info(agg_auth_data, factors, sensitive_key):
    # 1 get all the password keys
    host_start = agg_auth_data['start']
    host_end = agg_auth_data['end']
    min_ts = get_act_start_time(agg_auth_data)
    delta = get_host_guest_time_delta(host_start, min_ts)

    max_lifetime = factors['age']


    infos = {}
    full_results_key = 'full_%s_results'%sensitive_key

    if sensitive_key == "logfile":
        sensitive_key = "logentry"
    if sensitive_key ==  "sessionid":
        sensitive_key = 'sessionid'
    password_results = agg_auth_data['full_password_results']
    for user,value in agg_auth_data['http_values'].items():
        if 'error' in value and value['error']:
            continue
        info = {}
        start = get_host_guest_time(value['timestamp'], delta)
        end = value['end']
        if end != -1:
            end = get_host_guest_time(value['end'], delta)
        info['live'] = not value['completed']
        info['dur_exp'] = 0 if info['live'] else host_end - end
        info['user'] = user
        info['start'] = start
        info['end'] = end
        info['age'] = end - start if value['completed'] else host_end - start
        sensitive = info['sensitive'] = value[sensitive_key]
        info['found_in_full_mem'] = sensitive in agg_auth_data[full_results_key] and\
                                              agg_auth_data[full_results_key][sensitive] > 0
        info['full_occ'] = 0 if not info['found_in_full_mem'] else agg_auth_data[full_results_key][sensitive]
        infos[user] = info
    return infos, quantify(infos)

def calc_ssl_info_quant(agg_auth_data, _, use_unmatched=False):
    ssl_values = agg_auth_data['ssl_values']
    infos = {}
    quant = {}
    times = [i['timestamp'] for i in ssl_values.values()]
    times.sort()
    if len(ssl_values.values()) == 0:
        return infos, quant

    quant['start'] = ssl_values.values()[0]['timestamp']
    quant['last'] =  ssl_values.values()[-1]['timestamp']
    quant['unique_swkey_hit'] = 0
    quant['unique_cwkey_hit'] = 0

    quant['unique_live_pms_hit'] = 0
    quant['unique_live_ms_hit'] = 0
    quant['unique_live_mkeyblock_hit'] = 0
    quant['unique_exp_pms_hit'] = 0
    quant['unique_exp_ms_hit'] = 0
    quant['unique_exp_mkeyblock_hit'] = 0

    quant['unique_pms_hit'] = 0
    quant['unique_ms_hit'] = 0
    quant['unique_mkeyblock_hit'] = 0
    quant['unique_tot_hit'] = 0

    quant['swkey_hits'] = 0
    quant['cwkey_hits'] = 0
    quant['pms_hits'] = 0
    quant['ms_hits'] = 0
    quant['mkeyblock_hits'] = 0

    quant['live_pms_hits'] = 0
    quant['live_ms_hits'] = 0
    quant['live_mkeyblock_hits'] = 0

    quant['live_pms_hit'] = 0
    quant['live_ms_hit'] = 0
    quant['live_mkeyblock_hit'] = 0

    quant['exp_pms_hits'] = 0
    quant['exp_ms_hits'] = 0
    quant['exp_mkeyblock_hits'] = 0

    quant['exp_pms_hit'] = 0
    quant['exp_ms_hit'] = 0
    quant['exp_mkeyblock_hit'] = 0

    quant['tot_hits'] = 0
    quant['tot_ssl'] = 0
    ms_ages = []
    pms_ages = []
    mkeyblock_ages = []

    ms_dur_exp = []
    pms_dur_exp = []
    mkeyblock_dur_exp = []

    for value in ssl_values.values():
        if not value['matched'] and not use_unmatched:
            continue
        info = {}
        age = value['age']
        live = value['live']
        info['live'] = live
        info['age'] = age
        quant['start'] = min( value['timestamp'], quant['start'])
        quant['last'] =  max( value['timestamp'], quant['last'])

        info['timestamp'] = value['timestamp']
        info['swkey_hit'] = 1 if value['swkey_hits'] > 0 else 0
        info['cwkey_hit'] = 1 if value['cwkey_hits'] > 0 else 0
        info['pms_hit'] = 1 if value['pms_hits'] > 0 else 0
        info['ms_hit'] = 1 if value['ms_hits'] > 0 else 0
        info['mkeyblock_hit'] = 1 if value['mkeyblock_hits'] > 0 else 0
        info['pms_hit_age'] = value['age'] if value['pms_hits'] > 0 else 0
        info['ms_hit_age'] = value['age'] if value['ms_hits'] > 0 else 0
        info['mkeyblock_hit_age'] = value['age'] if value['mkeyblock_hits'] > 0 else 0

        if not live and value['pms_hits'] > 0:
            pms_ages.append(value['age'])
            pms_dur_exp.append(value['dur_exp'])

        if not live and value['mkeyblock_hits'] > 0:
            mkeyblock_ages.append(value['age'])
            mkeyblock_dur_exp.append(value['dur_exp'])

        if not live and value['ms_hits'] > 0:
            ms_ages.append(value['age'])
            ms_dur_exp.append(value['dur_exp'])


        if live:
            info['live_pms_hit'] = 1 if value['pms_hits'] > 0 else 0
            info['live_ms_hit'] = 1 if value['ms_hits'] > 0 else 0
            info['live_mkeyblock_hit'] = 1 if value['mkeyblock_hits'] > 0 else 0
            quant['live_pms_hit'] += info['live_pms_hit']
            quant['live_ms_hit'] += info['live_ms_hit']
            quant['live_mkeyblock_hit'] += info['live_mkeyblock_hit']
        else:
            info['exp_pms_hit'] = 1 if value['pms_hits'] > 0 else 0
            info['exp_ms_hit'] = 1 if value['ms_hits'] > 0 else 0
            info['exp_mkeyblock_hit'] = 1 if value['mkeyblock_hits'] > 0 else 0
            quant['exp_pms_hit'] += info['exp_pms_hit']
            quant['exp_ms_hit'] += info['exp_ms_hit']
            quant['exp_mkeyblock_hit'] += info['exp_mkeyblock_hit']


        info['swkey_hits'] = value['swkey_hits']
        info['cwkey_hits'] = value['cwkey_hits']
        info['pms_hits'] = value['pms_hits']
        info['ms_hits'] = value['ms_hits']
        info['mkeyblock_hits'] = value['mkeyblock_hits']
        if live:
            info['live_pms_hits'] = value['pms_hits']
            info['live_ms_hits'] = value['ms_hits']
            info['live_mkeyblock_hits'] = value['mkeyblock_hits']
            quant['live_pms_hits'] += value['pms_hits']
            quant['live_ms_hits'] += value['ms_hits']
            quant['live_mkeyblock_hits'] += value['mkeyblock_hits']

        else:
            info['exp_pms_hits'] = value['pms_hits']
            info['exp_ms_hits'] = value['ms_hits']
            info['exp_mkeyblock_hits'] = value['mkeyblock_hits']
            quant['exp_pms_hits'] += value['pms_hits']
            quant['exp_ms_hits'] += value['ms_hits']
            quant['exp_mkeyblock_hits'] += value['mkeyblock_hits']

        info['tot_hits'] = value['tot_hits']
        info['tot_hit'] = sum([info['swkey_hit'], info['cwkey_hit'],
                               info['pms_hit'], info['ms_hit'], info['mkeyblock_hit']])
        infos[value['ms']] = info

        quant['unique_tot_hit'] += info['tot_hit']
        quant['unique_swkey_hit'] += info['swkey_hit']
        quant['unique_cwkey_hit'] += info['cwkey_hit']
        quant['unique_pms_hit'] += info['pms_hit']
        quant['unique_ms_hit'] +=  info['ms_hit']
        quant['unique_mkeyblock_hit'] +=  info['mkeyblock_hit']
        if live:
            quant['unique_live_ms_hit'] +=  info['ms_hit']
            quant['unique_live_pms_hit'] += info['pms_hit']
            quant['unique_live_mkeyblock_hit'] += info['mkeyblock_hit']
        else:
            quant['unique_exp_ms_hit'] +=  info['ms_hit']
            quant['unique_exp_pms_hit'] += info['pms_hit']
            quant['unique_exp_mkeyblock_hit'] += info['mkeyblock_hit']

        quant['swkey_hits'] += value['swkey_hits']
        quant['cwkey_hits'] += value['cwkey_hits']
        quant['pms_hits'] += value['pms_hits']
        quant['ms_hits'] += value['ms_hits']
        quant['mkeyblock_hits'] += value['mkeyblock_hits']
        quant['tot_hits'] += value['tot_hits']
        quant['tot_ssl'] += 1

    tot_ssl = quant['tot_ssl']
    quant['prop_unique_ms_key'] = float(quant['unique_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_unique_pms_key'] = float(quant['unique_pms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_unique_mkeyblock_key'] = float(quant['unique_mkeyblock_hit']) / tot_ssl if tot_ssl > 0 else 0

    quant['prop_live_unique_ms_key'] = float(quant['unique_live_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_live_unique_pms_key'] = float(quant['unique_live_pms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_live_unique_mkeyblock_key'] = float(quant['unique_live_mkeyblock_hit']) / tot_ssl if tot_ssl > 0 else 0

    quant['prop_exp_unique_ms_key'] = float(quant['unique_exp_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_exp_unique_pms_key'] = float(quant['unique_exp_pms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_exp_unique_mkeyblock_key'] = float(quant['unique_exp_mkeyblock_hit']) / tot_ssl if tot_ssl > 0 else 0

    tot_hits = quant['tot_hits']
    quant['prop_ms_hits'] = float(quant['ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_pms_hits'] = float(quant['pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_mkeyblock_hits'] = float(quant['mkeyblock_hits']) / quant['tot_hits'] if tot_hits > 0 else 0

    quant['prop_live_ms_hits'] = float(quant['live_ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_live_pms_hits'] = float(quant['live_pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_live_mkeyblock_hits'] = float(quant['live_mkeyblock_hits']) / quant['tot_hits'] if tot_hits > 0 else 0

    quant['prop_exp_ms_hits'] = float(quant['exp_ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_exp_pms_hits'] = float(quant['exp_pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_exp_mkeyblock_hits'] = float(quant['exp_mkeyblock_hits']) / quant['tot_hits'] if tot_hits > 0 else 0

    quant['min_pms_age'] = -1 if len(pms_ages) == 0 else min(pms_ages)
    quant['min_ms_age'] = -1 if len(ms_ages) == 0 else min(ms_ages)
    quant['min_mkeyblock_age'] = -1 if len(mkeyblock_ages) == 0 else min(mkeyblock_ages)

    quant['min_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else min(pms_dur_exp)
    quant['min_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else min(ms_dur_exp)
    quant['min_mkeyblock_dur_exp'] = -1 if len(mkeyblock_dur_exp) == 0 else min(mkeyblock_dur_exp)

    quant['max_pms_age'] = -1 if len(pms_ages) == 0 else max(pms_ages)
    quant['max_ms_age'] = -1 if len(ms_ages) == 0 else max(ms_ages)
    quant['max_mkeyblock_age'] = -1 if len(mkeyblock_ages) == 0 else max(mkeyblock_ages)

    quant['max_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else max(pms_dur_exp)
    quant['max_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else max(ms_dur_exp)
    quant['max_mkeyblock_dur_exp'] = -1 if len(mkeyblock_dur_exp) == 0 else max(mkeyblock_dur_exp)

    quant['mean_pms_age'] = -1 if len(pms_ages) == 0 else np.mean(pms_ages)
    quant['mean_ms_age'] = -1 if len(ms_ages) == 0 else np.mean(ms_ages)
    quant['mean_mkeyblock_age'] = -1 if len(mkeyblock_ages) == 0 else np.mean(mkeyblock_ages)

    quant['mean_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.mean(pms_dur_exp)
    quant['mean_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.mean(ms_dur_exp)
    quant['mean_mkeyblock_dur_exp'] = -1 if len(mkeyblock_dur_exp) == 0 else np.mean(mkeyblock_dur_exp)

    quant['std_pms_age'] = -1 if len(pms_ages) == 0 else np.std(pms_ages)
    quant['std_ms_age'] = -1 if len(ms_ages) == 0 else np.std(ms_ages)
    quant['std_mkeyblock_age'] = -1 if len(mkeyblock_ages) == 0 else np.std(mkeyblock_ages)

    quant['std_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.std(pms_dur_exp)
    quant['std_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.std(ms_dur_exp)
    quant['std_mkeyblock_dur_exp'] = -1 if len(mkeyblock_dur_exp) == 0 else np.std(mkeyblock_dur_exp)

    quant['var_pms_age'] = -1 if len(pms_ages) == 0 else np.var(pms_ages)
    quant['var_ms_age'] = -1 if len(ms_ages) == 0 else np.var(ms_ages)
    quant['var_mkeyblock_age'] = -1 if len(mkeyblock_ages) == 0 else np.var(mkeyblock_ages)

    quant['var_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.var(pms_dur_exp)
    quant['var_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.var(ms_dur_exp)
    quant['var_mkeyblock_dur_exp'] = -1 if len(mkeyblock_dur_exp) == 0 else np.var(mkeyblock_dur_exp)

    return infos, quant

def init_task (base_location):
    proc = Process (target=perform_agg_tasks, args=(base_location,))
    proc.start()
    return proc

def cull_threads (active_threads):
    cnt = 0
    # lock acquire
    orig_len = len(active_threads)
    while cnt < len(active_threads):
        if active_threads[cnt][0].is_alive():
            cnt += 1
            continue
        active_threads.pop(cnt)
    return len(active_threads) != orig_len


def get_gc_log_data(base_location, archive='gc_log.txt.zip', gclog_file='java.hprof.txt'):
        print "%s: Loading Java Heap Profile from %s:%s"%(time_str(), archive, hprof_file)
        zipped_data = zipfile.ZipFile(os.path.join(base_location, archive))

        gclog_files = [i for i in zipped_data.namelist() if i.find(gclog_file) > -1]
        if len(gclog_files) > 0:
            gclog_file = gclog_files[0]
        elif len(zipped_data.namelist()) > 0:
            gclog_file = zipped_data.namelist()[0]
        else:
            raise Exception("Failed to specify the proper file in the java.hprof archive")

        data = zipped_data.open(gclog_file).read()
        return data

def get_extracted_hprof_data(base_location, interested_classes=set()):
    hprof_results = sifipr.get_java_hprof_results(base_location)
    try:
        data = open(hprof_results).read()
    except:
        print ("Failed to read the results from: %s"%base_location)
        return {}
    data = data.strip()
    items = [i.split(':') for i in data.split('-')]
    results = {}
    for k,v in items:
        if (k.find('_live') == len(k)-5 or\
            k.find('_dead') == len(k)-5) and\
            k.find('.') > 0:
            cname, stat = k.split('_')
            if len(interested_classes) > 0 and\
               cname in interested_classes:
               continue
            v = int(v)
            if not cname in results:
                results[cname] = {'live':0, 'dead':0}
            results[cname][stat] = v
        elif v.isdigit():
            results[k] = int(v)
        else:
            results[k] = v
    return results


# BASE_DIR = "/research_data/exp_results/results_1/"

# USING_MOD = False

# STREAMING = "streaming"
# TRANSACTION = "transaction"

# STREAMING_MOD_VM = "streaming_mod_vm"
# TRANSACTION_MOD_VM = "transaction_mod_vm"

# RESULTS_DIR_STREAMING = [os.path.join(BASE_DIR, STREAMING, i) for i in os.listdir(os.path.join(BASE_DIR, STREAMING))]
# RESULTS_DIR_TRANSACTION = [os.path.join(BASE_DIR, TRANSACTION, i) for i in os.listdir(os.path.join(BASE_DIR, TRANSACTION))]

# RESULTS_DIR_STREAMING.sort()
# RESULTS_DIR_TRANSACTION.sort()

# base_location = RESULTS_DIR_TRANSACTION[0]



# BASE_DIR = "/srv/nfs/cortana/logs/"
# RESULTS1 = "streaming"
# RESULTS2 = "streaming_mod_vm"
# RESULTS3 = "transaction"
# RESULTS4 = "transaction_mod_vm"
# BASE_RESULT_DIR = os.path.join(BASE_DIR, RESULTS1)
# BASE_RESULT2_DIR = os.path.join(BASE_DIR, RESULTS2)
# BASE_RESULT3_DIR = os.path.join(BASE_DIR, RESULTS3)
# BASE_RESULT4_DIR = os.path.join(BASE_DIR, RESULTS4)

if __name__ == "__main__":

    target_dir = ""
    start_str = time_str()
    if len(sys.argv) < 2:
        print ("%s base_dir_for_processing ]"%sys.argv[0])
        sys.exit(-1)

    BASE_DIR = sys.argv[1]
    results_dirs = [os.path.join(BASE_DIR, i) for i in os.listdir(BASE_DIR) if os.path.isdir(os.path.join(BASE_DIR, i))]
    start_str = time_str()

    active_threads = []
    procs = []
    remaining = len(results_dirs)
    results_dirs.sort()
    for results_dir in results_dirs:
        while len (active_threads) > PROCESSES:
            if not cull_threads(active_threads):
                time.sleep(5)
        proc = init_task (results_dir)
        procs.append(proc)
        active_threads.append((proc, (results_dir,)))
        remaining += -1
        print ("%s: aggregating %s, %d remaining"%(time_str(), results_dir, remaining))

    while len (active_threads) > 0:
        if not cull_threads(active_threads):
            time.sleep(.1)

    time.sleep(5)
    for r in procs:
        r.join()


    end_str = time_str()

    log_out ("%s: Started"%start_str)
    log_out ("%s: Ended"%end_str)




def get_tenured_heap_info(line):
    results = {}
    if line.find("[Tenured: ") == -1:
        return results
    tenure, gc_time = line.split('[Tenured: ')[1].split(',')[:2]
    gc_time = float(gc_time.strip().split()[0])
    before = long(tenure.split("K->")[0].strip())
    after = long(tenure.split("->")[1].split('K')[0].strip())
    size = long(tenure.split("K(")[1].split('K)')[0].strip())
    results = {'tenured_size':size, 'tenured_before':before, 'tenured_after':after, 'tenured_gc_time':gc_time}
    return results

def get_defnew_heap_info(line):
    results = {}
    if line.find("[DefNew: ") == -1:
        return results
    tenure, gc_time = line.split('[DefNew: ')[1].split(',')[:2]
    gc_time = float(gc_time.strip().split()[0])
    before = long(tenure.split("K->")[0].strip())
    after = long(tenure.split("->")[1].split('K')[0].strip())
    size = long(tenure.split("K(")[1].split('K)')[0].strip())
    results = {'defnew_size':size, 'defnew_before':before, 'defnew_after':after, 'defnew_gc_time':gc_time}
    return results

def get_metaspace_heap_info(line):
    results = {}
    if line.find("[Metaspace: ") == -1:
        return results
    tenure, gc_time = line.split('[Metaspace: ')[1].split(',')[:2]
    gc_time = float(gc_time.strip().split()[0])
    before = long(tenure.split("K->")[0].strip())
    after = long(tenure.split("->")[1].split('K')[0].strip())
    size = long(tenure.split("K(")[1].split('K)')[0].strip())
    results = {'metaspace_size':size, 'metaspace_before':before, 'metaspace_after':after, 'metaspace_gc_time':gc_time}
    return results

def try_extract_runtime(line):
    try:
        line_start = line.split('-0600:')[1]
        run_time = float(line_start.split(': ')[0].strip())
        return run_time
    except:
        return 0.0

def extract_gclog_data(base_location):
    gclog = os.path.join(base_location, 'gc_log.txt')
    clean_file, clean_lines_list  = clean_up_gclog_data(gclog)
    #data = [i.strip() for i in open(gclog).read().splitlines()]
    log_items = []
    log_items = []
    if len(clean_lines_list) < 4:
        return log_items    
    lines = clean_lines_list[3:]
        
    for line in lines:
        log = {}
        log['gc_full'] = line.find('[Full GC') > -1 
        log['gc'] = line.find('[GC ') > -1 
        log['allocation_failure'] = line.find('Allocation Failure') > -1 
        log['system_gc'] = line.find('System.gc') > -1 
        log['app_time'] = line.find('Application time:') > -1
        log['stop_time'] = line.find('Total time for which application threads were stopped') > -1
        log['run_time'] = try_extract_runtime(line)
        log['gc_reason'] = ''
        if log['allocation_failure']:
            log['gc_reason'] = 'allocation failure'
        elif log['allocation_failure']:
            log['gc_reason'] = 'system gc called'


        try:
            if log['app_time']:
                app_time = float(line.split('Application time: ')[1].split()[0])
                log['app_run_time'] = app_time
            elif log['gc'] or log['gc_full']:
                if line.find('[Times:') > 0:
                    gc_times = line.split("[Times: ")[1]
                    try:
                        usert, syst, realt, _ = gc_times.split()
                        log['user_time'] = eval(usert.split('=')[1])
                        log['sys_time'] = eval(syst.strip(',').split('=')[1])
                        log['real_time'] = eval(realt.strip(',').split('=')[1])
                    except:
                        print 'Failed to parse the GC times'
                if log['gc_full']:
                    try:
                        log.update(get_tenured_heap_info(rest))
                        log.update(get_metaspace_heap_info(rest))
                        log.update(get_defnew_heap_info(rest))
                    except:
                        print 'Failed to parse the Full GC times'
            elif log['stop_time']:
                stop_time = 0.0
                try:
                     stop_time = float(line.split('Total time for which application threads were stopped: ')[1].split()[0])
                except:
                     pass  
                log['stop_run_time'] = stop_time
        except:
            print "Failed to parse line: %s"%line
            traceback.print_exc()
        log_items.append(log)

    return log_items

def application_total_time(log_items):
    results = []
    for item in log_items:
        if 'app_run_time' in item:
            results.append(item['app_run_time'])
    return sum(results)

def stop_total_time(log_items):
    results = []
    for item in log_items:
        if 'stop_run_time' in item:
            results.append(item['stop_run_time'])
    return sum(results)

def gctime_total_time(log_items):
    results = []
    for item in log_items:
        if 'user_time' in item:
            results.append(item['user_time'])
    return sum(results)

def gctime_real_total_time(log_items):
    results = []
    for item in log_items:
        if 'real_time' in item:
            results.append(item['real_time'])
    return sum(results)

def count_number_tenured_collections(log_items):
    results = []
    num = 0
    for item in log_items:
        if 'tenured_size' in item:
            sz = item['tenured_size']
            net = item['tenured_after'] - item['tenured_before']
            results.append({'num':num, 'size':sz, 'net':net, 'gc_time':item['tenured_gc_time']})
            num += 1
    return results

def count_number_defnew_collections(log_items):
    results = []
    num = 0
    for item in log_items:
        if 'defnew_size' in item:
            sz = item['defnew_size']
            net = item['defnew_after'] - item['defnew_before']
            results.append({'num':num, 'size':sz, 'net':net, 'gc_time':item['defnew_gc_time']})
            num += 1
    return results

def count_number_metaspace_collections(log_items):
    results = []
    num = 0
    for item in log_items:
        if 'metaspace_size' in item:
            sz = item['metaspace_size']
            net = item['metaspace_after'] - item['metaspace_before']
            results.append({'num':num, 'size':sz, 'net':net, 'gc_time':item['metaspace_gc_time']})
            num += 1
    return results

def count_all_system_gc(log_items):
    num = 0
    for item in log_items:
        if item['system_gc']:
            num += 1
    return num

def count_all_full_gc(log_items):
    num = 0
    for item in log_items:
        if item['gc_full']:
            num += 1
    return num

def count_allocation_failure(log_items):
    num = 0
    for item in log_items:
        if item['allocation_failure']:
            num += 1
    return num

def count_all_gc(log_items):
    results = []
    num = 0
    for item in log_items:
        if item['gc_full'] or item['gc']:
            num += 1
    return num

def count_gc(log_items):
    results = []
    num = 0
    for item in log_items:
        if item['gc']:
            num += 1
    return num


def build_data_from_gc_log(log_items):
    defnew_data = count_number_defnew_collections(log_items)
    ten_data = count_number_tenured_collections(log_items)
    metaspace_data = count_number_metaspace_collections(log_items)
    results = {}
    results['num_inter_gc'] = count_gc(log_items)
    results['num_all_gc'] = count_all_gc(log_items)
    results['num_allocation_failure_gc'] = count_allocation_failure(log_items)
    results['num_system_gc'] = count_all_system_gc(log_items)
    results['num_full_gc'] = count_all_full_gc(log_items)
    results['app_time'] = application_total_time(log_items)
    results['stop_time'] = stop_total_time(log_items)
    results['gc_user_time'] = gctime_total_time(log_items)
    results['gc_real_time'] = gctime_real_total_time(log_items)
    results['ten_gc_time'] = sum([i['gc_time'] for i in ten_data])
    results['num_ten_gc'] = len(ten_data)
    results['defnew_gc_time'] = sum([i['gc_time'] for i in defnew_data])
    results['num_defnew_gc'] = len(defnew_data)
    results['metaspace_gc_time'] = sum([i['gc_time'] for i in metaspace_data])
    results['num_metaspace_gc'] = len(metaspace_data)
    return results

def get_results_string(log_items):
    results = build_data_from_gc_log(log_items)
    fmt = "-".join(["%s:{%s}"%(k, k) for k in results])
    s = fmt.format(**results)
    return s

def write_gc_log_data(base_location, log_items):
    s = get_results_string(log_items)
    str_results = os.path.join(base_location, "gc_token.txt")
    open(str_results, 'w').write(s)
    json_results = os.path.join(base_location, "gc_json.json")
    json.dump(log_items, open(json_results, 'w'))

def perform_gc_log_parse_save(base_location):
    log_items = extract_gclog_data(base_location)
    write_gc_log_data(base_location, log_items)
