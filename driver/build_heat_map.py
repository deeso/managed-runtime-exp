import sys
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from agg_exp_data import *

AGE_KEY = "age"
DUR_EXP = "dur_exp"
START = "start"
END = "end"

dest = "/srv/nfs/cortana/logs/cmd/"
sys.path.append(dest)
#from agg_exp_data import *
import single_factors_iteration_perform_runs as sifipr
import perform_post_process_dirs_strs as pppds
#from perform_post_process_dirs_strs import *
import agg_exp_data as aed

from single_factors_iteration_perform_runs import *


USE_64 = False
JSON_DATA = 'agg_auth_data.json'
TIME_LOG = 'timelog_start_end_actual.txt'






def get_sorted_bin_filters_labels(binned_data, reverse=False):
    labels_tup = [(k, v['min']) for k,v in binned_data.items() if 'min' in v]
    labels_tup = sorted(labels_tup, key = lambda x:x[1])
    l = [i[0] for i in labels_tup]
    if reverse:
        l.reverse()
    return l

def build_bin_filters_full_range(min_v=0, max_v=301, num=20, label_maker=None):
    label_maker = label_maker if not label_maker is None else lambda x,y: "%d_%d"%(x,y)
    bins = {}
    start = min_v
    end = min_v
    for i in xrange(min_v, max_v, num):
        p = i+num
        end = p if p < max_v else max_v
        start = i
        n = label_maker(start, end-1)
        b = {'min':start, 'max':end, 'values':[], 'match': lambda m,x: m['min']<=x and x <m['max']}
        bins[n] = b
    return bins

def build_bin_filters(min_v=0, max_v=300, num=20, label_maker=None):
    label_maker = label_maker if not label_maker is None else lambda x,y: "%d_%d"%(x,y)
    bins = {}
    start = min_v
    end = min_v
    for i in xrange(min_v, max_v, num):
        p = i+num
        end = p if p < max_v else max_v
        start = i
        n = label_maker(start, end-1)
        b = {'min':start, 'max':end, 'values':[], 'match': lambda m,x: m['min']<=x and x <m['max']}
        bins[n] = b
    return bins

def get_avg_num_vals(binned_data):
    rows = binned_data.keys()
    rows.sort()
    nums = []
    for r in rows:
        num = sum([len(i['values']) for i in binned_data[r].values()])
        nums.append(num)
    return int(np.mean(nums))


def create_binned_data(keyed_results, value_key, bin_filter_builder=None, label_maker=None, max_v=30000, min_v=0, num=5000):
    bresults = {}
    mbuilder = build_bin_filters if bin_filter_builder is None else bin_filter_builder
    for run,values in keyed_results['run_values'].items():
        bresults[run] = mbuilder(label_maker=label_maker, max_v=max_v, min_v=min_v, num=num)
        bins = bresults[run]
        for value in values:
            v = value[value_key]
            added = False
            for l,b in bins.items():
                if not 'match' in b:
                    continue
                if b['match'](b,v):
                    #print "Added %d to %s"%(v, l)
                    b['values'].append(v)
                    added = True
                    break
    return bresults

def build_bin_filters_full_range_bell(min_v=0, max_v=300000, num=5000, label_maker=None, bell_num=1000):
    label_maker = label_maker if not label_maker is None else lambda x,y: "%d_%d"%(x,y)
    bins = {}
    start = min_v
    end = min_v
    _max_v = max_v
    max_v = _max_v - 20
    for i in xrange(min_v, max_v, num):
        p = i+num
        end = p if p < max_v else max_v
        start = i
        n = label_maker(start, end-1)
        b = {'min':start, 'max':end, 'values':[], 'match': lambda m,x: m['min']<=x and x <m['max']}
        bins[n] = b


    min_v = max_v
    max_v = _max_v
    for i in xrange(min_v, max_v, num):
        p = i+num
        end = p if p < max_v else max_v
        start = i
        n = label_maker(start, end-1)
        b = {'min':start, 'max':end, 'values':[], 'match': lambda m,x: m['min']<=x and x <m['max']}
        bins[n] = b
    return bins

def create_data_binned_data(keyed_results, value_key, bin_filter_builder=None, label_maker=None, max_v=30000, min_v=0, num=5000):
    bresults = {}
    _values = []

    for run,values in keyed_results['run_values'].items():
        for value in values:
            v = value[value_key]
            _values.append(v)

    if max_v is None:
        max_v = max(_values)
    if min_v is None:
        min_v = min(_values)

    mbuilder = build_bin_filters if bin_filter_builder is None else bin_filter_builder
    for run,values in keyed_results['run_values'].items():
        bresults[run] = mbuilder(label_maker=label_maker, max_v=max_v, min_v=min_v, num=num)
        bins = bresults[run]
        for value in values:
            v = value[value_key]
            added = False
            for l,b in bins.items():
                if not 'match' in b:
                    continue
                if b['match'](b,v):
                    #print "Added %d to %s"%(v, l)
                    b['values'].append(value)
                    added = True
                    break
    return bresults

def get_key_cnt_per_row(keyed_results, binned_data=None):
    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    row_key_counts = {}
    if binned_data is None:
        label_maker = lambda s,e: s + (1+e-s)/2
        binned_data = create_data_binned_data(keyed_results, START, bin_filter_builder=build_bin_filters_full_range_bell, label_maker=label_maker, max_v=303000)

    rows = binned_data.keys()
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])

    for r in rows:
        row_key_counts[r] = sum([cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels])
    return row_key_counts


def create_age_heat_map(keyed_results, binned_data=None, min_value=0, max_value=5000, num=500, title=None, xlabel=None, ylabel=None, output_filename=None, show=False):
    row_key_counts = get_key_cnt_per_row(keyed_results, binned_data=binned_data)
    if binned_data is None:
        label_maker = lambda s,e: "%d"%(s + (1+e-s)/2)
        binned_data = create_binned_data(keyed_results, AGE_KEY, bin_filter_builder=build_bin_filters_full_range, label_maker=label_maker, min_v=0, max_v=max_value, num=num)

    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = ["%3d"%i if i%5 == 0 else '' for i in rows]

    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = [("%3d"%i if i%5 == 0 else '') for i in rows]
    #column_labels.reverse()
    # relabel columns
    data = []
    for r in rows:
        d = [len(binned_data[r][c_label]['values'])/float(row_key_counts[r]) for c_label in column_labels]
        print "Row:%d"%r, len(d), "=", d, "sum(d)=%d"%sum(d)
        data.insert(0, d)
    data.reverse()
    np_data = np.array(data)
    create_heat_map(column_labels, row_labels, np_data, 0, 100, title, xlabel, ylabel, output_filename, show, figsize=[16,11],
         xlim={"left":0, 'right':15},grid={'b':True, 'which':'both', 'axis':'y'}, cbar_ylabel="Percent of SSL Keys in a Run with Similar Ages")

def create_exp_heat_map(keyed_results, binned_data=None, min_value=0, max_value=1000, title=None, xlabel=None, ylabel=None, output_filename=None, show=False):
    if binned_data is None:
        binned_data = create_binned_data(keyed_results, DUR_EXP, bin_filter_builder=build_bin_filters)
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = ["%3d"%i for i in rows]
    column_labels.reverse()
    data = []
    for r in rows:
        d = [len(binned_data[r][c_label]['values']) for c_label in column_labels]
        print "Row:%d"%r, len(d), "=", d, "sum(d)=%d"%sum(d)
        data.insert(0, d)
    data.reverse()
    np_data = np.array(data)
    create_heat_map(column_labels, row_labels, np_data, min_value, max_value, title, xlabel, ylabel, output_filename, show)

def create_cum_pct_invalid_heat_map(keyed_results, binned_data=None, min_value=0, max_value=100, title=None, xlabel=None, ylabel=None, output_filename=None, show=False):
    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    row_key_counts = {}
    if binned_data is None:
        label_maker = lambda s,e: s + (1+e-s)/2

        binned_data = create_data_binned_data(keyed_results, START, bin_filter_builder=build_bin_filters_full_range_bell, label_maker=label_maker, max_v=303000)
        rows = binned_data.keys()
        column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])

        for r in rows:
            row_key_counts[r] = sum([cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels])

        binned_data = create_data_binned_data(keyed_results, END, bin_filter_builder=build_bin_filters_full_range_bell, label_maker=label_maker, max_v=303000)
        # rows = binned_data.keys()
        # updated_bin_data = {}
        # column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
        # new_labels = []
        # min_val = 0
        # data_num_valid = []
        # data_num_keys = []
        # # column_labels.reverse()
        # for c_label in column_labels:
        #     if c_label == 'default':
        #         continue
        #     nlabel = "%d-%d"%(min_val, min_val+20-1)
        #     min_val += 20
        #     new_labels.append(nlabel)


    # update binned data and
    # remap the column labels to time
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = ["%3d"%i for i in rows]
    #column_labels.reverse()

    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    cnt_not_live = lambda bd_col_vals: sum([1 for i in bd_col_vals if not i['live']])
    num_valid = []
    num_keys = []
    pct_invalid = []

    for r in rows:
        overallkeys = row_key_counts[r]
        invalid = [cnt_not_live(binned_data[r][c_label]['values']) for c_label in column_labels]
        results = [100* (float(invalid[x])/overallkeys) for x in xrange(0, len(invalid))]
        cum_invalid = [sum(results[0:x]) for x in xrange(1, len(invalid))]
        print "Num Keys Row:%d"%r, "=", overallkeys
        print "Percent invalid/total Row:%d"%r, len(cum_invalid), "=", cum_invalid, "sum(d)=%d"%sum(cum_invalid)
        #num_valid.insert(0, valid)
        #num_keys.insert(0, keys)
        pct_invalid.insert(0, cum_invalid)
    pct_invalid.reverse()
    #num_valid.reverse()
    #num_keys.reverse()
    print ("Columns :len=%d, %s "%(len(column_labels), column_labels))
    np_data = np.array(pct_invalid)
    create_heat_map(column_labels, row_labels, np_data, 0, 100, title, xlabel, ylabel, output_filename, show, cbar_ylabel="Cumulative Percent of Discoverable Dead SSL Keys")

def create_pct_valid_heat_map(keyed_results, binned_data=None, min_value=0, max_value=100, title=None, xlabel=None, ylabel=None, output_filename=None, show=False, num=1000):
    if binned_data is None:
        label_maker = lambda s,e: s + (1+e-s)/2
        binned_data = create_data_binned_data(keyed_results, START, bin_filter_builder=build_bin_filters, label_maker=label_maker, max_v=301000, num=num)

    # update binned data and
    # remap the column labels to time
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = [("%3d"%i if i%5 == 0 else '') for i in rows]
    #column_labels.reverse()

    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    cnt_live = lambda bd_col_vals: sum([1 for i in bd_col_vals if i['live']])
    num_valid = []
    num_keys = []
    pct_valid = []
    mv_test = 0
    for r in rows:
        keys = [cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels]
        valid = [cnt_live(binned_data[r][c_label]['values']) for c_label in column_labels]
        results = [100*(float(valid[x])/keys[x]) for x in xrange(0, len(valid))]
        pct_valid.insert(0, results)
    pct_valid.reverse()
    #num_valid.reverse()
    #num_keys.reverse()
    new_column_labels = ['' if (float(c)+num/2)%10000 != 0 else "%d"%( ((float(c)+num/2)/1000.0)) for c in column_labels]
    np_data = np.array(pct_valid)
    create_heat_map(new_column_labels, row_labels, np_data, 0, 100, title, xlabel, ylabel, output_filename, show,
         use_grid=False, cmap="Blues", cbar_ylabel="Percent of SSL Keys in a Bin that are Valid SSL Sessions", figsize=[16,11],
         xlim={"left":0, 'right':301},grid={'b':True, 'which':'both', 'axis':'y'})

def create_pct_invalid_heat_map(keyed_results, binned_data=None, min_value=0, max_value=100, title=None, xlabel=None, ylabel=None, output_filename=None, show=False, num=1000):
    if binned_data is None:
        label_maker = lambda s,e: s + (1+e-s)/2
        binned_data = create_data_binned_data(keyed_results, START, bin_filter_builder=build_bin_filters, label_maker=label_maker, max_v=301000, num=num)

    # update binned data and
    # remap the column labels to time
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = [("%3d"%i if i%5 == 0 else '') for i in rows]
    #column_labels.reverse()

    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    cnt_not_live = lambda bd_col_vals: sum([1 for i in bd_col_vals if not i['live']])
    num_valid = []
    num_keys = []
    pct_valid = []
    mv_test = 0
    for r in rows:
        keys = [cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels]
        valid = [cnt_not_live(binned_data[r][c_label]['values']) for c_label in column_labels]
        results = [100*(float(valid[x])/keys[x]) for x in xrange(0, len(valid))]
        pct_valid.insert(0, results)
    pct_valid.reverse()
    #num_valid.reverse()
    #num_keys.reverse()
    new_column_labels = ['' if (float(c)+num/2)%10000 != 0 else "%d"%( ((float(c)+num/2)/1000.0)) for c in column_labels]
    np_data = np.array(pct_valid)
    create_heat_map(new_column_labels, row_labels, np_data, 0, 100, title, xlabel, ylabel, output_filename, show,
         use_grid=False, cmap="Blues", cbar_ylabel="Percent of SSL Keys in a Bin that are expired but discoverable in memory", figsize=[16,11],
         xlim={"left":0, 'right':301},grid={'b':True, 'which':'both', 'axis':'y'})

def create_pct_diverging_heat_map(keyed_results, binned_data=None, min_value=0, max_value=100, title=None, xlabel=None, ylabel=None, output_filename=None, show=False, num=1000):
    if binned_data is None:
        label_maker = lambda s,e: s + (1+e-s)/2
        binned_data = create_data_binned_data(keyed_results, START, bin_filter_builder=build_bin_filters, label_maker=label_maker, max_v=301000, num=num)

    # update binned data and
    # remap the column labels to time
    column_labels = get_sorted_bin_filters_labels(binned_data.values()[0])
    rows = binned_data.keys()
    rows.sort()
    row_labels = [("%3d"%i if i%5 == 0 else '') for i in rows]
    #column_labels.reverse()

    cnt_keys = lambda bd_col_vals: 1.0 if len(bd_col_vals) == 0 else float(len(bd_col_vals))
    cnt_not_live = lambda bd_col_vals: sum([1 for i in bd_col_vals if not i['live']])
    cnt_live = lambda bd_col_vals: sum([1 for i in bd_col_vals if i['live']])
    pct_valid = []
    pct_invalid = []
    mv_test = 0
    # count invalid keys
    for r in rows:
        keys = [cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels]
        valid = [cnt_not_live(binned_data[r][c_label]['values']) for c_label in column_labels]
        results = [100*(float(valid[x])/keys[x]) for x in xrange(0, len(valid))]
        pct_invalid.insert(0, results)
    pct_invalid.reverse()

    # count valid keys
    for r in rows:
        keys = [cnt_keys(binned_data[r][c_label]['values']) for c_label in column_labels]
        valid = [cnt_live(binned_data[r][c_label]['values']) for c_label in column_labels]
        results = [100*(float(valid[x])/keys[x]) for x in xrange(0, len(valid))]
        pct_valid.insert(0, results)
    pct_valid.reverse()
    #num_valid.reverse()
    #num_keys.reverse()
    new_column_labels = ['' if (float(c)+num/2)%10000 != 0 else "%d"%( ((float(c)+num/2)/1000.0)) for c in column_labels]
    np_data_valid = np.array(pct_valid)
    np_data_invalid = np.array(pct_invalid)
    np_data = np_data_valid - np_data_invalid

    create_heat_map(new_column_labels, row_labels, np_data, min_value=-100, max_value=100, title=title, xlabel=xlabel,
          ylabel=ylabel, output_filename=output_filename, show=show,
         use_grid=False, cmap="RdBu", cbar_ylabel="Percent of Discoverable SSL Keys in Memory (Positive is net Live Keys and Negative is net Expired Keys) ", figsize=[16,11],
         xlim={"left":0, 'right':301},grid={'b':True, 'which':'both', 'axis':'y'}, num_cbar_ticks=8)



def create_heat_map(column_labels, row_labels, np_data, min_value=0, max_value=1000, title=None, xlabel=None, ylabel=None,
    output_filename=None, show=False, cbar_ylabel='# of elements', cmap='gray', use_grid=True, figsize=[16,11], use_frame=True,
    xlim={"left":None, 'right':None}, grid={'b':False, 'which':'major', 'axis':'both'}, num_cbar_ticks=4):

    fig_kwds = {'dpi':100, 'figsize':figsize, 'tight_layout':{'pad':0.4, 'w_pad':0.5, 'h_pad':1.0}}
    fig, ax = plt.subplots(**fig_kwds)


    if title:
        plt.title(title, fontsize=12)
    if xlabel:
        plt.xlabel(xlabel, fontsize=12)
    if ylabel:
        plt.ylabel(ylabel, fontsize=12)

    heatmap = ax.pcolor(np_data, cmap=plt.get_cmap(cmap))
    ax.set_frame_on(use_frame)
    ax.grid(**grid)
    ax.set_xticks(np.arange(np_data.shape[1])+0.5, minor=False)
    ax.set_yticks(np.arange(np_data.shape[0]), minor=False)
    cbar_label_fmt = "$%s$"
    mag = (max_value - min_value)
    quant = int (mag/num_cbar_ticks)

    cbar_mags = [i*quant+min_value for i in xrange(0, num_cbar_ticks)]
    cbar_labels = [cbar_label_fmt%i for i in cbar_mags]

    cbar = plt.colorbar(heatmap)
    cbar.ax.get_yaxis().set_ticks([])
    for j, lab in enumerate(cbar_labels):
        if j == 0 or j == len(cbar_ylabel)-1:
            #cbar.ax.text(.5, (2 * j) / float(2*len(cbar_labels)), lab, ha='center', va='center', size=12, fontweight=500)
            continue
        cbar.ax.text(.5, (2 * j) / float(2*len(cbar_labels)), lab, ha='center', va='center', size=12, fontweight=500)

    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(cbar_ylabel, rotation=270, fontsize=12)
    # want a more natural, table-like display
    #ax.invert_yaxis()
    #ax.xaxis.tick_top()

    ax.set_xticklabels(column_labels, minor=False)
    ax.set_xlim(**xlim)
    ax.set_yticklabels(row_labels, minor=False)

    #ax = plt.gca()

    for t in ax.xaxis.get_major_ticks():
        t.tick1On = len(t.label.get_text()) > 0
        t.tick2On = False
        t.label.set_fontsize(9)
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = len(t.label.get_text()) > 0
        t.tick2On = False
        t.label.set_fontsize(9)

    if not output_filename is None:
        plt.savefig(output_filename)
    if show:
        plt.show()



def bulk_heatmap(keyed_value_results, value_keys, outputlocation):
    fname_fmt = "%s-%s.png"
    try:
        os.mkdir(outputlocation)
    except:
        pass

    for key, keyed_values in keyed_value_results.items():
        avg_num_keys = np.mean([len(i) for i in keyed_values['run_values'].values()])
        #title = "%s heatmap of %s (Average Number of Keys: %d)"%(key, DUR_EXP, avg_num_keys)
        #filename = os.path.join(outputlocation, fname_fmt%(key, DUR_EXP))
        #create_exp_heat_map(keyed_values, binned_data=None, min_value=0, max_value=1000, title=title, ylabel="Run Number", xlabel="Time (Quantums of 5s)",show=False, output_filename=filename)
        model_name = keyed_values['model_name']
        try:
            title = "%s heatmap of %s (Average Number of Keys: %d)"%(model_name, AGE_KEY, int(avg_num_keys))
        except:
            continue
        model_abbr = keyed_values['model_abbr']
        filename = os.path.join(outputlocation, fname_fmt%(model_abbr.lower(), AGE_KEY))
        create_age_heat_map(keyed_values, binned_data=None, min_value=0, max_value=4000, num=250, title=title, ylabel="Experiment Run Number", xlabel="Time - Age in 250ms Bins",show=False, output_filename=filename)
        title = ''#"%s heatmap of Pct of Valid Keys (Average Number of Keys: %d)"%(model_name, int(avg_num_keys))
        #filename = os.path.join(outputlocation, fname_fmt%(key, "validkeys"))
        #create_pct_valid_heat_map(keyed_values, binned_data=None, min_value=0, max_value=100, title=title, ylabel="Experiment Run Number", xlabel="Time - Valid Keys in 1000ms Bins",show=False, output_filename=filename)
        #title = "%s heatmap of Pct of Invalid and Discoverable Keys (Average Number of Keys: %d)"%(key, avg_num_keys)
        #filename = os.path.join(outputlocation, fname_fmt%(key, "pct-discoverable-expired-keys-by-expiration-time"))
        #create_pct_invalid_heat_map(keyed_values, binned_data=None, min_value=0, max_value=100, title=title, ylabel="Experiment Run Number", xlabel="Time - Invalid Keys in 1000ms Bins",show=False, output_filename=filename)
        title = ""#"{} Heatmap of Discoverable TLS Key Material (Average Number of Keys: {:d})".format(model_name, int(avg_num_keys))
        filename = os.path.join(outputlocation, fname_fmt%(model_abbr.lower(), "pct-discoverable-keys-by-expiration-time"))
        create_pct_diverging_heat_map(keyed_values, binned_data=None, min_value=0, max_value=100, title=title, ylabel="Experiment Run Number", xlabel="Start Time of Discoverable Keys in One Second Bins",show=False, output_filename=filename)
        #title = "%s heatmap of Cumulative Pct of Invalid Keys at their Expiration Quantum (Average Number of Keys: %d)"%(key, avg_num_keys)
        #filename = os.path.join(outputlocation, fname_fmt%(key, "cum-pct-discoverable-expired-keys-by-expiration-time"))
        #create_cum_pct_invalid_heat_map(keyed_values, binned_data=None, min_value=0, max_value=100, title=title, ylabel="Run Number", xlabel="Time (Quantums of 10s and 2s)",show=False, output_filename=filename)


#int_dirs = results_dirs[4:6] + results_dirs[-2:]
#kresults = read_all_key_values(int_dirs, 'ssl_values', value_keys=['dur_exp', 'lifetime'], default_dead_values={'dur_exp':-1, 'lifetime':-1})

#results_dirs = ['/srv/nfs/cortana/logs/test_results_batch_x64_exp_gen_2560/']
#
#results_dirs = ['/srv/nfs/cortana/logs/test_results_batch_x64_exp_gen_2560/']
#value_keys=['dur_exp', 'age', 'tot_ssl', 'start', 'live', 'timestamp', 'end']
#t_heatmap_files = "/srv/nfs/cortana/logs/output/transaction/results_heatmaps_6_test"
#try:
#	os.mkdir(t_heatmap_files)
#except:
#	pass
##keyed_value_results = read_all_key_values(results_dirs, 'ssl_values', value_keys=value_keys, default_dead_values={'dur_exp':-1, 'lifetime':-1})
#
#tkeyed_value_results = read_all_key_values(results_dirs, 'ssl_values', is_streaming=False,value_keys=value_keys, default_dead_values={'dur_exp':-1, 'live':False})
#bulk_heatmap(tkeyed_value_results, value_keys, t_heatmap_files)
#
#s_heatmap_files = "/srv/nfs/cortana/logs/output/streaming/results_heatmaps"
#skeyed_value_results = read_all_key_values(int_dirs, 'ssl_values', is_streaming=True,value_keys=value_keys, default_dead_values={'dur_exp':-1, 'lifetime':-1, 'live':False})
#bulk_heatmap(skeyed_value_results, value_keys, s_heatmap_files)
#
#start = sifipr.time_str()
#sresults, sresults_x64 = load_all_dur_exp(results_dirs, is_streaming=True)
#end = sifipr.time_str()
#
#start = sifipr.time_str()
#tresults, tresults_x64 = load_all_dur_exp(results_dirs, is_streaming=False)
#end = sifipr.time_str()



