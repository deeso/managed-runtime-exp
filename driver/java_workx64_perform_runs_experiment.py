import sys, threading, time
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
sys.path.append(PATH_TO_RUN_MOD)
import single_factors_iteration_perform_runs as sifipr

RM_MEMORY = True
MAX_RUNNING =  5
if __name__ == "__main__":
    # ip -s -s neigh flush all
    use_python = False
    target_dir = ""
    start_str = sifipr.time_str()
    perform_profiling = False
    if len(sys.argv) < 5:
        print ("%s <32|64> <jvm_mem> <num_iterations> <results_base> <mem_press> [<sess_lifetime> <conc_session> <request_factor>]"%sys.argv[0])
        print ("Valid Factors: HIGH | MED | LOW ")
        sys.exit(-1)
    use_64 = sys.argv[1] == "64"
    jvm_mem = int(sys.argv[2])
    is_streaming = False
    num_iterations = int(sys.argv[3])
    results_fmt = "%s"
    transaction_results_base = sifipr.get_results_base(sys.argv[4], is_streaming=False)
    destroy_passwords = int(sys.argv[5])
    try:
        os.makedirs(transaction_results_base)
    except:
        pass

    sifipr.REMOVE_MEMORY = RM_MEMORY

    # server/service
    factors_server_01  = sifipr.get_factors_dict_str(*['HIGH','LOW', 'LOW',  'HIGH'])
    factors_server_02  = sifipr.get_factors_dict_str(*['HIGH','LOW', 'LOW',  'MED'])
    factors_server_03  = sifipr.get_factors_dict_str(*['HIGH','MED', 'LOW',  'HIGH'])
    factors_server_04  = sifipr.get_factors_dict_str(*['HIGH','MED', 'LOW',  'MED'])

    # idle client
    factors_thin_client_01  = sifipr.get_factors_dict_str(*['LOW', 'HIGH', 'LOW', 'LOW'])
    factors_thin_client_02  = sifipr.get_factors_dict_str(*['LOW', 'MED', 'LOW', 'LOW'])
    factors_thin_client_03  = sifipr.get_factors_dict_str(*['LOW', 'LOW', 'LOW', 'LOW'])
    factors_thin_client_04  = sifipr.get_factors_dict_str(*['LOW', 'HIGH', 'LOW','MED'])
    factors_thin_client_05  = sifipr.get_factors_dict_str(*['LOW', 'MED', 'LOW', 'MED'])
    factors_thin_client_06  = sifipr.get_factors_dict_str(*['LOW', 'LOW', 'LOW', 'MED'])

    # active gaming application
    factors_gaming_app_00  = sifipr.get_factors_dict_str(*['LOW', 'LOW', 'MED',  'LOW'])
    factors_gaming_app_01  = sifipr.get_factors_dict_str(*['MED', 'LOW', 'MED',  'LOW'])
    factors_gaming_app_02  = sifipr.get_factors_dict_str(*['HIGH', 'LOW', 'MED', 'LOW'])

    factors_list = [factors_server_01,]
    factors_list += [factors_server_02, factors_server_03, factors_server_04,]
    factors_list += [factors_thin_client_01, factors_thin_client_02, factors_thin_client_03, ]
    factors_list += [factors_thin_client_04, factors_thin_client_05, factors_thin_client_06, ]
    factors_list += [factors_gaming_app_00, factors_gaming_app_01, factors_gaming_app_02, ]

    #factors_list = [factors10]#, factors11, factors12]
    start_str = sifipr.time_str()

    JAVA_WORK_HOST = sifipr.JAVA_WORK_HOST_LIST if not use_64 else sifipr.JAVA_WORK_HOST_X64_LIST
    SSL_WORK_HOST = sifipr.SSL_HOST_LIST[:MAX_RUNNING]
    #stop_host_list(JAVA_WORK_HOST)

    MAX_RUNNING = min([MAX_RUNNING, len(JAVA_WORK_HOST), len(SSL_WORK_HOST)])
    if MAX_RUNNING > len(JAVA_WORK_HOST)/2:
        MAX_RUNNING = len(JAVA_WORK_HOST)/2

    if MAX_RUNNING*2 < len(JAVA_WORK_HOST):
        JAVA_WORK_HOST = JAVA_WORK_HOST[:MAX_RUNNING*2]
    print ("Experiment Log: Initializing VMs for the run")
    #stop_host_list(JAVA_WORK_HOST)
    #time.sleep(sifipr.SLEEP_SECS*12)
    sifipr.start_host_list(JAVA_WORK_HOST)
    print ("Sleeping waiting on the host to come up: %d s"%(sifipr.SLEEP_SECS))
    #time.sleep(sifipr.SLEEP_SECS*len(JAVA_WORK_HOST)/2)

    # init_available(JAVA_WORK_HOST_LIST, SSL_HOST_LIST)
    sifipr.init_available(JAVA_WORK_HOST, SSL_WORK_HOST, MAX_RUNNING)
    # for host in JAVA_WORK_HOST_LIST:
    if len(sifipr.AVAILABLE_EXP_HOSTS) < MAX_RUNNING:
        raise Exception("Failed to init to enough java hosts")

    #if len(sifipr.AVAILABLE_SSL) < len(SSL_WORK_HOST):
    #    raise Exception("Failed to init to all ssl hosts")

    print ("Experiment Log: VM startup and checks complete")
    active_threads = []
    destroy_passwords = 0
    # sotp comment
    #sifipr.WORKING_LIST = [i for i in generate_core_work_list()]
    #sifipr.WORKING_LIST.sort()
    ready_for_next_batch = True
    java_work_host_size = sifipr.get_java_host_len()
    print ("Experiment Log: Starting the work")

    sifipr.EXECING_VMS = True
    dumping_thread = threading.Thread(target=sifipr.handle_dumping_java_host)
    dumping_thread.start()
    sifipr.set_multiple_runs_working_list(factors_list, num_iterations, transaction_results_base)
    sifipr.start_compression_procs_thread()
    while not sifipr.is_working_list_empty()  or len (active_threads) > 0:
        if sifipr.is_java_host_avail() and sifipr.is_ssl_host_avail() and not sifipr.is_working_list_empty():
            while sifipr.HOSTS_DUMPING > sifipr.MAX_HOSTS_DUMPING:
                sl = 1 if sifipr.MAX_HOSTS_DUMPING == 0 else sifipr.MAX_HOSTS_DUMPING
                time.sleep( sifipr.HOSTS_DUMPING / float(sl))
            if sifipr.is_working_list_empty():
                break
            base_location, factors = sifipr.next_working_list_item()
            if base_location is None or factors is None:
                if sifipr.is_working_list_empty():
                    break
                continue
            java_host = sifipr.get_java_host()
            ssl_host = sifipr.get_ssl_host()
            if java_host is None:
                sifipr.add_ssl_host_back(ssl_host)
                continue
            cmd_success = sifipr.test_host_is_up(java_host, sifipr.JAVA_USER, sifipr.JAVA_PASS) and\
                          sifipr.test_host_is_up(ssl_host, sifipr.EXPSSL_USER, sifipr.EXPSSL_PASS)
            if not cmd_success:
                sifipr.add_ssl_host_back(ssl_host)
                sifipr.add_java_host_back(java_host)
                time.sleep(sifipr.SLEEP_SECS)
                continue
            else:
                print ("Experiment Log: Performing Java command using %s and %s, %d remaining"%(java_host, ssl_host, sifipr.working_list_len()))
                #java_ip = get_host_ip(java_host)
                #ssl_ip = get_host_ip(ssl_host)
                t = threading.Thread(target=sifipr.execute_command_steps, args=(java_host,
                                 ssl_host, factors, base_location, destroy_passwords, jvm_mem, is_streaming, use_64, use_python, perform_profiling))

                t.start()
                active_threads.append((t, java_host, ssl_host,
                                        factors, destroy_passwords))

                time.sleep(sifipr.SLEEP_SECS*2.5)
        else:

            #cull_threads(ACTIVE_PROC_EXTRACTION_THREADS)
            sifipr.cull_threads(active_threads)
            time.sleep(.1)
    while len (active_threads) > 0:
        if not sifipr.cull_threads(active_threads):
            time.sleep(.1)

    sifipr.set_perform_dumps(False)
    dumping_thread.join()
    sifipr.stop_compression_procs_thread()
    sifipr.wait_compression_procs_thread()
    sifipr.EXECING_VMS = False
    sifipr.stop_host_list(JAVA_WORK_HOST)
    #while len(ACTIVE_PROC_EXTRACTION_THREADS) > 0:
    #    if not cull_threads(ACTIVE_PROC_EXTRACTION_THREADS):
    #        time.sleep(.1)

    print "All threads completed."
    end_str = sifipr.time_str()
    print "%s Started processing"%start_str
    print "%s Ended processing round 1"%end_str

