from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, traceback
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
#from agg_exp_data import *
import single_factors_iteration_perform_runs as sifipr
from datetime import datetime, timedelta
 
PATH_TO_JVM_MODULE = "/research_data/code/git/jvm_analysis"
sys.path.append(PATH_TO_JVM_MODULE)
 
import jvm
from jvm.mem_chunks import MemChunk
from jvm.extract_process import ExtractProc
 
from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

TESTING = False
WORKING_LIST = []
SLEEP_SECS = 5.0
JAVA_USER = JAVA_PASS = "java"
EXPSSL_USER = EXPSSL_PASS = "expssl"
MAX_JAVA_HOST = 25 if not TESTING else 5
JAVA_HOST_FMT = "java-work-%d"
MAX_SSL_HOST = 25 if not TESTING else 5
SSL_HOST_FMT = "exp-ssl-%d"
MAX_RETRYS = 3
MAX_RETRY_SLEEP = 4.0
MAX_EXP_TIME = 5 # minutes
BATCH_HOST_SZ = 25 if not TESTING else 5

JAVALOG_STDERR = "stderr_log.txt"
JAVALOGBASE_DIR = "/srv/nfs/cortana/logs/output/"
ERROR_FILE = os.path.join(JAVALOGBASE_DIR, JAVALOG_STDERR)
ERROR_OUT_LOCK = threading.Lock()
#ERROR_OUT = open(ERROR_FILE, 'w')
 
JAVA_WORK_HOST_LIST = [JAVA_HOST_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
JAVA_WORK_HOST_LIST_2 = [JAVA_HOST_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]
SSL_HOST_LIST = [SSL_HOST_FMT%i for i in xrange(0, BATCH_HOST_SZ)]
SSL_HOST_LIST_2 = [SSL_HOST_FMT%i for i in xrange(BATCH_HOST_SZ, 2*BATCH_HOST_SZ)]
 
HOST_TO_IP_MAPPING = {}
FIND_POST_RE = re.compile(".*user=.*&password=.*Content-Type.*")
FIND_POST_ALL_RE = re.compile(".*user=.*&password=.*")

JBGREP_FILE = "jbgrep_strings.txt"
DUMPS_JBGREP_FILE = "jbgrep_strings.txt"
MERGED_KEYINFO = "merged_keyinfo.txt"
 
# sudo virsh destroy java-work-0
# virsh setmaxmem java-work-0 1048576 --config
# virsh start java-work-0
 
VIR_START = "virsh start {client}"
VIR_STOP = "virsh shutdown {client}"
#VIRT_CONN = libvirt.open("qemu:///system")


BASE_DIR = "/srv/nfs/cortana/logs/"
FAILED_POST_PROC_DUMP = "failed_post_proc.txt"
FAILED_POST_PROC_DUMP_F = os.path.join(BASE_DIR, FAILED_POST_PROC_DUMP)
FAILED_DUMP_OUT = open(FAILED_POST_PROC_DUMP_F, 'a')
 
SSLLOG_DIR = "ssllogs"
SSLLOG_BASE = os.path.join(BASE_DIR, SSLLOG_DIR)
SSLLOG_FMT_PREMASTER = "server-{ssl_host}-info-premaster.txt"
SSLLOG_FMT_KEYBLOCK = "server-{ssl_host}-info.txt"
SSLLOG_KEYBLOCK = "ssl_server_keyblock.txt"
SSLLOG_PREMASTER = "ssl_server_premaster.txt"
 
# factor directory
JAVALOG_DIR = "output"
JAVALOG_BASE = os.path.join(BASE_DIR, JAVALOG_DIR)
AUTH_LOG = JAVALOG_AUTH = "authlog.txt"
JSON_FILE = "agg_auth_data.json"
FAIL_FILE = "failed.txt"
COMPLETE_FILE = "completed.txt"
STDOUT_LOG = JAVALOG_STDOUT = "stdout_log.txt"
MISSING_USERS = "err_missing_users.txt"
FAIL_STRING = "FAILLAIF" * 3 
RESULTS_DIR = "results_no_stop_mod_vm"
RESULTS_BASE = os.path.join(BASE_DIR, RESULTS_DIR)

FACTOR_DIR_FMT = "{mem_pressure_factor}_{sess_lifetime_factor}"+\
             "_{conc_session}_{request_factor}"
 
FACTORS_MIN = 0
FACTORS_MAX = 4
FACTORS = {
"VHIGH":4,
"HIGH":3,
"MED":2,
"LOW":1,
"VLOW":0,
"RANDOM":-1,
4:"VHIGH",
3:"HIGH",
2:"MED",
1:"LOW",
0:"VLOW",
-1:"RANDOM"
}
 
CORE_FACTORS_MIN = 1
CORE_FACTORS_MAX = 3
CORE_FACTORS = {
"HIGH":3,
"MED":2,
"LOW":1,
"RANDOM":-1,
3:"HIGH",
2:"MED",
1:"LOW",
-1:"RANDOM"
}
 
FACTOR_NAMES = ["mem_pressure_factor", "sess_lifetime_factor"+\
             "conc_session", "request_factor"]

JVM_MEMORY_USED = 2560 
#JVM_MEMORY_USED = 512
JVM_NEW_MEMORY_USED = JVM_MEMORY_USED/2
JVM_SURVIVOR_MEM = JVM_NEW_MEMORY_USED/2
JVM_SURVIVOR_RATIO = (JVM_NEW_MEMORY_USED-2*JVM_SURVIVOR_MEM)/2
MEM_KEYS = {"jvm_mem":JVM_MEMORY_USED}
NEW_MEM_KEYS = {"jvm_mem":JVM_NEW_MEMORY_USED}
 
SURV_FACTOR = {"survivor_ratio":JVM_SURVIVOR_RATIO}
 
JAVA_CMD_ARGS = ["java", "-d32", "-Xnoclassgc",
        "-XX:-CMSClassUnloadingEnabled",
        "-XX:+UseSerialGC",
        "-XX:+AlwaysPreTouch",
        "-XX:InitialHeapSize={jvm_mem}M".format(**MEM_KEYS),
        "-XX:MaxHeapSize={jvm_mem}M".format(**MEM_KEYS),
        "-XX:NewSize={jvm_mem}M".format(**NEW_MEM_KEYS),
        "-XX:MaxNewSize={jvm_mem}".format(**NEW_MEM_KEYS),
        "-XX:InitialSurvivorRatio={survivor_ratio}".format(**SURV_FACTOR),
        "-jar",]

VOL_MEMMAP_FILE = "java_memmaps.txt" 
VOL_PATH = "/research_data/code/git/volatility/vol.py"
VOL_CMD = "python {vol} --profile {profile} -f {dump_file} -p {pid} linux_memmap > {target}"

JAR_BASE = DEST_LIBS

DUMPS_JBGREP_COUNT_RES_FILE = "dumps_found_ssl_keys_counts.txt"
DUMPS_JBGREP_FNAME_RES_FILE = "dumps_found_ssl_keys_f.txt"
DUMPS_JBGREP_GREP_RES_FILE = "dumps_found_ssl_keys_g.txt"
JBGREP_COUNT_RES_FILE = "found_ssl_keys_counts.txt"
JBGREP_FNAME_RES_FILE = "found_ssl_keys_f.txt"
JBGREP_GREP_RES_FILE = "found_ssl_keys_g.txt"
JBGREP_NUM_THREADS = 10
JBGREP_NUM_THREADS_NO_EXECING = 50

JAVA_JBGREP_JAR_FILE = "jbgrep-all-1.0.jar"
JAVA_JBGREP_JAR_CMD = os.path.join(JAR_BASE, JAVA_JBGREP_JAR_FILE)
JAVA_JBGREP_EXP_ARGS = " ".join(['java', '-jar', JAVA_JBGREP_JAR_CMD,
                       "-binaryFile {java_dump} ",
                       "-binaryStrings {jbgrep_strings} ",
                       "-byFilenameOutput {fname_output}",
                       "-grepOutput {grep_output}",
                       "-keyCountOutput {keycount_output}",
                       "-numThreads {num_threads}",
                       "-enableUnconstrainedRead",
                       #"-liveUpdate"
                       ])

FFASTRINGS_RES_FILE = "ffastrings_full.txt"
FFASTRINGS_LEN_RES_FILE = "ffastrings_len_full.txt"
FFASTRINGS_MIN_STR_LEN = 12
FFASTRINGS_MIN_STR_LEN = 12
FFASTRINGS_NUM_THREADS = 10
FFASTRINGS_NUM_THREADS_NO_EXECING = 50 

JAVA_FFASTRINGS_JAR_FILE = "ffastrings-all-1.0.jar"
JAVA_FFASTRINGS_JAR_CMD = os.path.join(JAR_BASE, JAVA_FFASTRINGS_JAR_FILE)
JAVA_FFASTRINGS_EXP_ARGS = " ".join(['java', '-jar', JAVA_FFASTRINGS_JAR_CMD,
                       "-binaryFile {java_dump} ",
                       #"-basicOutput {basic_output}",
                       "-outputWithByteLength {output_length}",
                       "-minStringLen {min_len}",
                       "-numThreads {num_threads}",
                       #"-liveUpdate"
                       ])

#JBGREP_WORKER_POOL = Pool(MAX_JAVA_HOST)
#JBGREP_WORKER_RESULTS = []


VIR_DUMP = """virsh dump {client} {dumploc} --bypass-cache""" +\
       """ --live --verbose --memory-only"""
 
JAVA_DUMPS = "java_dumps"
VIR_SNAPSHOT = """virsh snapshot-create {client}"""
VIR_REVERT = """virsh snapshot-revert {client} --current"""
 
JAVA_DUMP_FINAL = "java-work.dump"
STRING_OUTPUT = "strings_java_proc.txt"
STRING_OUTPUT_FULL = "strings_full.txt"
 
CHMOD_DUMP = "sudo chmod a+rw %s"
VOL_HOST_PROFILE = 'LinuxUbuntu1404x64x64'
VOL_HOST_PROFILE = 'LinuxUbuntu1404x86'
RM_MEM_DUMP = "rm {java_dump}"
 
AVAILABLE_JAVA = set()
AVAILABLE_SSL = set()

HEADER = "# Virtual Physical Size"
ALLOWED_PROC_EXTRACTION_THREADS_EXEC_VMS = 6 
ALLOWED_PROC_EXTRACTION_THREADS = 6 
ACTIVE_PROC_EXTRACTION_THREADS = []
PROC_EXTRACTION_QUEUE = []
COMPLETED = False
LISTENER_SOCKET = None
RUN_LISTENER = False
EXECING_VMS = False
def add_failed_dump(base_location):
    ERROR_OUT.acquire()
    FAILED_DUMP_OUT.write(base_location+"\n")
    ERROR_OUT.release()

def perform_string_extraction_thread(rm_dump=True):
    jobs = 0
    while not COMPLETED:
        # execute only one while VMs are in motion
        if len(ACTIVE_PROC_EXTRACTION_THREADS) == 0 and len(PROC_EXTRACTION_QUEUE) > 0:
            while len(ACTIVE_PROC_EXTRACTION_THREADS) < ALLOWED_PROC_EXTRACTION_THREADS and len(PROC_EXTRACTION_QUEUE) > 0:
                base_location, factors = PROC_EXTRACTION_QUEUE.pop()
                use_64 = base_location.find('x64') > -1
                t = Process(target=perform_post_mem_dump_task, args=(base_location, rm_dump, use_64))
                print ("%s: Processing %s, %d remaining"%(time_str(), base_location, len(PROC_EXTRACTION_QUEUE)))
                t.start()
                jobs+=1
                ACTIVE_PROC_EXTRACTION_THREADS.append((t, factors))
        elif len(PROC_EXTRACTION_QUEUE) > 0 and \
            ALLOWED_PROC_EXTRACTION_THREADS > len(ACTIVE_PROC_EXTRACTION_THREADS):
            base_location, factors = PROC_EXTRACTION_QUEUE.pop(0)
            use_64 = base_location.find('x64') > -1
            t = Process(target=perform_post_mem_dump_task, args=(base_location, rm_dump, use_64))
            t.start()
            print ("%s: Processing %s, %d remaining"%(time_str(), base_location, len(PROC_EXTRACTION_QUEUE)))
            ACTIVE_PROC_EXTRACTION_THREADS.append((t, factors))
            time.sleep(3)
            # XXXX -- Intentionally serialized here (allowing multiple dump analysis runs to take place.
            jobs+=1
            #t.join()
        if not cull_threads (ACTIVE_PROC_EXTRACTION_THREADS):
            time.sleep(1)
    print ("%s: VM executions completed, performing full bore SSL Key extraction of %d processes"%(time_str(), ALLOWED_PROC_EXTRACTION_THREADS))
    while len(PROC_EXTRACTION_QUEUE) > 0:
        if len(ACTIVE_PROC_EXTRACTION_THREADS) == 0:
            while len(ACTIVE_PROC_EXTRACTION_THREADS) < ALLOWED_PROC_EXTRACTION_THREADS and len(PROC_EXTRACTION_QUEUE) > 0:
                base_location, factors = PROC_EXTRACTION_QUEUE.pop()
                use_64 = base_location.find('x64') > -1
                t = Process(target=perform_post_mem_dump_task, args=(base_location, rm_dump, use_64))
                print ("%s: Processing %s, %d remaining"%(time_str(), base_location, len(PROC_EXTRACTION_QUEUE)))
                t.start()
                jobs+=1
                ACTIVE_PROC_EXTRACTION_THREADS.append((t, factors))
            time.sleep(2)
        elif len(ACTIVE_PROC_EXTRACTION_THREADS) < ALLOWED_PROC_EXTRACTION_THREADS and len(PROC_EXTRACTION_QUEUE) > 0:
            base_location, factors = PROC_EXTRACTION_QUEUE.pop()
            use_64 = base_location.find('x64') > -1
            t = Process(target=perform_post_mem_dump_task, args=(base_location, rm_dump, use_64))
            print ("%s: Processing %s, %d remaining"%(time_str(), base_location, len(PROC_EXTRACTION_QUEUE)))
            t.start()
            jobs+=1
            ACTIVE_PROC_EXTRACTION_THREADS.append((t, factors))
            time.sleep(3)

        if not cull_threads (ACTIVE_PROC_EXTRACTION_THREADS):
            time.sleep(.75)
    print ("%s: Exhausted string extraction jobs, waiting for %d to complete"%(time_str(), len(ACTIVE_PROC_EXTRACTION_THREADS)))
    while len(ACTIVE_PROC_EXTRACTION_THREADS) > 0:
        ACTIVE_PROC_EXTRACTION_THREADS[0][0].join()
        if not cull_threads (ACTIVE_PROC_EXTRACTION_THREADS):
            time.sleep(.75)        
    
    print ("%s: Completed all %d jobs"%(time_str(), jobs))

def perform_string_extraction_thread_no_rm():
    perform_string_extraction_thread(rm_dump=False)

def add_job_to_queue(base_location, factors):
    PROC_EXTRACTION_QUEUE.append((base_location, factors))
 
def log_error(msg):
    ERROR_OUT_LOCK.acquire()
    ERROR_OUT.write(msg+"\n")
    ERROR_OUT_LOCK.release()


def get_vol_memmap_file(base_location):
    return os.path.join(base_location, VOL_MEMMAP_FILE)

def get_vol_memmap_file(base_location):
    return os.path.join(base_location, VOL_MEMMAP_FILE)

def dump_process_memmaps(base_location, results={}, use_64=False):
    profile = sifipr.get_vol_profile(use_64=use_64)
    dump_file = get_java_dump_location(base_location)
    ex_java = None
    try:
        ex_java = ExtractProc(the_file=dump_file, profile=profile)
        ex_java.update_process_info()
        phys_vaddr_sz_list = ex_java.get_memmap_summary()
        lines = ["%09x %09x %09x"%(v, p, s) for p,v,s in phys_vaddr_sz_list]
        lines.sort()
        content = HEADER + "\n".join(lines)
        open(get_vol_memmap_file(base_location), 'w').write(content)
        results['rm_mem'] = False
    except:
        results['rm_mem'] = False
        raise
        #msg = "%s: Failed to find Java in %s "%(time_str(), dump_file)
        #log_error(msg)
        #return ''
    return results['rm_mem']

def dump_process_memory(base_location, results={}, use_64=False):
    dump_dir = get_java_dumps_location(base_location)
    profile = sifipr.get_vol_profile(use_64=use_64)
    dump_file = get_java_dump_location(base_location)
    ex_java = None
    try:
        ex_java = ExtractProc(the_file=dump_file, profile=profile)
        ex_java.update_process_info()
        chunks = MemChunk.chunks_from_task_or_file(task=ex_java.proc, MChunkCls=MemChunk)
        for chunk in chunks.values():
            chunk.check_load()
         
        for chunk in chunks.values():
            chunk.dump_data(outdir=dump_dir)
        
        results['rm_mem'] = False
    except:
        results['rm_mem'] = False
        raise
        #msg = "%s: Failed to find Java in %s "%(time_str(), dump_file)
        #log_error(msg)
        #return ''
    return results['rm_mem']

def get_ffastrings_len_res_name(base_location):
    return os.path.join(base_location, FFASTRINGS_LEN_RES_FILE)

def get_ffastrings_res_name(base_location):
    return os.path.join(base_location, FFASTRINGS_RES_FILE)

def create_ffastrings_java_cmd (java_dump, output_length, min_len, num_threads):
    keyed = {"java_dump":java_dump,
             "output_length":output_length,
             "min_len":'%02x'%min_len,
             "num_threads":'%02x'%num_threads}
    return JAVA_FFASTRINGS_EXP_ARGS.format(**keyed)

def get_ffastrings_java_cmd (base_location):
    java_dump = get_java_dump_location(base_location)
    #basic_output = get_ffastrings_res_name(base_location)
    output_length = get_ffastrings_len_res_name(base_location)
    min_len = FFASTRINGS_MIN_STR_LEN
    num_threads = FFASTRINGS_NUM_THREADS_NO_EXECING
    cmd = create_ffastrings_java_cmd(java_dump, output_length, min_len, num_threads)
    return cmd

def create_strings_output_full (base_location):
    strings_location = os.path.join(base_location, STRING_OUTPUT_FULL)
    java_dumps = os.path.join(base_location, JAVA_DUMP_FINAL)
    cmd_fmt_0 = "strings -n 8 -t x -f %s > %s"
    cmd = cmd_fmt_0%(java_dumps, strings_location)
    result = os.system(cmd)
    return result

def get_jbgrep_dumps_grep_res_name(base_location):
    return os.path.join(base_location, DUMPS_JBGREP_GREP_RES_FILE)

def get_jbgrep_dumps_fname_res_name(base_location):
    return os.path.join(base_location, DUMPS_JBGREP_FNAME_RES_FILE)

def get_jbgrep_dumps_count_res_name(base_location):
    return os.path.join(base_location, DUMPS_JBGREP_COUNT_RES_FILE)
 
def get_jbgrep_name(base_location):
    return os.path.join(base_location, JBGREP_FILE)

def get_jbgrep_grep_res_name(base_location):
    return os.path.join(base_location, JBGREP_GREP_RES_FILE)

def get_jbgrep_fname_res_name(base_location):
    return os.path.join(base_location, JBGREP_FNAME_RES_FILE)

def get_jbgrep_count_res_name(base_location):
    return os.path.join(base_location, JBGREP_COUNT_RES_FILE)

def create_jbgrep_java_cmd (java_dump, jbgrep_strings, jbgrep_grep_res, jbgrep_fname_res, jbgrep_count_res, num_threads):
    keyed = {"java_dump":java_dump,
             "jbgrep_strings":jbgrep_strings,
             "fname_output":jbgrep_fname_res,
             "grep_output":jbgrep_grep_res,
             "num_threads":'%02x'%num_threads,
             "keycount_output":jbgrep_count_res}
    return JAVA_JBGREP_EXP_ARGS.format(**keyed)

def get_jbgrep_java_dumps_cmd (base_location):
    java_dump = get_java_dumps_location(base_location)
    jbgrep_strings = get_jbgrep_name(base_location)
    jbgrep_grep_res = get_jbgrep_dumps_grep_res_name(base_location)
    jbgrep_fname_res = get_jbgrep_dumps_fname_res_name(base_location)
    jbgrep_count_res = get_jbgrep_dumps_count_res_name(base_location)
    num_threads = JBGREP_NUM_THREADS if EXECING_VMS else JBGREP_NUM_THREADS_NO_EXECING
    cmd = create_jbgrep_java_cmd (java_dump, jbgrep_strings, jbgrep_grep_res, jbgrep_fname_res, jbgrep_count_res, num_threads)
    return cmd

def get_jbgrep_java_cmd (base_location):
    java_dump = get_java_dump_location(base_location)
    jbgrep_strings = get_jbgrep_name(base_location)
    jbgrep_grep_res = get_jbgrep_grep_res_name(base_location)
    jbgrep_fname_res = get_jbgrep_fname_res_name(base_location)
    jbgrep_count_res = get_jbgrep_count_res_name(base_location)
    num_threads = JBGREP_NUM_THREADS if EXECING_VMS else JBGREP_NUM_THREADS_NO_EXECING
    cmd = create_jbgrep_java_cmd (java_dump, jbgrep_strings, jbgrep_grep_res, jbgrep_fname_res, jbgrep_count_res, num_threads)
    return cmd

def perform_post_mem_dump_task(base_location, user_rm_dump, use_64):
    start_time = time_str()
    if get_java_post_process_complete(base_location):
        print "post processing complete, nothing to do."
        return
    dump_file = get_java_dump_location(base_location)
    profile = sifipr.get_vol_profile(use_64=use_64)
    try:
        os.stat(dump_file)
    except:
        print ("%s Dump file not found: %s ... done"%(time_str(), dump_file))
        set_java_post_process_fail(base_location, msg="dump file missing")
        return

    outstr = ("%s: Running strings_full: %s"%(time_str(), base_location))
    print (outstr)
    #log_error(outstr)
    results = {}
    q = Process(target=dump_process_memory, args=(base_location,results, use_64))
    q.start()
    s = Process(target=dump_process_memmaps, args=(base_location,results, use_64))
    s.start()
    outstr = ("%s: Dumping %s"%(time_str(), base_location))
    print (outstr)
    outstr = ("%s: Perforiming ssl key search %s"%(time_str(), base_location))
    print (outstr)
    t = Process(target=perform_ssl_key_search, args=(base_location,))
    t.start()
    outstr = ("%s: Perforiming strings search %s"%(time_str(), base_location))
    print (outstr)
    u = Process(target=perform_ffastrings, args=(base_location,))
    u.start()
    q.join()
    s.join()
    t.join()
    u.join()
    try:
        r = Process(target=perform_dumps_ssl_key_search, args=(base_location,))
        r.start()
        r.join()
    except:
        traceback.print_exc()
        
    rm_dump_file = False
    try:
        ex_java = ExtractProc(the_file=dump_file, profile=profile)
        ex_java.update_process_info()
        rm_dump_file = False and user_rm_dump
    except:
        pass
    outstr = ("%s: Completed SSL key search and string dump in %s"%(time_str(), base_location))
    print (outstr)
    # XXXX - Attempting to alleviate space by deleting the dump after the extracting strings and 
    # key locations
    if rm_dump_file:
        perform_rm_memory_dump (base_location)
        outstr = ("%s: Removed the memory dump %s"%(time_str(), base_location))
    else:
        outstr = ("%s: Failure occured, the memory dump not removed %s"%(time_str(), base_location))
        set_java_post_process_fail(base_location, msg=outstr)
    print (outstr)
    set_java_post_process_complete(base_location)
    print("%s: job complete start"%(start_time))
    print("%s: job complete end %s"%(time_str(), base_location))

def get_java_post_proc_file(base_location):
    return os.path.join(base_location, COMPLETE_FILE)

def get_java_post_proc_file_fail(base_location):
    return os.path.join(base_location, FAIL_FILE)

def set_java_post_process_complete(base_location):
    open(get_java_post_proc_file(base_location), 'w').write("done")

def set_java_post_process_fail(base_location, msg='failed'):
    open(get_java_post_proc_file_fail(base_location), 'w').write(msg)

def get_java_post_process_complete(base_location):
    complete = True
    try:
        os.stat(get_java_post_proc_file(base_location))
    except:
        complete = False
    return complete
 
def get_java_dumps_location(base_location):
    java_base = base_location
    return os.path.join(java_base, JAVA_DUMPS, '')

def get_java_dump_location(base_location):
    java_base = base_location
    return os.path.join(java_base, JAVA_DUMP_FINAL)
 
def get_vol_java_dump_location(base_location):
    java_base = base_location
    return "file:///" + os.path.join(java_base, JAVA_DUMP_FINAL)

def perform_dumps_ssl_key_search(base_location):
    jbgrep_cmd = get_jbgrep_java_dumps_cmd(base_location)
    exec_command (jbgrep_cmd)

def perform_ssl_key_search(base_location):
    jbgrep_cmd = get_jbgrep_java_cmd(base_location)
    exec_command (jbgrep_cmd)

def perform_ffastrings(base_location):
    ffastrings_cmd = get_ffastrings_java_cmd(base_location)
    exec_command (ffastrings_cmd)

def perform_rm_memory_dump(base_location):
    java_dump = get_java_dump_location (base_location)
    rm_cmd = RM_MEM_DUMP.format(**{"java_dump":java_dump})
    exec_command (rm_cmd)

def exec_command (cmd):
    cmdlist = []
    if isinstance(cmd, list):
        cmdlist = cmd
        cmd = " ".join(cmd)
    elif isinstance(cmd, str):
        cmdlist = cmd.split()
    else:
        raise Exception("exec_command requires a string or list as a parameter")
 
    print ("%s: Executing cmd: %s"%(time_str(), cmd ))
    p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()
    content = p.communicate()[0].decode('ascii', 'replace')
    content = content.replace('\x1b[0K\n', '').replace('\x1b[0K\r', '')
    return content

def cull_threads (active_threads):
    cnt = 0
    # lock acquire
    orig_len = len(active_threads)
    while cnt < len(active_threads):
        if active_threads[cnt][0].is_alive():
            cnt += 1
            continue
        active_threads.pop(cnt)
    return len(active_threads) == 0 or len(active_threads) != orig_len

def listener_for_files(host="10.16.121.15", port=4545):
    global LISTENER_SOCKET, RUN_LISTENER
    RUN_LISTENER = True
    LISTENER_SOCKET = socket.socket()
    LISTENER_SOCKET.bind((host, port))
    LISTENER_SOCKET.listen(1000)
    LISTENER_SOCKET.setblocking(0)
    inputs = [LISTENER_SOCKET]
    outputs = []
    print ("Starting listener on %s:%d"%(host, port))
    try:
        while RUN_LISTENER:
            readable, writable, exceptional = select.select(inputs, outputs, inputs)
            for s in readable:
                if s is LISTENER_SOCKET:
                    c, a = LISTENER_SOCKET.accept()
                    data = c.recv(4096)
                    print ("Recv'ed: %s"%data)
                    if data.lower().find("DONE") > -1:
                       COMPLETED = True
                       RUN_LISTENER = False
                       break
                    elif data.lower().find("KILL") > -1:
                       COMPLETED = True
                       RUN_LISTENER = False
                       break
                    add_job_to_queue(data, {})
            time.sleep(1)
    except:
        COMPLETED = True
        raise
        

if __name__ == "__main__":
    # ip -s -s neigh flush all
    target_dir = ""
    start_str = time_str()
    if len(sys.argv) < 4:
        print ("%s <pcnt> <log_dir> [-f <base_locations_dir> | -s HOST PORT] "%sys.argv[0])
        sys.exit(-1)
    
    ALLOWED_PROC_EXTRACTION_THREADS = 5 # int(sys.argv[1])
    do_file = sys.argv[3] == "-f"
    if do_file:
        base_locations_dir = sys.argv[4]
        BASE_DIR = sys.argv[2]
        FAILED_POST_PROC_DUMP = "failed_post_proc.txt"
        FAILED_POST_PROC_DUMP_F = os.path.join(BASE_DIR, FAILED_POST_PROC_DUMP)
        FAILED_DUMP_OUT = open(FAILED_POST_PROC_DUMP_F, 'a')
        base_locations = []
        for base_locations_dir in sys.argv[4:]:
            base_locations += [os.path.join(base_locations_dir, i) for i in os.listdir(base_locations_dir)]

        base_locations.sort()
        active_post_process_memdump_thread = threading.Thread(target=perform_string_extraction_thread)
        for base_location in base_locations:
          factors = {}
          add_job_to_queue(base_location, factors)
        COMPLETED = True
    else:
        RUN_LISTENER = True
        host = sys.argv[4]
        port = int(sys.argv[5])
        active_post_process_memdump_thread = threading.Thread(target=perform_string_extraction_thread)
        active_post_process_memdump_listener = threading.Thread(target=listener_for_files, args=(host, port))
        active_post_process_memdump_listener.start()
    
    active_post_process_memdump_thread.start()

    active_post_process_memdump_thread.join()
