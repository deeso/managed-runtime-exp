import sh
import itertools
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
from multiprocessing import Process
import os, urllib, json, pipes, traceback
import binascii, subprocess, json, shutil
import multiprocessing
from datetime import datetime
import numpy as np
from numpy import mean
from pandas import DataFrame
#from single_factors_iteration_perform_runs import *
import single_factors_iteration_perform_runs as sifipr
import matplotlib.pyplot as plt
AUGMENTING_DATA = "augmented_data.txt"


def parse_num_request(line):
    _time, _numreq = line.split("-")
    _time = eval('0x'+_time.split(':')[1])
    _numreq = eval(_numreq.split(':')[1])
    return {'time':_time, 'num_request':_numreq}

def get_introspection_data(base_location, archive='gc_log.txt.zip', gclog_file='java.hprof.txt'):
    print "%s: Loading Java Heap Profile from %s:%s"%(time_str(), archive, hprof_file)
    zipped_data = zipfile.ZipFile(os.path.join(base_location, archive))

    gclog_files = [i for i in zipped_data.namelist() if i.find(gclog_file) > -1]
    if len(gclog_files) > 0:
        gclog_file = gclog_files[0]
    elif len(zipped_data.namelist()) > 0:
        gclog_file = zipped_data.namelist()[0]
    else:
        raise Exception("Failed to specify the proper file in the java.hprof archive")

    data = zipped_data.open(gclog_file).read()
    return data


