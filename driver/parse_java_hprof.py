from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, calendar, zipfile, traceback
from socket import error as socket_error
from datetime import datetime, timedelta
from datetime import datetime

def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))


ROOT_STR = "ROOT"
CLS_STR = "CLS"
OBJ_STR = "OBJ"
ARR_STR = "ARR"

ROOT_TYPE = 0
CLASS_TYPE = 1
OBJ_TYPE = 2
FIELD_TYPE = 3
ARR_TYPE = 4
CLS_FIELD_TYPE = 5
ARR_FIELD_TYPE = 6

HEAP_DUMP_BEGIN = "HEAP DUMP BEGIN"
HEAP_DUMP_END = "HEAP DUMP END"

SITES_BEGIN = "SITES BEGIN"
SITES_END = "SITES END"

THREADS_BEGIN = "THREAD START"
THREADS_END = "TRACE"
THREAD_BEGIN = "THREAD START"
THREAD_END = "THREAD END"

TRACE_BEGIN = "TRACE"
TRACE_END = HEAP_DUMP_BEGIN

def get_thread_lines(lines):
    dump_lines = []
    add_heap_dump_lines = False
    for i in lines:
        if i.find(THREADS_BEGIN) == 0 and\
           i.find("(") > -1:
            add_heap_dump_lines = True
        if add_heap_dump_lines and\
           i.find(THREADS_END) == 0:
            break
        elif add_heap_dump_lines:
            dump_lines.append(i)
    return dump_lines

def get_trace_lines(lines):
    dump_lines = []
    add_heap_dump_lines = False
    for i in lines:
        if i.find(TRACE_BEGIN) == 0 and\
           i.find(':') == len(i)-1:
            add_heap_dump_lines = True
        if i.find(TRACE_END) == 0:
            break
        if add_heap_dump_lines:
            dump_lines.append(i)
    return dump_lines

def get_heap_dump_lines(lines):
    dump_lines = []
    add_heap_dump_lines = False
    for i in lines:
        if i.find(HEAP_DUMP_BEGIN) == 0:
            add_heap_dump_lines = True

        if i.find(HEAP_DUMP_END) == 0:
            break
        elif add_heap_dump_lines:
            dump_lines.append(i)
    return dump_lines

def get_sites_dump_lines(lines):
    dump_lines = []
    add_heap_dump_lines = False
    for i in lines:
        if i.find(SITES_BEGIN) == 0:
            add_heap_dump_lines = True

        if i.find(SITES_END) == 0:
            break
        elif add_heap_dump_lines:
            dump_lines.append(i)
    return dump_lines

BASE_SITE = {'rank':-1, 'class_name':'', 'self':-1.0, 'accum':-1.0,
            'trace_id':-1,  'live_bytes':-1, 'live_objs':-1,
            'alloc_bytes':-1, 'alloc_objs':-1,'trace':None}

BASE_THREAD = {'name':-1, 'group':'', 'thread_id':-1.0, 'obj':-1.0,
            'addr':-1,  'heap_loc':-1, 'live':True, 'in_root_set':False}

BASE_DATA = {'class_name':'', 'kind':'', 'id':-1,
            'trace_id':-1,  'thread':-1, 'frame':-1,
            'fields':None, 'sz':-1, 'addr':-1,
            'heap_loc':-1, 'nelems':-1, 'elem type':'',
            'class_addr':-1, 'class_heap_loc':-1}

BASE_TRACE = {'trace_id':-1, 'caller_class_name':'', 'caller_method_name':'',
              'caller':'', 'callee':'', 'back_trace':None,
              'callee_class_name':'', 'callee_method_name':'','site':None}

address_handler = lambda heap_loc: int(heap_loc.strip(), 16) << 4
address_printer = lambda addr: "%08x"%addr if addr else ""

heap_loc_handler = lambda heap_loc: long(heap_loc.strip(), 16)
heap_loc_printer = lambda heap_loc: "%08x"%heap_loc if heap_loc else ""

generic_hex_handler = lambda v: int(v.strip(), 16)
generic_int_handler = lambda v: int(v.strip())
generic_lhex_handler = lambda v: long(v.strip(), 16)
generic_long_handler = lambda v: long(v.strip())
generic_float_handler = lambda v: float(v.strip())
generic_int_printer = lambda v: "%d"%v if v else ""

generic_str_handler = lambda v: v.strip()
generic_str_printer = lambda v: v if v else ""

class_name_extract_from_backtrace_line = lambda line: ".".join(line.split('(')[0].split('.')[:-1])
method_name_extract_from_backtrace_line = lambda line: line.split('(')[0].split('.')[-1]

def parse_threads_lines(lines):
    threads = {}
    for line in lines:
        #print line
        if line.find(THREAD_BEGIN) == 0:
            parenthetical = line.split("(")[-1].strip(")")
            results = thread_paren_handler(parenthetical)
            thread_id = results['thread_id']
            #print results
            o = {}
            o.update(BASE_THREAD)
            for k in o:
                o[k] = results.get(k, o[k])
            threads[o['thread_id']] = o
        elif line.find(THREAD_END) == 0:
            parenthetical = line.split("(")[-1].strip(")")
            results = thread_paren_handler(parenthetical)
            thread_id = results['thread_id']
            threads[thread_id]['live'] = False
    return threads

def parse_sites_lines(lines, traces={}):
    sites = []
    for i in lines[3:]:
        results = {}
        results.update(BASE_SITE)
        items = i.strip().replace("%", '').split()
        results['rank'] = generic_int_handler(items[0])
        results['self'] = generic_float_handler(items[1])
        results['accum'] = generic_float_handler(items[2])
        results['live_bytes'] = generic_long_handler(items[3])
        results['live_objs'] = generic_int_handler(items[4])
        results['alloc_bytes'] = generic_long_handler(items[5])
        results['alloc_objs'] = generic_int_handler(items[6])
        results['trace_id'] = generic_int_handler(items[7])
        results['class_name'] = generic_str_handler(items[8])
        if results['trace_id'] in traces:
            results['trace'] = traces[results['trace_id']]
            traces[results['trace_id']]['site'] = results
        sites.append(results)
    return sites

def parse_traces_lines(lines):
    traces = {}
    po = None
    for line in lines:
        line = line.strip()
        if line.find(TRACE_BEGIN) == 0 and\
            line.find(':') == len(line)-1:
            o = {}
            o.update(BASE_TRACE)
            o['trace_id'] = generic_long_handler(line.split()[-1].strip(':').strip())
            traces[o['trace_id']] = o
            o['back_trace'] = []
            if not po is None and len(po['back_trace']) > 0:
                po['caller'] = po['back_trace'][-1]
                po['callee'] = po['back_trace'][0]
                if po['caller'] != '<empty>':
                    po['caller_class_name'] = class_name_extract_from_backtrace_line(po['caller'])
                    po['callee_class_name'] = class_name_extract_from_backtrace_line(po['callee'])
                    po['caller_method_name'] = method_name_extract_from_backtrace_line(po['caller'])
                    po['callee_method_name'] = method_name_extract_from_backtrace_line(po['callee'])
            po = o
            continue
        elif not po is None:
            po['back_trace'].append(line)
    return traces

class GenericItem(object):
    def __init__(self, _dict):
        for k in _dict:
            setattr(self, k, _dict[k])

def parse_heap_dump_lines(lines):
    last_loc = -1
    in_obj = False
    in_cls = False
    in_arr = False
    results = {}
    po = None
    results['klasses_by_name'] = {}
    results['klasses'] = {}
    results['instances'] = {}
    results['objects'] = {}
    results['roots'] = {}
    results['heap_locs'] = {}
    results['arrays'] = {}
    results['num_objects'] = -1
    results['num_bytes'] = -1
    if len(lines) < 10:
        print ("Something failed, no line to parse for the heap dump")
        return results

    line = lines[0]

    num_objects = line.split('(')[1].split(" objects")[0].strip()
    num_bytes = line.split(' bytes)')[0].split(" objects,")[1].strip()
    results['num_objects'] = long(num_objects)
    results['num_bytes'] = long(num_bytes)

    for line in lines[1:]:
        if line.find(HEAP_DUMP_END) == 0:
            break
        o = None
        o_type = -1
        o_loc = -1
        #print line
        if line.find(ROOT_STR) == 0:
            o = HANDLERS[ROOT_STR](line)
            o_type = o['type']
            o_loc = o['heap_loc']
            o['class_name'] = o['name'] if 'name' in o else ''
            if 'name' in o:
                del o['name']
            in_cls = in_arr = in_obj = False
        elif line.find(ARR_STR) == 0:
            o = HANDLERS[ARR_STR](line)
            o_type = o['type']
            o_loc = o['heap_loc']
            in_arr = True
            in_cls = in_obj = False
        elif line.find(OBJ_STR) == 0:
            o = HANDLERS[OBJ_STR](line)
            o_type = o['type']
            o_loc = o['heap_loc']
            in_obj = True
            in_arr = in_cls = False
            po = o
        elif line.find(CLS_STR) == 0:
            o = HANDLERS[CLS_STR](line)
            o_type = o['type']
            o_loc = o['heap_loc']
            o['class_name'] = o['name']
            del o['name']
            in_cls = True
            in_arr = in_obj = False
            po = o
            results['klasses_by_name'][o['class_name']] = o
        elif in_obj:
            o = HANDLERS['field'](line)
            o_type = o['type']
            o_loc = o['heap_loc']
        elif in_cls:
            o = HANDLERS['cls_field'](line)
            o_type = o['type']
            o_loc = o['heap_loc']
        elif in_arr:
            o = HANDLERS['arr_field'](line)
            o_type = o['type']
            o_loc = o['heap_loc']
        else:
            raise

        results['heap_locs'][o_loc] = o
        if o_type == ROOT_TYPE:
            results['roots'][o_loc] = o
        elif o_type == OBJ_TYPE:
            results['instances'][o_loc] = o
            results['objects'][o_loc] = o
        elif o_type == ARR_TYPE:
            results['instances'][o_loc] = o
            results['arrays'][o_loc] = o
        elif o_type == FIELD_TYPE:
            po['fields'][o['name']] = o
        elif o_type == CLASS_TYPE:
            results['klasses'][o_loc] = o
        elif o_type == CLS_FIELD_TYPE:
            po['fields'][o['name']] = o

    for r in results['roots'].values():
        if r['class_name'] in results['klasses_by_name']:
            r['class_class'] = results['klasses_by_name'][r['class_name']]

    for a in results['arrays'].values():
        if a['class_name'] in results['klasses_by_name']:
            a['class_addr'] = results['klasses_by_name'][a['class_name']]['class_addr']
            a['class_heap_loc'] = results['klasses_by_name'][a['class_name']]['class_heap_loc']

    return results

def class_handler(klass):
    name,addr = klass.split("@")
    results = {}
    results['class_name'] = name
    results['class_addr'] = HANDLERS['class_addr'](addr)
    results['class_heap_loc'] = HANDLERS['class_heap_loc'](addr)
    return results

def root_handler(line):
    results = {}
    results.update(BASE_DATA)
    if line.find(ROOT_STR) != 0:
        return None
    prepend, parenthetical = line.split("(")
    results['type'] = ROOT_TYPE
    results['fields'] = {}
    results.update(generic_prepend(prepend))
    results.update(generic_paren_handler(parenthetical))
    return results

def arr_handler(line):
    results = {}
    results.update(BASE_DATA)
    if line.find(ARR_STR) != 0:
        return None
    prepend, parenthetical = line.split("(")
    results['type'] = ARR_TYPE
    results['fields'] = {}

    results.update(generic_prepend(prepend))
    results.update(generic_paren_handler(parenthetical))
    results['trace_id'] = generic_int_handler(results['trace'])
    if 'elem type' in results and\
       results['elem type'].find('[]@') > 0:
        caddr = results['elem type'].split('[]@')[1]
        cname = results['elem type'].split('[]@')[0]
        results['class_addr'] = HANDLERS['addr'](caddr)
        results['class_heap_loc'] = HANDLERS['heap_loc'](caddr)
        results['class_name'] = cname
    elif 'elem type' in results:
        cname = results['elem type'].replace('[]', '')
        results['class_name'] = cname

    del results['elem type']
    del results['frame']
    del results['kind']
    del results['id']
    del results['thread']
    del results['trace']
    return results

def cls_handler(line):
    results = {}
    results.update(BASE_DATA)
    if line.find(CLS_STR) != 0:
        return None
    prepend, parenthetical = line.split("(")
    results['type'] = CLASS_TYPE
    results['fields'] = {}
    results.update(generic_prepend(prepend))
    results.update(generic_paren_handler(parenthetical))
    results['class_addr'] = results['addr']
    results['class_heap_loc'] = results['heap_loc']
    results['trace_id'] = generic_int_handler(results['trace'])
    del results['trace']
    del results['id']
    del results['frame']
    del results['kind']
    del results['nelems']
    del results['sz']
    del results['thread']
    return results

def obj_handler(line):
    results = {}
    results.update(BASE_DATA)
    if line.find(OBJ_STR) != 0:
        return None
    prepend, parenthetical = line.split("(")
    results['type'] = OBJ_TYPE
    results['fields'] = {}
    if parenthetical.find('@') > 0:
        caddr = parenthetical.split('@')[1].split(')')[0]
        results['class_addr'] = HANDLERS['addr'](caddr)
        results['class_heap_loc'] = HANDLERS['heap_loc'](caddr)

    results.update(generic_prepend(prepend))
    results.update(generic_paren_handler(parenthetical))
    results['trace_id'] = generic_int_handler(results['trace'])
    del results['trace']
    del results['id']
    del results['frame']
    del results['kind']
    del results['nelems']
    del results['thread']
    del results['elem type']
    return results

def generic_prepend(prepend):
    results = {}
    addr = prepend.split()[-1].strip()
    results['addr'] = HANDLERS['addr'](addr)
    results['heap_loc'] = HANDLERS['heap_loc'](addr)
    return results

def generic_paren_handler(parenthetical):
    results = {}
    parenthetical = parenthetical.strip().strip(")").strip("(").strip()
    items = parenthetical.split(",")
    for i in items:
        k,v = i.split("=")
        k = k.strip()
        if k in HANDLERS:
            if k == 'trace':
                k = 'trace_id'
            if k == 'class':
                results.update(HANDLERS[k](v))
            else:
                results[k] = HANDLERS[k](v)
        else:
            results[k] = v
    return results

def thread_paren_handler(parenthetical):
    results = {}
    parenthetical = parenthetical.strip().replace(")", '').replace("(", '').strip()
    items = parenthetical.split(",")
    for i in items:
        k,v = i.split("=")
        k = k.strip()
        v.strip()
        if k in HANDLERS:
            if k == 'id':
                k = 'thread_id'
            results[k] = HANDLERS[k](v)
            if k == 'name':
                results['name'] = results['name'].replace('"', '').replace("'", '')
        elif k == 'obj':
            results['heap_loc'] = HANDLERS['heap_loc'](v)
            results['addr'] = HANDLERS['addr'](v)
            results[k] = results['heap_loc']
        else:
            results[k] = v
    return results

def field_handler(line):
    line = line.strip()
    name, heap_loc = line.split()
    return {"type":FIELD_TYPE, "name":name.strip(),
            'heap_loc':HANDLERS['heap_loc'](heap_loc),
            'addr':HANDLERS['addr'](heap_loc) }

def cls_field_handler(line):
    line = line.strip()
    name = " ".join(line.split()[:-1])
    addr = line.split()[-1]
    return {"type":CLS_FIELD_TYPE, "name":name.strip(),
            'heap_loc':HANDLERS['heap_loc'](addr),
            'addr':HANDLERS['addr'](addr) }

def arr_field_handler(line):
    line = line.strip()
    name = " ".join(line.split()[:-1])
    name = name.replace("[", '').replace(']', '')
    addr = line.split()[-1]
    return {"type":ARR_FIELD_TYPE, "name":name.strip(),
            'heap_loc':HANDLERS['heap_loc'](addr),
            'addr':HANDLERS['addr'](addr) }

def field_printer(field):
    return "{name} {heap_loc:08x}".format(**field)

HANDLERS = {}
HANDLERS['name'] = generic_str_handler
HANDLERS['kind'] = generic_str_handler
HANDLERS['id'] = generic_hex_handler
HANDLERS['thread_id'] = generic_int_handler
HANDLERS['trace_id'] = generic_int_handler
HANDLERS['thread'] = generic_int_handler
HANDLERS['frame'] = generic_int_handler
HANDLERS['field'] = field_handler
#HANDLERS['fields'] = fields_handler
HANDLERS['sz'] = generic_int_handler
HANDLERS['addr'] = address_handler
HANDLERS['heap_loc'] = heap_loc_handler
HANDLERS['addr'] = address_handler
HANDLERS['elem type'] = generic_str_handler
HANDLERS['nelems'] = generic_int_handler
HANDLERS['class_addr'] = address_handler
HANDLERS['class_heap_loc'] = heap_loc_handler
HANDLERS['class'] = class_handler
HANDLERS['cls_field'] = cls_field_handler
HANDLERS['arr_field'] = arr_field_handler
HANDLERS[ROOT_STR] = root_handler
HANDLERS[ARR_STR] = arr_handler
HANDLERS[OBJ_STR] = obj_handler
HANDLERS[CLS_STR] = cls_handler



class JavaHeapProfData(object):
    def __init__(self, data_lines):
        print ("%s: Getting threads lines"%time_str())
        threads_lines = get_thread_lines(data_lines)
        print ("%s: Getting sites lines"%time_str())
        sites_lines = get_sites_dump_lines(data_lines)
        print ("%s: Getting heap lines"%time_str())
        heap_lines = get_heap_dump_lines(data_lines)
        print ("%s: Getting trace lines"%time_str())
        trace_lines = get_trace_lines(data_lines)
        print ("%s: Parsing threads lines"%time_str())
        self.threads = parse_threads_lines(threads_lines)
        print ("%s: Parsing traces lines"%time_str())
        self.traces = parse_traces_lines(trace_lines)
        print ("%s: Parsing sites lines"%time_str())
        self.sites = parse_sites_lines(sites_lines)

        print ("%s: Parsing heaps lines"%time_str())
        results = parse_heap_dump_lines(heap_lines)
        self.klasses = results['klasses']
        self.klasses_by_name = results['klasses_by_name']
        self.instances = results['instances']
        self.all_heap_locs = []

        self.objects = results['objects']
        self.objects_by_name = dict([[name, set()] for name in self.klasses_by_name])
        self.roots = results['roots']
        self.root_reachable_flat = dict([(i['heap_loc'], set()) \
                                    for i in self.roots.values()])

        self.heap_locs = results['heap_locs']
        self.arrays = results['arrays']
        self.num_objects = results['num_objects']
        del threads_lines
        del sites_lines
        del heap_lines
        del trace_lines
        self.merge_thread_information()
        self.grouped_threads = {'Thread':[], 'Timer':[], 'main':[]}

        self.reference_to = {}
        self.reference_from = {}
        self.all_graphs = {}
        self.thread_flat_sets = {}

        self.root_reachable_live_objects = set()
        self.root_reachable_dead_objects = set()
        self.unreachable_roots = {}

        self.references_to_clean = {}
        self.references_from_clean = {}
        self.group_threads()
        self.create_objects_by_name()
        self.build_all_references()
        #self.build_all_graphs()
        self.build_roots_reachable_reference_graph()
    
    def build_all_references(self):
        for instance in self.instances.values():
            from_loc = instance['heap_loc']
            for field in instance['fields'].values():
                to_loc = field['heap_loc']
                self.add_reference_clean(from_loc, to_loc)
        

    def build_all_graphs(self):
        all_heap_locs = set()
        print "%s: Enumerating all valid heap locations"%(time_str())
        for instance in self.instances.values():
            from_loc = instance['heap_loc']
            all_heap_locs.add(from_loc)
            for field in instance['fields'].values():
                to_loc = field['heap_loc']
                self.add_reference(from_loc, to_loc)
                all_heap_locs.add(to_loc)
        self.all_heap_locs = list (all_heap_locs)
        self.all_heap_locs.sort()

        print "%s: building reachable sub graph for each heap object"%(time_str())
        for heap_loc in self.all_heap_locs:
            self.all_graphs[heap_loc] = self.build_reachable_flat_set(heap_loc)
        print "%s: done building reachable sub graph for each heap object"%(time_str())

    def build_all_unreachable_graph(self):
        live_locs = self.root_reachable_live_objects
        unreachable_locs = [loc for loc in self.all_heap_locs if not loc in live_locs]
        unreachable_graphs = {}

        print "%s: building unreachable roots graph"%(time_str())
        for loc in unreachable_locs:
            unreachable_graphs[loc] = self.all_graphs[loc]

        # order unreachable keys by size of subgraph
        subgraph_order = []
        for loc in unreachable_graphs.keys():
            subgraph_order.append( (loc, len(unreachable_graphs[loc])) )
        subgraph_order = sorted(subgraph_order, key=lambda s_o: s_o[1])
        subgraph_order.reverse()

        stable = False
        while not stable:
            new_unreachable_graphs = {}
            new_unreachable_graphs.update(unreachable_graphs)
            subgraph_order = [s_o for s_o in subgraph_order if s_o[0] in unreachable_graphs]
            for s_o in subgraph_order:
                loc = s_o[0]
                prune = False
                # heap_loc == node
                # graph is directedt graph based on refs and fields in the ref
                if not loc in unreachable_graphs:
                    continue
                nodes = set(unreachable_graphs.keys())
                c_graph = unreachable_graphs[loc]
                if (nodes & c_graph) > 0:
                    locs_to_prune = nodes & c_graph
                    prune_set = set()
                    for s_loc in locs_to_prune:
                        prune_set |= new_unreachable_graphs[s_loc] if s_loc in new_unreachable_graphs\
                                            else set()

                    for s_loc in unreachable_graphs[loc]:
                        prune_set |= unreachable_graphs[s_loc] if s_loc in unreachable_graphs\
                                               else set()

                    new_unreachable_graphs[loc] |= prune_set
                    pruned = 0
                    for s_loc in prune_set:
                        if s_loc in new_unreachable_graphs:
                            pruned += 1
                            del new_unreachable_graphs[s_loc]
                        if s_loc in unreachable_graphs:
                            pruned += 1
                            del unreachable_graphs[s_loc]

                    fmt = "{}: (Graph Prune) Processed: {:08x}, pruned additional locs: {}, current Unreachable sz: {} new Unreachable sz: {}"
                    print (fmt.format(time_str(), loc, pruned, len(new_unreachable_graphs), len(unreachable_graphs)))
                    continue

                for node, graph in unreachable_graphs.items():
                    if node == loc or not node in new_unreachable_graphs:
                        continue
                    # node and loc are present in the new_unreachable_graphs
                    elif loc in graph and \
                         loc in new_unreachable_graphs:
                        new_unreachable_graphs[node] |= new_unreachable_graphs[loc]
                        prune = True
                        break

                if prune:
                    # found loc in a graph, so now we can prune all other locs
                    # related to the merged one (kind kludgy)
                    prune_set = set()
                    if loc in new_unreachable_graphs:
                        for s_loc in new_unreachable_graphs[loc]:
                            prune_set |= new_unreachable_graphs[s_loc] if s_loc in new_unreachable_graphs\
                                               else set()

                    if loc in unreachable_graphs:
                        for s_loc in unreachable_graphs[loc]:
                            prune_set |= unreachable_graphs[s_loc] if s_loc in unreachable_graphs\
                                               else set()

                    new_unreachable_graphs[loc] |= prune_set
                    pruned = 0
                    for s_loc in prune_set:
                        if s_loc in unreachable_graphs:
                            pruned += 1
                            del unreachable_graphs[s_loc]
                    fmt = "{}: (Iterative Prune) Processed: {:08x}, pruned additional locs: {}, current Unreachable sz: {} new Unreachable sz: {}"
                    print (fmt.format(time_str(), loc, pruned, len(new_unreachable_graphs), len(unreachable_graphs)))

            stable = len(new_unreachable_graphs) == len(unreachable_graphs)
            fmt = "{}: Reached fix point: {} Num New Unreachable Graphs: {} Num Unreachable Graphs: {}"
            print (fmt.format(time_str(), stable, len(new_unreachable_graphs), len(unreachable_graphs)))
            unreachable_graphs = new_unreachable_graphs

        self.unreachable_roots = unreachable_graphs
        print "%s: done building unreachable roots graph"%(time_str())

    def build_all_unreachable_graph2(self, ignore_objs_less_than=10):
        live_locs = self.root_reachable_live_objects
        unreachable_locs = [loc for loc in self.all_heap_locs if not loc in live_locs]
        unreachable_graphs = {}


        print "%s: building unreachable roots graph"%(time_str())
        for loc in unreachable_locs:
            unreachable_graphs[loc] = self.all_graphs[loc]

        # order unreachable keys by size of subgraph
        subgraph_order = []
        for loc in unreachable_graphs.keys():
            subgraph_order.append( (loc, len(unreachable_graphs[loc])) )
        subgraph_order = sorted(subgraph_order, key=lambda s_o: s_o[1])
        subgraph_order.reverse()
        fmt = "{}: Merging {} subgraphs"
        print (fmt.format(time_str(), len(subgraph_order)))

        stable = False
        iteration = 0
        while not stable:
            unreachable_orig_size = len(unreachable_graphs)
            subgraph_order = [s_o for s_o in subgraph_order if s_o[0] in unreachable_graphs]
            pos = 0
            while pos < len(subgraph_order):
            #for s_o in subgraph_order:
                s_o = subgraph_order[pos]
                loc = s_o[0]
                if not loc in unreachable_graphs:
                    pos += 1
                    continue
                elif ignore_objs_less_than > 0 and s_o[1] < ignore_objs_less_than:
                    pos += 1
                    break
                # treat all the keys in unreachable graphs as potential nodes in the current
                # graph
                nodes = set(unreachable_graphs.keys())
                c_graph = unreachable_graphs[loc]
                # Take the intersection, and if there are any commonalities
                # the nodes will be considers subgraphs of this dominant graph
                if len(nodes & c_graph) == 0:
                    pos += 1
                    continue
                elif len(nodes & c_graph) == 1 and\
                    loc in nodes & c_graph:
                    pos += 1
                    continue

                # accumulate all the nodes in the nodes of the subgraphs
                # remove them from the pool of subgraphs
                locs_added = set()
                locs_to_add = nodes & c_graph
                locs_to_add_list = list(locs_to_add)

                while len(locs_to_add_list) > 0:
                    s_loc = locs_to_add_list.pop()
                    locs_to_add.remove(s_loc)
                    locs_added.add(s_loc)
                    if s_loc == loc or\
                       not s_loc in unreachable_graphs:
                        continue

                    i_locs = set()
                    for i_loc in unreachable_graphs[s_loc]:
                        if not i_loc in locs_added and\
                           not i_loc in locs_to_add:
                            locs_to_add_list.append(i_loc)
                            locs_to_add.add(i_loc)

                old_len = len(unreachable_graphs[loc])
                unreachable_graphs[loc] |= locs_added
                pruned = len(unreachable_graphs[loc]) - old_len
                for i_loc in locs_added:
                    if not i_loc == s_loc and i_loc in unreachable_graphs:
                        del unreachable_graphs[i_loc]

                pruned = len(locs_added)
                fmt = "{}: (Graph Prune) subgraph pos: {} Len(sub_graph) = {} Processed: {:08x}, pruned additional locs: {}, current Unreachable sz: {} new Unreachable sz: {}"
                print (fmt.format(time_str(), pos, s_o[1], loc, pruned, unreachable_orig_size, len(unreachable_graphs)))
                stable = len(unreachable_graphs) == unreachable_orig_size

            fmt = "{}: ***** Completed Iteration {} loc = {:8x} current Unreachable sz: {} new Unreachable sz: {}"
            stable = len(unreachable_graphs) == unreachable_orig_size
            iteration += 1
            print (fmt.format(time_str(), iteration, loc, unreachable_orig_size-len(unreachable_graphs), unreachable_orig_size, len(unreachable_graphs)))
        fmt = "{}: Reached fix point: {} Num New Unreachable Graphs: {} Num Unreachable Graphs: {}"
        print (fmt.format(time_str(), stable, len(unreachable_graphs), len(unreachable_graphs)))
        return unreachable_graphs

    def add_reference_clean(self, from_loc, to_loc):
        if not from_loc in self.references_to_clean:
            self.references_to_clean[from_loc] = set()
        self.references_to_clean[from_loc].add(to_loc)
        if not to_loc in self.references_from_clean:
            self.references_from_clean[to_loc] = set()
        self.references_from_clean[to_loc].add(from_loc)

    def add_reference(self, from_loc, to_loc):
        if not from_loc in self.reference_to:
            self.reference_to[from_loc] = set()
        self.reference_to[from_loc].add(to_loc)
        if not to_loc in self.reference_from:
            self.reference_from[to_loc] = set()
        self.reference_from[to_loc].add(from_loc)

    def get_flat_reachable_set(self, heap_loc):
        results = []
        for r,s in self.root_reachable_flat.items():
            if heap_loc in s:
                results.append((r,s))
        return results

    def create_objects_by_name(self):
        for obj in self.objects.values():
            cname = obj['class_name']
            heap_loc = obj['heap_loc']
            if not cname in self.objects_by_name:
                self.objects_by_name[cname] = set()
            self.objects_by_name[cname].add(heap_loc)


    def group_threads(self):
        for i in self.threads.values():
            if i['name'].find('Thread-') == 0:
                self.grouped_threads['Thread'].append(i)
            elif i['name'].find('Timer-') == 0:
                self.grouped_threads['Timer'].append(i)
            else:
                #print i, i['name'].find('Thread-')
                if not i['name'] in self.grouped_threads:
                    self.grouped_threads[i['name']] = []
                self.grouped_threads[i['name']].append(i)

    def merge_thread_information(self):
        for t in self.threads.values():
            heap_loc = t['heap_loc']
            self.threads[t['thread_id']]['in_root_set'] = heap_loc in self.roots
            if heap_loc in self.instances:
                self.instances[heap_loc].update(t)

    def build_threads_flat_set(self):
        print ("%s: building set of reachable heap locations for each thread"%time_str())
        self.thread_flat_sets = {}
        for t in self.threads.values():
            heap_loc = t['heap_loc']
            if heap_loc in self.root_reachable_flat:
                self.thread_flat_sets[heap_loc] = self.root_reachable_flat[heap_loc]
            else:
                self.thread_flat_sets[heap_loc] = self.build_reachable_flat_set2(heap_loc)
        print ("%s: done building set of reachable heap locations for each thread"%time_str())
        
    
    def get_dead_heap_threads(self):
        return [ i for i in self.threads.values()\
                 if i['heap_loc'] in self.instances and not i['live']]

    def get_dead_root_threads(self):
        return [ i for i in self.threads.values()\
                 if i['heap_loc'] in self.roots and not i['live']]

    def get_live_root_threads(self):
        return [ i for i in self.threads.values() if i['live']]

    def build_reachable_flat_set_live(self):
        results = {}
        dead_root_threads = set([i['heap_loc'] for i in self.get_dead_root_threads()])
        live_roots = [i for i in self.roots if not i in dead_root_threads]
        for _heap_loc in live_roots:
            results[_heap_loc] = self.build_reachable_flat_set2(_heap_loc)
        return results

    def build_reachable_flat_set2(self, start_heap_loc):
        add_locs = set([start_heap_loc])
        add_locs_list = [start_heap_loc]
        result_set = set([start_heap_loc])
        added_locs = set()
        while len(add_locs) > 0:
            heap_loc = add_locs_list.pop()
            add_locs.remove(heap_loc)
            added_locs.add(heap_loc)

            ref_to = self.references_to_clean[heap_loc] if heap_loc in self.references_to_clean else set()
            for loc in ref_to:
                if not loc in added_locs and\
                   not loc in add_locs:
                   add_locs.add(loc)
                   add_locs_list.append(loc)
                   result_set.add(loc)
        return result_set

    def build_reachable_flat_set(self, start_heap_loc):
        add_locs = set([start_heap_loc])
        add_locs_list = [start_heap_loc]
        result_set = set([start_heap_loc])
        added_locs = set()
        while len(add_locs) > 0:
            heap_loc = add_locs_list.pop()
            add_locs.remove(heap_loc)
            added_locs.add(heap_loc)
            if heap_loc in self.all_graphs:
                result_set |= self.all_graphs[heap_loc]
                added_locs |= self.all_graphs[heap_loc]

            ref_to = self.reference_to[heap_loc] if heap_loc in self.reference_to else set()
            for loc in ref_to:
                if not loc in added_locs and\
                   not loc in add_locs:
                   add_locs.add(loc)
                   add_locs_list.append(loc)
                   result_set.add(loc)
        return result_set

    def build_roots_reachable_reference_graph(self):
        print ("%s: building set of reachable heap locations for each root"%time_str())
        for r in self.root_reachable_flat:
            self.root_reachable_flat[r] = self.build_reachable_flat_set2(r)

        dead_root_threads = set([i['heap_loc'] for i in self.get_dead_root_threads()])
        live_roots = [i for i in self.roots if not i in dead_root_threads]
        for r in live_roots:
            self.root_reachable_live_objects |= self.root_reachable_flat[r]

        for r in dead_root_threads:
            self.root_reachable_dead_objects |= self.root_reachable_flat[r]
        
        print ("%s: done building set of reachable heap locations for each root"%time_str())
        self.build_threads_flat_set()

    def find_objs_by_name(self, class_name):
        klass = self.find_class(class_name)
        if klass is None:
            return self._find_objs_by_class_name(class_name)
        return self.find_objs_by_heap_loc(klass['class_heap_loc'])

    def find_instances_by_name(self, class_name):
        klass = self.find_class(class_name)
        if klass is None:
            return self._find_instances_by_class_name(class_name)
        return self.find_instances_by_heap_loc(klass['class_heap_loc'])

    def find_arrays_by_type(self, class_name):
        klass = self.find_class(class_name)
        if klass is None:
            return self._find_arrays_by_class_name(class_name)
        return self.find_arrays_by_heap_loc(klass['class_heap_loc'])

    def find_objs_by_heap_loc(self, heap_loc):
        return [i for i in self.objects.values() if i['class_heap_loc'] == heap_loc]

    def find_instances_by_heap_loc(self, heap_loc):
        return [i for i in self.objects.values() if i['class_heap_loc'] == heap_loc]

    def find_arrays_by_heap_loc(self, heap_loc):
        return [i for i in self.arrays.values() if i['class_heap_loc'] == heap_loc]

    def _find_objs_by_class_name(self, class_name):
        return [i for i in self.objects.values() if i['class_name'] == class_name]

    def _find_instances_by_class_name(self, class_name):
        return [i for i in self.objects.values() if i['class_name'] == class_name]

    def _find_arrays_by_class_name(self, class_name):
        return [i for i in self.arrays.values() if i['class_name'] == class_name]

    def find_class(self, class_name):
        for i,j in self.klasses_by_name.items():
            if i==class_name:
                return j
        return None

    def merge_back(self, heap_loc):
        results = set()
        locs_to_add = set([heap_loc])
        locs_to_add_list = list(locs_to_add)
        locs_added = set()
        while len(locs_to_add_list) > 0:
            a_loc = locs_to_add_list.pop()
            locs_to_add.remove(a_loc)
            locs_added.add(a_loc)
            if a_loc in self.reference_to:
                for i_loc in self.reference_to[a_loc]:
                    if not i_loc in locs_added and\
                       not i_loc in locs_to_add:
                       locs_to_add.add(i_loc)
                       locs_to_add_list.append(i_loc)
            if a_loc in self.reference_from:
                for i_loc in self.reference_from[a_loc]:
                    if not i_loc in locs_added and\
                       not i_loc in locs_to_add:
                       locs_to_add.add(i_loc)
                       locs_to_add_list.append(i_loc)
        return locs_added

    @staticmethod
    def from_zipfile(archive='java.hprof.txt.zip', hprof_file='java.hprof.txt'):
        print "%s: Loading Java Heap Profile from %s:%s"%(time_str(), archive, hprof_file)
        zipped_data = zipfile.ZipFile(archive)
        hprof_files = [i for i in zipped_data.namelist() if i.find(hprof_file) > -1]
        if len(hprof_files) > 0:
            hprof_file = hprof_files[0]
        elif len(zipped_data.namelist()) > 0:
            hprof_file = zipped_data.namelist()[0]
        else:
            raise Exception("Failed to specify the proper file in the java.hprof archive")

        data = zipped_data.open(hprof_file).read()
        return JavaHeapProfData([i.strip() for i in data.splitlines()])

    @staticmethod
    def from_file(hprof_file='java.hprof.txt'):
        print "%s: Loading Java Heap Profile from %s"%(time_str(), fname)
        data = open(hprof_file).read()
        return JavaHeapProfData([i.strip() for i in data.splitlines()])

SSL_SESSION_IMPL = 'sun.security.ssl.SSLSessionImpl'
SSL_SOCKET_IMPL = 'sun.security.ssl.SSLSocketImpl'
TLS_KEY_PARAM_GEN = 'com.sun.crypto.provider.TlsKeyMaterialGenerator'
TLS_KEY_PARAM_SPEC = 'sun.security.internal.spec.TlsKeyMaterialParameterSpec'
TLS_MS_PARAM_SPEC = 'sun.security.internal.spec.TlsMasterSecretParameterSpec'
TLS_MS_GEN = 'com.sun.crypto.provider.TlsMasterSecretGenerator'
TLS_MS_GEN_SECRET_KEY = 'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey'
TLS_PRF_GEN = 'com.sun.crypto.provider.TlsPrfGenerator'
TLS_PRF_GEN_V12 = 'com.sun.crypto.provider.TlsPrfGenerator$V12'
EXP_SESSION_INSTANCES = 'edu.rice.cs.seclab.dso.ExperimentSession'
APACHE_HTTP_CLIENT = 'org.apache.http.impl.client.CloseableHttpClient'
APACHE_COOKIE_STORE = 'org.apache.http.impl.client.BasicCookieStore'
APACHE_BASIC_NV_PAIR = 'org.apache.http.message.BasicNameValuePair'
APACHE_HTTP_GET = 'org.apache.http.client.methods.HttpGet'
APACHE_HTTP_REQ_WRAPPER = 'org.apache.http.client.methods.HttpRequestWrapper'
APACHE_HTTP_RESPONSE = 'org.apache.http.message.BasicHttpResponse'
APACHE_HTTP_REQ_WRITER = 'org.apache.http.impl.io.DefaultHttpRequestWriter'
APACHE_HTTP_SESSIONOUTPUT_BUFFER = 'org.apache.http.impl.io.SessionOutputBufferImpl'

JAVA_NET_SSL_SSLCONTEXT = 'javax.net.ssl.SSLContext'
JAVA_NET_SSL_SSLCONTEXT_SPI = 'javax.net.ssl.SSLContextSpi'
JAVA_NET_SSL_SSLSESSION_CONTEXT = 'javax.net.ssl.SSLSessionContext'
APACHE_HTTP_CLIENT_CONTEXT = 'org.apache.http.client.protocol.HttpClientContext'
APACHE_HTTP_PROTO_BASIC_HTTP_CONTEXT = 'org.apache.http.protocol.BasicHttpContext'
APACHE_HTTP_PROTO_HTTP_CONTEXT = 'org.apache.http.protocol.HttpContext'
APACHE_HTTP_PROTO_HTTP_CORE_CONTEXT = 'org.apache.http.protocol.HttpCoreContext'
APACHE_HTTP_SSL_CONTEXT_BUILDER = 'org.apache.http.ssl.SSLContextBuilder'
APACHE_HTTP_SSL_CONTEXT_BUILDER_TRUST_DELEGATE = 'org.apache.http.ssl.SSLContextBuilder$TrustManagerDelegate'
APACHE_HTTP_SSL_CONTEXT_BUILDER_CONTEXTS = 'org.apache.http.ssl.SSLContexts'
SUN_SECURITY_SSL_SSLCONTEXT = 'sun.security.ssl.SSLContextImpl'
SUN_SECURITY_SSL_SSLCONTEXT_ABS_SSLCON = 'sun.security.ssl.SSLContextImpl$AbstractSSLContext'
SUN_SECURITY_SSL_SSLCONTEXT_CUSTOMISED = 'sun.security.ssl.SSLContextImpl$CustomizedSSLContext'
SUN_SECURITY_SSL_SSLCONTEXT_TLS_CONTEXT = 'sun.security.ssl.SSLContextImpl$TLSContext'
SUN_SECURITY_SSL_SSL_SESSION_CONTEXT = 'sun.security.ssl.SSLSessionContextImpl'
SUN_SECURITY_SSL_SSL_SESSION_CONTEXT_1 = 'sun.security.ssl.SSLSessionContextImpl$1'
APACHE_HTTP_CLIENT_PROTOCOL_HTTPCLIENTCONTEXT = 'org.apache.http.client.protocol.HttpClientContext'
APACHE_HTTP_IMPL_CLIENT_HTTPCLIENTBUILDER = 'org.apache.http.impl.client.HttpClientBuilder'
APACHE_HTTP_IMPL_CLIENT_HTTPCLIENTBUILDER_2 = 'org.apache.http.impl.client.HttpClientBuilder$2'
APACHE_HTTP_IMPL_CLIENT_INTERNALHTTPCLIENT = 'org.apache.http.impl.client.InternalHttpClient'
APACHE_HTTP_IMPL_CONN_DEFAULTHTTPCLIENTCONNECTIONOPERATOR = 'org.apache.http.impl.conn.DefaultHttpClientConnectionOperator'
APACHE_HTTP_IMPL_CONN_LOGGINGMANAGEDHTTPCLIENTCONNECTION = 'org.apache.http.impl.conn.LoggingManagedHttpClientConnection'
APACHE_HTTP_IMPL_CONN_MANAGEDHTTPCLIENTCONNECTIONFACTORY = 'org.apache.http.impl.conn.ManagedHttpClientConnectionFactory'
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER = 'org.apache.http.impl.conn.PoolingHttpClientConnectionManager'
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_1 = 'org.apache.http.impl.conn.PoolingHttpClientConnectionManager$1'
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_CONFIGDATA = 'org.apache.http.impl.conn.PoolingHttpClientConnectionManager$ConfigData'
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_INTERNALCONNECTIONFACTORY = 'org.apache.http.impl.conn.PoolingHttpClientConnectionManager$InternalConnectionFactory'
JAVA_UTIL_TIMER_THREAD = 'java.util.TimerThread'
JAVA_LANG_THREAD = 'java.lang.Thread'

INTERESTED_INSTANCES = [SSL_SESSION_IMPL,
SSL_SOCKET_IMPL,
TLS_KEY_PARAM_GEN,
TLS_KEY_PARAM_SPEC,
TLS_MS_PARAM_SPEC,
TLS_MS_GEN,
TLS_MS_GEN_SECRET_KEY,
TLS_PRF_GEN,
TLS_PRF_GEN_V12,
EXP_SESSION_INSTANCES,
APACHE_HTTP_CLIENT,
APACHE_COOKIE_STORE,
APACHE_BASIC_NV_PAIR,
APACHE_HTTP_GET,
APACHE_HTTP_REQ_WRAPPER,
APACHE_HTTP_RESPONSE,
APACHE_HTTP_REQ_WRITER,
APACHE_HTTP_SESSIONOUTPUT_BUFFER,
JAVA_NET_SSL_SSLCONTEXT,
JAVA_NET_SSL_SSLCONTEXT_SPI,
JAVA_NET_SSL_SSLSESSION_CONTEXT,
APACHE_HTTP_CLIENT_CONTEXT,
APACHE_HTTP_PROTO_BASIC_HTTP_CONTEXT,
APACHE_HTTP_PROTO_HTTP_CONTEXT,
APACHE_HTTP_PROTO_HTTP_CORE_CONTEXT,
APACHE_HTTP_SSL_CONTEXT_BUILDER,
APACHE_HTTP_SSL_CONTEXT_BUILDER_TRUST_DELEGATE,
APACHE_HTTP_SSL_CONTEXT_BUILDER_CONTEXTS,
SUN_SECURITY_SSL_SSLCONTEXT,
SUN_SECURITY_SSL_SSLCONTEXT_ABS_SSLCON,
SUN_SECURITY_SSL_SSLCONTEXT_CUSTOMISED,
SUN_SECURITY_SSL_SSLCONTEXT_TLS_CONTEXT,
SUN_SECURITY_SSL_SSL_SESSION_CONTEXT,
SUN_SECURITY_SSL_SSL_SESSION_CONTEXT_1,
APACHE_HTTP_CLIENT_PROTOCOL_HTTPCLIENTCONTEXT,
APACHE_HTTP_IMPL_CLIENT_HTTPCLIENTBUILDER,
APACHE_HTTP_IMPL_CLIENT_HTTPCLIENTBUILDER_2,
APACHE_HTTP_IMPL_CLIENT_INTERNALHTTPCLIENT,
APACHE_HTTP_IMPL_CONN_DEFAULTHTTPCLIENTCONNECTIONOPERATOR,
APACHE_HTTP_IMPL_CONN_LOGGINGMANAGEDHTTPCLIENTCONNECTION,
APACHE_HTTP_IMPL_CONN_MANAGEDHTTPCLIENTCONNECTIONFACTORY,
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER,
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_1,
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_CONFIGDATA,
APACHE_HTTP_IMPL_CONN_POOLINGHTTPCLIENTCONNECTIONMANAGER_INTERNALCONNECTIONFACTORY,
JAVA_UTIL_TIMER_THREAD,
JAVA_LANG_THREAD,
]

def capture_hprof_stats(zipped_fname):
    results = {}
    intermediate = {}
    jhp = JavaHeapProfData.from_zipfile(zipped_fname)
    dead_threads_root = jhp.get_dead_root_threads()
    dead_threads_heap = jhp.get_dead_heap_threads()
    num_dead_objects_from_threads = sum([len(jhp.thread_flat_sets[i['heap_loc']]) for i in dead_threads_heap])
    #dead_threads_heap_flat_reachable = {}
    #for dth in dead_threads_heap:
    #    heap_loc = dth['heap_loc']
    #    dead_threads_heap_flat_reachable[heap_loc] = jhp.build_reachable_flat_set2(heap_loc)
    results['num_objects'] = jhp.num_objects
    results['num_instances_found'] = len(jhp.instances)
    results['num_arrays_found'] = len(jhp.arrays)
    results['num_objects_found'] = len(jhp.objects)
    results['num_roots_found'] = len(jhp.roots)
    results['num_threads_found'] = len(jhp.threads)
    results['num_classes_found'] = len(jhp.klasses)
    results['num_dead_threads'] = len(dead_threads_heap)
    results['num_dead_threads_root'] = len(dead_threads_root)
    results['num_dead_objects_from_threads'] = num_dead_objects_from_threads

    for k in INTERESTED_INSTANCES:
        intermediate[k] = jhp.find_instances_by_name(k)


    #klass = [i for i in jhp.klasses_by_name if i.find('HttpClient') > 0]
    #for k in klass:
    #    intermediate[k] = jhp.find_instances_by_name(k)


    im_instances = {}
    for klass in intermediate:
        live_key = klass+'_live'
        dead_key = klass+'_dead'
        im_instances[live_key] = []
        im_instances[dead_key] = []
        instances = intermediate[klass]
        for instance in instances:
            heap_loc = instance['heap_loc']
            if heap_loc in jhp.root_reachable_live_objects:
                im_instances[live_key].append(instance)
            else:
                im_instances[dead_key].append(instance)

    for k in im_instances:
        results[k] = len(im_instances[k])

    #for d_heap_loc, dthfr in dead_threads_heap_flat_reachable.items():
    #    print d_heap_loc, len(dthfr)

    keys = results.keys()
    keys.sort()
    out_str = "-".join(["%s:%s"%(key, results[key]) for key in keys])
    return out_str, results

def get_extracted_hprof_data(base_location):
    hprof_results = sifipr.get_java_hprof_results(base_location)
    try:
        data = open(hprof_results).read()
    except:
        print ("Failed to read the results from: %s"%base_location)
        return {}
    data = data.strip()
    items = [i.split(':') for i in data.split('-')]
    results = {}
    for k,v in items:
        if (k.find('_live') == len(k)-5 or\
            k.find('_dead') == len(k)-5) and\
            k.find('.') > 0:
            cname, stat = k.split('_')
            v = int(v)
            if not cname in results:
                results[cname] = {'live':0, 'dead':0}
            results[cname][stat] = v
        elif v.isdigit():
            results[k] = int(v)
        else:
            results[k] = v
    return results


