from datetime import datetime, timedelta
from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, traceback
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
#from agg_exp_data import *
import single_factors_iteration_perform_runs as sifipr
import perform_post_process_dirs_strs as pppds
import unzip_agg_data as uad
import agg_exp_data as aed
import parse_java_hprof as pjh
import basic_multi_host_commands as bhmc
from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

USE_64 = True
WORKING_BASE = "/srv/result_data/working_dir"
SLEEP_SECS = 3.0
WORKING_QUEUE = []
AGG_WORKING_QUEUE = []
ALLOWED_WORKING_AGG_TASK = 3
WORKING_QUEUE_LOCK = threading.Lock()
SLEEP_TIME_BETWEEN_STARTS = 75
SLEEP_BETWEEN_STARTS = True
INCOMPLETE_DIRS = set()
COMPLETE_DIRS = set()
INCOMPLETE_DIRS_LOCK = threading.Lock()

FULL_JBGREP_COMPLETE = "full_jbgrep_memory_process_complete.txt"
FFASTRINGS_COMPLETE = "ffastrings_memory_process_complete.txt"
UNZIP_MEMORY_PROCESS = "unzip_memory_process_complete.txt"
VOL_UNZIP_MEMORY_PROCESS = "vol_unzip_memory_process_complete.txt"
UNZIP_JAVA_HPROF = "java.hprof.txt.zip"
MEMORY_DUMP = 'memory-work.dump'
MEMORY_DUMP_ZIP = 'memory-work.dump.zip'
RUNNING_WITH_QEMU = False

import random, string
ALLOWED_PROC_EXTRACTION_THREADS = 5 # int(sys.argv[1])
EXTRACT_MEMORY_DATA = False
EXTRACT_HPROF_DATA = False
ACTIVE_AGG_PROCS = []
ACTIVE_PROCS = []
ACTIVE_PROCS_LOCK = threading.Lock()

BASE_LOCATION_NO_LONGER_VALID = set()
STOP_ANALYSIS_COUNT_DOWN = False
TERM_ANALYSIS = False
def stop_analysis(wait_minutes, sleep_interval_minutes):
    global EXTRACT_MEMORY_DATA, STOP_ANALYSIS_COUNT_DOWN
    start = datetime.now()
    STOP_ANALYSIS_COUNT_DOWN = False
    period = timedelta(minutes=wait_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    TERM_ANALYSIS = True
    while not stop and not STOP_ANALYSIS_COUNT_DOWN:
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

    if TERM_ANALYSIS:
        EXTRACT_MEMORY_DATA = False

def sharing_with_vms(sharing=True):
    pppds.EXECING_VMS = sharing
    RUNNING_WITH_QEMU = sharing

STOP_SCANNING = False
def periodic_scan(base_location, scan_minutes, sleep_interval_minutes):
    global STOP_SCANNING
    STOP_SCANNING=False
    start = datetime.now()
    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    while not stop and not STOP_SCANNING:
        r = scan_location_for_files(base_location)
        add_files_work_queue(r)
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

# results_base_location = '/srv/nfs/cortana/logs/results_batch_x64_exp_gen_2560/transaction/'
# scan_thread = start_periodic_scan(results_base_location, 240, 5)
# term_analysis = terminate_analysis_thread(300, 5)


KEEP_SCANNING_THE_LOCATIONS = True
THE_THREAD = None
SECONDS_TO_WAIT_FOR_SCAN = 420

SCANNER_THREADS = {}
def perform_scan_til_present(the_location):
    global SCANNER_THREADS, KEEP_SCANNING_THE_LOCATIONS, SECONDS_TO_WAIT_FOR_SCAN
    print ("Sleeping for %d minutes"%(SECONDS_TO_WAIT_FOR_SCAN/60))
    time.sleep(SECONDS_TO_WAIT_FOR_SCAN)
    try:
        os.stat(the_location)
        r = scan_location_for_files(the_location)
        add_files_work_queue(r)
        print ("Success, the files for %s were added"%the_location)
        SCANNER_THREADS[the_location] = None
    except:
        if KEEP_SCANNING_THE_LOCATIONS:
            print ("Scheduling the next check")
            the_thread = threading.Thread(target=perform_scan_til_present, args=(the_location,))
            the_thread.start()
            SCANNER_THREADS[the_location] = the_thread
            return the_thread
        else:
            SCANNER_THREADS[the_location] = None
            print ("Not Scheduling the next check")
    return None

def start_modified_proc_scan(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    start_time = datetime.now() if start_time is None else start_time
    the_thread = threading.Thread(target=perform_proc_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time, ignore_memdump_f))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def start_modified_scan(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    start_time = datetime.now() if start_time is None else start_time
    the_thread = threading.Thread(target=perform_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time, ignore_memdump_f))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def start_write_heap_locs_scan(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    start_time = datetime.now() if start_time is None else start_time
    the_thread = threading.Thread(target=perform_write_heap_locs_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time, ignore_memdump_f))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def perform_write_heap_locs_scan_modified(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    global SCANNER_THREADS, KEEP_SCANNING_THE_LOCATIONS, SECONDS_TO_WAIT_FOR_SCAN
    start_time = datetime.now() if start_time is None else start_time
    try:
        os.stat(the_location)
        r = scan_location_for_ready_for_heap_locs(the_location, ignore_memdump_f)
        add_files_work_queue(r)
    except:
        pass

    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time

    print ("Sleeping for %d minutes"%(sleep_interval_minutes))
    time.sleep(sleep_interval_minutes*60)

    if SCANNER_THREADS[the_location] is None or\
       stop or\
       not KEEP_SCANNING_THE_LOCATIONS:
        if stop:
           print ("Scanning period has ended for %s"%(the_location))
        elif KEEP_SCANNING_THE_LOCATIONS:
           print ("No longer wish to KEEP_SCANNING_THE_LOCATIONS")
        else:
            print ("Current thread is None")
        return None

    print ("Scheduling the next check")
    the_thread = threading.Thread(target=perform_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def perform_proc_scan_modified(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    global SCANNER_THREADS, KEEP_SCANNING_THE_LOCATIONS, SECONDS_TO_WAIT_FOR_SCAN
    start_time = datetime.now() if start_time is None else start_time
    try:
        os.stat(the_location)
        r = scan_location_for_proc_files(the_location, ignore_memdump_f)
        for bloc in r:
            sifipr.generate_jbgrep_merged_info(bloc)
        add_files_work_queue(r)
    except:
        pass

    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time

    print ("Sleeping for %d minutes"%(sleep_interval_minutes))
    time.sleep(sleep_interval_minutes*60)

    if SCANNER_THREADS[the_location] is None or\
       stop or\
       not KEEP_SCANNING_THE_LOCATIONS:
        if stop:
           print ("Scanning period has ended for %s"%(the_location))
        elif KEEP_SCANNING_THE_LOCATIONS:
           print ("No longer wish to KEEP_SCANNING_THE_LOCATIONS")
        else:
            print ("Current thread is None")
        return None

    print ("Scheduling the next check")
    the_thread = threading.Thread(target=perform_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def perform_scan_modified(the_location, scan_minutes, sleep_interval_minutes, start_time=None, ignore_memdump_f=False):
    global SCANNER_THREADS, KEEP_SCANNING_THE_LOCATIONS, SECONDS_TO_WAIT_FOR_SCAN
    start_time = datetime.now() if start_time is None else start_time
    try:
        os.stat(the_location)
        r = scan_location_for_files(the_location, ignore_memdump_f)
        for bloc in r:
            sifipr.generate_jbgrep_merged_info(bloc)
        add_files_work_queue(r)
    except:
        pass

    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time

    print ("Sleeping for %d minutes"%(sleep_interval_minutes))
    time.sleep(sleep_interval_minutes*60)

    if SCANNER_THREADS[the_location] is None or\
       stop or\
       not KEEP_SCANNING_THE_LOCATIONS:
        if stop:
           print ("Scanning period has ended for %s"%(the_location))
        elif KEEP_SCANNING_THE_LOCATIONS:
           print ("No longer wish to KEEP_SCANNING_THE_LOCATIONS")
        else:
            print ("Current thread is None")
        return None

    print ("Scheduling the next check")
    the_thread = threading.Thread(target=perform_scan_modified, args=(the_location,scan_minutes, sleep_interval_minutes, start_time))
    the_thread.start()
    SCANNER_THREADS[the_location] = the_thread
    return the_thread

def periodic_scan_rm_dmp(base_location, scan_minutes, sleep_interval_minutes):
    global STOP_SCANNING
    STOP_SCANNING=False
    start = datetime.now()
    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    while not stop and not STOP_SCANNING:
        scan_location_rm_memdump(base_location)
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

def periodic_scan_hprof_analysis(base_location, scan_minutes, sleep_interval_minutes):
    STOP_SCANNING=False
    start = datetime.now()
    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    while not stop and not STOP_SCANNING:
        r = scan_location_for_hprof(base_location)
        r = [j for j in r if not j in WORKING_QUEUE and not j in INCOMPLETE_DIRS ]
        add_files_work_queue(r)
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

def start_periodic_scan_hprof_analysis(base_location, scan_minutes, sleep_interval_minutes):
    t = threading.Thread(target=periodic_scan_hprof_analysis, args=(base_location, scan_minutes, sleep_interval_minutes))
    t.start()
    return t


def scan_location_rm_memdump(location):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        if dir_has_memory_dump(d):
          mdmp = os.path.join(d, MEMORY_DUMP)
          print "Removing memory dump file: %s"%mdmp
          os.remove(mdmp)

def start_periodic_rm_scan(base_location, scan_minutes, sleep_interval_minutes):
    t = threading.Thread(target=periodic_scan_rm_dmp, args=(base_location, scan_minutes, sleep_interval_minutes))
    t.start()
    return t

def start_periodic_scan(base_location, scan_minutes, sleep_interval_minutes):
    t = threading.Thread(target=periodic_scan, args=(base_location, scan_minutes, sleep_interval_minutes))
    t.start()
    return t

def terminate_analysis_thread(wait_minutes, sleep_interval_minutes):
    t = threading.Thread(target=stop_analysis, args=(wait_minutes, sleep_interval_minutes))
    t.start()
    return t

def check_full_jbgrep_completed_memory_extraction_completed(base_location):
    f = os.path.join(base_location, FULL_JBGREP_COMPLETE)
    try:
        os.stat(f)
        return True
    except:
        return False

def check_full_ffastrings_completed_memory_extraction_completed(base_location):
    f = os.path.join(base_location, FFASTRINGS_COMPLETE)
    try:
        os.stat(f)
        return True
    except:
        return False

def check_completed_all_memory_extraction_completed(base_location):
    return check_completed_memory_extraction_completed(base_location) and\
           check_completed_proc_memory_extraction_completed(base_location)

def check_completed_memory_extraction_completed(base_location):
    f = os.path.join(base_location, UNZIP_MEMORY_PROCESS)
    try:
        os.stat(f)
        return True
    except:
        return False

def check_completed_proc_memory_extraction_completed(base_location):
    f = os.path.join(base_location, VOL_UNZIP_MEMORY_PROCESS)
    try:
        os.stat(f)
        return True
    except:
        return False

def check_completed_has_java_hprof(base_location):
    f = os.path.join(base_location, UNZIP_JAVA_HPROF)
    try:
        os.stat(f)
        return True
    except:
        return False

def mark_location_complete(base_location):
    INCOMPLETE_DIRS_LOCK.acquire()
    COMPLETE_DIRS.add(base_location)
    if base_location in INCOMPLETE_DIRS:
        INCOMPLETE_DIRS.remove(base_location)
    INCOMPLETE_DIRS_LOCK.release()

def prune_active_ffastring_procs():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            # was not able to successfully complete the directory
            if check_full_ffastrings_completed_memory_extraction_completed(base_location):
                print("%s: Marking %s as completed"%(time_str(), base_location))
                mark_location_complete(base_location)
                if check_full_jbgrep_completed_memory_extraction_completed(base_location) or \
                   check_completed_memory_extraction_completed(base_location):
                    AGG_WORKING_QUEUE.append((base_location, working_dir))
                continue
            else:
                try:
                    os.stat(base_location)
                    reapply_work(loc)
                except:
                    BASE_LOCATION_NO_LONGER_VALID.add(base_location)
                    print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
                try:
                    os.stat(working_dir)
                    print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                    shutil.rmtree(working_dir)
                except:
                    pass
    ACTIVE_PROCS = new_active_procs

    new_active_agg_procs = []
    active_agg_set = set()
    for p, base_location, working_dir in ACTIVE_AGG_PROCS:
        if p.is_alive():
            active_agg_set.add(base_location)
            new_active_agg_procs.append((p, base_location, working_dir))
        else:
            try:
                os.stat(working_dir)
                print("%s: Cleaning up agg working dir: %s %s"%(time_str(), working_dir, base_location))
                shutil.rmtree(working_dir)
            except:
                pass

    while len(new_active_procs) < ALLOWED_WORKING_AGG_TASK:
        if len(AGG_WORKING_QUEUE) == 0:
            break
        base_location, working_dir = AGG_WORKING_QUEUE.pop(0)
        if base_location in active_agg_set:
            continue
        agg_proc = Process(target=perform_work_data_agg,args=(base_location, working_dir, True, False, False))
        agg_proc.start()
        new_active_agg_procs.append((agg_proc, base_location, working_dir))

    ACTIVE_AGG_PROCS = new_active_agg_procs
    ACTIVE_PROCS_LOCK.release()

def prune_active_procs():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            # was not able to successfully complete the directory
            if check_completed_memory_extraction_completed(base_location):
                print("%s: Marking %s as completed"%(time_str(), base_location))
                mark_location_complete(base_location)
                AGG_WORKING_QUEUE.append((base_location, working_dir))
                continue
            else:
                try:
                    os.stat(base_location)
                    reapply_work(loc)
                except:
                    BASE_LOCATION_NO_LONGER_VALID.add(base_location)
                    print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
                try:
                    os.stat(working_dir)
                    print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                    shutil.rmtree(working_dir)
                except:
                    pass
    ACTIVE_PROCS = new_active_procs

    new_active_agg_procs = []
    active_agg_set = set()
    for p, base_location, working_dir in ACTIVE_AGG_PROCS:
        if p.is_alive():
            active_agg_set.add(base_location)
            new_active_agg_procs.append((p, base_location, working_dir))
        else:
            try:
                os.stat(working_dir)
                print("%s: Cleaning up agg working dir: %s %s"%(time_str(), working_dir, base_location))
                shutil.rmtree(working_dir)
            except:
                pass

    while len(new_active_agg_procs) < ALLOWED_WORKING_AGG_TASK:
        if len(AGG_WORKING_QUEUE) == 0:
            break
        base_location, working_dir = AGG_WORKING_QUEUE.pop(0)
        if base_location in active_agg_set:
            continue
        agg_proc = Process(target=perform_work_data_agg,args=(base_location, working_dir, True, False, False))
        agg_proc.start()
        new_active_agg_procs.append((agg_proc, base_location, working_dir))

    ACTIVE_AGG_PROCS = new_active_agg_procs
    ACTIVE_PROCS_LOCK.release()

def prune_hprof_active_procs():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            mark_location_complete(base_location)
            try:
                os.stat(working_dir)
                print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                shutil.rmtree(working_dir)
            except:
                pass
    ACTIVE_PROCS = new_active_procs
    ACTIVE_PROCS_LOCK.release()

def add_active_procs(p, base_location, working_dir):
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK
    ACTIVE_PROCS_LOCK.acquire()
    ACTIVE_PROCS.append((p, base_location, working_dir))
    ACTIVE_PROCS_LOCK.release()

def can_start_new_proc():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ALLOWED_PROC_EXTRACTION_THREADS
    return len(ACTIVE_PROCS) < ALLOWED_PROC_EXTRACTION_THREADS

def has_active_procs():
    global ACTIVE_PROCS
    return len(ACTIVE_PROCS) > 0

def start_add_extraction_process(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    working_dir = working_dir if not working_dir is None else get_working_dir()
    kargs ={}
    kargs['perform_heap_locs_dump']=perform_heap_locs_dump
    kargs['working_dir']=working_dir
    kargs['use_64']=use_64
    kargs['use_python']=use_python
    kargs['perform_agg']=perform_agg
    kargs['perform_strings']=perform_strings
    p = Process(target=perform_extraction_work, args=(base_location,), kwargs=kargs)
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p

def start_add_jbgrep_extraction_process(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    working_dir = working_dir if not working_dir is None else get_working_dir()
    kargs ={}
    kargs['perform_heap_locs_dump']=perform_heap_locs_dump
    kargs['working_dir']=working_dir
    kargs['use_64']=use_64
    kargs['use_python']=use_python
    kargs['perform_agg']=perform_agg
    kargs['perform_strings']=perform_strings
    p = Process(target=perform_extraction_jbgrep_work, args=(base_location,), kwargs=kargs)
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p

def start_add_ffastrings_extraction_process(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    working_dir = working_dir if not working_dir is None else get_working_dir()
    kargs ={}
    kargs['perform_heap_locs_dump']=perform_heap_locs_dump
    kargs['working_dir']=working_dir
    kargs['use_64']=use_64
    kargs['use_python']=use_python
    kargs['perform_agg']=perform_agg
    kargs['perform_strings']=perform_strings
    p = Process(target=perform_extraction_ffastrings_work, args=(base_location,), kwargs=kargs)
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p


def start_add_proc_extraction_process(base_location,working_dir=None):
    working_dir = working_dir if not working_dir is None else get_working_dir()
    kargs ={}
    kargs['working_dir']=working_dir
    kargs['use_64']=USE_64
    kargs['use_python']=False

    p = Process(target=perform_proc_extraction_work, args=(base_location, ), kwargs=kargs)
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p

def start_add_agg_process(base_location,working_dir=None):
    kargs ={}
    kargs['working_dir']=None
    kargs['use_64']=USE_64
    kargs['use_python']=False
    p = Process(target=perform_work_data_agg,args=(base_location, ), kwargs=kargs)
    #p = Process(target=perform_proc_extraction_work, args=(base_location, ), kwargs=kargs)
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p

def start_add_heaploc_extraction_process(base_location,working_dir=None):
    working_dir = working_dir if not working_dir is None else get_working_dir()
    kargs ={}
    kargs['working_dir']=working_dir
    kargs['use_64']=USE_64
    kargs['use_python']=False

    p = Process(target=uad.perform_work_write_java_heap_locs_only, args=(base_location, working_dir, kargs['use_64'], kargs['use_python']))
    p.start()
    add_active_procs(p, base_location, working_dir)
    return p

def perform_heap_locs_write():
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_procs_only()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_procs_only()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = start_add_heaploc_extraction_process(base_location, working_dir)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_procs_only()
        time.sleep(SLEEP_SECS)

def perform_jbgrep_extraction(perform_strings=True, perform_heap_locs_dump=False):
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_jbgrep_only()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_jbgrep_only()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None and not check_full_jbgrep_completed_memory_extraction_completed(base_location):
            working_dir = get_working_dir()
            p = start_add_jbgrep_extraction_process(base_location, working_dir, use_64, use_python, perform_agg, perform_strings, perform_heap_locs_dump)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_jbgrep_only()
        time.sleep(SLEEP_SECS)

def perform_ffastrings_extraction(perform_strings=True, perform_heap_locs_dump=False):
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_jbgrep_only()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_jbgrep_only()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None and not check_full_ffastrings_completed_memory_extraction_completed(base_location):
            working_dir = get_working_dir()
            p = start_add_jbgrep_extraction_process(base_location, working_dir, use_64, use_python, perform_agg, perform_strings, perform_heap_locs_dump)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_jbgrep_only()
        time.sleep(SLEEP_SECS)

def perform_extraction(perform_strings=True, perform_heap_locs_dump=False):
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS,USE_64
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_procs()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_procs()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None and not check_completed_memory_extraction_completed(base_location):
            working_dir = get_working_dir()
            p = start_add_extraction_process(base_location, working_dir, use_64, use_python, perform_agg, perform_strings, perform_heap_locs_dump)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_procs()
        time.sleep(SLEEP_SECS)

def perform_aggregation(perform_strings=True, perform_heap_locs_dump=True):
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS,USE_64
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_procs_only()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_procs_only()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            p = start_add_agg_process(base_location)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_procs_only()
        time.sleep(SLEEP_SECS)

def prune_active_procs_only():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            # was not able to successfully complete the directory
            if check_completed_memory_extraction_completed(base_location):
                mark_location_complete(base_location)
                print("%s: Marking %s as completed"%(time_str(), base_location))
            try:
                os.stat(working_dir)
                print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                shutil.rmtree(working_dir)
            except:
                pass
    ACTIVE_PROCS = new_active_procs
    ACTIVE_PROCS_LOCK.release()

def prune_active_jbgrep_only():
    global ACTIVE_PROCS, ACTIVE_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            # was not able to successfully complete the directory
            if check_full_jbgrep_completed_memory_extraction_completed(base_location):
                mark_location_complete(base_location)
                print("%s: Marking %s as completed"%(time_str(), base_location))
            try:
                os.stat(working_dir)
                print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                shutil.rmtree(working_dir)
            except:
                pass
    ACTIVE_PROCS = new_active_procs
    ACTIVE_PROCS_LOCK.release()

def perform_proc_extraction():
    global EXTRACT_MEMORY_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_MEMORY_DATA=True
    use_64=USE_64
    use_python=False
    perform_agg=True
    while EXTRACT_MEMORY_DATA:
        prune_active_procs_only()
        while EXTRACT_MEMORY_DATA and not can_start_new_proc():
            prune_active_procs_only()
            if not EXTRACT_MEMORY_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = start_add_proc_extraction_process(base_location, working_dir)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_active_procs_only()
        time.sleep(SLEEP_SECS)

def perform_hprof_analysis():
    global EXTRACT_HPROF_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_HPROF_DATA=True
    while EXTRACT_HPROF_DATA:
        prune_hprof_active_procs()
        while EXTRACT_HPROF_DATA and not can_start_new_proc():
            prune_hprof_active_procs()
            if not EXTRACT_HPROF_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = Process(target=perform_hprof_analysis_work, args=(base_location, working_dir, True, False))
            p.start()
            add_active_procs(p, base_location, working_dir)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_hprof_active_procs()
        time.sleep(SLEEP_SECS)

def perform_augmented_data_extraction():
    global EXTRACT_HPROF_DATA, SLEEP_TIME_BETWEEN_STARTS, SLEEP_BETWEEN_STARTS
    EXTRACT_HPROF_DATA=True
    while EXTRACT_HPROF_DATA:
        prune_hprof_active_procs()
        while EXTRACT_HPROF_DATA and not can_start_new_proc():
            prune_hprof_active_procs()
            if not EXTRACT_HPROF_DATA:
                break
            elif can_start_new_proc():
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = Process(target=perform_augmented_work, args=(base_location, working_dir))
            p.start()
            add_active_procs(p, base_location, working_dir)
            if SLEEP_BETWEEN_STARTS:
                time.sleep(SLEEP_TIME_BETWEEN_STARTS)

    while has_active_procs():
        prune_hprof_active_procs()
        time.sleep(SLEEP_SECS)

def start_extraction_thread():
    global EXTRACT_MEMORY_DATA
    EXTRACT_MEMORY_DATA = True
    t = threading.Thread(target=perform_extraction)
    t.start()
    return t

def start_hprof_analysis_thread():
    global EXTRACT_HPROF_DATA
    EXTRACT_HPROF_DATA = True
    t = threading.Thread(target=perform_hprof_analysis)
    t.start()
    return t

def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))

def dir_has_memory_dump(location):
    has_it = False
    try:
        os.stat(os.path.join(location, MEMORY_DUMP))
        has_it = True
    except:
        has_it = False
    return has_it

def dir_has_memory_dump_zip(location):
    has_it = False
    try:
        os.stat(os.path.join(location, MEMORY_DUMP_ZIP))
        has_it = True
    except:
        has_it = False
    return has_it

def dir_has_jbgrep_strings(location):
    has_it = False
    try:
        os.stat(pppds.get_jbgrep_name(location))
        has_it = True
    except:
        has_it = False
    return has_it

def scan_location_for_complete_memory_process(location):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        if check_completed_memory_extraction_completed(d):
            found_dirs.add(d)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if not d in COMPLETE_DIRS and\
           check_completed_memory_extraction_completed(d):
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def scan_location_for_hprof(location):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        if check_completed_has_java_hprof(d):
            found_dirs.add(d)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if not d in COMPLETE_DIRS:
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def scan_location_for_proc_files(location, ignore_memdump_f=False):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        if (not dir_has_memory_dump(d) or ignore_memdump_f) and\
           dir_has_memory_dump_zip(d) and\
           dir_has_jbgrep_strings(d):
            found_dirs.add(d)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if not d in COMPLETE_DIRS:
           not vol_work_is_complete(d) and not d in INCOMPLETE_DIRS and\
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def scan_location_for_ready_for_heap_locs(location, ignore_memdump_f=False):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for k in dirs:
        if check_completed_all_memory_extraction_completed(k):
            found_dirs.add(k)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if check_completed_all_memory_extraction_completed(d) and \
           not d in COMPLETE_DIRS and\
           not d in INCOMPLETE_DIRS:
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def scan_location_for_files(location, ignore_memdump_f=False):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        if (not dir_has_memory_dump(d) or ignore_memdump_f) and\
           dir_has_memory_dump_zip(d) and\
           dir_has_jbgrep_strings(d):
            found_dirs.add(d)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if not d in COMPLETE_DIRS:
           not check_completed_memory_extraction_completed(d) and not d in INCOMPLETE_DIRS and\
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def add_files_work_queue(locations):
    try:
        INCOMPLETE_DIRS_LOCK.acquire()
        for loc in locations:
            WORKING_QUEUE.insert(0, loc)
            INCOMPLETE_DIRS.add(loc)
    except:
        raise
    finally:
        INCOMPLETE_DIRS_LOCK.release()

def reapply_work(location):
    INCOMPLETE_DIRS_LOCK.acquire()
    WORKING_QUEUE.insert(0, location)
    INCOMPLETE_DIRS_LOCK.release()

def get_base_location():
    o = None
    INCOMPLETE_DIRS_LOCK.acquire()
    if len(WORKING_QUEUE) > 0:
        o = WORKING_QUEUE.pop()
    INCOMPLETE_DIRS_LOCK.release()
    return o

def get_working_dir(base=None):
    if base is None:
        base = WORKING_BASE
    working_dir = os.path.join(base, randomword(20))
    os.makedirs(working_dir)
    return working_dir


def perform_copy(base_location, working_dir, perform_strings=True):
    if not perform_strings:
        try:
            fstrings = pppds.get_ffastrings_zip_len_res_name(base_location)
            k = os.stat(fstrings)
            if k.st_size < (1024 * 1024 *1024):
                os.remove(fstrings)
            else:
                shutil.copy(fstrings, working_dir)
        except:
            pass

    jbgrep_grep_res = sifipr.get_jbgrep_grep_res_name(base_location)
    jbgrep_fname_res = sifipr.get_jbgrep_fname_res_name(base_location)
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(base_location)

    if os.path.exists(jbgrep_grep_res):
        shutil.copy(jbgrep_grep_res, working_dir)
    if os.path.exists(jbgrep_fname_res):
        shutil.copy(jbgrep_fname_res, working_dir)
    if os.path.exists(jbgrep_count_res):
        shutil.copy(jbgrep_count_res, working_dir)

    mem_dump_zip = sifipr.get_java_dump_zip_location(base_location)
    jbgrep_strings = pppds.get_jbgrep_name(base_location)
    shutil.copy(mem_dump_zip, working_dir)
    shutil.copy(jbgrep_strings, working_dir)

def perform_copy_proc(base_location, working_dir):
    if os.path.exists(sifipr.get_auth_data_name(base_location)):
        shutil.copy(sifipr.get_auth_data_name(base_location), working_dir)
    if os.path.exists(sifipr.get_keyinfo_name(base_location)):
        shutil.copy(sifipr.get_keyinfo_name(base_location), working_dir)
    mem_dump_zip = sifipr.get_java_dump_zip_location(base_location)
    jbgrep_strings = pppds.get_jbgrep_name(base_location)
    shutil.copy(mem_dump_zip, working_dir)
    shutil.copy(jbgrep_strings, working_dir)


def perform_copy_hprof(base_location, working_dir):
    hprof_zip = sifipr.get_java_hprof_zip(base_location)
    shutil.copy(hprof_zip, working_dir)

def perform_copy_augmented(base_location, working_dir):
    auth_data_file = aed.get_auth_data_filename(base_location)
    shutil.copy(auth_data_file, working_dir)

def perform_copy_augmented_results(base_location, working_dir):
    auth_data_file = aed.get_augmented_data_filename(working_dir)
    shutil.copy(auth_data_file, base_location)

def perform_copy_gclog(base_location, working_dir):
    gclog = os.path.join(base_location, 'gc_log.txt')
    try:
        shutil.copy(gclog, working_dir)
        return True
    except:
        return False

def perform_copy_gclog_results(base_location, working_dir):
    gclog_tokn = os.path.join(working_dir, 'gc_token.txt')
    gclog_json = os.path.join(working_dir, 'gc_json.json')
    shutil.copy(gclog_tokn, base_location)
    shutil.copy(gclog_json, base_location)

def perform_copy_jbgrep_results(base_location, working_dir):
    jbgrep_grep_res = sifipr.get_jbgrep_grep_res_name(working_dir)
    jbgrep_fname_res = sifipr.get_jbgrep_fname_res_name(working_dir)
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(working_dir)
    files_to_copy = [
        jbgrep_grep_res,
        jbgrep_fname_res,
        jbgrep_count_res,
    ]
    if not os.path.exists(jbgrep_grep_res) or not os.stat(jbgrep_grep_res).st_size > 10:
        os.remove(jbgrep_grep_res)
        os.remove(jbgrep_fname_res)
        os.remove(jbgrep_count_res)

    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, base_location)
            print("Copying %s to %s"%(f, base_location))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            break

    if len(all_success) == len(files_to_copy):
        try:
            os.system("touch %s"%(os.path.join(base_location, FULL_JBGREP_COMPLETE)))
            return True
        except:
            return False
    return False

def perform_copy_ffastrings_results(base_location, working_dir):
    ffastrings_zip = pppds.get_ffastrings_zip_len_res_name(working_dir)
    files_to_copy = [
        ffastrings_zip,
    ]
    try:
        k = os.stat(ffastrings_zip)
        if k.st_size < 1024:
            os.remove(ffastrings_zip)
            files_to_copy.remove(ffastrings_zip)
    except:
        files_to_copy.remove(ffastrings_zip)

    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, base_location)
            print("Copying %s to %s"%(f, base_location))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            break

    if len(all_success) == len(files_to_copy):
        try:
            os.system("touch %s"%(os.path.join(base_location, FFASTRINGS_COMPLETE)))
            return True
        except:
            return False
    return False

def perform_copy_results(base_location, working_dir):
    jbgrep_grep_res = sifipr.get_jbgrep_grep_res_name(working_dir)
    jbgrep_fname_res = sifipr.get_jbgrep_fname_res_name(working_dir)
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(working_dir)
    jbgrep_grep_resd = pppds.get_jbgrep_dumps_grep_res_name(working_dir)
    jbgrep_fname_resd = pppds.get_jbgrep_dumps_fname_res_name(working_dir)
    jbgrep_count_resd = pppds.get_jbgrep_dumps_count_res_name(working_dir)
    ffastrings_zip = pppds.get_ffastrings_zip_len_res_name(working_dir)
    files_to_copy = [
        ffastrings_zip,
        jbgrep_grep_res,
        jbgrep_fname_res,
        jbgrep_count_res,
        #jbgrep_grep_resd,
        #jbgrep_fname_resd,
        #jbgrep_count_resd,
    ]
    try:
        k = os.stat(ffastrings_zip)
        if k.st_size < 1024:
            os.remove(ffastrings_zip)
    except:
        pass

    if os.path.exists(jbgrep_grep_resd):
        files_to_copy.append(jbgrep_grep_resd)
        os.system("touch %s"%(os.path.join(base_location, VOL_UNZIP_MEMORY_PROCESS)))

    if os.path.exists(jbgrep_grep_res) and os.stat(jbgrep_grep_res).st_size > 10:
        os.system("touch %s"%(os.path.join(base_location, FULL_JBGREP_COMPLETE)))

    if os.path.exists(ffastrings_zip):
        os.system("touch %s"%(os.path.join(base_location, FFASTRINGS_COMPLETE)))


    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, base_location)
            print("Copying %s to %s"%(f, base_location))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            break

    if len(all_success) == len(files_to_copy):
        try:
            os.system("touch %s"%(os.path.join(base_location, UNZIP_MEMORY_PROCESS)))
            return True
        except:
            return False
    return False

def perform_copy_proc_results(base_location, working_dir):
    jbgrep_grep_resd = pppds.get_jbgrep_dumps_grep_res_name(working_dir)
    jbgrep_fname_resd = pppds.get_jbgrep_dumps_fname_res_name(working_dir)
    jbgrep_count_resd = pppds.get_jbgrep_dumps_count_res_name(working_dir)
    files_to_copy = [
        jbgrep_grep_resd,
        jbgrep_fname_resd,
        jbgrep_count_resd,
    ]
    try:
        java_heap_key_locs = sifipr.get_java_heap_key_locations(working_dir)
        os.stat(java_heap_key_locs)
        files_to_copy.append(java_heap_key_locs)
    except:
        pass

    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, base_location)
            print("Copying %s to %s"%(f, base_location))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            break

    if len(all_success) == len(files_to_copy):
        try:
            os.system("touch %s"%(os.path.join(base_location, VOL_UNZIP_MEMORY_PROCESS)))
            return True
        except:
            return False
    return False

def work_is_complete(base_location):
    f = os.path.join(base_location, UNZIP_MEMORY_PROCESS)
    try:
        os.stat(f)
        return True
    except:
        return False

def vol_work_is_complete(base_location):
    f = os.path.join(base_location, VOL_UNZIP_MEMORY_PROCESS)
    try:
        os.stat(f)
        return True
    except:
        return False

def perform_rm_dump_files(base_location, use_python):
    dumps_loc = sifipr.get_java_dumps_location(base_location, use_python)
    if os.path.exists(dumps_loc):
        shutil.rmtree(dumps_loc)


def perform_extraction_work(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    global RUNNING_WITH_QEMU
    decomp_strings=True
    start_time = time_str()

    if not perform_strings:
        try:
            os.stat(pppds.get_ffastrings_zip_len_res_name(base_location))
        except:
            perform_strings = True

    # copy over location
    if work_is_complete(base_location):
        if decomp_strings:
            uad.perform_agg_copy(base_location, working_dir)
            if os.path.exists(pppds.get_ffastrings_zip_len_res_name(base_location)):
                os.remove(pppds.get_ffastrings_zip_len_res_name(base_location))
            #decomp_strings_proc = Process(target=uad.perform_strings_decompression, args=(working_dir,))
            uad.perform_strings_decompression(working_dir)
            #decomp_strings_proc.start()
            #decomp_strings_proc.join()
        #if perform_agg:
        #    perform_work_data_agg(base_location, working_dir, use_64, use_python, decomp_strings=decomp_strings)
        print("%s: job complete start"%(start_time))
        print("%s: Nothing to do, work has already been done: %s"%(time_str(), base_location))
        return

    if working_dir is None:
        working_dir = get_working_dir()

    ffastrings_unzip_proc = None

    perform_copy(base_location, working_dir, perform_strings)
    decomp_strings_proc = None
    ffastrings_zip_proc = None
    ffastrings_proc = None

    if not perform_strings:
        try:
            os.stat(pppds.get_ffastrings_zip_len_res_name(working_dir))
            decomp_strings_proc = Process(target=uad.perform_strings_decompression, args=(working_dir,))
            decomp_strings_proc.start()
        except:
            perform_strings = True

    pppds.perform_mem_dump_decompression(working_dir, use_python=use_python)
    dump_proc_proc = None
    if perform_heap_locs_dump:
        outstr = ("%s: Modified **** Perforiming volatility process dump %s"%(time_str(), working_dir))
        dump_proc_proc = Process(target=pppds.dump_process_memory, args=(working_dir,{}, use_64, use_python))
        dump_proc_proc.start()
    if RUNNING_WITH_QEMU:
        bhmc.prioritize_qemu()

    full_key_search_proc = None
    if not check_full_jbgrep_completed_memory_extraction_completed(base_location):
        outstr = ("%s: Perforiming ssl key search %s"%(time_str(), working_dir))
        print (outstr)
        full_key_search_proc = Process(target=pppds.perform_ssl_key_search, args=(working_dir,use_python))
        full_key_search_proc.start()


    if perform_strings:
        outstr = ("%s: Perforiming strings search %s"%(time_str(), working_dir))
        print (outstr)
        ffastrings_proc = Process(target=pppds.perform_ffastrings, args=(working_dir,use_python))
        ffastrings_proc.start()
    if RUNNING_WITH_QEMU:
        bhmc.prioritize_qemu()

    if perform_heap_locs_dump:
        dump_proc_proc.join()
        outstr = ("%s: Perforiming ssl key search in the dump files %s"%(time_str(), working_dir))
        print (outstr)
        if RUNNING_WITH_QEMU:
            bhmc.prioritize_qemu()
        dumps_key_search_proc = Process(target=pppds.perform_dumps_ssl_key_search, args=(working_dir,use_python))
        dumps_key_search_proc.start()

    ffastrings_zip_proc = None
    if ffastrings_proc:
        ffastrings_proc.join()
        outstr = ("%s: Zipping the strings file %s"%(time_str(), working_dir))
        print (outstr)
        ffastrings_zip_proc = Process(target=pppds.perform_zip_ffastrings_output, args=(working_dir,))
        ffastrings_zip_proc.start()

    if not full_key_search_proc is None:
        full_key_search_proc.join()

    pppds.perform_rm_memory_dump(working_dir, use_python)

    if perform_heap_locs_dump:
        dumps_key_search_proc.join()
        rm_dump_files_proc = Process(target=perform_rm_dump_files, args=(working_dir, use_python))
        rm_dump_files_proc.start()

    if ffastrings_zip_proc:
        ffastrings_zip_proc.join()

    outstr = ("%s: Copying the results from %s to %s"%(time_str(), working_dir, base_location))
    print (outstr)
    if not perform_copy_results(base_location, working_dir):
        outstr = ("%s: Failed to copy results %s for %s"%(time_str(), working_dir, base_location))
        print (outstr)


    if decomp_strings_proc:
        decomp_strings_proc.join()
    outstr = ("%s: Removing the extracted memmap files %s"%(time_str(), base_location))
    print (outstr)
    print("%s: job complete start"%(start_time))
    print("%s: job complete end %s"%(time_str(), base_location))

def perform_extraction_ffastrings_work(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    global RUNNING_WITH_QEMU
    decomp_strings=True
    start_time = time_str()

    del_working_dir = False
    if working_dir is None:
        working_dir = get_working_dir()
        del_working_dir = True

    if check_full_ffastrings_completed_memory_extraction_completed(base_location):
        if del_working_dir:
            shutil.rmtree(working_dir)
        return

    ffastrings_unzip_proc = None

    perform_copy(base_location, working_dir, perform_strings)
    decomp_strings_proc = None
    ffastrings_zip_proc = None
    ffastrings_proc = None

    pppds.perform_mem_dump_decompression(working_dir, use_python=use_python)
    outstr = ("%s: Modified **** Perforiming volatility process dump %s"%(time_str(), working_dir))

    outstr = ("%s: Perforiming strings search %s"%(time_str(), working_dir))
    print (outstr)

    if perform_strings:
        ffastrings_proc = Process(target=pppds.perform_ffastrings, args=(working_dir,use_python))
        ffastrings_proc.start()
    if RUNNING_WITH_QEMU:
        bhmc.prioritize_qemu()

    if ffastrings_proc:
        ffastrings_proc.join()
        outstr = ("%s: Zipping the strings file %s"%(time_str(), working_dir))
        print (outstr)
        ffastrings_zip_proc = Process(target=pppds.perform_zip_ffastrings_output, args=(working_dir,))
        ffastrings_zip_proc.start()

    pppds.perform_rm_memory_dump(working_dir, use_python)

    if ffastrings_zip_proc:
        ffastrings_zip_proc.join()

    outstr = ("%s: Copying the results from %s to %s"%(time_str(), working_dir, base_location))
    print (outstr)
    if not perform_copy_ffastrings_results(base_location, working_dir):
        outstr = ("%s: Failed to copy results %s for %s"%(time_str(), working_dir, base_location))
        print (outstr)


    print (outstr)
    print("%s: job complete start"%(start_time))
    print("%s: job complete end %s"%(time_str(), base_location))
    if del_working_dir:
        shutil.rmtree(working_dir)

def perform_extraction_jbgrep_work(base_location, working_dir=None, use_64=True, use_python=False, perform_agg=True, perform_strings=True, perform_heap_locs_dump=False):
    global RUNNING_WITH_QEMU
    start_time = time_str()
    del_working_dir = False
    if working_dir is None:
        working_dir = get_working_dir()
        del_working_dir = True

    if check_full_jbgrep_completed_memory_extraction_completed(base_location):
        if del_working_dir:
            shutil.rmtree(working_dir)
        return

    perform_copy(base_location, working_dir, perform_strings)
    pppds.perform_mem_dump_decompression(working_dir, use_python=use_python)

    outstr = ("%s: Modified **** Perforiming volatility process dump %s"%(time_str(), working_dir))
    if RUNNING_WITH_QEMU:
        bhmc.prioritize_qemu()

    outstr = ("%s: Perforiming ssl key search %s"%(time_str(), working_dir))
    print (outstr)
    full_key_search_proc = Process(target=pppds.perform_ssl_key_search, args=(working_dir,use_python))
    full_key_search_proc.start()

    outstr = ("%s: Removing the dump file %s"%(time_str(), working_dir))
    print (outstr)

    full_key_search_proc.join()
    pppds.perform_rm_memory_dump(working_dir, use_python)

    outstr = ("%s: Copying the results from %s to %s"%(time_str(), working_dir, base_location))
    print (outstr)
    if not perform_copy_jbgrep_results(base_location, working_dir):
        outstr = ("%s: Failed to copy results %s for %s"%(time_str(), working_dir, base_location))
        print (outstr)


    outstr = ("%s: Removing the extracted memmap files %s"%(time_str(), base_location))
    print (outstr)
    print("%s: job complete start"%(start_time))
    print("%s: job complete end %s"%(time_str(), base_location))
    if del_working_dir:
        shutil.rmtree(working_dir)

def perform_work_data_agg(base_location, working_dir=None, use_64=True, use_python=False, decomp_strings=False):
    start_time = time_str()
    # copy over location
    outstr = ("%s: Modified Processing %s"%(time_str(), base_location))
    print outstr
    destroy_working_dir = False
    #if work_is_complete(base_location):
    #    print("%s: job complete start"%(start_time))
    #    print("%s: Nothing to do, work has already been done: %s"%(time_str(), base_location))
    #    return
    if working_dir is None:
        working_dir = get_working_dir()
        destroy_working_dir = True

    uad.perform_agg_copy(base_location, working_dir)
    strings_full_filename = os.path.join(base_location, aed.STRING_OUTPUT_FULL)
    if not os.path.exists(strings_full_filename) and os.path.exists(strings_full_filename+".zip"):
        decomp_strings=True
    elif not os.path.exists(strings_full_filename):
        outstr = ("%s: No strings, so cannot complete the processing of %s"%(time_str(), base_location))
        print outstr
        return

    decomp_strings_proc = None
    if decomp_strings:
        decomp_strings_proc = Process(target=uad.perform_strings_decompression, args=(working_dir,))
        decomp_strings_proc.start()
    outstr = ("%s: Perforiming strings file decompression %s"%(time_str(), working_dir))

    is_streaming = False
    aed.log_out("%s: Reading strings and event information for %s"%(time_str(), working_dir))
    factors = sifipr.get_factors_data(base_location)
    times = aed.get_times(working_dir)
    auth_data = aed.read_event_information(working_dir, factors)
    factors['age'] = (auth_data['max_time'] - auth_data['min_time'])/1000
    auth_data['ssl_values'] = aed.merge_key_hits_key_info(working_dir)
    auth_data['ssl_ordered_comms'] = aed.build_ordered_ssl_comms(auth_data['ssl_values'])
    auth_data.update(times)
    if not decomp_strings_proc is None:
        decomp_strings_proc.join()
    strings_data = aed.read_strings_information(working_dir, auth_data)
    auth_data['http_values'] = aed.update_shadow_auth_info(strings_data, auth_data['http_values'])
    aed.scale_timestamps_ssl_values(auth_data['ssl_values'].values(), times)
    aed.scale_timestamps_http_values(auth_data['http_values'].values(), times)
    auth_data['matched_comms'] = aed.match_http_with_ssl_keys(auth_data, times, is_streaming=False )
    # scale all the timestamp http_values

    # make sure the external end clock time is the same, and if not update
    #update_end_time(auth_data, times)

    infos = aed.extract_basic_infos(auth_data, factors)
    for user, info in infos.items():
        auth_data['http_values'][user]['age'] = info['age']
        auth_data['http_values'][user]['live'] = info['live']

    #update the ssl ages here
    # comm timestamp is considered the end of a communication
    host_start = times['start']
    host_end = times['act_end']
    for comm in auth_data['matched_comms']:
        user = comm['user']
        ms = comm['ms']
        age = comm['age']
        dur_exp = -1
        v = auth_data['ssl_values'][ms]
        if is_streaming:
            live = not auth_data['http_values'][user]['completed']
            age = auth_data['http_values'][user]['age']
        else:
            live = False

        if not live and (comm['ms_hit'] or comm['pms_hit']) and user in auth_data['http_values']:
            dur_exp = auth_data['http_values'][user]['end'] - host_start
            dur_exp = 0 if dur_exp < 0 else dur_exp
        else:
            dur_exp = 0

        v['live'] = live
        v['matched'] = True
        v['dur_exp'] = comm['dur_exp']
        v['unscaled_timestamp'] = v['timestamp']
        v['timestamp'] = comm['ssl_timestamp']
        v['age'] = age
        v['start'] = comm['ssl_timestamp'] - host_start
        v['end'] = v['start'] + age if not live else -1
        v['end_timestamp'] = comm['http_timestamp'] if not live else -1

        comm['dur_exp'] = host_end - comm['end'] if not live else 0
        comm['live'] = live
        comm['end'] = comm['start'] + age if not live else -1
        comm['end_timestamp'] = comm['http_timestamp'] if not live else -1

    matched_comm_ssl = {}
    for comm in auth_data['matched_comms']:
        matched_comm_ssl[comm['ms']] = comm

    for comm in auth_data['ssl_ordered_comms']:
        ms = comm['ms']
        v = auth_data['ssl_values'][ms]
        age = comm['age']
        comm['end'] = matched_comm_ssl[ms]['end'] if ms in matched_comm_ssl else -1
        comm['end_timestamp'] = matched_comm_ssl[ms]['end_timestamp'] if ms in matched_comm_ssl else -1

        if 'matched' in v and v['matched']:
            continue
        v['age'] = 0
        v['live'] = True
        v['unscaled_timestamp'] = auth_data['ssl_values'][ms]['timestamp']
        v['timestamp'] = comm['timestamp']
        v['start'] = comm['timestamp'] - host_start
        v['end'] = -1
        v['end_timestamp'] = -1
        v['dur_exp'] = -1

    auth_data.update(times)
    aed.log_out("%s: Performing data searches for %s"%(time_str(), working_dir))
    aed.perform_data_search(working_dir, strings_data, auth_data)
    aed.log_out("%s: Caclulating the probabilities %s"%(time_str(), working_dir))
    # probability that a session token could be recovered
    # probability that a password could be recovered (alone)
    # probability that a post uri could be recovered
    auth_data['live_quant'] = {"est_comms":len(auth_data['matched_comms']),
                               "num_ssl_sessions":len(auth_data['ssl_values']),
                               "num":len(auth_data['http_values'].values()),
                               "num_live":sum([1 for i in auth_data['http_values'].values() if i['live']]) }
    infos, results = aed.calc_password_info(auth_data, factors)
    auth_data['password_infos'] = infos
    auth_data['password_quant'] = results
    infos, results = aed.calc_sessionid_info(auth_data, factors)
    auth_data['sessionid_infos'] = infos
    auth_data['sessionid_quant'] = results
    infos, results = aed.calc_cookie_sessionid_info(auth_data, factors)
    auth_data['cookie_sessionid_infos'] = infos
    auth_data['cookie_sessionid_quant'] = results
    infos, results = aed.calc_post_uri_info(auth_data, factors)
    auth_data['post_uri_infos'] = infos
    auth_data['post_uri_quant'] = results
    infos, results = aed.calc_logentry_info(auth_data, factors)
    auth_data['logentry_infos'] = infos
    auth_data['logentry_quant'] = results
    memory_consumption = aed.calculate_memory_consumption(auth_data, factors)
    auth_data['memory_consumption'] = memory_consumption
    infos, results = aed.calc_ssl_info_quant(auth_data, factors)
    auth_data['ssl_infos'] = infos
    auth_data['ssl_quant'] = results
    infos, results = aed.calc_ssl_info_quant(auth_data, factors, use_unmatched=True)
    auth_data['ssl_infos_unmatched'] = infos
    auth_data['ssl_quant_unmatched'] = results

    aed.log_out("%s: writing results to json file %s"%(time_str(), working_dir))
    aed.perform_results_write(working_dir, auth_data)
    jvm_mem = 4096
    try:
        use_64, _, jvm_mem, factors = aed.get_run_info(base_location)
    except:
        pass
    try:
        uad.write_java_heap_locations(working_dir, auth_data, heap_size=jvm_mem)
    except:
        outstr = ("%s: Unable to write heap location data %s"%(time_str(), base_location))
        print (outstr)
        traceback.print_exc()
    uad.write_compromised_summary_data(working_dir, auth_data)
    uad.perform_agg_copy_results(base_location, working_dir)
    perform_gclog_work(base_location, working_dir)
    aed.log_out("%s: job complete %s"%(time_str(), working_dir))
    if destroy_working_dir:
        shutil.rmtree(working_dir)

    #os.remove(pppds.get_ffastrings_zip_len_res_name(working_dir))
    #perform_hprof_analysis_work(base_location, working_dir, use_64, use_python, decomp_strings)
    return auth_data


def perform_hprof_analysis_work(base_location, working_dir=None, use_64=True, use_python=False, decomp_strings=False):
    if working_dir is None:
        working_dir = get_working_dir()

    outstr = ("%s: Perforiming hprof data extraction %s -> %s"%(time_str(), base_location, working_dir))
    print outstr
    zipped_fname = sifipr.get_java_hprof_zip(working_dir)
    perform_copy_hprof(base_location, working_dir)
    results = {}
    intermediate = {}

    out_str, results = pjh.capture_hprof_stats(zipped_fname)
    open(sifipr.get_java_hprof_results(working_dir), 'w').write(out_str)
    perform_copy_hprof_results(base_location, working_dir)
    return out_str, results



def perform_gclog_work(base_location, working_dir=None):
    destroy_working_dir = False
    if working_dir is None:
        destroy_working_dir=True
        working_dir = uppd.get_working_dir()
    outstr = ("%s: Perforiming gclog data extraction %s -> %s"%(time_str(), base_location, working_dir))
    print outstr
    if perform_copy_gclog(base_location, working_dir):
        aed.perform_gc_log_parse_save(working_dir)
        perform_copy_gclog_results(base_location, working_dir)
        outstr = ("%s: Completed gclog data extraction %s -> %s"%(time_str(), base_location, working_dir))
        print outstr
        if destroy_working_dir:
            shutil.rmtree(working_dir)

def perform_augmented_work(base_location, working_dir=None):
    destroy_working_dir = False
    if working_dir is None:
        destroy_working_dir=True
        working_dir = uppd.get_working_dir()
    outstr = ("%s: Perforiming gclog data extraction %s -> %s"%(time_str(), base_location, working_dir))
    print outstr
    perform_copy_augmented(base_location, working_dir)
    aed.write_augmented_data(working_dir)
    perform_copy_augmented_results(base_location, working_dir)
    outstr = ("%s: Completed augmented data extraction %s -> %s"%(time_str(), base_location, working_dir))
    print outstr
    if destroy_working_dir:
        shutil.rmtree(working_dir)

def handle_large_serial_files(base_location, num_threads=16):
    pppds.FFASTRINGS_NUM_THREADS_NO_EXECING=num_threads
    pppds.JBGREP_NUM_THREADS_NO_EXECING=num_threads
    pppds.FFASTRINGS_NUM_THREADS=num_threads
    pppds.JBGREP_NUM_THREADS=num_threads
    use_64 = True
    use_python = False
    working_dir = False
    working_dir = get_working_dir()

    ffastrings_unzip_proc = None
    decomp_strings_proc = None
    ffastrings_zip_proc = None
    ffastrings_proc = None

    perform_copy(base_location, working_dir, True)
    outstr = ("%s: Perforiming strings search %s"%(time_str(), working_dir))
    print (outstr)
    pppds.perform_mem_dump_decompression(working_dir, use_python=False)
    outstr = ("%s: Modified **** Perforiming volatility process dump %s"%(time_str(), working_dir))
    dump_proc_proc = Process(target=pppds.dump_process_memory, args=(working_dir,{}, use_64, use_python))

    dump_proc_proc.start()
    ffastrings_proc = Process(target=pppds.perform_ffastrings, args=(working_dir,False))
    ffastrings_proc.start()
    ffastrings_proc.join()
    ffastrings_zip_proc = Process(target=pppds.perform_zip_ffastrings_output, args=(working_dir,))

    outstr = ("%s: Perforiming ssl key search %s"%(time_str(), working_dir))
    print (outstr)
    full_key_search_proc = Process(target=pppds.perform_ssl_key_search, args=(working_dir,False))
    full_key_search_proc.start()
    full_key_search_proc.join()
    ffastrings_zip_proc.start()

    dump_proc_proc.join()

    outstr = ("%s: Perforiming ssl key search in the dump files %s"%(time_str(), working_dir))
    print (outstr)
    dumps_key_search_proc = Process(target=pppds.perform_dumps_ssl_key_search, args=(working_dir,use_python))
    dumps_key_search_proc.start()
    ffastrings_zip_proc.join()
    dumps_key_search_proc.join()
    outstr = ("%s: Done, copying results"%(time_str()))
    perform_copy_results(base_location, working_dir)
    perform_work_data_agg(base_location, working_dir, True, False, False)
    shutil.rmtree(working_dir)

def perform_proc_extraction_work(base_location, working_dir=None, use_64=True, use_python=False):
    global RUNNING_WITH_QEMU
    start_time = time_str()

    # copy over location
    if vol_work_is_complete(base_location):
        print("%s: job complete start"%(start_time))
        print("%s: Nothing to do, work has already been done: %s"%(time_str(), base_location))
        return

    if working_dir is None:
        working_dir = get_working_dir()

    ffastrings_unzip_proc = None

    perform_copy_proc(base_location, working_dir)

    pppds.perform_mem_dump_decompression(working_dir, use_python=use_python)
    outstr = ("%s: Modified **** Perforiming volatility process dump %s"%(time_str(), working_dir))
    try:
        os.mkdir(os.path.join(working_dir, 'java_dumps'))
    except:
        pass
    dump_proc_proc = Process(target=pppds.dump_process_memory, args=(working_dir,{}, use_64, use_python))
    dump_proc_proc.start()
    dump_proc_proc.join()

    outstr = ("%s: Perforiming ssl key search in the dump files %s"%(time_str(), working_dir))
    print (outstr)
    dumps_key_search_proc = Process(target=pppds.perform_dumps_ssl_key_search, args=(working_dir,use_python))
    dumps_key_search_proc.start()
    outstr = ("%s: Removing the dump file %s"%(time_str(), working_dir))
    print (outstr)
    pppds.perform_rm_memory_dump(working_dir, use_python)


    dumps_key_search_proc.join()
    rm_dump_files_proc = Process(target=perform_rm_dump_files, args=(working_dir, use_python))
    rm_dump_files_proc.start()

    outstr = ("%s: Copying the results from %s to %s"%(time_str(), working_dir, base_location))
    print (outstr)
    if not perform_copy_proc_results(base_location, working_dir):
        outstr = ("%s: Failed to copy results %s for %s"%(time_str(), working_dir, base_location))
        print (outstr)

    jvm_mem = 4096
    try:
        use_64, _, jvm_mem, factors = aed.get_run_info(base_location)
    except:
        pass

    try:
        os.stat(sifipr.get_auth_data_name(working_dir))
        auth_data = aed.get_auth_data(working_dir)
        uad.write_java_heap_locations(working_dir, auth_data, heap_size=jvm_mem)
    except:
        outstr = ("%s: Unable to write heap location data %s"%(time_str(), base_location))
        print (outstr)
        traceback.print_exc()
    rm_dump_files_proc.join()
    outstr = ("%s: Removing the extracted memmap files %s"%(time_str(), base_location))
    print (outstr)
    print("%s: job complete start"%(start_time))
    print("%s: job complete end %s"%(time_str(), base_location))

