# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-00 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-01 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-02 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-03 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-04 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-05 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-06 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-07 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-08 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-09 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-10 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-11 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-12 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-13 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-14 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-15 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-16 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-17 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-18 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-19 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-20 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-21 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-22 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-23 java java
# python /srv/nfs/cortana/logs/cmd/install_tlslite.py java-workx64-24 java java



import paramiko, re, time, sys
def ssh_to_target (hostname, username, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, username=username, password=password)
    return client
 
def exec_cmds(client, password, cmds):
    for cmd in cmds:
        print "Executing command: ", cmd
        transport = client.get_transport()
        session = transport.open_session()
        session.set_combine_stderr(True)
        session.get_pty()
        session.exec_command(cmd)
        stdin = session.makefile('wb', -1)
        stdout = session.makefile('rb', -1)
        #you have to check if you really need to send password here 
        stdin.write(password +'\n')
        stdin.flush()

def perform_install_tlslite(user, password, host):
    cp_jre = '''sudo easy_install tlslite'''
    cmd_list = [
      cp_jre,
    ]
    print "connecting to host:", host
    client = ssh_to_target(host, username=user, password=password)
    exec_cmds (client, password, cmd_list)

    
if __name__ == "__main__":
    if len(sys.argv) < 3:
      cmd = "%s <host_update> <user> <password>"%sys.argv[0]
      print cmd
      sys.exit(-1)
    host = sys.argv[1]
    user = sys.argv[2]
    password = sys.argv[3]
    perform_install_tlslite(user, password, host)
