from multiprocessing import Process
import os, urllib, json
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil
import multiprocessing
from datetime import datetime
import numpy as np


PROCESSES = 3
OUT_LOCK = threading.Lock()
JAVA_DUMP_FINAL = "java-work.dump"
BASE_DIR = None
STRING_OUTPUT_FULL = "ffastrings_len_full.txt"
STDOUT_LOG = "stdout_log.txt"
AUTH_LOG = "authlog.txt"
SSL_MERGED_INFO = "merged_keyinfo.txt"
SSL_FOUND_KEYS = "found_ssl_keys_hits.txt"
SSL_FOUND_KEYS_G = "found_ssl_keys_g.txt"
JSON_FILE = "agg_auth_data.json"
MISSING_USERS = "err_missing_users.txt"
JAVA_DUMPS = "java_dumps"
VOL_HOST_PROFILE = 'LinuxUbuntu1404x86'

LOG_OUT_FILE = "stderr_agg_auth_data.txt"
LOG_OUT = None

ERR_FILE = "stderr_agg_auth_data.txt"
ERR_OUT = None
FAIL_STRING = "FAILLAIF" * 3

FIND_POST_RE = re.compile(".*user=.*&password=.*Content-Type.*")
FIND_POST_ALL_RE = re.compile(".*user=.*&password=.*")

MISSING_USERS = "err_missing_users.txt"
BASE_NFS_DIR = None
FAIL_STRING = "FAILLAIF" * 3

AUTH_JSON_OUTFILE = "agg_auth_data.json"

EXPERIMENT_TIME = 520
CUTOFF_TIME = 300
MAX_SESSION_LIFETIME = long(.75*EXPERIMENT_TIME)
MAX_CONCURRENT_SESSIONS = 120
MAX_ALLOWED_REQ = 120

FACTOR_CONV = {"LOW":.1, "MED":.4, "HIGH":.6, 'RANDOM':-1}
SSL_JSON_FILE = 'ssl_info.json'
MAX_INT = 0xffffffff
TIME_LOG = "timelog_start_end_actual.txt"
LINE_LEN = 86
STDOOUT_LINE_LEN = 120

def generate_memory_image_ranges(runs):
    file_ranges = []
    mappings = []
    for i in runs:
        base, offset, sz = i
        mapping = {'file':[offset, offset+sz],
            'mem':[base, base+sz], 'sz':sz}
        mappings.append(mapping)
    return mappings

def file_addr_in_memory(foffset, mappings=[]):
    for v in mappings:
        frange = v['file']
        if foffset_in_range(foffset, frange):
            return frange[0], frange[1], v['sz']
    return None

def foffset_in_range(foffset, frange):
    return frange[0] <= foffset and foffset < frange[1]

def convert_offset(foffset, frange, prange):
    offset = foffset - frange[0]
    return offset+prange[0]

def get_phys_addr(foffset, mappings):
    for v in mappings:
        frange = v['file']
        prange = v['mem']
        if foffset_in_range(foffset, frange):
            return convert_offset(foffset, frange, prange)
    return None

def scale_timestamps_comms(comm_data, times):
    data_times = [i['timestamp'] for i in comm_data]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta
    for i in comm_data:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts

def scale_timestamps_http_values(http_values, times):
    data_times = [i['timestamp'] for i in http_values]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta

    for i in http_values:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts
        if i['end'] != -1:
            ts = host_start + long(scale * (i['end'] - min_ts))
            i['unscaled_end'] = i['end']
            i['end'] = ts
            i['age'] = i['end'] - i['timestamp']
        
        for e in i['events']:
            ts = host_start + long(scale * (e['timestamp'] - min_ts))
            e['unscaled_timestamp'] = e['timestamp']
            e['timestamp'] = ts

def scale_timestamps_ssl_values(ssl_values, times):
    data_times = [i['timestamp'] for i in ssl_values]
    max_ts = max(data_times)
    min_ts = min(data_times)
    host_start = times['start']
    host_end = times['act_end']
    delta = (max_ts - min_ts) if max_ts != min_ts else 1
    scale = float(host_end - host_start) / delta

    for i in ssl_values:
        ts = host_start + long(scale * (i['timestamp'] - min_ts))
        i['unscaled_timestamp'] = i['timestamp']
        i['timestamp'] = ts
        i['end'] = -1


def get_found_keys_greppable_data(base_location):
    return [parse_found_keys_g(i.strip()) for i in
            open(os.path.join(base_location, SSL_FOUND_KEYS_G)).readlines()]

def parse_found_keys_g(line):
    fname, loc, data = line.split()
    fname.strip(":")
    loc = long(loc, 16)
    return {"file":fname, "offset":loc, "key":data}

def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

def get_time_log_file(base_location):
    return os.path.join(base_location, TIME_LOG)

def get_times(base_location):
    data = open(get_time_log_file(base_location)).read().strip()
    start, end, act_end = [long(i, 16) for i in data.split(',')]
    return {'start':start, 'end':end, 'act_end':act_end}

def get_host_guest_time_delta(host_vm_start, guest_vm_start_time):
    return host_vm_start - guest_vm_start_time

def get_host_guest_time(guest_vm_time, delta):
    return guest_vm_time + delta

def log_err(msg):
    global BASE_DIR, ERR_OUT
    if ERR_OUT is None:
        try:
            ERR_OUT = open(os.path.join(BASE_DIR,ERR_FILE), 'a')
        except:
            print (msg)
            return
    OUT_LOCK.acquire()
    ERR_OUT.write(msg+'\n')
    OUT_LOCK.release()

def log_out(msg):
    global BASE_DIR, LOG_OUT, LOG_OUT_FILE, OUT_LOCK
    if LOG_OUT is None:
        try:
            LOG_OUT = open(os.path.join(BASE_DIR,LOG_OUT_FILE), 'a')
        except:
            print msg
            return
    OUT_LOCK.acquire()
    print(msg)
    OUT_LOCK.release()

def get_java_dump_location(factors):
    java_base = get_dir_name_from_factors(factors)
    return os.path.join(java_base, JAVA_DUMP_FINAL)

def get_vol_java_dump_location(factors):
    java_base = get_dir_name_from_factors(factors)
    return "file:///" + os.path.join(java_base, JAVA_DUMP_FINAL)

def dump_process(filename, dump_dir):
    profile = VOL_HOST_PROFILE
    ex_java = None
    try:
        ex_java = ExtractProc(the_file=filename, profile=profile)
        ex_java.update_process_info()
    except:
        msg = "%s: Failed to find Java in %s "%(time_str(), filename)
        log_err(msg)
        return

    chunks = MemChunk.chunks_from_task_or_file(task=ex_java.proc,
                                            MChunkCls=MemChunk)
    for chunk in chunks.values():
        chunk.check_load()

    for chunk in chunks.values():
        chunk.dump_data(outdir=dump_dir)


def perform_long_way (base_location):
    java_dumps = os.path.join(base_location, JAVA_DUMPS)
    java_dump_files = [os.path.join(java_dumps, k) for k in os.listdir(java_dumps)]

    strings_location = os.path.join(base_location, STRING_OUTPUT)
    cmd_fmt_0 = "strings -n 8 -t x -f %s > %s"
    cmd_fmt_1 = "strings -n 8 -t x -f %s >> %s"
    cmds = [cmd_fmt_0%(java_dump_files[0], strings_location)]
    if len (cmds) == 0:
        return -1
    for f in java_dump_files[1:]:
        cmds.append(cmd_fmt_1%(f, strings_location))

    #print ("Executing: %s"%(str(cmd)))
    for cmd in cmds:
        try:
            result = os.system(cmd)
            if result != 0:
                print ("[------] Failed: %s"%(str(cmd)))
        except KeyboardInterrupt:
            break
    return result

def create_event_json(line):
    event = {}
    if line.find("Starting") == 0:
        event['starting'] = True
        line = line[len("Starting-"):]

    for i in line.split('-'):
        if len(i.split(":")) < 2:
            continue
        k,v = i.split(":")
        k = k.lower()
        if k == 'time':
            v = long(v, 16)
            k = 'timestamp'
        elif k == 'min':
            v = long(v, 16)
        elif k == 'steps':
            v = long(v, 16)
        elif k == 'max':
            v = long(v, 16)
        elif k == 'completed':
            v = bool(long(v,16))
        elif k == 'maxed':
            v = bool(long(v,16))
        elif k == 'session':
            k = 'user'
        elif k == 'mem':
            v = long(v, 16)
        event[k] = v
    return event

def extract_auth_info_from_log(line):
    v = line.strip().split(',')
    if len(v) == 4:
        try:
            long(v[0], 16)
            return v
        except:
            return None
    else:
        return None

def form_post_uri(user, password):
    post_uri_fmt = "user=%s&password=%s"
    return post_uri_fmt%( urllib.quote_plus(user), urllib.quote_plus(password))

def read_event_information(base_location, factors):
    auth_filename = os.path.join(base_location, AUTH_LOG)
    auth_data = [i.strip().split(',') for i in open(auth_filename).read().splitlines() if len(i.strip()) > 0]
    comm_steps = set()
    jsonified = {'http_values':{}, 'http_ordered_comms':[], }
    logfile_fmt = "%08x,%s,%s,%s"
    cookie_sessionid_fmt = "Cookie: THESESSIONID=%s"
    for i in auth_data:
        
        d = {'end':-1, 'last':long(i[0], 16), 'timestamp':long(i[0], 16), "user":i[1], "password":i[2], 'logentry':'',
             'sessionid':i[3], 'events':[], 'shadow':False, 'requires_update':False,
             'completed':False, 'live':True, 'age':0}
        
        entry = logfile_fmt%(d['timestamp'], d['user'], d['password'], d['sessionid'])
        d['logentry'] = entry
        d['post_uri'] = form_post_uri(i[1], i[2])
        d['cookie_sessionid'] = cookie_sessionid_fmt%i[3]
        event = {'timestamp':d['timestamp'], 'min':0, 'max':0, 'steps':0, 'completed':False, 'user':d['user']}
        d['events'].append(event)
        jsonified['http_values'][d['user']] = d

    stdout_filename = os.path.join(base_location, STDOUT_LOG)
    stdout_data = [i.strip() for i in open(stdout_filename).read().splitlines() if len(i.strip()) > 0 and (i.find("Time:") == 0 or i.find("Starting-Time:") == 0)]
    for line in stdout_data:
        if line.find("Starting") == -1 and len(line) != STDOOUT_LINE_LEN:
            continue
        event = create_event_json(line)
        user = event.pop('user', None)
        completed = event.pop('completed', False)
        starting = event.pop('starting', False)
        if not starting and 'steps' in event:
            step = event['steps']
            key = "%s:%d"%(user, step)
            if not key in comm_steps:
                jsonified['http_ordered_comms'].append({'timestamp':event['timestamp'], 'user':user, 'step':step})
                comm_steps.add(key)
        if user and user in jsonified['http_values'] and jsonified['http_values'][user]['completed']:
            print("%s: %s found event after %s completed."%(time_str(), base_location, user))
            continue
        if starting and user and user in jsonified['http_values']:
            info = jsonified['http_values'][user]
            info['end'] = -1
            info['timestamp'] = event['timestamp']
            info['last'] = event['timestamp']
            info['completed'] = False
            info['age'] = info['last'] - info['timestamp']
        elif user and user in jsonified['http_values']:
            info = jsonified['http_values'][user]
            #if not completed:
            info['events'].append(event)
            info['end'] = event['timestamp'] if completed else -1
            info['last'] = event['timestamp']
            info['completed'] = completed
            info['live'] = not completed
            info['age'] = info['last'] - info['timestamp']
        elif user:
            d = {'end':event['timestamp'], 'last':event['timestamp'], 'timestamp':event['timestamp'], "user":user, "password":None, 'logentry':'',
             'sessionid':i[3], 'events':[event], 'shadow':True, 'requires_update':True, 'completed':completed,
             'cookie_sessionid':'','post_uri':'', 'live':(not completed), 'age':0}
            jsonified['http_values'][user] = d
            d['cookie_sessionid'] = cookie_sessionid_fmt%i[3]
            ut = "%s: [----] %s::%s not found in the authlog, potential race condition line: %s"%(time_str(), base_location, user, line)
            open(os.path.join(base_location, MISSING_USERS), 'a').write(ut+'\n')
            #print (ut)
    return jsonified

def update_auth_user_password_from_post_uri(strings_value, auth_value):
    user = auth_value['user']
    candidates = [i for i in strings_value.values() if i['value'].find(user) > -1]
    for str_dict in candidates:
        v = str_dict['value']
        t = extract_user_password_from_string(v)
        if t:
            auth_value['password'] = t[1]
            auth_value['post_uri'] = form_post_uri(auth_value['user'], auth_value['password'])
            return True
    return False

def extract_user_password_from_string(line):
    s = FIND_POST_ALL_RE.match(line)
    if s:
        data = line[s.start():s.end()].split("Content-Type:")[0]
        user = data.split("user=")[1].split("&password=")[0]
        password = urllib.unquote(data.split("&password=")[1])
        return urllib.unquote(user), password[:20]
    return None

def update_auth_value_from_strings(strings_value, auth_value):
    logfile_fmt = "%08x,%s,%s,%s"
    user = auth_value['user']
    candidates = [i for i in strings_value.values() if i['value'].find(user) > -1]
    for str_dict in candidates:
        v = str_dict['value']
        value = extract_auth_info_from_log(v)
        if value and value[1] == user :
            timestamp = long(value[0], 16)
            if timestamp < auth_value['timestamp'] and auth_value['timestamp'] > 0:
                auth_value['timestamp'] = timestamp
            if timestamp > auth_value['last']:
                auth_value['last'] = auth_value['timestamp']
            if 'completed' in value:
                completed = bool(long(v,16))
                if completed:
                    auth_value['live'] = not completed
                    auth_value['end'] = timestamp
                    auth_value['completed'] = completed
            auth_value['age'] = auth_value['last'] - auth_value['timestamp']
            auth_value['password'] = value[2]
            auth_value['sessionid'] = value[3]
            auth_value['post_uri'] = form_post_uri(value[1], value[2])
            entry = logfile_fmt%(auth_value['timestamp'], auth_value['user'], auth_value['password'], auth_value['sessionid'])
            auth_value['logentry'] = entry
            auth_value['requires_update'] = False
            break
    return not auth_value['requires_update']



def update_shadow_auth_info(strings_data, auth_values):
    update_values = [i for i in auth_values.values() if i['requires_update']]
    if len(update_values) == 0:
        return auth_values
    print("%s: Updating shadow auth information for %d records"%(time_str(), len(update_values)))
    strings_value = strings_data['full_dump']
    for auth_value in update_values:
        r = update_auth_value_from_strings (strings_value, auth_value)
        if not r:
            auth_value['password'] = FAIL_STRING
            auth_value['sessionid'] = FAIL_STRING
            auth_value['logentry'] = FAIL_STRING
            auth_value['post_uri'] = FAIL_STRING

    if len(update_values) == 0:
        return auth_values

    update_values = [i for i in auth_values.values() if i['requires_update']]
    for i in update_values:
        update_auth_user_password_from_post_uri(strings_value, auth_value)
    return auth_values

def parse_strings_line(line):
    items = line.split()
    filename = items[0].strip(':')
    offset = long(items[1].strip() , 16)
    str_len = long(items[2].strip() , 16)
    data = " ".join(line.split(" ")[3:])
    return {'filename':filename, "offset":offset, "str_len":str_len, "value":data}

def read_strings_information(base_location):
    strings_full_filename = os.path.join(base_location, STRING_OUTPUT_FULL)
    strings_full_data = None

    try:
        strings_full_data = [parse_strings_line(i.strip()) for i in open(strings_full_filename).read().splitlines() if len(i.strip()) > 0]
    except:
        log_err("%s: %s had an error with reading %s"%(time_str(), base_location, strings_full_filename))
        print("%s: %s had an error with reading %s"%(time_str(), base_location, strings_full_filename))
        raise
    return {'full_dump':dict([ (i['offset'], i) for i in strings_full_data]),}


def create_logentry_term_set (auth_data):
    return set(i['logentry'] for i in auth_data['http_values'].values())

def create_cookie_sessionid_term_set (auth_data):
    return set(i['cookie_sessionid'] for i in auth_data['http_values'].values())

def create_sessionid_term_set (auth_data):
    return set(i['sessionid'] for i in auth_data['http_values'].values())

def create_password_term_set (auth_data):
    return set(i['password'] for i in auth_data['http_values'].values())

def create_post_uri_term_set (auth_data):
    return set(i['post_uri'] for i in auth_data['http_values'].values())

def build_keyed_db (auth_data):
    keyed_db = {}
    for rec in auth_data.values():
        keyed_db[rec['logentry']] = rec
        keyed_db[rec['password']] = rec
        keyed_db[rec['post_uri']] = rec
    return keyed_db

def get_result_keys(auth_data):
    data = None
    for data in auth_data.values():
        break
    return [i for i in data.keys() if i.find('results') > -1]


def search_data(data_set, term_set):
    results = dict([(i, 0) for i in term_set])
    for rec in data_set.values():
        if 'value' in rec and rec['value'] in term_set:
            results[rec['value']] += 1
    return results

def check_post_uri_and_other(data_set, term_set):
    results = dict([(i, 0) for i in term_set])
    for rec in data_set.values():
        if 'value' in rec and rec['value'] in term_set:
            results[rec['value']] += 1
        elif FIND_POST_ALL_RE.match(rec['value']):
            t = extract_user_password_from_string(rec['value'])
            p = form_post_uri(t[0], t[1])
            if not p in results:
                results[p] = 0
            results[p] += 1
    return results

def search_logfile_data(strings_data, auth_data):
    # log entries
    term_set = create_logentry_term_set(auth_data)
    # find occ in logfile set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_logentry_results'] = full_results

def search_post_uri_data(strings_data, auth_data):
    # post uris
    term_set = create_post_uri_term_set(auth_data)
    full_results = check_post_uri_and_other(strings_data['full_dump'], term_set)#search_data(strings_data['full_dump'], term_set)
    auth_data['full_post_uri_results'] = full_results

def search_password_data(strings_data, auth_data):
    # Password terms
    term_set = create_password_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_password_results'] = full_results


def search_cookie_sessionid_data(strings_data, auth_data):
    # Password terms
    term_set = create_cookie_sessionid_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_cookie_sessionid_results'] = full_results


def search_sessionid_data(strings_data, auth_data):
    # Password terms
    term_set = create_sessionid_term_set(auth_data)
    # find occ in password set
    full_results = search_data(strings_data['full_dump'], term_set)
    auth_data['full_sessionid_results'] = full_results

def perform_data_search(base_location, strings_data, auth_data):
    print("%s: performing password search in %s"%(time_str(), base_location))
    search_password_data(strings_data, auth_data)
    print("%s: performing post uri search in %s"%(time_str(), base_location))
    search_post_uri_data(strings_data, auth_data)
    print("%s: performing logfile search in %s"%(time_str(), base_location))
    search_logfile_data(strings_data, auth_data)
    print("%s: performing sessionid search in %s"%(time_str(), base_location))
    search_sessionid_data(strings_data, auth_data)
    print("%s: performing cookie sessionid search in %s"%(time_str(), base_location))
    search_cookie_sessionid_data(strings_data, auth_data)
    print("%s: [++] Searches complete for %s"%(time_str(), base_location))

def perform_results_write(base_location, auth_data):
    json_file = os.path.join(base_location, JSON_FILE)
    json.dump(auth_data, open(json_file, 'w'))

def match_http_with_ssl_keys(auth_data, times, is_streaming=False):
    host_start = times['start']
    host_end = times['act_end']
    ssl_comms = auth_data['ssl_ordered_comms']
    scale_timestamps_comms(ssl_comms, times)
    ssl_comms = sorted(ssl_comms, key=lambda x: x['timestamp'])
    http_comms = auth_data['http_ordered_comms']
    scale_timestamps_comms(http_comms, times)
    http_comms = sorted(http_comms, key=lambda x: x['timestamp'])
    if is_streaming:
        http_comms = [i for i in http_comms if 'steps' in i and i['steps'] == 1]
    # num ssl keys should always be greater than http
    end = max(len(http_comms), len(ssl_comms))
    s_pos = 0
    h_pos = 0
    h_end = len(http_comms)
    s_end = len(ssl_comms)
    matched_comms = []
    while (h_pos < h_end and s_pos < s_end and s_pos < end and h_pos < end):
    #for pos in xrange(0, end):
        m = {}
        s = ssl_comms[s_pos]
        h = http_comms[h_pos]
        age = -1
        live = is_streaming and h['timestamp'] != -1
        # these were normalized to 0 so they should be close to correct
        # http is always reported after the communication takes place
        # assuming the ssl key is created before the session ends
        m['plausible_match'] = False
        if h['timestamp'] >= s['timestamp']:
            age = abs(h['timestamp'] - s['timestamp'])
            if age == 0:
                age = 1
            m['plausible_match'] = True
            h_pos += 1
            s_pos += 1
        elif h['timestamp'] < s['timestamp']:
            h_pos += 1
            continue

        #  update with http last, b/c that is the timestamp we care about
        # both http and ssl should have the same timestamp
        m.update(s)
        m.update(h)
        m['ssl_timestamp'] = s['timestamp']
        m['http_timestamp'] = h['timestamp']
        m['unscaled_ssl_timestamp'] = s['unscaled_timestamp']
        m['unscaled_http_timestamp'] = h['unscaled_timestamp']
        m['timestamp'] = s['timestamp']
        m['end'] = h['timestamp'] - host_start
        m['start'] = s['timestamp'] - host_start
        m['dur_exp'] = host_end - h['timestamp']
        # update these after finalizing the https data
        m['live'] = live
        m['age'] = age

        matched_comms.append(m)
    return matched_comms



def perform_agg_tasks(base_location, is_streaming=None):
    is_streaming = base_location.find("streaming") > -1 if is_streaming is None else is_streaming
    log_out("%s: Reading strings and event information for %s"%(time_str(), base_location))
    factors = get_factors(base_location, trim=True)
    times = get_times(base_location)
    auth_data = read_event_information(base_location, factors)
    auth_data['ssl_values'] = merge_key_hits_key_info(base_location)
    auth_data['ssl_ordered_comms'] = build_ordered_ssl_comms(auth_data['ssl_values'])
    auth_data.update(times)
    

    strings_data = read_strings_information(base_location)
    auth_data['http_values'] = update_shadow_auth_info(strings_data, auth_data['http_values'])
    scale_timestamps_ssl_values(auth_data['ssl_values'].values(), times)
    scale_timestamps_http_values(auth_data['http_values'].values(), times)
    auth_data['matched_comms'] = match_http_with_ssl_keys(auth_data, times, is_streaming=is_streaming )
    # scale all the timestamp http_values

    # make sure the external end clock time is the same, and if not update
    #update_end_time(auth_data, times)
    infos = extract_basic_infos(auth_data, factors)
    for user, info in infos.items():
        auth_data['http_values'][user]['age'] = info['age']
        auth_data['http_values'][user]['live'] = info['live']

    #update the ssl ages here
    # comm timestamp is considered the end of a communication
    host_start = times['start']
    host_end = times['act_end']
    
    for comm in auth_data['matched_comms']:
        user = comm['user']
        ms = comm['ms']
        age = comm['age']
        dur_exp = -1
        v = auth_data['ssl_values'][ms]
        if is_streaming:
            live = not auth_data['http_values'][user]['completed']
            age = auth_data['http_values'][user]['age']
        else:
            live = False

        if not live and (comm['ms_hit'] or comm['pms_hit']):
            dur_exp = auth_data['http_values'][user]['end'] - host_start
            dur_exp = 0 if dur_exp < 0 else dur_exp

        v['live'] = live
        v['matched'] = True
        v['dur_exp'] = comm['dur_exp']
        v['unscaled_timestamp'] = v['timestamp']
        v['timestamp'] = comm['ssl_timestamp']
        v['age'] = age
        v['start'] = comm['ssl_timestamp'] - host_start
        v['end'] = v['start'] + age if not live else -1
        v['end_timestamp'] = comm['http_timestamp'] if not live else -1
        

        comm['dur_exp'] = host_end - comm['end'] if not live else 0
        comm['live'] = live
        comm['end'] = comm['start'] + age if not live else -1
        comm['end_timestamp'] = comm['http_timestamp'] if not live else -1

    matched_comm_ssl = {}
    for comm in auth_data['matched_comms']:
        matched_comm_ssl[comm['ms']] = comm

    for comm in auth_data['ssl_ordered_comms']:
        ms = comm['ms']
        v = auth_data['ssl_values'][ms]
        age = comm['age']
        comm['end'] = matched_comm_ssl[ms]['end'] if ms in matched_comm_ssl else -1
        comm['end_timestamp'] = matched_comm_ssl[ms]['end_timestamp'] if ms in matched_comm_ssl else -1

        if 'matched' in v and v['matched']:
            continue
        v['age'] = 0
        v['live'] = True
        v['unscaled_timestamp'] = auth_data['ssl_values'][ms]['timestamp']
        v['timestamp'] = comm['timestamp']
        v['start'] = comm['timestamp'] - host_start
        v['end'] = -1
        v['end_timestamp'] = -1
        v['dur_exp'] = -1
        

    auth_data.update(times)
    log_out("%s: Performing data searches for %s"%(time_str(), base_location))
    perform_data_search(base_location, strings_data, auth_data)
    log_out("%s: Caclulating the probabilities %s"%(time_str(), base_location))
    # probability that a session token could be recovered
    # probability that a password could be recovered (alone)
    # probability that a post uri could be recovered
    auth_data['live_quant'] = {"est_comms":len(auth_data['matched_comms']),
                               "num_ssl_sessions":len(auth_data['ssl_values']),
                               "num":len(auth_data['http_values'].values()),
                               "num_live":sum([1 for i in auth_data['http_values'].values() if i['live']]) }
    infos, results = calc_password_info(auth_data, factors)
    auth_data['password_infos'] = infos
    auth_data['password_quant'] = results
    infos, results = calc_sessionid_info(auth_data, factors)
    auth_data['sessionid_infos'] = infos
    auth_data['sessionid_quant'] = results
    infos, results = calc_cookie_sessionid_info(auth_data, factors)
    auth_data['cookie_sessionid_infos'] = infos
    auth_data['cookie_sessionid_quant'] = results
    infos, results = calc_post_uri_info(auth_data, factors)
    auth_data['post_uri_infos'] = infos
    auth_data['post_uri_quant'] = results
    infos, results = calc_logentry_info(auth_data, factors)
    auth_data['logentry_infos'] = infos
    auth_data['logentry_quant'] = results
    memory_consumption = calculate_memory_consumption(auth_data, factors)
    auth_data['memory_consumption'] = memory_consumption
    infos, results = calc_ssl_info_quant(auth_data, factors)
    auth_data['ssl_infos'] = infos
    auth_data['ssl_quant'] = results
    infos, results = calc_ssl_info_quant(auth_data, factors, use_unmatched=True)
    auth_data['ssl_infos_unmatched'] = infos
    auth_data['ssl_quant_unmatched'] = results

    log_out("%s: writing results to json file %s"%(time_str(), base_location))
    perform_results_write(base_location, auth_data)
    log_out("%s: job complete %s"%(time_str(), base_location))
    return auth_data

KEY_HITS = 'found_ssl_keys_counts.txt'
MERGED_KEY_INFO = 'merged_keyinfo.txt'

KEY_VALUES = set(['ms', 'pms', 'swkey', 'cwkey', 'crandom', 'srandom'])

def get_found_key_counts_dict(base_location):
    fname = os.path.join(base_location, KEY_HITS)
    data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]
    results = {}
    for i in data:
        key, hit = i.split()
        results[key] = long(hit, 16)
    return results

def parse_merged_info_line(line, key_counts={}):
    results = {}
    if not line or line.strip() == "":
        return results

    p = line.strip()
    results['tot_hits'] = 0
    elements = p.split("-")
    #print elements
    for e in elements:
        s = e.split(":")
        k, v, =  s[0], s[1] if len(s) == 2 else (None, None,)
        if k is None or k == '':
            continue
        if k == 'time':
            v = long(v, 16)
            k = 'timestamp'
        elif k in KEY_VALUES:
            hits = key_counts.get(v, 0)
            results['tot_hits'] += hits
            results['%s_hits'%k] = hits
        results[k] = v
    return results

def normalize_comm_timestamps(comm_info, times):
    scale_timestamps(comm_info, times)
    min_ts = min([i['timestamp'] for i in comm_info])
    delta = get_host_guest_time_delta(times['start'], min_ts)
    #for i in comm_info:
    #    i['timestamp'] = get_host_guest_time(i['timestamp'], delta)

# def normalize_http_data_timestamps(http_data, act_start):
#     min_ts = min([i['timestamp'] for i in http_data.values()])
#     delta = get_host_guest_time_delta(act_start, min_ts)
#     for i in http_data.values():
#         i['timestamp'] = get_host_guest_time(i['timestamp'], delta)
#         i['end'] = get_host_guest_time(i['end'], delta)
#         for e in i['events']:
#             if 'timestamp' in e:
#                 e['timestamp'] = get_host_guest_time(e['timestamp'], delta)

def merge_key_hits_key_info(base_location, act_start=0):
    key_counts = get_found_key_counts_dict(base_location)
    merged_info = get_merged_key_info(base_location, key_counts)
    return merged_info

def build_ordered_ssl_comms(ssl_data):
    ordered_use = []
    for ms, v in ssl_data.items():
        pms_hit = 1 if v['pms_hits'] > 0 else 0
        ms_hit = 1 if v['ms_hits'] > 0 else 0
        ordered_use.append({'timestamp':v['timestamp'], 'ms':ms, 'ms_hit':ms_hit, 'pms_hit':pms_hit, 
            'end':-1, 'live':True, 'age':-1, 'dur_exp':-1, 'start':-1})
    return sorted(ordered_use, key=lambda k:k['timestamp'])

def get_merged_key_info(base_location, key_counts={}):
    fname = os.path.join(base_location, MERGED_KEY_INFO)
    data = [i.strip() for i in open(fname).readlines() if len(i.strip()) > 0]
    results = {}

    for line in data:
        pline = parse_merged_info_line(line, key_counts)
        if 'ms' in pline:
            pline['age'] = 0
            pline['live'] = True
            pline['matched'] = False
            pline['dur_exp'] = 0
            pline['end'] = -1
            pline['start'] = 0
            results[pline['ms']] = pline
    return results

def calc_password_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='password')

def calc_sessionid_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='sessionid')

def calc_cookie_sessionid_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='cookie_sessionid')

def calc_post_uri_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='post_uri')

def calc_logentry_info(agg_auth_data, factors):
    return calculate_prob_info(agg_auth_data, factors, sensitive_key='logentry')

def calculate_memory_consumption(auth_data, _):
    memory_consumption= {}
    for session in auth_data['http_values'].values():
        # so its possible to have multiple events in a single quantum
        # so we use the last event in the list at the time slot.
        # filtering loop
        events = {}
        for event in session['events']:
            t = event.get('timestamp', 0)
            if t == 0 or not 'mem' in event:
                continue
            mem = event['mem']
            max = event['max'] if 'max' in event else mem
            maxed = mem == max and event['maxed'] if 'maxed' in event else False
            events[t] = (mem, max, maxed)

        for t, vals in events.items():
            #print vals
            #print mem
            mem, max, maxed = vals
            if not t in memory_consumption:
                memory_consumption[t] = {"tot_mem":0, "maxed_sessions":dict()}
            memory_consumption[t]['tot_mem'] += mem
            if maxed:
                memory_consumption[t]['maxed_sessions'][session['user']] = 1
    return memory_consumption

def find_key_by_10(val, ssl_data):
    results = []
    for k in ssl_data.keys():
        if k[:10] == val:
            results.append(k)
    return results

def get_ssl_data(base_location):
    json_file = os.path.join(base_location, SSL_JSON_FILE)
    ssl_data = json.load(open(json_file))
    # update times since there was a bug in the capture
    for k in ssl_data:
        if 'timestamp' in ssl_data[k]:
            ssl_data[k]['timestamp'] &= MAX_INT
    return ssl_data

def print_summary_pms(full_dump_results):
    cnt = 0
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        pms = results.get('pre_master_secret_results_cnts', 0)
        if pms > 0:
            cnt += 1

    print "Showing %d locations with pms keys:"%cnt
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        cw = results.get('client_wkey_results_cnts', 0)
        sw = results.get('server_wkey_results_cnts', 0)
        ms = results.get('master_secret_results_cnts', 0)
        pms = results.get('pre_master_secret_results_cnts', 0)
        if pms > 0:
            cnt += 1
            print "Master key[:10] = %s client_wkey_cnts = %d server_wkey_cnts = %d ms_cnts = %d pms_cnts = %d "%(master_key[:10], cw, sw, ms, pms)

def print_summary_ms(full_dump_results):
    cnt = 0
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        ms = results.get('master_secret_results_cnts', 0)
        if ms > 0:
            cnt += 1

    print "Showing %d locations with ms keys:"%cnt
    for master_key in full_dump_results.keys():
        results = full_dump_results[master_key]
        cw = results.get('client_wkey_results_cnts', 0)
        sw = results.get('server_wkey_results_cnts', 0)
        ms = results.get('master_secret_results_cnts', 0)
        pms = results.get('pre_master_secret_results_cnts', 0)
        if ms > 0:
            cnt += 1
            print "Master key[:10] = %s client_wkey_cnts = %d server_wkey_cnts = %d ms_cnts = %d pms_cnts = %d "%(master_key[:10], cw, sw, ms, pms)


def get_factors(filepath, trim=False):
    res = {}
    convert_string = os.path.split(filepath.strip("/"))
    if len(convert_string) < 2:
        return None
    factors = convert_string[-1].split("_")
    if (trim and factors[-1].isdigit()):
        factors = factors[-5:-1]
    if len(factors) == 4:
        is_random = lambda x:factors[x] == "RANDOM"
        res['mem_pressure_factor'] = FACTOR_CONV[factors[0]]
        res['lifetime_factor'] = FACTOR_CONV[factors[1]]
        res['requests_factor'] = FACTOR_CONV[factors[2]]
        res['concurrent_sessions_factor'] = FACTOR_CONV[factors[3]]
        res['mem_pressure'] = long(FACTOR_CONV[factors[0]] * (512*1024*1024*8)*.8)
        res['age'] = long(FACTOR_CONV[factors[1]] *MAX_SESSION_LIFETIME) if not is_random(1) else MAX_SESSION_LIFETIME*FACTOR_CONV['HIGH']
        res['requests'] = long(FACTOR_CONV[factors[2]] *MAX_ALLOWED_REQ)
        res['concurrent_sessions'] = long(FACTOR_CONV[factors[3]] *MAX_CONCURRENT_SESSIONS)
        res['exp_cut_off'] = CUTOFF_TIME*1000
        return res
    return None

def get_auth_data_filename(base_location):
    return os.path.join(base_location, AUTH_JSON_OUTFILE)

def get_ssl_data_filename(base_location):
    return os.path.join(base_location, SSL_JSON_OUTFILE)

def get_auth_data(base_location):
    return json.load(open(get_auth_data_filename(base_location)))

def get_ssl_data(base_location):
    return json.load(open(get_ssl_data_filename(base_location)))


# 1) Keys Recovered
# 2) Keys recovered after lifetime ended
# 3) Post Uri Copies present per key
# 4) SessionIds available after lifetime ended
# 5) Sessions that could be recovered after lifetime ended

def find_age_range(agg_auth_data):
    ages = []
    for v in agg_auth_data['http_values'].values():
        start = v['timestamp']
        end = v['end']
        ages.append(end-start)
    return min(ages), max(ages)

def get_final_deadline(agg_auth_data):
    end = -1
    for v in agg_auth_data['http_values'].values():
        end = max(end, v['end'])
    return end

def calc_age(agg_auth_value):
    start = agg_auth_value['timestamp']
    end = agg_auth_value['end']
    if end == -1:
        return start-min_start
    return end-start


def get_ages(agg_auth_data):
    ages = []
    min_start = get_act_start_time(agg_auth_data)
    for v in agg_auth_data['http_values'].values():
        ages.append(calc_age(v))
    ages.sort()
    return ages

def find_max_ages(agg_auth_data,n=1):
    ages = get_ages(agg_auth_data)
    return ages[-n:]

def get_act_start_time(agg_auth_data):
    #XXX - this needs to be fixed
    try:
        return min([i['timestamp'] for i in agg_auth_data['http_values'].values()])
    except:
        return 0

def get_act_end_time(agg_auth_data):
    #XXX - this needs to be fixed
    try:
        return max([i['end'] for i in agg_auth_data['http_values'].values()])
    except:
        return 0

def update_end_time(auth_data, times):
    max_ts = get_act_end_time(auth_data)
    host_start = times['start']
    delta = get_host_guest_time_delta(host_start, max_ts)
    if times['start'] + delta > times['act_end']:
        times['act_end'] = delta+host_start


def find_min_ages(agg_auth_data,n=1):
    ages = get_ages(agg_auth_data)
    return ages[:n]

def calculate_max_age(agg_auth_data, n=10):
    return sum(find_max_ages(agg_auth_data,n))/n

def quantify(infos):
    results = {}
    results['total_found_live'] = 0
    results['num_found_live'] = 0
    results['total_found_dead_value'] = 0
    results['num_found_dead_value'] = 0
    results['num_live'] = 0
    results['num_dead'] = 0
    results['tot'] = len(infos)
    exp_ages = []
    liv_ages = []
    dur_expired = []
    results['min_age_found_expired'] = 0
    results['max_age_found_expired'] = 0
    results['mean_age_found_expired'] = 0
    results['std_age_found_expired'] = 0
    results['var_age_found_expired'] = 0

    results['min_age_found_live'] = 0
    results['max_age_found_live'] = 0
    results['mean_age_found_live'] = 0
    results['std_age_found_live'] = 0
    results['var_age_found_live'] = 0

    results['min_age_dur_exp'] = 0
    results['max_age_dur_exp'] = 0
    results['mean_age_dur_exp'] = 0
    results['std_age_dur_exp'] = 0
    results['var_age_dur_exp'] = 0

    for info in infos.values():
        if not info['live']:
            results['num_dead'] += 1
        else:
            results['num_live'] += 1
        if info['full_occ'] > 0 and not info['live']:
            exp_ages.append(info['age'])
            results['num_found_dead_value'] +=1
            results['total_found_dead_value'] += info['full_occ']
        elif info['full_occ'] > 0 and info['live']:
            liv_ages.append(info['age'])
            dur_expired.append(info['dur_exp'])
            results['num_found_live'] +=1
            results['total_found_live'] += info['full_occ']
    results['prop_found_live'] = float(results['total_found_live']) / results['tot']

    if len(exp_ages) > 0:
        results['min_age_found_expired'] = min(exp_ages)
        results['max_age_found_expired'] = max(exp_ages)
        results['mean_age_found_expired'] = np.mean(exp_ages)
        results['std_age_found_expired'] = np.std(exp_ages)
        results['var_age_found_expired'] = np.var(exp_ages)
    if len(liv_ages) > 0:
        results['min_age_found_live'] = min(liv_ages)
        results['max_age_found_live'] = max(liv_ages)
        results['mean_age_found_live'] = np.mean(liv_ages)
        results['std_age_found_live'] = np.std(liv_ages)
        results['var_age_found_live'] = np.var(liv_ages)
    if len(dur_expired) > 0:
        results['min_age_dur_exp'] = min(liv_ages)
        results['max_age_dur_exp'] = max(liv_ages)
        results['mean_age_dur_exp'] = np.mean(liv_ages)
        results['std_age_dur_exp'] = np.std(liv_ages)
        results['var_age_dur_exp'] = np.var(liv_ages)

    return results

def extract_basic_infos(agg_auth_data, factors):
    # 1 get all the password keys
    host_start = agg_auth_data['start']
    host_end = agg_auth_data['end']
    min_ts = get_act_start_time(agg_auth_data)
    delta = get_host_guest_time_delta(host_start, min_ts)

    max_age = factors['age']

    infos = {}
    for user,value in agg_auth_data['http_values'].items():
        info = {}
        start = get_host_guest_time(value['timestamp'], delta)
        end = value['end']
        if end != -1:
            end = get_host_guest_time(value['end'], delta)
        info['live'] = not value['completed']
        info['dur_exp'] = 0 if info['live'] else host_end - end
        info['user'] = user
        info['start'] = start
        info['end'] = end
        info['age'] = end - start if value['completed'] else host_end - start
        infos[user] = info
    return infos


def calculate_prob_info(agg_auth_data, factors, sensitive_key):
    # 1 get all the password keys
    host_start = agg_auth_data['start']
    host_end = agg_auth_data['end']
    min_ts = get_act_start_time(agg_auth_data)
    delta = get_host_guest_time_delta(host_start, min_ts)

    max_lifetime = factors['age']


    infos = {}
    full_results_key = 'full_%s_results'%sensitive_key

    if sensitive_key == "logfile":
        sensitive_key = "logentry"
    if sensitive_key ==  "sessionid":
        sensitive_key = 'sessionid'
    password_results = agg_auth_data['full_password_results']
    for user,value in agg_auth_data['http_values'].items():
        info = {}
        start = get_host_guest_time(value['timestamp'], delta)
        end = value['end']
        if end != -1:
            end = get_host_guest_time(value['end'], delta)
        info['live'] = not value['completed']
        info['dur_exp'] = 0 if info['live'] else host_end - end
        info['user'] = user
        info['start'] = start
        info['end'] = end
        info['age'] = end - start if value['completed'] else host_end - start
        sensitive = info['sensitive'] = value[sensitive_key]
        info['found_in_full_mem'] = sensitive in agg_auth_data[full_results_key] and\
                                              agg_auth_data[full_results_key][sensitive] > 0
        info['full_occ'] = 0 if not info['found_in_full_mem'] else agg_auth_data[full_results_key][sensitive]
        infos[user] = info
    return infos, quantify(infos)

def calc_ssl_info_quant(agg_auth_data, _, use_unmatched=False):
    ssl_values = agg_auth_data['ssl_values']
    infos = {}
    quant = {}
    times = [i['timestamp'] for i in ssl_values.values()]
    times.sort()
    if len(ssl_values.values()) == 0:
        return infos, quant

    quant['start'] = ssl_values.values()[0]['timestamp']
    quant['last'] =  ssl_values.values()[-1]['timestamp']
    quant['unique_swkey_hit'] = 0
    quant['unique_cwkey_hit'] = 0
    
    quant['unique_live_pms_hit'] = 0
    quant['unique_live_ms_hit'] = 0
    quant['unique_exp_pms_hit'] = 0
    quant['unique_exp_ms_hit'] = 0
    
    quant['unique_pms_hit'] = 0
    quant['unique_ms_hit'] = 0
    quant['unique_tot_hit'] = 0

    quant['swkey_hits'] = 0
    quant['cwkey_hits'] = 0
    quant['pms_hits'] = 0
    quant['ms_hits'] = 0
    
    quant['live_pms_hits'] = 0
    quant['live_ms_hits'] = 0

    quant['live_pms_hit'] = 0
    quant['live_ms_hit'] = 0

    quant['exp_pms_hits'] = 0
    quant['exp_ms_hits'] = 0

    quant['exp_pms_hit'] = 0
    quant['exp_ms_hit'] = 0

    quant['tot_hits'] = 0
    quant['tot_ssl'] = 0
    ms_ages = []
    pms_ages = []

    ms_dur_exp = []
    pms_dur_exp = []


    for value in ssl_values.values():
        if not value['matched'] and not use_unmatched:
            continue
        info = {}
        age = value['age']
        live = value['live']
        info['live'] = live
        info['age'] = age
        quant['start'] = min( value['timestamp'], quant['start'])
        quant['last'] =  max( value['timestamp'], quant['last'])

        info['timestamp'] = value['timestamp']
        info['swkey_hit'] = 1 if value['swkey_hits'] > 0 else 0
        info['cwkey_hit'] = 1 if value['cwkey_hits'] > 0 else 0
        info['pms_hit'] = 1 if value['pms_hits'] > 0 else 0
        info['ms_hit'] = 1 if value['ms_hits'] > 0 else 0
        info['pms_hit_age'] = value['age'] if value['pms_hits'] > 0 else 0
        info['ms_hit_age'] = value['age'] if value['ms_hits'] > 0 else 0

        if not live and value['pms_hits'] > 0:
            pms_ages.append(value['age'])
            pms_dur_exp.append(value['dur_exp'])

        if not live and value['ms_hits'] > 0:
            ms_ages.append(value['age'])
            ms_dur_exp.append(value['dur_exp'])


        if live:
            info['live_pms_hit'] = 1 if value['pms_hits'] > 0 else 0
            info['live_ms_hit'] = 1 if value['ms_hits'] > 0 else 0
            quant['live_pms_hit'] += info['live_pms_hit']
            quant['live_ms_hit'] += info['live_ms_hit']
        else:
            info['exp_pms_hit'] = 1 if value['pms_hits'] > 0 else 0
            info['exp_ms_hit'] = 1 if value['ms_hits'] > 0 else 0
            quant['exp_pms_hit'] += info['exp_pms_hit']
            quant['exp_ms_hit'] += info['exp_ms_hit']


        info['swkey_hits'] = value['swkey_hits']
        info['cwkey_hits'] = value['cwkey_hits']
        info['pms_hits'] = value['pms_hits']
        info['ms_hits'] = value['ms_hits']
        if live:
            info['live_pms_hits'] = value['pms_hits']
            info['live_ms_hits'] = value['ms_hits']
            quant['live_pms_hits'] += value['pms_hits']
            quant['live_ms_hits'] += value['ms_hits']
        else:
            info['exp_pms_hits'] = value['pms_hits']
            info['exp_ms_hits'] = value['ms_hits']
            quant['exp_pms_hits'] += value['pms_hits']
            quant['exp_ms_hits'] += value['ms_hits']

        info['tot_hits'] = value['tot_hits']
        info['tot_hit'] = sum([info['swkey_hit'], info['cwkey_hit'],
                               info['pms_hit'], info['ms_hit'],])
        infos[value['ms']] = info

        quant['unique_tot_hit'] += info['tot_hit']
        quant['unique_swkey_hit'] += info['swkey_hit']
        quant['unique_cwkey_hit'] += info['cwkey_hit']
        quant['unique_pms_hit'] += info['pms_hit']
        quant['unique_ms_hit'] +=  info['ms_hit']
        if live:
            quant['unique_live_ms_hit'] +=  info['ms_hit']
            quant['unique_live_pms_hit'] += info['pms_hit']
        else:
            quant['unique_exp_ms_hit'] +=  info['ms_hit']
            quant['unique_exp_pms_hit'] += info['pms_hit']

        quant['swkey_hits'] += value['swkey_hits']
        quant['cwkey_hits'] += value['cwkey_hits']
        quant['pms_hits'] += value['pms_hits']
        quant['ms_hits'] += value['ms_hits']
        quant['tot_hits'] += value['tot_hits']
        quant['tot_ssl'] += 1

    tot_ssl = quant['tot_ssl']
    quant['prop_unique_ms_key'] = float(quant['unique_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_unique_pms_key'] = float(quant['unique_pms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_live_unique_ms_key'] = float(quant['unique_live_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_live_unique_pms_key'] = float(quant['unique_live_pms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_exp_unique_ms_key'] = float(quant['unique_exp_ms_hit']) / tot_ssl if tot_ssl > 0 else 0
    quant['prop_exp_unique_pms_key'] = float(quant['unique_exp_pms_hit']) / tot_ssl if tot_ssl > 0 else 0

    tot_hits = quant['tot_hits']
    quant['prop_ms_hits'] = float(quant['ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_pms_hits'] = float(quant['pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_live_ms_hits'] = float(quant['live_ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_live_pms_hits'] = float(quant['live_pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_exp_ms_hits'] = float(quant['exp_ms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0
    quant['prop_exp_pms_hits'] = float(quant['exp_pms_hits']) / quant['tot_hits'] if tot_hits > 0 else 0


    quant['min_pms_age'] = -1 if len(pms_ages) == 0 else min(pms_ages)
    quant['min_ms_age'] = -1 if len(ms_ages) == 0 else min(ms_ages)
    quant['min_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else min(pms_dur_exp)
    quant['min_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else min(ms_dur_exp)

    quant['max_pms_age'] = -1 if len(pms_ages) == 0 else max(pms_ages)
    quant['max_ms_age'] = -1 if len(ms_ages) == 0 else max(ms_ages)
    quant['max_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else max(pms_dur_exp)
    quant['max_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else max(ms_dur_exp)

    quant['mean_pms_age'] = -1 if len(pms_ages) == 0 else np.mean(pms_ages)
    quant['mean_ms_age'] = -1 if len(ms_ages) == 0 else np.mean(ms_ages)
    quant['mean_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.mean(pms_dur_exp)
    quant['mean_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.mean(ms_dur_exp)

    quant['std_pms_age'] = -1 if len(pms_ages) == 0 else np.std(pms_ages)
    quant['std_ms_age'] = -1 if len(ms_ages) == 0 else np.std(ms_ages)
    quant['std_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.std(pms_dur_exp)
    quant['std_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.std(ms_dur_exp)

    quant['var_pms_age'] = -1 if len(pms_ages) == 0 else np.var(pms_ages)
    quant['var_ms_age'] = -1 if len(ms_ages) == 0 else np.var(ms_ages)
    quant['var_pms_dur_exp'] = -1 if len(pms_dur_exp) == 0 else np.var(pms_dur_exp)
    quant['var_ms_dur_exp'] = -1 if len(ms_dur_exp) == 0 else np.var(ms_dur_exp)

    return infos, quant

def init_task (base_location):
    proc = Process (target=perform_agg_tasks, args=(base_location,))
    proc.start()
    return proc

def cull_threads (active_threads):
    cnt = 0
    # lock acquire
    orig_len = len(active_threads)
    while cnt < len(active_threads):
        if active_threads[cnt][0].is_alive():
            cnt += 1
            continue
        active_threads.pop(cnt)
    return len(active_threads) != orig_len

# BASE_DIR = "/research_data/exp_results/results_1/"

# USING_MOD = False

# STREAMING = "streaming"
# TRANSACTION = "transaction"

# STREAMING_MOD_VM = "streaming_mod_vm"
# TRANSACTION_MOD_VM = "transaction_mod_vm"

# RESULTS_DIR_STREAMING = [os.path.join(BASE_DIR, STREAMING, i) for i in os.listdir(os.path.join(BASE_DIR, STREAMING))]
# RESULTS_DIR_TRANSACTION = [os.path.join(BASE_DIR, TRANSACTION, i) for i in os.listdir(os.path.join(BASE_DIR, TRANSACTION))]

# RESULTS_DIR_STREAMING.sort()
# RESULTS_DIR_TRANSACTION.sort()

# base_location = RESULTS_DIR_TRANSACTION[0]



# BASE_DIR = "/srv/nfs/cortana/logs/"
# RESULTS1 = "streaming"
# RESULTS2 = "streaming_mod_vm"
# RESULTS3 = "transaction"
# RESULTS4 = "transaction_mod_vm"
# BASE_RESULT_DIR = os.path.join(BASE_DIR, RESULTS1)
# BASE_RESULT2_DIR = os.path.join(BASE_DIR, RESULTS2)
# BASE_RESULT3_DIR = os.path.join(BASE_DIR, RESULTS3)
# BASE_RESULT4_DIR = os.path.join(BASE_DIR, RESULTS4)

if __name__ == "__main__":

    target_dir = ""
    start_str = time_str()
    if len(sys.argv) < 2:
        print ("%s base_dir_for_processing ]"%sys.argv[0])
        sys.exit(-1)

    BASE_DIR = sys.argv[1]
    results_dirs = [os.path.join(BASE_DIR, i) for i in os.listdir(BASE_DIR) if os.path.isdir(os.path.join(BASE_DIR, i))]
    start_str = time_str()

    active_threads = []
    procs = []
    remaining = len(results_dirs)
    results_dirs.sort()
    for results_dir in results_dirs:
        while len (active_threads) > PROCESSES:
            if not cull_threads(active_threads):
                time.sleep(5)
        proc = init_task (results_dir)
        procs.append(proc)
        active_threads.append((proc, (results_dir,)))
        remaining += -1
        print ("%s: aggregating %s, %d remaining"%(time_str(), results_dir, remaining))

    while len (active_threads) > 0:
        if not cull_threads(active_threads):
            time.sleep(.1)

    time.sleep(5)
    for r in procs:
        r.join()


    end_str = time_str()

    log_out ("%s: Started"%start_str)
    log_out ("%s: Ended"%end_str)
