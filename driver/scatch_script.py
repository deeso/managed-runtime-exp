dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
import sys, re, libvirt, paramiko, subprocess, time, os, threading, select, errno
sys.path.append(dest)
import matplotlib.pyplot as plt
import agg_exp_data as aed

app_datas = {}
summary_infos = {}
table4s = {}


from agg_exp_data import *

for base_location in dirs:
    app_datas[base_location] = read_compromised_application_data([base_location, ], False)
    summary_infos[base_location] = get_app_data_summarys(app_datas[base_location])
    table4s[base_location] = build_table2(summary_infos[base_location])

for d in table4s:
    print d
    print table4s[d]



heap_infos2 = {}
table3s = {}
for base_location in dirs:
    heap_infos2[base_location] = aed.read_heap_summary_data(base_location)
    table3s[base_location] = aed.build_table3(heap_infos[base_location])

# get the thin client model from the heapdata information


heap_data = heap_infos['/srv/nfs/cortana/logs/unmod_jvm_bc_destroy_data_mod_results_batch_x64_exp_gen_2560/']


title = "Thin Client Model #2 % Recovered Keys vs. Observed Keys in Experiments"
xlabel = "Number of TLS Keys Observed"
ylabel = "Number of Recovered Keys"
plt.title(title, fontsize=fontsize)
plt.xlabel(xlabel, fontsize=fontsize)
plt.ylabel(ylabel, fontsize=fontsize)
plt.ylim((0.0,1.0))
plt.xlim((0,max(x)+int(max(x)*.1) ))
x = [i[-1] for i in data]
y = [i[1] for i in data]
colors = [i[0] for i in data]
sc = plt.scatter(x,y,c=colors, s=35,)
cbar = plt.colorbar(sc)
cbar.ax.get_yaxis().labelpad = 15
cbar.ax.set_ylabel("Experiment Run #", rotation=270, fontsize=fontsize)

plt.show()

target_outdir = '/srv/nfs/cortana/logs/output/scatter_plots/'
import matplotlib.pyplot as plt
def get_scatter_data_not_unique(model_abbrs, heap_data):
    data = []
    for model_abbr in model_abbrs:
        rvalues = [i['run_values'] for i in heap_data.values() if i['model_abbr'].lower() == model_abbr.lower()]
        for rvalue in rvalues:
            for run, _data in rvalue.items():
                items_list = {}
                items_list['model'] = model_abbr
                items_list['run'] = run
                items_list['tot_keys_found'] = _data['key_mkeyblock_hits']
                items_list['tot_keys_found'] += _data['key_pms_hits']
                items_list['unique_keys'] = _data['tot_unique']
                items_list['tot_ssl'] = _data['tot_ssl']
                data.append(items_list)
    return data

def get_scatter_data_unique(model_abbrs, heap_data):
    data = []
    for model_abbr in model_abbrs:
        rvalues = [i['run_values'] for i in heap_data.values() if i['model_abbr'].lower() == model_abbr.lower()]
        for rvalue in rvalues:
            for run, _data in rvalue.items():
                items_list = {}
                items_list['model'] = model_abbr
                items_list['run'] = run
                items_list['tot_keys_found'] = _data['key_pms_unique_hits']
                items_list['tot_keys_found'] += _data['key_mkeyblock_unique_hits']
                items_list['unique_keys'] = _data['key_tot_unique']
                items_list['tot_ssl'] = _data['tot_ssl']
                data.append(items_list)
    return data

def get_scatter_data_unique_live_objects(model_abbrs, heap_data):
    data = []
    for model_abbr in model_abbrs:
        rvalues = [i['run_values'] for i in heap_data.values() if i['model_abbr'].lower() == model_abbr.lower()]
        for rvalue in rvalues:
            for run, _data in rvalue.items():
                if not 'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey' in _data:
                    return None
                master_keys = 'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey'
                items_list = {}
                items_list['model'] = model_abbr
                items_list['run'] = run
                items_list['tot_keys_found'] = _data[master_keys]['live']
                items_list['tot_keys_found'] += _data[master_keys]['dead']
                items_list['live'] = _data[master_keys]['live']
                items_list['dead'] = _data[master_keys]['dead']
                items_list['unique_keys'] = _data['key_tot_unique']
                data.append(items_list)
    return data

MAX_ALIVE_SESSIONS = [(.8*240, ':', 'y', 1), .(1.6*240, '-', 'r', 2 )]
def make_scatter_plot(x, y, colors, title, xlablel, ylabel, cbar_label, cbar_labels=None, ylim=(0.0,1.0), grid={'b':False, 'which':'major', 'axis':'both'}, fontsize=12, output_filename=None):
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    #plt.ylim(ylim)
    max_x = max(x)+500
    plt.xlim(0, max_x)


    sc = plt.scatter(x,y,c=colors, s=35,)
    cbar = None
    if cbar_labels is None:
        cbar = plt.colorbar(sc)
    else:
        bounds = len([i for i in enumerate(cbar_labels)])
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cbar = plt.colorbar(sc, boundaries=bounds, ticks=bounds, norm=norm)
        cbar.ax.set_yticklabels(cbar_labels)

    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(cbar_label, rotation=270, fontsize=fontsize)

    #plot the lines for different factors
    for y_factor, linestyle, color, linewidth in MAX_ALIVE_SESSIONS:
        plt.hlines(y_factor, 0, max_x, color=color, linewidth=linewidth, linestyle=linestyle)

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)

def make_scatter_shape_plot(shape_data, title, xlablel, ylabel, cbar_labels=None, ylim=(0.0,1.0), grid={'b':False, 'which':'major', 'axis':'both'}, fontsize=12, output_filename=None):
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    #plt.ylim(ylim)
    max_x = 0
    plt.xlim(0, max_x)

    shape_keys = shape_data.keys()
    shape_keys.sort()
    sc_plots = []
    for shape in shape_keys:
        print shape_data[shape]['shape']
        x = shape_data[shape]['x']
        y = shape_data[shape]['y']
        print len(x)
        print len(y)
        marker = shape_data[shape]['shape']
        sc = plt.scatter(x, y,marker=marker)
        sc_plots.append(sc)
        max_x = max(x+[max_x])

    max_x += 500
    plt.xlim(0, max_x)
    plt.legend(sc_plots,
           [i.upper() for i in shape_keys],
           loc='upper left',
           scatterpoints=1,
           ncol=3,
           fontsize=fontsize)

    #plot the lines for different factors
    for y_factor, linestyle, color, linewidth in MAX_ALIVE_SESSIONS:
        plt.hlines(y_factor, 0, max_x, color=color, linewidth=linewidth, linestyle=linestyle)

    if output_filename is None:
        plt.show()
    else:
        plt.savefig(output_filename)

dirs2 = ['/srv/nfs/cortana/logs/small_oracle8_unmod_jvm_no_destroy_data_mod_results_batch_x64_exp_gen_2560/',
'/srv/nfs/cortana/logs/small_mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/',
'/srv/nfs/cortana/logs/small_mod_jvm_mod_ms_no_destroy_data_mod_results_batch_x64_exp_gen_2560/',
'/srv/nfs/cortana/logs/small_mod_jvm_mod_jce_mod_ms_destroy_data_mod_results_batch_x64_exp_gen_2560/',]

manual_dirs = [
'/srv/nfs/cortana/logs/manual_runs/profiling_runs_x64_2560',
'/srv/nfs/cortana/logs/manual_runs/profiling_runs_mod_ms_tenured_collect_always_x64_2560',
]

heap_infos = {}
table3s = {}
for base_location in dirs2:
    heap_infos[base_location] = aed.read_heap_summary_data(base_location)
    table3s[base_location] = aed.build_table3(heap_infos[base_location])


manual_heap_infos = {}
manual_table3s = {}
for base_location in manual_dirs:
    manual_heap_infos[base_location] = aed.read_heap_summary_data(base_location)
    manual_table3s[base_location] = aed.build_table3(manual_heap_infos[base_location])

bug_dirs2 = ['/srv/nfs/cortana/logs/KEEP_bug_in_experiment/mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560', ]
bug_heap_infos = {}
bug_table3s = {}
for base_location in bug_dirs2:
    bug_heap_infos[base_location] = aed.read_heap_summary_data(base_location)
    bug_table3s[base_location] = aed.build_table3(bug_heap_infos[base_location])

cbar_labels_tcm = ['tcm1', 'tcm2','tcm3','tcm4', 'tcm5','tcm6']
cbar_labels_aam = ['aam1', 'aam2','aam3']
cbar_labels_sm = ['sm1', 'sm2','sm3','sm4']

unmod_jvm = '/srv/nfs/cortana/logs/small_oracle8_unmod_jvm_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'
mod_ms_ten = '/srv/nfs/cortana/logs/small_mod_jvm_mod_ms_tenured_always_collect_no_destroy_data_mod_results_batch_x64_exp_gen_2560/'

s_unmod_jvm = manual_dirs[0]
s_mod_ms_ten = manual_dirs[1]
bug_mod_ms_ten = bug_dirs2[0]


tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, heap_infos[unmod_jvm])
aam_data = get_scatter_data_not_unique(cbar_labels_aam, heap_infos[unmod_jvm])
sm_data = get_scatter_data_not_unique(cbar_labels_sm, heap_infos[unmod_jvm])

mod_ms_ten_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, heap_infos[mod_ms_ten])
mod_ms_ten_aam_data = get_scatter_data_not_unique(cbar_labels_aam, heap_infos[mod_ms_ten])
mod_ms_ten_sm_data = get_scatter_data_not_unique(cbar_labels_sm, heap_infos[mod_ms_ten])

manual_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, manual_heap_infos[s_unmod_jvm])
manual_aam_data = get_scatter_data_not_unique(cbar_labels_aam, manual_heap_infos[s_unmod_jvm])
manual_sm_data = get_scatter_data_not_unique(cbar_labels_sm, manual_heap_infos[s_unmod_jvm])

ten_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, heap_infos[mod_ms_ten])
ten_aam_data = get_scatter_data_not_unique(cbar_labels_aam, heap_infos[mod_ms_ten])
ten_sm_data = get_scatter_data_not_unique(cbar_labels_sm, heap_infos[mod_ms_ten])

uniq_ten_tcm_data = get_scatter_data_unique(cbar_labels_tcm, heap_infos[mod_ms_ten])
uniq_ten_aam_data = get_scatter_data_unique(cbar_labels_aam, heap_infos[mod_ms_ten])
uniq_ten_sm_data = get_scatter_data_unique(cbar_labels_sm, heap_infos[mod_ms_ten])


manual_ten_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, manual_heap_infos[s_mod_ms_ten])
manual_ten_aam_data = get_scatter_data_not_unique(cbar_labels_aam, manual_heap_infos[s_mod_ms_ten])
manual_ten_sm_data = get_scatter_data_not_unique(cbar_labels_sm, manual_heap_infos[s_mod_ms_ten])

uniq_manual_ten_tcm_data = get_scatter_data_unique(cbar_labels_tcm, manual_heap_infos[s_mod_ms_ten])
uniq_manual_ten_aam_data = get_scatter_data_unique(cbar_labels_aam, manual_heap_infos[s_mod_ms_ten])
uniq_manual_ten_sm_data = get_scatter_data_unique(cbar_labels_sm, manual_heap_infos[s_mod_ms_ten])

bug_ten_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, bug_heap_infos[bug_mod_ms_ten])
bug_ten_aam_data = get_scatter_data_not_unique(cbar_labels_aam, bug_heap_infos[bug_mod_ms_ten])
bug_ten_sm_data = get_scatter_data_not_unique(cbar_labels_sm, bug_heap_infos[bug_mod_ms_ten])

uniq_bug_ten_tcm_data = get_scatter_data_unique(cbar_labels_tcm, bug_heap_infos[bug_mod_ms_ten])
uniq_bug_ten_aam_data = get_scatter_data_unique(cbar_labels_aam, bug_heap_infos[bug_mod_ms_ten])
uniq_bug_ten_sm_data = get_scatter_data_unique(cbar_labels_sm, bug_heap_infos[bug_mod_ms_ten])

test_ten_tcm_data = get_scatter_data_not_unique(cbar_labels_tcm, test_heap_infos[p])
test_ten_aam_data = get_scatter_data_not_unique(cbar_labels_aam, test_heap_infos[p])
test_ten_sm_data = get_scatter_data_not_unique(cbar_labels_sm, test_heap_infos[p])

uniq_test_ten_tcm_data = get_scatter_data_unique(cbar_labels_tcm, test_heap_infos[p])
uniq_test_ten_aam_data = get_scatter_data_unique(cbar_labels_aam, test_heap_infos[p])
uniq_test_ten_sm_data = get_scatter_data_unique(cbar_labels_sm, test_heap_infos[p])


SHAPES = ["x", 'o', '^', 's', '+', 'd']
def get_x_y_shapes(data):
    shape_results = {}
    shape_labels = set([i['model'] for i in data])
    shape_labels = list(shape_labels)
    shape_labels.sort()
    shape_map = dict([(i[1], i[0]) for i in enumerate(shape_labels)])
    for l in shape_labels:
        shape = SHAPES[shape_map[l]]
        shape_results[l] = {'x':[], 'y':[], 'shape':shape}
        x = []
        y = []
        for i in data:
            x = [i['tot_ssl'] for i in data if i['model'] == l]
            y = [i['unique_keys'] for i in data if i['model'] == l]
        shape_results[l]['x'] = x
        shape_results[l]['y'] = y
    return shape_results

def get_x_y_colors(data):
    cbar_labels = set([i['model'] for i in data])
    cbar_labels = list(cbar_labels)
    cbar_labels.sort()
    color_map = dict([(i[1], i[0]) for i in enumerate(cbar_labels)])
    colors = [color_map[i['model']] for i in data]
    x = [i['tot_ssl'] for i in data]
    y = [i['unique_keys'] for i in data]
    return x, y, colors, cbar_labels


title=''
cbar_label="Experiment Run #"
xlabel = "Number of TLS Keys Observed"
ylabel = "Recovered to Observed Keys"

data = [tcm_data,
aam_data,
sm_data,]

manual_ten_shape_tcm_data = get_x_y_shapes(manual_ten_tcm_data)
manual_ten_shape_aam_data = get_x_y_shapes(manual_ten_aam_data)
manual_ten_shape_sm_data = get_x_y_shapes(manua_tenl_sm_data)

uniq_manual_ten_shape_tcm_data = get_x_y_shapes(uniq_manual_ten_tcm_data)
uniq_manual_ten_shape_aam_data = get_x_y_shapes(uniq_manual_ten_aam_data)
uniq_manual_ten_shape_sm_data = get_x_y_shapes(uniq_manua_ten_sm_data)

ten_shape_tcm_data = get_x_y_shapes(ten_tcm_data)
ten_shape_aam_data = get_x_y_shapes(ten_aam_data)
ten_shape_sm_data = get_x_y_shapes(ten_sm_data)

uniq_ten_shape_tcm_data = get_x_y_shapes(uniq_ten_tcm_data)
uniq_ten_shape_aam_data = get_x_y_shapes(uniq_ten_aam_data)
uniq_ten_shape_sm_data = get_x_y_shapes(uniq_ten_sm_data)

bug_ten_shape_tcm_data = get_x_y_shapes(bug_ten_tcm_data)
bug_ten_shape_aam_data = get_x_y_shapes(bug_ten_aam_data)
bug_ten_shape_sm_data = get_x_y_shapes(bug_ten_sm_data)

uniq_bug_ten_shape_tcm_data = get_x_y_shapes(uniq_bug_ten_tcm_data)
uniq_bug_ten_shape_aam_data = get_x_y_shapes(uniq_bug_ten_aam_data)
uniq_bug_ten_shape_sm_data = get_x_y_shapes(uniq_bug_ten_sm_data)

test_ten_shape_tcm_data = get_x_y_shapes(test_ten_tcm_data)
test_ten_shape_aam_data = get_x_y_shapes(test_ten_aam_data)
test_ten_shape_sm_data = get_x_y_shapes(test_ten_sm_data)

uniq_test_ten_shape_tcm_data = get_x_y_shapes(uniq_test_ten_tcm_data)
uniq_test_ten_shape_aam_data = get_x_y_shapes(uniq_test_ten_aam_data)
uniq_test_ten_shape_sm_data = get_x_y_shapes(uniq_test_ten_sm_data)

make_scatter_shape_plot(test_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(test_ten_shape_aam_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(test_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)

make_scatter_shape_plot(uniq_test_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(uniq_test_ten_shape_aam_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(uniq_test_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)



ten_shape_tcm_data = get_x_y_shapes(mod_ms_ten_tcm_data)
ten_shape_aam_data = get_x_y_shapes(mod_ms_ten_aam_data)
ten_shape_sm_data = get_x_y_shapes(mod_ms_ten_sm_data)

uniq_ten_shape_tcm_data = get_x_y_shapes(uniq_ten_tcm_data)
uniq_ten_shape_aam_data = get_x_y_shapes(uniq_ten_aam_data)
uniq_ten_shape_sm_data = get_x_y_shapes(uniq_ten_sm_data)

make_scatter_shape_plot(uniq_manual_ten_sm_data, title, xlabel, ylabel, cbar_label, output_filename="tcm_uniq_bug_ms_tenured.png")
make_scatter_shape_plot(uniq_bug_ten_shape_aam_data, title, xlabel, ylabel, cbar_label, output_filename="aam_uniq_bug_ms_tenured.png")
make_scatter_shape_plot(uniq_bug_ten_shape_sm_data, title, xlabel, ylabel, cbar_label, output_filename="sm_uniq_bug_ms_tenured.png")


make_scatter_shape_plot(uniq_manual_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(uniq_manual_ten_shape_aam_data, title, xlabel, ylabel, cbar_label)
make_scatter_shape_plot(uniq_manual_ten_shape_sm_data, title, xlabel, ylabel, cbar_label)


make_scatter_shape_plot(uniq_bug_ten_shape_tcm_data, title, xlabel, ylabel, cbar_label, output_filename="tcm_uniq_bug_ms_tenured.png")
make_scatter_shape_plot(uniq_bug_ten_shape_aam_data, title, xlabel, ylabel, cbar_label, output_filename="aam_uniq_bug_ms_tenured.png")
make_scatter_shape_plot(uniq_bug_ten_shape_sm_data, title, xlabel, ylabel, cbar_label, output_filename="sm_uniq_bug_ms_tenured.png")

x,y, colors, cbar_labels = get_x_y_colors(aam_data)

make_scatter_plot(x,y, colors, title, xlabel, ylabel, cbar_label),
make_scatter_shape_plot(shape_aam_data, title, xlabel, ylabel, cbar_label),
make_scatter_shape_plot(manual_shape_aam_data, title, xlabel, ylabel, cbar_label),

base_locations = ['/srv/nfs/cortana/logs/openjdk8_x64_exp_gen_2560/transaction',]
base_location = base_locations[0]
import single_factors_iteration_perform_runs as sifipr
experiments = [os.path.join(base_location, i) for i in os.listdir(base_location)]

dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
sys.path.append(dest)
import single_factors_iteration_perform_runs as sifipr
import unzip_perform_process_dirs as uppd

r_base9 = '/srv/nfs/cortana/logs/manual_runs/profiling_runs_mod_ms_tenured_collect_always/'
r_base8 = '/srv/nfs/cortana/logs/manual_runs/profiling_runs_mod_jce/'
r_base7 = '/srv/nfs/cortana/logs/manual_runs/profiling_runs/'
locs = [r_base7, r_base8, r_base9]
base_locations = []
for l in locs:
    base_locations = base_locations + [os.path.join(l, i) for i in os.listdir(l)]

process = [i for i in base_locations if os.path.exists(sifipr.get_java_hprof_zip(i)) and os.path.exists(sifipr.get_java_heap_key_locations(i))]


interested_classes = [
'sun.security.ssl.SSLContextImpl$TLSContext',
'sun.security.ssl.SSLSessionContextImpl',
'sun.security.ssl.SSLSessionContextImpl$1',
'sun.security.ssl.SSLSessionImpl',
'sun.security.ssl.SSLSocketImpl',
'javax.net.ssl.SSLContext',
'javax.net.ssl.SSLContextSpi',
'javax.net.ssl.SSLSessionContext',
'com.sun.crypto.provider.TlsKeyMaterialGenerator',
'com.sun.crypto.provider.TlsMasterSecretGenerator',
'com.sun.crypto.provider.TlsMasterSecretGenerator$TlsMasterSecretKey',
'com.sun.crypto.provider.TlsPrfGenerator$V12',
'edu.rice.cs.seclab.dso.ExperimentSession',
'org.apache.http.client.methods.HttpGet',
'org.apache.http.client.methods.HttpRequestWrapper',
'org.apache.http.client.protocol.HttpClientContext',
'org.apache.http.impl.client.BasicCookieStore',
'org.apache.http.impl.client.InternalHttpClient',
'org.apache.http.impl.client.HttpClientBuilder',
'org.apache.http.impl.client.HttpClientBuilder$2',
'org.apache.http.impl.io.DefaultHttpRequestWriter',
'org.apache.http.impl.io.SessionOutputBufferImpl',
'java.util.TimerThread',
'java.lang.Thread',
]

import tabulate
def print_tables(base_location):
    iklasses = set(interested_classes)
    nresults = get_extracted_hprof_data(base_location)
    if len(nresults) == 0:
        print "XXXX - Failed to print table: %s"%base_location
        return
    klasses = [i for i in nresults if i.find('.') > 0 and i in iklasses]
    klasses.sort()
    tabulate_data = [(k, nresults[k]['live'], nresults[k]['dead']) for k in klasses]
    headers = ['Java Class %s'%base_location, 'Live Objects', 'Dead Objects']
    print tabulate.tabulate(tabulate_data, headers)


