import sys
PATH_TO_RUN_MOD = "/srv/nfs/cortana/logs/cmd/"
sys.path.append(PATH_TO_RUN_MOD)
import single_factors_iteration_perform_runs as sifipr
from single_factors_iteration_perform_runs import *

MAX_RUNNING = 10
if __name__ == "__main__":
    # ip -s -s neigh flush all
    use_python = True
    target_dir = ""
    start_str = time_str()
    if len(sys.argv) < 7:
        print ("%s <32|64> <jvm_mem> <num_iterations> <transaction|streaming> <mod|no_mod> <results_base> <mem_press> [<sess_lifetime> <conc_session> <request_factor>]"%sys.argv[0])
        print ("Valid Factors: HIGH | MED | LOW ")
        sys.exit(-1)
    use_64 = sys.argv[1] == "64"
    jvm_mem = int(sys.argv[2])
    is_streaming = False
    num_iterations = int(sys.argv[3])
    results_fmt = "%s"

    USE_MOD_VM = True if sys.argv[5] == "mod" else False    
    if USE_MOD_VM:
        results_fmt = "%s_mod_vm"
    transaction_results_base = get_results_base(sys.argv[6], is_streaming=False)
    streaming_results_base = get_results_base(sys.argv[6], is_streaming=True)
    try:
        os.makedirs(transaction_results_base)
    except:
        pass
    try:
        os.makedirs(streaming_results_base)
    except:
        pass

    #f_str = []
    #for i in sys.argv[6:]:
    #    if not i in FACTORS:
    #        raise Exception("%s is not a valid factor enum"%i)
    #    f_str.append(i)
    #if len(f_str) > 0 and len(f_str) < 4:
    #    i = f_str[0]
    #    f_str = [i for k in xrange(0,4)]
    #elif len(f_str) != 4:
    #    raise Exception("Not enough factor enums provided"%i)
    #factors1 = get_factors_dict_str(*f_str)
    # Comment below to return to normal procedures
    #factors1  = get_factors_dict_str(*['LOW', 'LOW','HIGH', 'MED'])
    #factors2  = get_factors_dict_str(*['LOW', 'LOW','LOW',  'MED'])
    #factors3  = get_factors_dict_str(*['LOW', 'MED','HIGH', 'MED'])
    # Memory contention
    #factors4  = get_factors_dict_str(*['LOW', 'MED','MED',  'MED'])
    #factors5  = get_factors_dict_str(*['MED', 'MED','MED', 'MED'])
    #factors6  = get_factors_dict_str(*['HIGH','MED','MED',  'MED'])
    factors4  = get_factors_dict_str(*['LOW', 'LOW', 'MED',  'MED'])
    factors6  = get_factors_dict_str(*['HIGH','LOW', 'MED',  'MED'])
    
    factors7  = get_factors_dict_str(*['LOW', 'HIGH', 'HIGH', 'HIGH'])
    factors9  = get_factors_dict_str(*['HIGH','HIGH', 'HIGH', 'HIGH'])

    factors10  = get_factors_dict_str(*['LOW', 'LOW', 'MED',  'HIGH'])
    factors11  = get_factors_dict_str(*['HIGH', 'LOW', 'MED',  'HIGH'])
    
    #factors14  = get_factors_dict_str(*['RANDOM','RANDOM', 'MED',  'MED'])
    #factors15  = get_factors_dict_str(*['RANDOM','LOW', 'RANDOM',  'MED'])
   
    #factors16  = get_factors_dict_str(*['RANDOM','LOW', 'RANDOM',  'RANDOM'])
    #factors17  = get_factors_dict_str(*['RANDOM','HIGH', 'RANDOM',  'RANDOM'])
    #factors_list = [factors4, factors5, factors6]
    factors_list = [factors4, factors6, factors7, factors9, factors10, factors11]#, factors11, factors12]
    #factors_list = [factors10]#, factors11, factors12]
    start_str = time_str()

    # create results dir
    results_fmt = "%s"
    if USE_MOD_VM:
        results_fmt = "%s_mod_vm"

    PYTHON_WORK_HOST = PYTHON_WORK_HOST_LIST if not use_64 else PYTHON_WORK_HOST_X64_LIST 
    SSL_WORK_HOST = SSL_HOST_LIST
    #stop_host_list(PYTHON_WORK_HOST)
    MAX_RUNNING = min([MAX_RUNNING, len(PYTHON_WORK_HOST), len(SSL_WORK_HOST)])
    if MAX_RUNNING > len(PYTHON_WORK_HOST)/2:
        MAX_RUNNING = len(PYTHON_WORK_HOST)/2
    if MAX_RUNNING*2 < len(PYTHON_WORK_HOST):
        PYTHON_WORK_HOST = PYTHON_WORK_HOST[:MAX_RUNNING*2]
    SSL_WORK_HOST = SSL_HOST_LIST[:MAX_RUNNING]
    print ("Experiment Log: Initializing VMs for the run")
    #stop_host_list(PYTHON_WORK_HOST)
    #time.sleep(sifipr.SLEEP_SECS*12)
    start_host_list(PYTHON_WORK_HOST)
    print ("Sleeping waiting on the host to come up: %d s"%(sifipr.SLEEP_SECS))
    #time.sleep(sifipr.SLEEP_SECS*len(PYTHON_WORK_HOST)/2)
 
    # init_available(PYTHON_WORK_HOST_LIST, SSL_HOST_LIST)
    init_available(PYTHON_WORK_HOST, SSL_WORK_HOST, MAX_RUNNING)
    # for host in PYTHON_WORK_HOST_LIST:
    if len(sifipr.AVAILABLE_EXP_HOSTS) < len(PYTHON_WORK_HOST):
        raise Exception("Failed to init to all java hosts")

    if len(sifipr.AVAILABLE_SSL) < len(SSL_WORK_HOST):
        raise Exception("Failed to init to all ssl hosts")

    print ("Experiment Log: VM startup and checks complete")
    active_threads = []
    destroy_passwords = 0
    set_multiple_runs_working_list(factors_list, num_iterations, transaction_results_base)
    # sotp comment
    #sifipr.WORKING_LIST = [i for i in generate_core_work_list()]
    #sifipr.WORKING_LIST.sort()
    ready_for_next_batch = True
    java_work_host_size = get_java_host_len()
    print ("Experiment Log: Starting the work")
    first_run = True
    sifipr.EXECING_VMS = True
    dumping_thread = threading.Thread(target=handle_dumping_java_host)
    dumping_thread.start()
    is_streaming = False
    set_multiple_runs_working_list(factors_list, num_iterations, transaction_results_base)
    while not is_working_list_empty()  or len (active_threads) > 0:
        if is_java_host_avail() and is_ssl_host_avail() and not is_working_list_empty():
            while sifipr.HOSTS_DUMPING > sifipr.MAX_HOSTS_DUMPING:
                sl = 1 if sifipr.MAX_HOSTS_DUMPING == 0 else sifipr.MAX_HOSTS_DUMPING
                time.sleep( sifipr.HOSTS_DUMPING / float(sl))
            if is_working_list_empty():
                break
            base_location, factors = next_working_list_item()
            if base_location is None or factors is None:
                if is_working_list_empty():
                    break
                continue
            java_host = get_java_host()
            ssl_host = get_ssl_host()
            if java_host is None:
                add_ssl_host_back(ssl_host)
                continue
            cmd_success = test_host_is_up(java_host, PYTHON_USER, PYTHON_PASS) and\
                          test_host_is_up(ssl_host, EXPSSL_USER, EXPSSL_PASS)
            if not cmd_success:
                add_ssl_host_back(ssl_host)
                add_java_host_back(java_host)
                time.sleep(sifipr.SLEEP_SECS)
                continue
            else:
                print ("Experiment Log: Performing Java command using %s and %s, %d remaining"%(java_host, ssl_host, working_list_len()))
                #java_ip = get_host_ip(java_host)
                #ssl_ip = get_host_ip(ssl_host)
                t = threading.Thread(target=execute_command_steps, args=(java_host,
                                 ssl_host, factors, base_location, destroy_passwords, jvm_mem, is_streaming, use_64, use_python))
    
                t.start()
                active_threads.append((t, java_host, ssl_host,
                                        factors, destroy_passwords))
                #if first_run:
                time.sleep(sifipr.SLEEP_SECS*2.5)
        else:
            first_run = False
            #cull_threads(ACTIVE_PROC_EXTRACTION_THREADS)
            cull_threads(active_threads)
            time.sleep(.1)
    while len (active_threads) > 0:
        if not cull_threads(active_threads):
            time.sleep(.1)
    
    set_perform_dumps(False)
    dumping_thread.join()
    sifipr.EXECING_VMS = False    
    stop_host_list(PYTHON_WORK_HOST)
    #while len(ACTIVE_PROC_EXTRACTION_THREADS) > 0:
    #    if not cull_threads(ACTIVE_PROC_EXTRACTION_THREADS):
    #        time.sleep(.1)
    
    print "All threads completed."
    end_str = time_str()
    print "%s Started processing"%start_str
    print "%s Ended processing round 1"%end_str

