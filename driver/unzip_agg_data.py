from datetime import datetime, timedelta
from multiprocessing import Process
import sys, libvirt, paramiko, subprocess, time, os, threading, select, errno
import binascii, subprocess, json, shutil, random, urllib, multiprocessing, re
import socket, traceback, json
dest = "/srv/nfs/cortana/logs/cmd/exp_code/driver/"
DEST_LIBS = "/srv/nfs/cortana/logs/cmd/exp_code/libs/"
sys.path.append(dest)
#from agg_exp_data import *
import single_factors_iteration_perform_runs as sifipr
import perform_post_process_dirs_strs as pppds
import agg_exp_data as aed
from datetime import datetime
def time_str():
    return str(datetime.now().strftime("%H:%M:%S.%f %m-%d-%Y"))

WORKING_BASE = "/srv/result_data/working_dir"
SLEEP_SECS = 3.0
WORKING_QUEUE = []
WORKING_QUEUE_LOCK = threading.Lock()

INCOMPLETE_DIRS = set()
COMPLETE_DIRS = set()
INCOMPLETE_DIRS_LOCK = threading.Lock()

AGG_COMPLETED = "data_agg_process_complete.txt"
UNZIP_MEMORY_PROCESS = "unzip_memory_process_complete.txt"
MEMORY_DUMP = 'memory-work.dump'
MEMORY_DUMP_ZIP = 'memory-work.dump.zip'

import random, string
ALLOWED_PROC_EXTRACTION_THREADS = 23 # int(sys.argv[1])
ACTIVE_PROCS = []
ACTIVE_PROCS_LOCK = threading.Lock()
EXTRACT_MEMORY_DATA = False

ALLOWED_PROC_AGG_THREADS = 23 # int(sys.argv[1])
ACTIVE_AGG_PROCS = []
ACTIVE_AGG_PROCS_LOCK = threading.Lock()
AGG_MEMORY_DATA = False

BASE_LOCATION_NO_LONGER_VALID = set()
STOP_ANALYSIS_COUNT_DOWN = False
TERM_ANALYSIS = False

GENERATIONS_0512 = {
      'eden':{'start':0x00e0000000, 'end':0x00ecce0000},
    'hemi_0':{'start':0x00ecce0000, 'end':0x00ee670000},
    'hemi_1':{'start':0x00ee670000, 'end':0x00f0000000},
   'tenured':{'start':0x00f0000000, 'end':0x00ecce0000},
   'threads':{'start':0x00ecce0000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x800000000000},
   'other_2':{'start':0x800000000000, 'end':0x800000000000},
}

GENERATIONS_1024 = {
      'eden':{'start':0x00c0000000, 'end':0x00d99a0000},
    'hemi_0':{'start':0x00d99a0000, 'end':0x00dccd0000},
    'hemi_1':{'start':0x00dccd0000, 'end':0x00e0000000},
   'tenured':{'start':0x00e0000000, 'end':0x0100000000},
   'threads':{'start':0x0100000000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x800000000000},
   'other_2':{'start':0x800000000000, 'end':0x800000000000},
}

GENERATIONS_2048 = {
      'eden':{'start':0x0080000000, 'end':0x00b3340000},
    'hemi_0':{'start':0x00b3340000, 'end':0x00b99a0000},
    'hemi_1':{'start':0x00b99a0000, 'end':0x00c0000000},
   'tenured':{'start':0x00c0000000, 'end':0x0100000000},
   'threads':{'start':0x0100000000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x800000000000},
   'other_2':{'start':0x800000000000, 'end':0x800000000000},
}


GENERATIONS_4096 = {
      'eden':{'start':0x06c0000000, 'end':0x0726680000},
    'hemi_0':{'start':0x0726680000, 'end':0x0733340000},
    'hemi_1':{'start':0x0733340000, 'end':0x0740000000},
   'tenured':{'start':0x0740000000, 'end':0x07c0000000},
   'threads':{'start':0x07c0000000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x8800000c0000},
   'other_2':{'start':0x8800000c0000, 'end':0x8800bfffe000},
}

GENERATIONS_8192 = {
      'eden':{'start':0x05c0000000, 'end':0x068cce0000},
    'hemi_0':{'start':0x068cce0000, 'end':0x06a6670000},
    'hemi_1':{'start':0x06a6670000, 'end':0x06c0000000},
   'tenured':{'start':0x06c0000000, 'end':0x07c0000000},
   'threads':{'start':0x07c0000000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x800000000000},
   'other_2':{'start':0x800000000000, 'end':0x800000000000},
}

GENERATIONS_16384 = {
      'eden':{'start':0x03c0000000, 'end':0x0554ce0000},
    'hemi_0':{'start':0x0554ce0000, 'end':0x0587670000},
    'hemi_1':{'start':0x0587670000, 'end':0x05ba000000},
   'tenured':{'start':0x05ba000000, 'end':0x07c0000000},
   'threads':{'start':0x07c0000000, 'end':0x800000000000},
   'other_1':{'start':0x800000000000, 'end':0x800000000000},
   'other_2':{'start':0x800000000000, 'end':0x800000000000},
}

GENERATIONS_DICT = {
    512:GENERATIONS_0512,
    1024:GENERATIONS_1024,
    2048:GENERATIONS_2048,
    4096:GENERATIONS_4096,
    8192:GENERATIONS_8192,
    16384:GENERATIONS_16384,
}

GENERATIONS = GENERATIONS_4096

def get_generation(generations, location):
    for g,ranges in generations.items():
        if ranges['start'] <= location and location < ranges['end']:
            return g
    return None

COMPROMISED_SENSITIVE = "expstart:{exp_start:03d}-start:{start}-end:{end}-age:{age}-count:{count}-value:{value}-user:{user}"
def get_compromised_line(info_dict, t_start):
    t = {}
    t.update(info_dict)
    abs_start = (t['start'] - t_start)/1000
    count = info_dict['full_occ'] if 'full_occ' in info_dict else 0
    value = info_dict['sensitive'] if 'sensitive' in info_dict else 0
    t['count'] = count
    t['value'] = value
    t['exp_start'] = abs_start
    output = COMPROMISED_SENSITIVE.format(**t)
    return output


def stop_analysis(wait_minutes, sleep_interval_minutes):
    global EXTRACT_MEMORY_DATA, STOP_ANALYSIS_COUNT_DOWN
    start = datetime.now()
    STOP_ANALYSIS_COUNT_DOWN = False
    period = timedelta(minutes=wait_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    TERM_ANALYSIS = True
    while not stop and not STOP_ANALYSIS_COUNT_DOWN:
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

    if TERM_ANALYSIS:
        EXTRACT_MEMORY_DATA = False


STOP_SCANNING = False
def periodic_scan(base_location, scan_minutes, sleep_interval_minutes):
    global STOP_SCANNING
    start = datetime.now()
    period = timedelta(minutes=scan_minutes)
    next_time = datetime.now() + period
    now = datetime.now()
    stop = now > next_time
    while not stop and not STOP_SCANNING:
        r = scan_location_for_files(base_location)
        add_files_work_queue(r)
        time.sleep(sleep_interval_minutes*60)
        stop = now > next_time

def start_periodic_scan(base_location, scan_minutes, sleep_interval_minutes):
    t = threading.Thread(target=periodic_scan, args=(base_location, scan_minutes, sleep_interval_minutes))
    t.start()
    return t

def terminate_analysis_thread(wait_minutes, sleep_interval_minutes):
    t = threading.Thread(target=stop_analysis, args=(wait_minutes, sleep_interval_minutes))
    t.start()
    return t

def check_completed_memory_extraction_completed(base_location):
    f = os.path.join(base_location, UNZIP_MEMORY_PROCESS)
    try:
        os.stat(f)
        return True
    except:
        return False

def check_data_agg_completed(base_location):
    f = os.path.join(base_location, AGG_COMPLETED)
    try:
        os.stat(f)
        return True
    except:
        return False

def mark_location_complete(base_location):
    INCOMPLETE_DIRS_LOCK.acquire()
    COMPLETE_DIRS.add(base_location)
    if base_location in INCOMPLETE_DIRS:
        INCOMPLETE_DIRS.remove(base_location)
    INCOMPLETE_DIRS_LOCK.release()

def has_active_agg_procs():
    global ACTIVE_AGG_PROCS
    return len(ACTIVE_AGG_PROCS) > 0

def prune_active_agg_procs():
    global ACTIVE_AGG_PROCS, ACTIVE_AGG_PROCS_LOCK
    ACTIVE_AGG_PROCS_LOCK.acquire()
    new_active_procs = []
    for p, base_location, working_dir in ACTIVE_AGG_PROCS:
        if p.is_alive():
            new_active_procs.append((p, base_location, working_dir))
        elif base_location in INCOMPLETE_DIRS:
            # was not able to successfully complete the directory
            if check_completed_memory_extraction_completed(base_location):
                print("%s: Marking %s as completed"%(time_str(), base_location))
                mark_location_complete(base_location)
            else:
                try:
                    os.stat(base_location)
                    reapply_work(loc)
                except:
                    BASE_LOCATION_NO_LONGER_VALID.add(base_location)
                    print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            try:
                os.stat(working_dir)
                print("%s: Cleaning up working dir, must have been a failure: %s"%(time_str(), working_dir))
                shutil.rmtree(working_dir)
            except:
                pass

    ACTIVE_AGG_PROCS = new_active_procs
    ACTIVE_AGG_PROCS_LOCK.release()


def add_active_agg_procs(p, base_location, working_dir):
    global ACTIVE_AGG_PROCS_LOCK, ACTIVE_AGG_PROCS
    ACTIVE_AGG_PROCS_LOCK.acquire()
    ACTIVE_AGG_PROCS.append((p, base_location, working_dir))
    ACTIVE_AGG_PROCS_LOCK.release()

def can_start_new_agg_proc():
    global ACTIVE_AGG_PROCS_LOCK, ACTIVE_AGG_PROCS, ALLOWED_PROC_AGG_THREADS
    return len(ACTIVE_AGG_PROCS) < ALLOWED_PROC_AGG_THREADS

def perform_data_aggregation():
    global ACTIVE_AGG_PROCS_LOCK, ACTIVE_AGG_PROCS, ALLOWED_PROC_AGG_THREADS, AGG_MEMORY_DATA
    AGG_MEMORY_DATA = True
    while AGG_MEMORY_DATA:
        prune_active_agg_procs()
        while AGG_MEMORY_DATA and not can_start_new_agg_proc():
            prune_active_agg_procs()
            if can_start_new_agg_proc() or len(WORKING_QUEUE) == 0:
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = Process(target=perform_work_data_agg, args=(base_location, working_dir, True, False))
            p.start()
            add_active_agg_procs(p, base_location, working_dir)
        else:
            break
    while has_active_agg_procs():
        prune_active_agg_procs()
        time.sleep(SLEEP_SECS)


def perform_heap_locs_write():
    global ACTIVE_AGG_PROCS_LOCK, ACTIVE_AGG_PROCS, ALLOWED_PROC_AGG_THREADS, AGG_MEMORY_DATA
    AGG_MEMORY_DATA = True
    while AGG_MEMORY_DATA:
        prune_active_agg_procs()
        while AGG_MEMORY_DATA and not can_start_new_agg_proc():
            prune_active_agg_procs()
            if can_start_new_agg_proc() or len(WORKING_QUEUE) == 0:
                break
            time.sleep(SLEEP_SECS)
        base_location = get_base_location()
        if not base_location is None:
            working_dir = get_working_dir()
            p = Process(target=perform_work_write_java_heap_locs_only, args=(base_location, working_dir, True, False))
            p.start()
            add_active_agg_procs(p, base_location, working_dir)
        else:
            break

    while has_active_agg_procs():
        prune_active_agg_procs()
        time.sleep(SLEEP_SECS)

def start_extraction_thread():
    global EXTRACT_MEMORY_DATA
    EXTRACT_MEMORY_DATA = True
    t = threading.Thread(target=perform_extraction)
    t.start()
    return t

def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))

def dir_has_memory_dump(location):
    has_it = False
    try:
        os.stat(os.path.join(location, MEMORY_DUMP))
        has_it = True
    except:
        has_it = False
    return has_it

def dir_has_memory_dump_zip(location):
    has_it = False
    try:
        os.stat(os.path.join(location, MEMORY_DUMP_ZIP))
        has_it = True
    except:
        has_it = False
    return has_it

def dir_has_jbgrep_strings(location):
    has_it = False
    try:
        os.stat(pppds.get_jbgrep_name(location))
        has_it = True
    except:
        has_it = False
    return has_it

def scan_location_for_files(location):
    found_dirs = set()
    results = set()
    dirs = [os.path.join(location, i) for i in os.listdir(location)]
    for d in dirs:
        found_dirs.add(d)

    INCOMPLETE_DIRS_LOCK.acquire()
    for d in found_dirs:
        if not d in INCOMPLETE_DIRS and\
           not d in COMPLETE_DIRS:
           results.add(d)
    INCOMPLETE_DIRS_LOCK.release()
    return results

def add_files_work_queue(locations):
    INCOMPLETE_DIRS_LOCK.acquire()
    for loc in locations:
        WORKING_QUEUE.insert(0, loc)
        INCOMPLETE_DIRS.add(loc)
    INCOMPLETE_DIRS_LOCK.release()

def reapply_work(location):
    INCOMPLETE_DIRS_LOCK.acquire()
    WORKING_QUEUE.insert(0, location)
    INCOMPLETE_DIRS_LOCK.release()

def get_base_location():
    o = None
    INCOMPLETE_DIRS_LOCK.acquire()
    if len(WORKING_QUEUE) > 0:
        o = WORKING_QUEUE.pop()
    INCOMPLETE_DIRS_LOCK.release()
    return o

def get_working_dir(base=None):
    if base is None:
        base = WORKING_BASE
    working_dir = os.path.join(base, randomword(10))
    os.makedirs(working_dir)
    return working_dir

def perform_heap_agg_copy(base_location, working_dir):
    jbgrep_grep_res = sifipr.get_jbgrep_grep_res_name(base_location)
    jbgrep_fname_res = sifipr.get_jbgrep_fname_res_name(base_location)
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(base_location)
    jbgrep_grep_resd = pppds.get_jbgrep_dumps_grep_res_name(base_location)
    jbgrep_fname_resd = pppds.get_jbgrep_dumps_fname_res_name(base_location)
    jbgrep_count_resd = pppds.get_jbgrep_dumps_count_res_name(base_location)
    auth_data = sifipr.get_auth_data_name(base_location)
    merged_keyinfo = sifipr.get_keyinfo_name(base_location)
    files_to_copy = [
        jbgrep_grep_res,
        jbgrep_fname_res,
        jbgrep_count_res,
        jbgrep_grep_resd,
        jbgrep_fname_resd,
        jbgrep_count_resd,
        merged_keyinfo,
        auth_data,
    ]
    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, working_dir)
            print("Copying %s to %s"%(f, working_dir))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), f))
            break

    if len(all_success) == len(files_to_copy):
        return True
    return False

def perform_agg_copy(base_location, working_dir):
    # copy over ffastings.txt.zip
    # copy over stdout_log.txt
    # copy over authlog.txt
    # copy over found_ssl_keys_counts.txt
    # copy over merged_keyinfo.txt
    jbgrep_grep_res = sifipr.get_jbgrep_grep_res_name(base_location)
    jbgrep_fname_res = sifipr.get_jbgrep_fname_res_name(base_location)
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(base_location)
    factors = sifipr.get_factors_filename(base_location)
    jbgrep_grep_resd = pppds.get_jbgrep_dumps_grep_res_name(base_location)
    jbgrep_fname_resd = pppds.get_jbgrep_dumps_fname_res_name(base_location)
    jbgrep_count_resd = pppds.get_jbgrep_dumps_count_res_name(base_location)
    time_log = sifipr.get_time_log(base_location)
    ffastrings_zip = pppds.get_ffastrings_zip_len_res_name(base_location)
    merged_keyinfo = sifipr.get_keyinfo_name(base_location)
    authlog = sifipr.get_java_auth_log(base_location)
    stdout_log = sifipr.get_java_stdoutlog(base_location)

    files_to_copy = [
        jbgrep_grep_res,
        jbgrep_fname_res,
        jbgrep_count_res,
        factors,
        #jbgrep_grep_resd,
        #jbgrep_fname_resd,
        #jbgrep_count_resd,
        ffastrings_zip,
        merged_keyinfo,
        authlog,
        stdout_log,
        time_log
    ]
    if os.path.exists(jbgrep_grep_resd):
        files_to_copy.append(jbgrep_grep_resd)
    if os.path.exists(jbgrep_fname_resd):
        files_to_copy.append(jbgrep_fname_resd)
    if os.path.exists(jbgrep_count_resd):
        files_to_copy.append(jbgrep_count_resd)

    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, working_dir)
            print("Copying %s to %s"%(f, working_dir))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), f))
            #break

    if len(all_success) == len(files_to_copy):
        return True
    return False

def write_compromised_summary_data(base_location, auth_data):
    pct_out = sifipr.get_pct_file(base_location)
    s_compromised_keys = write_compromised_keys(base_location, auth_data)
    post_data = write_post_data(base_location, auth_data)
    password_data = write_session_passwords(base_location, auth_data)
    session_data = write_session_identifiers(base_location, auth_data)
    r_keys = {}

    r_keys['pct_password_data'] = (100.0 * len(password_data)) / len(auth_data['password_infos'].values())
    r_keys['pct_session_data'] = (100.0 * len(session_data)) / len(auth_data['cookie_sessionid_infos'].values())
    r_keys['pct_compromised_keys'] = (100.0 * len(s_compromised_keys)) / len(auth_data['ssl_values'].values())
    r_keys['pct_post_uri'] = (100.0 * len(post_data)) / len(auth_data['post_uri_infos'].values())
    r_keys['connection_errors'] = sum([1 for v in auth_data['http_values'].values() if 'error' in v and v['error']])
    r_keys['num_password_data'] = len(password_data)
    r_keys['num_session_data'] = len(session_data)
    r_keys['num_compromised_keys'] = len(s_compromised_keys)
    r_keys['num_post_uri'] = len(post_data)
    r_keys['pct_password_data'] = (100.0 * len(password_data)) / len(auth_data['password_infos'].values())
    r_keys['pct_session_data'] = (100.0 * len(session_data)) / len(auth_data['cookie_sessionid_infos'].values())
    r_keys['pct_compromised_keys'] = (100.0 * len(s_compromised_keys)) / len(auth_data['ssl_values'].values())
    r_keys['pct_post_uri'] = (100.0 * len(post_data)) / len(auth_data['post_uri_infos'].values())
    r_keys['num_ssl'] = len(auth_data['ssl_infos'])
    r_keys['max_age'] = auth_data['max_age']


    fmt = "-".join(["%s:{%s:.2f}"%(i,i) for i in r_keys])
    fmt = fmt+'-connection_errors:{connection_errors}'
    open(pct_out, 'w').write(fmt.format(**r_keys)+'\n')


def write_compromised_keys(base_location, auth_data):
    ssl_data = auth_data['ssl_values']
    t_start = auth_data['start']
    jbgrep_count_res = sifipr.get_jbgrep_count_res_name(base_location)
    key_counts = [i.strip() for i in open(jbgrep_count_res).readlines() if len(i.strip()) > 0 ]
    hits = [(i.split()[0], int(i.split()[1], 16)) for i in key_counts]
    key_hits = dict(hits)
    set_hits = set([i for i in key_hits])

    compromised_keys = []
    for k, _v in ssl_data.items():
        if _v['pms'] in set_hits or\
           _v['ms'] in set_hits or\
           _v['mkeyblock'] in set_hits:
            compromised_keys.append(_v)

    s_compromised_keys = sorted(compromised_keys, key=lambda kv: kv['timestamp'])
    outfile = 'compromised_keys.txt'
    keyed_fmt = {}
    output_fmt = "expstart:{exp_start:03d}-timestamp:{timestamp}-ms:{ms}|{ms_hits}-pms:{pms}|{pms_hits}-mkeyblock:{mkeyblock}|{mkeyblock_hits}"
    output_results = []
    for ck in s_compromised_keys:
        ck['pms_hits'] = 0 if not ck['pms'] in key_hits else key_hits[ck['pms']]
        ck['ms_hits'] = 0 if not ck['ms'] in key_hits else key_hits[ck['ms']]
        ck['mkeyblock_hits'] = 0 if not ck['mkeyblock'] in key_hits else key_hits[ck['mkeyblock']]
        ck['exp_start'] = (ck['timestamp']-t_start)/1000
        output_results.append(output_fmt.format(**ck))
    output_results.sort()
    compromised_keys_out = sifipr.get_compromised_keys(base_location)
    open(compromised_keys_out, 'w').write("\n".join(output_results))
    return s_compromised_keys

def write_post_data(base_location, auth_data):
    post_data_out = sifipr.get_compromised_post_data(base_location)
    t_start = auth_data['start']
    data = [ i for i in auth_data['post_uri_infos'].values() if i['full_occ'] > 0 and not i['live']]
    lines = [get_compromised_line(i, t_start) for i in data]
    lines.sort()
    open(post_data_out, 'w').write("\n".join(lines)+'\n')
    return data

def write_session_identifiers(base_location, auth_data):
    t_start = auth_data['start']
    session_id_out = sifipr.get_compromised_sessions(base_location)
    data = [ i for i in auth_data['cookie_sessionid_infos'].values() if i['full_occ'] > 0 and not i['live']]
    lines = [get_compromised_line(i, t_start) for i in data]
    lines.sort()
    open(session_id_out, 'w').write("\n".join(lines)+'\n')
    return data

def write_session_passwords(base_location, auth_data):
    session_passwords_out = sifipr.get_compromised_passwords(base_location)
    t_start = auth_data['start']
    data = [ i for i in auth_data['password_infos'].values() if i['full_occ'] > 0 and not i['live']]
    lines = [get_compromised_line(i, t_start) for i in data]
    lines.sort()
    open(session_passwords_out, 'w').write("\n".join(lines)+'\n')
    return data

def write_java_heap_locations(base_location, auth_data, heap_size=4096):
    global GENERATIONS_DICT
    generation_info = GENERATIONS_DICT[heap_size] if heap_size in GENERATIONS_DICT else GENERATIONS_4096
    java_heap_key_locs_out = sifipr.get_java_heap_key_locations(base_location)
    dump_hits_grep = jbgrep_grep_resd = pppds.get_jbgrep_dumps_grep_res_name(base_location)
    dump_entrys = aed.get_found_key_offsets(base_location)
    expired_keys = set()
    #for e in auth_data['matched_comms']:
    for e in auth_data['ssl_values'].values():
        ms = e['ms']
        expired_keys.add(auth_data['ssl_values'][ms]['pms'])
        expired_keys.add(auth_data['ssl_values'][ms]['mkeyblock'])
        expired_keys.add(ms)

    merged_info = aed.merge_key_hits_key_info(base_location)
    mapped_keys = {}
    mapped_pms_keys = set()
    mapped_mkeyblocks_keys = set()
    for k, mi in merged_info.items():
        mapped_pms_keys.add(mi['pms'])
        mapped_mkeyblocks_keys.add(mi['mkeyblock'])
        p = [mi['mkeyblock'], mi['pms'], mi['ms']]
        for i in p:
            mapped_keys[i] = k

    range_hits = {}
    range_mapping = {}
    range_loc_mapping = {}
    hits_grouped_by_gen = {'other_1':[], 'other_2':[], 'threads':[], 'orphaned':[], 'eden':[], 
                           'hemi_1':[], 'hemi_0':[], 'hemi':[], 'tenured':[]}
    uniq_hits_grouped_by_gen = dict([(i, set()) for i in hits_grouped_by_gen])
    compromised_key_locs = dict([(i, []) for i in hits_grouped_by_gen])
    uniq_compromised_key_locs = dict([(i, set()) for i in hits_grouped_by_gen])
    key_pms_hits = []
    key_mkeyblock_hits = []

    for hit, hit_list in dump_entrys.items():
        for hit_offset, dump_range in hit_list:
            gen = get_generation(generation_info, hit_offset)
            key = mapped_keys[hit.strip()]
            if not hit in expired_keys:
                continue

            if hit in mapped_pms_keys:
                key_pms_hits.append(hit)
            elif hit in mapped_mkeyblocks_keys:
                key_mkeyblock_hits.append(hit)
            if gen is None:
                hits_grouped_by_gen['orphaned'].append((hit_offset, hit.strip()))
                uniq_hits_grouped_by_gen['orphaned'].add(hit.strip())
                compromised_key_locs['orphaned'].append(key)
                uniq_compromised_key_locs['orphaned'].add(key)
            else:
                hits_grouped_by_gen[gen].append((hit_offset, hit.strip()))
                uniq_hits_grouped_by_gen[gen].add(hit.strip())
                compromised_key_locs[gen].append(key)
                uniq_compromised_key_locs[gen].add(key)
                if gen.find('hemi') == 0:
                    hits_grouped_by_gen['hemi'].append((hit_offset, hit.strip()))
                    uniq_hits_grouped_by_gen['hemi'].add(hit.strip())
                    compromised_key_locs['hemi'].append(key)
                    uniq_compromised_key_locs['hemi'].add(key)
            if not dump_range in range_hits:
                range_hits[dump_range] = 0
                range_mapping[dump_range] = []
                range_loc_mapping[dump_range] = []
            range_hits[dump_range] += 1
            range_mapping[dump_range].append(hit)
            range_loc_mapping[dump_range].append([hit_offset, hit])
    r_keys = range_hits.keys()
    r_keys.sort()
    thread_hits = {}

    java_heap_key_locs_out_f = open(java_heap_key_locs_out, 'w')

    results_dict = {}
    results_dict['key_pms_hits'] = len(key_pms_hits)
    results_dict['key_mkeyblock_hits'] = len(key_mkeyblock_hits)
    results_dict['key_pms_unique_hits'] = len(set(key_pms_hits))
    results_dict['key_mkeyblock_unique_hits'] = len(set(key_mkeyblock_hits))

    results_dict['tot_ssl'] = len(merged_info)
    for name, hits in hits_grouped_by_gen.items():
        results_dict[name] = len(hits)

    for name, hits in uniq_hits_grouped_by_gen.items():
        results_dict[name+'_unique'] = len(hits)

    for name, hits in compromised_key_locs.items():
        results_dict[name] = len(hits)

    for name, hits in uniq_compromised_key_locs.items():
        results_dict[name+'_unique'] = len(hits)

    # need to find all the keys in the tenure set that are in the eden and hemi set
    tot_unique = set.union(*uniq_hits_grouped_by_gen.values())
    tot_heap_unique = set.union(*[uniq_hits_grouped_by_gen[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])
    eden_in_threads = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['threads']])
    eden_in_all = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['hemi_1', 'hemi_0', 'tenured']])
    eden_in_hemi_0 = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['hemi_0']])
    eden_in_hemi_1 = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['hemi_1']])
    eden_in_hemi = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['hemi_1', 'hemi_0']])
    eden_in_tenured = uniq_hits_grouped_by_gen['eden'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['tenured']])
    hemi_in_tenured = (uniq_hits_grouped_by_gen['hemi_1'] |uniq_hits_grouped_by_gen['hemi_0']) & uniq_hits_grouped_by_gen['tenured']
    other_2_heap_common = uniq_hits_grouped_by_gen['other_2'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])
    thread_heap_common = uniq_hits_grouped_by_gen['threads'] & set.union(*[uniq_hits_grouped_by_gen[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])

    results_dict['tot_unique'] = len(tot_unique)
    results_dict['tot_heap_unique'] = len(tot_heap_unique)
    results_dict['eden_in_threads'] = len(eden_in_threads)
    results_dict['eden_in_all'] = len(eden_in_all)
    results_dict['eden_in_hemi_0'] = len(eden_in_hemi_0)
    results_dict['eden_in_hemi_1'] = len(eden_in_hemi_1)
    results_dict['eden_in_hemi'] = len(eden_in_hemi)
    results_dict['eden_in_tenured'] = len(eden_in_tenured)
    results_dict['hemi_in_tenured'] = len(hemi_in_tenured)
    results_dict['other_2_heap_common'] = len(other_2_heap_common)
    results_dict['thread_heap_common'] = len(thread_heap_common)

    eden_hits = len(hits_grouped_by_gen['eden'])
    hemi_0_hits = len(hits_grouped_by_gen['hemi_0'])
    hemi_1_hits = len(hits_grouped_by_gen['hemi_1'])
    hemi_hits = sum([hemi_1_hits, hemi_0_hits])
    tenure_hits = len(hits_grouped_by_gen['tenured'])
    thread_hits = len(hits_grouped_by_gen['threads'])
    other_1_hits = len(hits_grouped_by_gen['other_1'])
    other_2_hits = len(hits_grouped_by_gen['other_2'])
    tot_gen_hits = sum([eden_hits, hemi_hits, tenure_hits])
    tot_all_hits = sum([thread_hits, tot_gen_hits])

    results_dict['eden_hits'] = eden_hits
    results_dict['hemi_0_hits'] = hemi_0_hits
    results_dict['hemi_1_hits'] = hemi_1_hits
    results_dict['hemi_hits'] = hemi_hits
    results_dict['tenure_hits'] = tenure_hits
    results_dict['thread_hits'] = thread_hits
    results_dict['other_1_hits'] = other_1_hits
    results_dict['other_2_hits'] = other_2_hits
    results_dict['tot_gen_hits'] = tot_gen_hits
    results_dict['tot_all_hits'] = tot_all_hits


    key_tot_unique = set.union(*uniq_compromised_key_locs.values())
    key_tot_heap_unique = set.union(*[uniq_compromised_key_locs[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])
    key_eden_in_threads = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['threads']])
    key_eden_in_all = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['hemi_1', 'hemi_0', 'tenured']])
    key_eden_in_hemi_0 = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['hemi_0']])
    key_eden_in_hemi_1 = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['hemi_1']])
    key_eden_in_hemi = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['hemi_1', 'hemi_0']])
    key_eden_in_tenured = uniq_compromised_key_locs['eden'] & set.union(*[uniq_compromised_key_locs[i] for i in ['tenured']])
    key_hemi_in_tenured = (uniq_compromised_key_locs['hemi_1'] |uniq_compromised_key_locs['hemi_0']) & uniq_compromised_key_locs['tenured']
    key_other_2_heap_common = uniq_compromised_key_locs['other_2'] & set.union(*[uniq_compromised_key_locs[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])
    key_thread_heap_common = uniq_compromised_key_locs['threads'] & set.union(*[uniq_compromised_key_locs[i] for i in ['eden', 'hemi_1', 'hemi_0', 'tenured']])

    results_dict['key_tot_unique'] = len(key_tot_unique)
    results_dict['key_tot_heap_unique'] = len(key_tot_heap_unique)
    results_dict['key_eden_in_threads'] = len(key_eden_in_threads)
    results_dict['key_eden_in_all'] = len(key_eden_in_all)
    results_dict['key_eden_in_hemi_0'] = len(key_eden_in_hemi_0)
    results_dict['key_eden_in_hemi_1'] = len(key_eden_in_hemi_1)
    results_dict['key_eden_in_hemi'] = len(key_eden_in_hemi)
    results_dict['key_eden_in_tenured'] = len(key_eden_in_tenured)
    results_dict['key_hemi_in_tenured'] = len(key_hemi_in_tenured)
    results_dict['key_other_2_heap_common'] = len(key_other_2_heap_common)
    results_dict['key_thread_heap_common'] = len(key_thread_heap_common)

    key_eden_hits = len(compromised_key_locs['eden'])
    key_hemi_0_hits = len(compromised_key_locs['hemi_0'])
    key_hemi_1_hits = len(compromised_key_locs['hemi_1'])
    key_hemi_hits = sum([key_hemi_1_hits, key_hemi_0_hits])
    key_tenure_hits = len(compromised_key_locs['tenured'])
    key_thread_hits = len(compromised_key_locs['threads'])
    key_other_1_hits = len(compromised_key_locs['other_1'])
    key_other_2_hits = len(compromised_key_locs['other_2'])
    key_tot_gen_hits = sum([key_eden_hits, key_hemi_hits, key_tenure_hits])
    key_tot_all_hits = sum([key_thread_hits, key_tot_gen_hits])

    results_dict['key_eden_hits'] = key_eden_hits
    results_dict['key_hemi_0_hits'] = key_hemi_0_hits
    results_dict['key_hemi_1_hits'] = key_hemi_1_hits
    results_dict['key_hemi_hits'] = key_hemi_hits
    results_dict['key_tenure_hits'] = key_tenure_hits
    results_dict['key_thread_hits'] = key_thread_hits
    results_dict['key_other_1_hits'] = key_other_1_hits
    results_dict['key_other_2_hits'] = key_other_2_hits
    results_dict['key_tot_gen_hits'] = key_tot_gen_hits
    results_dict['key_tot_all_hits'] = key_tot_all_hits

    fmt_unique_hits = "-".join(["%s:{%s}"%(name, name) for name in results_dict])
    # fmt_unique_hits = fmt_unique_hits + "-tot_unique_mem:{tot_unique}"
    # fmt_unique_hits = fmt_unique_hits + "-heap_in_threads:{thread_heap_common}"
    # fmt_unique_hits = fmt_unique_hits + "-heap_in_other:{other_2_heap_common}"
    # fmt_unique_hits = fmt_unique_hits + "-tot_unique_heap:{tot_heap_unique}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_unique:{tot_heap_unique}"
    # fmt_unique_hits = fmt_unique_hits + "-tot_unique_heap:{tot_heap_unique}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_threads:{eden_in_threads}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_ogens:{eden_in_all}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_hemi_0:{eden_in_hemi_0}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_hemi_1:{eden_in_hemi_1}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_hemi:{eden_in_hemi}"
    # fmt_unique_hits = fmt_unique_hits + "-eden_in_tenured:{eden_in_tenured}"
    # fmt_unique_hits = fmt_unique_hits + "-hemi_in_tenured:{hemi_in_tenured}"

    outline = fmt_unique_hits.format(**results_dict)
    java_heap_key_locs_out_f.write(outline+'\n')

    for name, hits in hits_grouped_by_gen.items():
        java_heap_key_locs_out_f.write("%s hits = %d\n"%(name, len(hits)))

    for name, hits in uniq_hits_grouped_by_gen.items():
        java_heap_key_locs_out_f.write("%s unique hits = %d\n"%(name, len(hits)))

    #{'other_1':[], 'other_2':[], 'threads':[], 'orphaned':[], 'eden':[], 
    #                       'hemi_1':[], 'hemi_0':[], 'hemi':[], 'tenured':[]}

    java_heap_key_locs_out_f.write(
        "Total hits in Eden Space: %d\n"%(eden_hits) +\
        "Total hits in Survivor 0 Generations: %d\n"%(hemi_0_hits) +\
        "Total hits in Survivor 1 Generations: %d\n"%(hemi_1_hits) +\
        "Total hits in Survivor (both) Generations: %d\n"%(hemi_hits) +\
        "Total hits in Tenured Generations: %d\n"%(tenure_hits)+\
        "Total hits in Threads: %d\n"%(thread_hits)+\
        "Total hits Other: %d\n"%(other_2_hits+other_1_hits)+\
        "Total hits in all Generations: %d\n"%(tot_gen_hits)+\
        "Total hits altogether (no other): %d\n"%(tot_all_hits) +\
        "Total hits altogether: %d\n"%(tot_all_hits+other_2_hits+other_1_hits) +\
        "Total key hits in Eden Space: %d\n"%(key_eden_hits) +\
        "Total key hits in Survivor 0 Generations: %d\n"%(key_hemi_0_hits) +\
        "Total key hits in Survivor 1 Generations: %d\n"%(key_hemi_1_hits) +\
        "Total key hits in Survivor (both) Generations: %d\n"%(key_hemi_hits) +\
        "Total key hits in Tenured Generations: %d\n"%(key_tenure_hits)+\
        "Total key hits in Threads: %d\n"%(key_thread_hits)+\
        "Total key hits Other: %d\n"%(key_other_2_hits+key_other_1_hits)+\
        "Total key hits in all Generations: %d\n"%(key_tot_gen_hits)+\
        "Total key hits altogether (no other): %d\n"%(key_tot_all_hits) +\
        "Total key hits altogether: %d\n"%(key_tot_all_hits+key_other_2_hits+key_other_1_hits) +\
        "Unique hits from Eden Space in Threads: %d\n"%(len(eden_in_threads)) +\
        "Unique hits from Eden Space in other Generations: %d\n"%(len(eden_in_all)) +\
        "Unique hits from Eden Space in Survivor 0 Generations: %d\n"%(len(eden_in_hemi_0)) +\
        "Unique hits from Eden Space in Survivor 1 Generations: %d\n"%(len(eden_in_hemi_1)) +\
        "Unique hits from Eden Space in Survivor (both) Generations: %d\n"%(len(eden_in_hemi)) +\
        "Unique hits from Eden Space in Tenured Generations: %d\n"%(len(eden_in_tenured))+\
        "Unique hits from Hemi in Tenured Generations: %d\n"%(len(hemi_in_tenured))+\
        "Unique hits from all memory: %d\n"%(len(tot_unique))+\
        "Unique hits in all Generations: %d\n"%(len(tot_heap_unique))+\
        "Unique hits in Common with Heap and threads: %d\n"%(len(thread_heap_common)) +\
        "Unique hits in Common with Heap and Other: %d\n"%(len(other_2_heap_common)) +\
        "Unique key hits from Eden Space in Threads: %d\n"%(len(key_eden_in_threads)) +\
        "Unique key hits from Eden Space in other Generations: %d\n"%(len(key_eden_in_all)) +\
        "Unique key hits from Eden Space in Survivor 0 Generations: %d\n"%(len(key_eden_in_hemi_0)) +\
        "Unique key hits from Eden Space in Survivor 1 Generations: %d\n"%(len(key_eden_in_hemi_1)) +\
        "Unique key hits from Eden Space in Survivor (both) Generations: %d\n"%(len(key_eden_in_hemi)) +\
        "Unique key hits from Eden Space in Tenured Generations: %d\n"%(len(key_eden_in_tenured))+\
        "Unique key hits from Hemi in Tenured Generations: %d\n"%(len(key_hemi_in_tenured))+\
        "Unique key hits from all memory: %d\n"%(len(key_tot_unique))+\
        "Unique key hits in all Generations: %d\n"%(len(key_tot_heap_unique))+\
        "Unique key hits in Common with Heap and threads: %d\n"%(len(key_thread_heap_common)) +\
        "Unique key hits in Common with Heap and Other: %d\n"%(len(key_other_2_heap_common)))

    return results_dict

def perform_agg_copy_results(base_location, working_dir):
    # compromised keys merged-keyinfo format and how compromised-keys (PMS, MS, KEYBLOCK)
    # locations of the keys
    # compromised username + session identifiers + age
    # compromised password + usernames + age
    # zipped authdata
    compromised_keys = sifipr.get_compromised_keys(working_dir)
    compromised_post_data = sifipr.get_compromised_post_data(working_dir)
    java_heap_key_locs = sifipr.get_java_heap_key_locations(working_dir)
    compromised_sessions = sifipr.get_compromised_sessions(working_dir)
    compromised_passwords = sifipr.get_compromised_passwords(working_dir)
    auth_data = sifipr.get_auth_data_name(working_dir)
    pct_out = sifipr.get_pct_file(working_dir)
    files_to_copy = [ pct_out,
        auth_data,
        compromised_post_data,
        compromised_keys,
        compromised_passwords,
        compromised_sessions,
    ]
    if os.path.exists(java_heap_key_locs):
        files_to_copy.append(java_heap_key_locs)
    all_success = []
    for f in files_to_copy:
        try:
            #os.stat(base_location)
            shutil.copy(f, base_location)
            print("Copying %s to %s"%(f, base_location))
            all_success.append(1)
        except:
            print("%s: Nothing to do, location appears to be invalid: %s"%(time_str(), base_location))
            break

    if len(all_success) == len(files_to_copy):
        try:
            #os.system("touch %s"%(os.path.join(base_location, UNZIP_MEMORY_PROCESS)))
            return True
        except:
            return False
    return False

def work_is_complete(base_location):
    f = sifipr.get_compressed_auth_data(base_location)
    try:
        os.stat(f)
        return True
    except:
        return False

def perform_rm_dump_files(base_location, use_python):
    dumps_loc = sifipr.get_java_dumps_location(base_location, use_python)
    dumps_files = [os.path.join(i, dumps_loc) for i in os.listdir(base_location)]
    for f in dumps_files:
        open(f, 'w').write('0')
    shutil.rmtree(dumps_loc)


def perform_strings_decompression(base_location):
    the_file = pppds.get_ffastrings_zip_len_res_name(base_location)
    unzip_command = pppds.create_unzip_cmd(the_file, base_location)
    sifipr.exec_command(unzip_command)

def perform_work_data_agg(base_location, working_dir=None, use_64=True, use_python=False):
    start_time = time_str()
    # copy over location
    outstr = ("%s: Processing %s"%(time_str(), base_location))
    print outstr
    #if work_is_complete(base_location):
    #    print("%s: job complete start"%(start_time))
    #    print("%s: Nothing to do, work has already been done: %s"%(time_str(), base_location))
    #    return

    if working_dir is None:
        working_dir = get_working_dir()

    perform_agg_copy(base_location, working_dir)

    outstr = ("%s: Perforiming strings file decompression %s"%(time_str(), working_dir))
    decomp_strings_proc = Process(target=perform_strings_decompression, args=(working_dir,))
    decomp_strings_proc.start()

    is_streaming = False
    aed.log_out("%s: Reading strings and event information for %s"%(time_str(), working_dir))
    factors = aed.get_factors(base_location, trim=True)
    times = aed.get_times(working_dir)
    auth_data = aed.read_event_information(working_dir, factors)
    factors['age'] = (auth_data['max_time'] - auth_data['min_time'])/1000
    auth_data['ssl_values'] = aed.merge_key_hits_key_info(working_dir)
    auth_data['ssl_ordered_comms'] = aed.build_ordered_ssl_comms(auth_data['ssl_values'])
    auth_data.update(times)
    decomp_strings_proc.join()
    strings_data = aed.read_strings_information(working_dir, auth_data)
    auth_data['http_values'] = aed.update_shadow_auth_info(strings_data, auth_data['http_values'])
    aed.scale_timestamps_ssl_values(auth_data['ssl_values'].values(), times)
    aed.scale_timestamps_http_values(auth_data['http_values'].values(), times)
    auth_data['matched_comms'] = aed.match_http_with_ssl_keys(auth_data, times, is_streaming=is_streaming )
    # scale all the timestamp http_values

    # make sure the external end clock time is the same, and if not update
    #update_end_time(auth_data, times)
    infos = aed.extract_basic_infos(auth_data, factors)
    for user, info in infos.items():
        auth_data['http_values'][user]['age'] = info['age']
        auth_data['http_values'][user]['live'] = info['live']

    #update the ssl ages here
    # comm timestamp is considered the end of a communication
    host_start = times['start']
    host_end = times['act_end']
    for comm in auth_data['matched_comms']:
        user = comm['user']
        ms = comm['ms']
        age = comm['age']
        dur_exp = -1
        v = auth_data['ssl_values'][ms]
        if is_streaming:
            live = not auth_data['http_values'][user]['completed']
            age = auth_data['http_values'][user]['age']
        else:
            live = False

        if not live and (comm['ms_hit'] or comm['pms_hit']):
            dur_exp = auth_data['http_values'][user]['end'] - host_start
            dur_exp = 0 if dur_exp < 0 else dur_exp

        v['live'] = live
        v['matched'] = True
        v['dur_exp'] = comm['dur_exp']
        v['unscaled_timestamp'] = v['timestamp']
        v['timestamp'] = comm['ssl_timestamp']
        v['age'] = age
        v['start'] = comm['ssl_timestamp'] - host_start
        v['end'] = v['start'] + age if not live else -1
        v['end_timestamp'] = comm['http_timestamp'] if not live else -1

        comm['dur_exp'] = host_end - comm['end'] if not live else 0
        comm['live'] = live
        comm['end'] = comm['start'] + age if not live else -1
        comm['end_timestamp'] = comm['http_timestamp'] if not live else -1

    matched_comm_ssl = {}
    for comm in auth_data['matched_comms']:
        matched_comm_ssl[comm['ms']] = comm

    for comm in auth_data['ssl_ordered_comms']:
        ms = comm['ms']
        v = auth_data['ssl_values'][ms]
        age = comm['age']
        comm['end'] = matched_comm_ssl[ms]['end'] if ms in matched_comm_ssl else -1
        comm['end_timestamp'] = matched_comm_ssl[ms]['end_timestamp'] if ms in matched_comm_ssl else -1

        if 'matched' in v and v['matched']:
            continue
        v['age'] = 0
        v['live'] = True
        v['unscaled_timestamp'] = auth_data['ssl_values'][ms]['timestamp']
        v['timestamp'] = comm['timestamp']
        v['start'] = comm['timestamp'] - host_start
        v['end'] = -1
        v['end_timestamp'] = -1
        v['dur_exp'] = -1

    auth_data.update(times)
    aed.log_out("%s: Performing data searches for %s"%(time_str(), working_dir))
    aed.perform_data_search(working_dir, strings_data, auth_data)
    aed.log_out("%s: Caclulating the probabilities %s"%(time_str(), working_dir))
    # probability that a session token could be recovered
    # probability that a password could be recovered (alone)
    # probability that a post uri could be recovered
    auth_data['live_quant'] = {"est_comms":len(auth_data['matched_comms']),
                               "num_ssl_sessions":len(auth_data['ssl_values']),
                               "num":len(auth_data['http_values'].values()),
                               "num_live":sum([1 for i in auth_data['http_values'].values() if i['live']]) }
    infos, results = aed.calc_password_info(auth_data, factors)
    auth_data['password_infos'] = infos
    auth_data['password_quant'] = results
    infos, results = aed.calc_sessionid_info(auth_data, factors)
    auth_data['sessionid_infos'] = infos
    auth_data['sessionid_quant'] = results
    infos, results = aed.calc_cookie_sessionid_info(auth_data, factors)
    auth_data['cookie_sessionid_infos'] = infos
    auth_data['cookie_sessionid_quant'] = results
    infos, results = aed.calc_post_uri_info(auth_data, factors)
    auth_data['post_uri_infos'] = infos
    auth_data['post_uri_quant'] = results
    infos, results = aed.calc_logentry_info(auth_data, factors)
    auth_data['logentry_infos'] = infos
    auth_data['logentry_quant'] = results
    memory_consumption = aed.calculate_memory_consumption(auth_data, factors)
    auth_data['memory_consumption'] = memory_consumption
    infos, results = aed.calc_ssl_info_quant(auth_data, factors)
    auth_data['ssl_infos'] = infos
    auth_data['ssl_quant'] = results
    infos, results = aed.calc_ssl_info_quant(auth_data, factors, use_unmatched=True)
    auth_data['ssl_infos_unmatched'] = infos
    auth_data['ssl_quant_unmatched'] = results

    aed.log_out("%s: writing results to json file %s"%(time_str(), working_dir))
    aed.perform_results_write(working_dir, auth_data)
    jvm_mem = 4096
    try:
        use_64, _, jvm_mem, factors = aed.get_run_info(base_location)
    except:
        pass

    write_java_heap_locations(working_dir, auth_data, heap_size=jvm_mem)
    write_compromised_summary_data(working_dir, auth_data)
    #write_compromised_keys(working_dir, auth_data)
    #write_session_identifiers(working_dir, auth_data)
    #write_session_passwords(working_dir, auth_data)
    #write_post_data(working_dir, auth_data)
    perform_agg_copy_results(base_location, working_dir)
    aed.log_out("%s: job complete %s"%(time_str(), working_dir))
    return auth_data


def perform_work_write_java_heap_locs_only(base_location, working_dir=None, use_64=True, use_python=False):
    start_time = time_str()
    # copy over location
    outstr = ("%s: Processing %s"%(time_str(), base_location))
    print outstr
    #if work_is_complete(base_location):
    #    print("%s: job complete start"%(start_time))
    #    print("%s: Nothing to do, work has already been done: %s"%(time_str(), base_location))
    #    return
    destroy_wd = False
    if working_dir is None:
        working_dir = get_working_dir()
        destroy_wd = True

    perform_heap_agg_copy(base_location, working_dir)
    auth_data = json.load(open(sifipr.get_auth_data_name(base_location)))
    jvm_mem = 4096
    try:
        use_64, _, jvm_mem, factors = aed.get_run_info(base_location)
    except:
        pass

    write_java_heap_locations(working_dir, auth_data, heap_size=jvm_mem)
    java_heap_key_locs = sifipr.get_java_heap_key_locations(working_dir)
    print("Copying %s to %s"%(java_heap_key_locs, base_location))
    shutil.copy(java_heap_key_locs, base_location)
    if destroy_wd:
        shutil.rmtree(working_dir)

